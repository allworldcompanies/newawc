<?php

require 'view.php';

$view = new View('home.tpl');
$view->set('title', 'This is a simple template file!');
$view->set('greeting', 'Hello, World');
echo $view->output();

?>