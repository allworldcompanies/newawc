<?php
include_once("../initialize.php");


//Track workflow
$workflowStepsObj = new Db_WorkflowSteps();
$workflowStepsObj->saveWorkflow('s1', $S->Get(md5('sessionid')));

$datenschutz_content = $TPL->display('datenschutz', true);
$TPL->assign('datenschutz_content', $datenschutz_content);

$carTypes = Db_CarTypes::getCarTypesForSelect();
$licenceTypes = Db_LicenceTypes::getLicenceTypesForSelect();
$TPL->assign('carTypes', $carTypes);
$TPL->assign('licenceTypes', $licenceTypes);

$groupLicenceTypes = Db_LicenceTypesGroup::getGroupLicenceTypes();
$TPL->assign('groupLicenceTypes', $groupLicenceTypes);


$TPL->assign('postcodeGet', $postcodeGet);
$TPL->assign('licenceTypeGet', $licenceTypeGet);
$TPL->assign('cityGet', $cityGet);


/** @var $first_step_form. Show Frist step Form  */
$firstStepForm = $TPL->display('first_step_form', true);
$TPL->assign('first_step_form', $firstStepForm);


$siteTitle = 'Das Fahrschulportal';
$TPL->assign('site_title', $siteTitle);


/** @var $pagination. Show all steps on head of the page */
/*$noNavigation = true;
$TPL->assign('noNavigation', $noNavigation);*/
$currentBreadcrumbs = 'step1';
$TPL->assign('currentBreadcrumbs', $currentBreadcrumbs);

$textareaDefaultText = 'Deine Nachricht';
$TPL->assign('textareaDefaultText', $textareaDefaultText);

$is_map = 1;
$TPL->assign('is_map', $is_map);

$lpForm = $TPL->display('lp-form',true);
$TPL->assign('lpForm', $lpForm);

$lpFormStyle = 'lp-form.css';
$TPL->assign('lpFormStyle', $lpFormStyle);

$headerClass = 'old-static';
$TPL->assign('headerClass', $headerClass);

$angeboteErhaltenStyle  = 'angebote-erhalten.css';
$TPL->assign('angeboteErhaltenStyle', $angeboteErhaltenStyle);

/*
$resultList                 = Db_DriverSchools::getRandomPayingOffices(40);
$TPL->assign('officeFullList', $resultList);

$payingSchools =  $TPL->display('template/result-list-school-default',true);
$TPL->assign('payingSchools', $payingSchools);

$owlCarousel = 1;
$TPL->assign('owlCarousel', $owlCarousel);

$randomOpinions = Db_MastersOpinions::getRandomOpinions(20);
$TPL->assign('randomOpinions', $randomOpinions);
*/

print $TPL->display('header',true);
print $TPL->display('breadcrumbs',true);
print $TPL->display($curr_url,true);
print $TPL->display('footer',true);
