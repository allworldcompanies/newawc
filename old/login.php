<?php

 ob_start();

?>
<?php require("session.php"); ?>
<?php require("connection.php"); ?>
<?php require("functions.php"); ?>
<?php include("header.php"); ?>

<?php
	if (logged_in()) {
			header('Location: ' . $_SERVER['HTTP_REFERER']);
	}


	// START FORM PROCESSING
	if (isset($_POST['submit'])){ // Form has been submitted.

		// perform validations on the form data
		
		$email = trim(mysql_prep($_POST['email']));
		$password = trim(mysql_prep($_POST['password']));
		$hashed_password = sha1($password);

		if (!empty($_POST['email']) && !empty($_POST['password'])) { 
		
			// Check database to see if name and the hashed password exist there.
			$query = "SELECT * ";
			$query .= "FROM users ";
			$query .= "WHERE users_email = '{$email}' ";
			$query .= "AND users_pass = '{$hashed_password}' ";
			$query .= "LIMIT 1";
			$result_set = mysql_query($query);
			if (mysql_num_rows($result_set) == 1) {
				// name/password authenticated
				// and only 1 match
				$found_user = mysql_fetch_array($result_set);
				$_SESSION['user_id'] = $found_user['users_id'];
				$_SESSION['email'] = $found_user['users_email'];
				$_SESSION['users_fname'] = $found_user['users_fname'];
				$_SESSION['users_lname'] = $found_user['users_lname'];
				$_SESSION['users_role'] = $found_user['users_role'];
				$_SESSION['users_active'] = $found_user['users_active'];
				if (isset($_GET['id'])) {
					header('Location: ' . $_SERVER['REQUEST_URI']);
				}	else {
				header('Location: ' . $_SERVER['HTTP_REFERER']);
				}
			}	


				else {
				// name/password combo was not found in the database
                echo ("<SCRIPT LANGUAGE='JavaScript'>
                window.alert('Verify your password or e-mail!')
                window.location.href='index.php';
                </SCRIPT>");

			}
		} 
			else {
                echo ("<SCRIPT LANGUAGE='JavaScript'>
                window.alert('Complete all the fields')
                window.location.href='index.php';
                </SCRIPT>");
			}

	} 

	
	else  { // Form has not been submitted.
		if (isset($_GET['logout']) && $_GET['logout'] == 1) {
			$message = "You are now logged out.";
		} 
		$name = "";
		$password = "";
		}
		

	
?>
<?php 
require("footer.php"); 
?>