<?php

//Carrega Array com Grupos do ScriptCase
$db_sgdb           = $this->GetVar('db_sgdb');
$btn_avanc         = $this->GetVar('btn_avanc');
$btn_retor         = $this->GetVar('btn_retor');
$sgdb              = $this->GetVar('sgdb');
$decimal           = $this->GetVar('decimal');
$use_persistent    = $this->GetVar('use_persistent');
$retrieve_schema   = $this->GetVar('retrieve_schema');
$edit_conn         = $this->GetVar('edit_conn') == "S";
$filter_list       = $this->GetVar('arr_filters');
$id_edit_conn      = $this->GetVar('id_edit_conn');
$force_name_conn   = $this->GetVar('force_name_conn');

if(empty($decimal))
{
	$decimal = '.';
}

$dbms = $this->GetVar('dbms');
$conn = $this->GetVar('conn');
$user = $this->GetVar('user');
$pass = $this->GetVar('pass');

if (!$edit_conn && $this->GetVar('nome_conn_sugerido') != "")
{
	$conn = $this->GetVar('nome_conn_sugerido');
}

$str_title_conn = $nm_lang['mainmenu_new_conn'];
$lbl_filter     = $nm_lang['btn_filt']; 

if ($edit_conn)
{
	$str_title_conn = $nm_lang['lbl_connection_edit'];
}

/*
?>

<script language="javascript" src="<?php echo $nm_config['url_js_thirddevel']; ?>wz_tooltip/wz_tooltip.js"></script>
<?php
*/
?>

<script language="javascript">

	$(document).ready(function() {
		
	   setTimeout(function () {nm_post_conn_ajax('S');}, 1);	   	   	   
	   
	});
	

<?php
/*	
	
	var num_seq_row_filter = <?php echo (count($filter_list['list']) - 1); ?>;
	
	function fc_check_add_new_filter(str_focus)
	{				
		rows_filter = $('tr', $('#tab_filter'));
		bln_add     = true;
		
		//Limpa linhas vazias
		for(nI = rows_filter.length - 1; nI > 0; nI--)
		{
			num_seq = $(rows_filter[nI]).attr('id_obj');
						
			if ($('#filter_table_' + num_seq).val() == "" && $('#filter_owner_' + num_seq).val() == "" && nI != (rows_filter.length - 1) )
			{
				document.getElementById('tab_filter').deleteRow(nI);
			}
		}
		
		//Verifica se precisa adicionar mais uma linha
		for(nI = rows_filter.length - 1; nI > 0; nI--)
		{	
			num_seq = $(rows_filter[nI]).attr('id_obj');
			if ($('#filter_table_' + num_seq).val() == "" && $('#filter_owner_' + num_seq).val() == "")
			{
				bln_add = false;
				break;
			}			
		}
		
		//Adiciona mais uma linha
		if (bln_add)
		{
			num_seq_row_filter++;
			
			new_row = document.getElementById('tab_filter').insertRow(rows_filter.length);
			$(new_row).attr('id_obj', num_seq_row_filter);	
			cell_table = new_row.insertCell(0);
			cell_owner = new_row.insertCell(1);
			cell_show  = new_row.insertCell(2);		
			
			cell_table.innerHTML = "<input id='filter_table_"+ num_seq_row_filter +"' type='text' name='filter_table["+ num_seq_row_filter +"]' value='' size='15' class='nmInput' onchange=\"fc_check_add_new_filter('filter_owner_" + num_seq_row_filter + "')\">";
			cell_owner.innerHTML = "<input id='filter_owner_"+ num_seq_row_filter +"' type='text' name='filter_owner["+ num_seq_row_filter +"]' value='' size='15' class='nmInput' onchange=\"fc_check_add_new_filter('filter_show_" + num_seq_row_filter + "')\">";
			cell_show.innerHTML  = "<select id='filter_show_"+ num_seq_row_filter +"' class='nmInput' name='filter_show["+ num_seq_row_filter +"]'>" + 
								   "	<option value='Y'><?php echo $nm_lang['option_yes']; ?></option>" +
								   "	<option value='N' selected><?php echo $nm_lang['option_no']; ?></option>" +
								   "</select>";
		}									  
				
	}	
	
*/
?>
	
</script>

<form name='frm_back_edit' style="display:none" action="<?php echo nm_url_rand($nm_config['url_iface'] . 'admin_sys_allconections_create_wizard.php'); ?>" method="GET">
	<input type="hidden" name="conn_open" value="S" />
</form>


<form action="" onsubmit="return false;" name="form_create" METHOD="post">
<input type="hidden" name="dbms" id="dbms" value="<?php echo $dbms; ?>" />
<input type="hidden" name="edit_conn" value="<?php echo $this->GetVar('edit_conn'); ?>" /> 
<input type="hidden" name="id_edit_conn" value="<?php echo $id_edit_conn; ?>" />
<input type="hidden" id="carregar_db" value="S" />
<center>
<!--br-->
<div style="position:relative; width:550px">


<table id='tab_new_conn' width="550