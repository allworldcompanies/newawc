<?php
  $this->Nm_lang = array();
  $this->Nm_lang['Nm_charset']     = "EUC-CN";
  $this->Nm_lang['lang_jscr_dcml'] = "分隔符";
  $this->Nm_lang['lang_jscr_decm'] = "十分之一秒";
  $this->Nm_lang['lang_jscr_hour'] = "小时";
  $this->Nm_lang['lang_jscr_iday'] = "日";
  $this->Nm_lang['lang_jscr_intg'] = "整数和";
  $this->Nm_lang['lang_jscr_ivcr'] = "信用卡号无效";
  $this->Nm_lang['lang_jscr_ivdt'] = "数据无效";
  $this->Nm_lang['lang_jscr_ivem'] = "邮箱地址无效";
  $this->Nm_lang['lang_jscr_ivnb'] = "整数无效";
  $this->Nm_lang['lang_jscr_ivtm'] = "时间格式无效";
  $this->Nm_lang['lang_jscr_ivv2'] = "日期格式无效";
  $this->Nm_lang['lang_jscr_ivvl'] = "小数无效";
  $this->Nm_lang['lang_jscr_maxm'] = "最大";
  $this->Nm_lang['lang_jscr_maxm_date'] = "最大日期";
  $this->Nm_lang['lang_jscr_minm'] = "最小";
  $this->Nm_lang['lang_jscr_minm_date'] = "最小日期";
  $this->Nm_lang['lang_jscr_mint'] = "分钟";
  $this->Nm_lang['lang_jscr_mnth'] = "月";
  $this->Nm_lang['lang_jscr_msfr'] = "表格未命名";
  $this->Nm_lang['lang_jscr_mslg'] = "无此用户名";
  $this->Nm_lang['lang_jscr_msob'] = "表格未命名";
  $this->Nm_lang['lang_jscr_mxdg'] = "数字超出预期值";
  $this->Nm_lang['lang_jscr_mxvl'] = "超过最大长度";
  $this->Nm_lang['lang_jscr_nnum'] = "仅填数字";
  $this->Nm_lang['lang_jscr_nvlf'] = "负号须在左侧";
  $this->Nm_lang['lang_jscr_reqr'] = "必填项!";
  $this->Nm_lang['lang_jscr_secd'] = "秒";
  $this->Nm_lang['lang_jscr_wfix'] = "是否需要更正?";
  $this->Nm_lang['lang_btns_ajax'] = "搜索";
  $this->Nm_lang['lang_btns_ajax_hint'] = "搜索数据";
  $this->Nm_lang['lang_btns_ajax_close'] = "取消";
  $this->Nm_lang['lang_btns_ajax_close_hint'] = "关闭";
  $this->Nm_lang['lang_btns_back'] = "退后";
  $this->Nm_lang['lang_btns_back_hint'] = "退后";
  $this->Nm_lang['lang_btns_calc'] = "计算器";
  $this->Nm_lang['lang_btns_calc_hint'] = "使用计算器";
  $this->Nm_lang['lang_btns_cfrm'] = "确定";
  $this->Nm_lang['lang_btns_cfrm_hint'] = "再次确定";
  $this->Nm_lang['lang_btns_chrt'] = "图表";
  $this->Nm_lang['lang_btns_chrt_hint'] = "显示图表";
  $this->Nm_lang['lang_btns_chrt_pdff_hint'] = "PDF图表";
  $this->Nm_lang['lang_btns_chrt_stng'] = "图表选项";
  $this->Nm_lang['lang_btns_chrt_stng_hint'] = "配置图表";
  $this->Nm_lang['lang_btns_cldr'] = "日历";
  $this->Nm_lang['lang_btns_cldr_hint'] = "使用日历";
  $this->Nm_lang['lang_btns_clea'] = "清除";
  $this->Nm_lang['lang_btns_clea_hint'] = "清除数据";
  $this->Nm_lang['lang_btns_clmn'] = "数据项";
  $this->Nm_lang['lang_btns_clmn_hint'] = "选择数据项";
  $this->Nm_lang['lang_btns_clmn_limt_hint'] = "更改数据项";
  $this->Nm_lang['lang_btns_clse_hint'] = "关闭";
  $this->Nm_lang['lang_btns_errm_clse'] = "关闭";
  $this->Nm_lang['lang_btns_errm_clse_hint'] = "关闭";
  $this->Nm_lang['lang_btns_expv'] = "查看";
  $this->Nm_lang['lang_btns_expv_hint'] = "查看文件";
  $this->Nm_lang['lang_btns_cncl'] = "取消";
  $this->Nm_lang['lang_btns_cncl_hint'] = "取消";
  $this->Nm_lang['lang_btns_cncl_prnt_hint'] = "取消打印";
  $this->Nm_lang['lang_btns_colr_cncl_hint'] = "取消更改";
  $this->Nm_lang['lang_btns_colr_updt_hint'] = "保存更改";
  $this->Nm_lang['lang_btns_cptc_rfim'] = "刷新图像";
  $this->Nm_lang['lang_btns_cptc_rfim_hint'] = "刷新图像";
  $this->Nm_lang['lang_btns_csvf'] = "CSV";
  $this->Nm_lang['lang_btns_csvf_hint'] = "导出数据为CSV";
  $this->Nm_lang['lang_btns_dele'] = "删除";
  $this->Nm_lang['lang_btns_dele_hint'] = "删除当前记录";
  $this->Nm_lang['lang_btns_delt_hint'] = "删除记录";
  $this->Nm_lang['lang_btns_dtai_rtrn_hint'] = "回到行式报表";
  $this->Nm_lang['lang_btns_edit_1row_hint'] = "修改";
  $this->Nm_lang['lang_btns_emai'] = "电子邮件";
  $this->Nm_lang['lang_btns_emai_hint'] = "发送电子邮件";
  $this->Nm_lang['lang_btns_exit'] = "退出";
  $this->Nm_lang['lang_btns_exit_appl_hint'] = "退出程序";
  $this->Nm_lang['lang_btns_exit_hint'] = "退出程序";
  $this->Nm_lang['lang_btns_expo'] = "导出";
  $this->Nm_lang['lang_btns_expo_hint'] = "导出数据";
  $this->Nm_lang['lang_btns_frst'] = "首记录";
  $this->Nm_lang['lang_btns_frst_hint'] = "移至首条记录";
  $this->Nm_lang['lang_btns_frst_page'] = "首页";
  $this->Nm_lang['lang_btns_frst_page_hint'] = "返回首页";
  $this->Nm_lang['lang_btns_grid_hint'] = "行式报表";
  $this->Nm_lang['lang_btns_help'] = "帮助";
  $this->Nm_lang['lang_btns_help_hint'] = "显示帮助";
  $this->Nm_lang['lang_btns_hlpf'] = "帮助";
  $this->Nm_lang['lang_btns_hlpf_hint'] = "数据填写帮助";
  $this->Nm_lang['lang_btns_inst'] = "插入";
  $this->Nm_lang['lang_btns_inst_hint'] = "插入记录";
  $this->Nm_lang['lang_btns_iurl'] = "访问";
  $this->Nm_lang['lang_btns_iurl_hint'] = "访问该地址";
  $this->Nm_lang['lang_btns_jump'] = "转至";
  $this->Nm_lang['lang_btns_jump_hint'] = "转至行";
  $this->Nm_lang['lang_btns_lang'] = "语言";
  $this->Nm_lang['lang_btns_lang_hint'] = "选择语言";
  $this->Nm_lang['lang_btns_last'] = "末记录";
  $this->Nm_lang['lang_btns_last_hint'] = "移到末条记录";
  $this->Nm_lang['lang_btns_last_page'] = "末页";
  $this->Nm_lang['lang_btns_last_page_hint'] = "移到末页";
  $this->Nm_lang['lang_btns_lens'] = "明细";
  $this->Nm_lang['lang_btns_lens_hint'] = "查看明细";
  $this->Nm_lang['lang_btns_mdtl_cncl'] = "取消";
  $this->Nm_lang['lang_btns_mdtl_cncl_hint'] = "取消";
  $this->Nm_lang['lang_btns_mdtl_dele'] = "删除";
  $this->Nm_lang['lang_btns_mdtl_dele_hint'] = "删除本行";
  $this->Nm_lang['lang_btns_mdtl_edit'] = "更改";
  $this->Nm_lang['lang_btns_mdtl_edit_hint'] = "更改本行";
  $this->Nm_lang['lang_btns_mdtl_inst'] = "插入";
  $this->Nm_lang['lang_btns_mdtl_inst_hint'] = "插入行";
  $this->Nm_lang['lang_btns_mdtl_updt'] = "更新";
  $this->Nm_lang['lang_btns_mdtl_updt_hint'] = "更新本行";
  $this->Nm_lang['lang_btns_menu_rtrn_hint'] = "返回开发环境";
  $this->Nm_lang['lang_btns_mess_clse'] = "关闭";
  $this->Nm_lang['lang_btns_mess_clse_hint'] = "关闭";
  $this->Nm_lang['lang_btns_mode_prnt_hint'] = "打印";
  $this->Nm_lang['lang_btns_newn_hint'] = "打开新记录";
  $this->Nm_lang['lang_btns_neww'] = "新增";
  $this->Nm_lang['lang_btns_neww_hint'] = "添加新记录";
  $this->Nm_lang['lang_btns_next'] = "下个";
  $this->Nm_lang['lang_btns_next_hint'] = "移到下条记录";
  $this->Nm_lang['lang_btns_next_page'] = "下个";
  $this->Nm_lang['lang_btns_next_page_hint'] = "移到下一页";
  $this->Nm_lang['lang_btns_pdfc'] = "PDF";
  $this->Nm_lang['lang_btns_pdfc_hint'] = "创建PDF文档";
  $this->Nm_lang['lang_btns_pick'] = "选择";
  $this->Nm_lang['lang_btns_pick_hint'] = "选择此列";
  $this->Nm_lang['lang_btns_pncl'] = "修改";
  $this->Nm_lang['lang_btns_pncl_hint'] = "修改本列";
  $this->Nm_lang['lang_btns_prev'] = "向前";
  $this->Nm_lang['lang_btns_prev_hint'] = "移到前面的记录";
  $this->Nm_lang['lang_btns_prev_page'] = "向前";
  $this->Nm_lang['lang_btns_prev_page_hint'] = "返回前一页";
  $this->Nm_lang['lang_btns_prnt'] = "打印";
  $this->Nm_lang['lang_btns_prnt_hint'] = "打印版本";
  $this->Nm_lang['lang_btns_prnt_prep_hint'] = "打印选项";
  $this->Nm_lang['lang_btns_prtn_titl_hint'] = "打印设置";
  $this->Nm_lang['lang_btns_qtch'] = "...";
  $this->Nm_lang['lang_btns_rows'] = "查看";
  $this->Nm_lang['lang_btns_rows_clmn_lmit_hint'] = "更改行数和列数";
  $this->Nm_lang['lang_btns_rows_hint'] = "更改列数";
  $this->Nm_lang['lang_btns_rtff'] = "RTF";
  $this->Nm_lang['lang_btns_rtff_hint'] = "导出为RTF文件";
  $this->Nm_lang['lang_btns_rtrn_hint'] = "返回之前的应用";
  $this->Nm_lang['lang_btns_rtrn_scrp_hint'] = "返回ScriptCase";
  $this->Nm_lang['lang_btns_rtrv_form'] = "修改";
  $this->Nm_lang['lang_btns_rtrv_form_hint'] = "修改表单数据";
  $this->Nm_lang['lang_btns_rtrv_grid'] = "取值";
  $this->Nm_lang['lang_btns_rtrv_grid_hint'] = "从行式报表取数据";
  $this->Nm_lang['lang_btns_save'] = "保存";
  $this->Nm_lang['lang_btns_save_hint'] = "保存修改";
  $this->Nm_lang['lang_btns_smry'] = "汇总";
  $this->Nm_lang['lang_btns_smry_hint'] = "显示汇总";
  $this->Nm_lang['lang_btns_smry_msge_avgt'] = "平均";
  $this->Nm_lang['lang_btns_smry_msge_mint'] = "最小";
  $this->Nm_lang['lang_btns_smry_msge_maxm'] = "最大";
  $this->Nm_lang['lang_btns_smry_msge_sumt'] = "合计";
  $this->Nm_lang['lang_btns_smry_rtte'] = "转换";
  $this->Nm_lang['lang_btns_smry_rtte_hint'] = "转换汇总";
  $this->Nm_lang['lang_btns_smry_conf'] = "设置";
  $this->Nm_lang['lang_btns_smry_conf_hint'] = "枢轴设置";
  $this->Nm_lang['lang_btns_smry_drll'] = "明细";
  $this->Nm_lang['lang_btns_smry_drll_hint'] = "查看明细";
  $this->Nm_lang['lang_btns_sort'] = "排序";
  $this->Nm_lang['lang_btns_sort_hint'] = "高级排序";
  $this->Nm_lang['lang_btns_srch'] = "搜索";
  $this->Nm_lang['lang_btns_srch_edit'] = "编辑";
  $this->Nm_lang['lang_btns_srch_edit_hint'] = "条件过滤";
  $this->Nm_lang['lang_btns_srch_hint'] = "搜索列";
  $this->Nm_lang['lang_btns_srch_mtmf'] = "搜索";
  $this->Nm_lang['lang_btns_srch_mtmf_hint'] = "搜索项目";
  $this->Nm_lang['lang_btns_srgb'] = "RGB";
  $this->Nm_lang['lang_btns_srgb_hint'] = "选择颜色";
  $this->Nm_lang['lang_btns_updt'] = "保存";
  $this->Nm_lang['lang_btns_updt_hint'] = "保存记录";
  $this->Nm_lang['lang_btns_view_chrt_hint'] = "显示图表";
  $this->Nm_lang['lang_btns_view_dtai_hint'] = "显示详情";
  $this->Nm_lang['lang_btns_xlsf'] = "XLS";
  $this->Nm_lang['lang_btns_xlsf_hint'] = "导出为Excel文件";
  $this->Nm_lang['lang_btns_xmlf'] = "XML";
  $this->Nm_lang['lang_btns_xmlf_hint'] = "导出为XML文件";
  $this->Nm_lang['lang_btns_zpcd'] = "邮编";
  $this->Nm_lang['lang_btns_zpcd_hint'] = "邮政编码";
  $this->Nm_lang['lang_btns_quck_srch'] = "快捷搜索";
  $this->Nm_lang['lang_btns_quck_srch_hint'] = "快捷搜索";
  $this->Nm_lang['lang_btns_copy'] = "复制";
  $this->Nm_lang['lang_btns_copy_hint'] = "复制当前记录";
  $this->Nm_lang['lang_btns_word_hint'] = "导出到Word";
  $this->Nm_lang['lang_btns_word'] = "WORD";
  $this->Nm_lang['lang_btns_mdtl_neww'] = "新增";
  $this->Nm_lang['lang_btns_mdtl_neww_hint'] = "添加新记录";
  $this->Nm_lang['lang_btns_maps'] = "谷歌地图";
  $this->Nm_lang['lang_btns_maps_hint'] = "在谷歌地图中显示";
  $this->Nm_lang['lang_btns_yutb'] = "YouTube";
  $this->Nm_lang['lang_btns_yutb_hint'] = "打开YouTube视频";
  $this->Nm_lang['lang_per_month'] = "月";
  $this->Nm_lang['lang_btns_down_hint'] = "下载";
  $this->Nm_lang['lang_btns_down'] = "下载";
  $this->Nm_lang['lang_btns_sv_sel'] = "保存选定";
  $this->Nm_lang['lang_btns_sv_sel_hint'] = "保存选定的记录";
  $this->Nm_lang['lang_btns_dl_sel'] = "删除选定";
  $this->Nm_lang['lang_btns_dl_sel_hint'] = "删除选定的记录";
  $this->Nm_lang['lang_btns_expt'] = "导出";
  $this->Nm_lang['lang_btns_jump_page_hint'] = "转至页码：";
  $this->Nm_lang['lang_flsh_chrt_radr'] = "雷达";
  $this->Nm_lang['lang_flsh_chrt_bars_funl'] = "坐标值";
  $this->Nm_lang['lang_flsh_chrt_funl'] = "漏斗";
  $this->Nm_lang['lang_per_allday'] = "整天";
  $this->Nm_lang['lang_per_today'] = "今日";
  $this->Nm_lang['lang_per_day'] = "天";
  $this->Nm_lang['lang_per_week'] = "周";
  $this->Nm_lang['lang_shrt_days_sem'] = "周";
  $this->Nm_lang['lang_substr_days_frid'] = "五";
  $this->Nm_lang['lang_substr_days_mond'] = "一";
  $this->Nm_lang['lang_substr_days_satd'] = "六";
  $this->Nm_lang['lang_substr_days_sund'] = "日";
  $this->Nm_lang['lang_substr_days_thud'] = "四";
  $this->Nm_lang['lang_substr_days_tued'] = "二";
  $this->Nm_lang['lang_substr_days_wend'] = "三";
  $this->Nm_lang['lang_msgs_totl'] = "报表总共";
  $this->Nm_lang['lang_othr_ajax_frmu'] = "数据已更改";
  $this->Nm_lang['lang_othr_bndw'] = "黑白";
  $this->Nm_lang['lang_othr_cptc_errm'] = "不匹配";
  $this->Nm_lang['lang_othr_cptc_lbel'] = "键入字母";
  $this->Nm_lang['lang_othr_chrt_totl'] = "总共";
  $this->Nm_lang['lang_othr_colr'] = "全颜色";
  $this->Nm_lang['lang_othr_cplt_defn'] = "设定颜色";
  $this->Nm_lang['lang_othr_cplt_titl'] = "色板";
  $this->Nm_lang['lang_othr_csvf_msg1'] = "CSV文件创建于：";
  $this->Nm_lang['lang_othr_csvf_msg2'] = "点击链接查看";
  $this->Nm_lang['lang_othr_curr_page'] = "当前页";
  $this->Nm_lang['lang_othr_date_days'] = "dd";
  $this->Nm_lang['lang_othr_date_mnth'] = "mm";
  $this->Nm_lang['lang_othr_date_year'] = "yyyy";
  $this->Nm_lang['lang_othr_date_hour'] = "hh";
  $this->Nm_lang['lang_othr_date_mint'] = "mm";
  $this->Nm_lang['lang_othr_date_scnd'] = "ss";
  $this->Nm_lang['lang_othr_detl_titl'] = "明细";
  $this->Nm_lang['lang_othr_file_msge'] = "文件创建成功";
  $this->Nm_lang['lang_othr_frmu_nlin'] = "线";
  $this->Nm_lang['lang_othr_frmi_titl'] = "（增加）";
  $this->Nm_lang['lang_othr_frmu_titl'] = "更改";
  $this->Nm_lang['lang_othr_full'] = "完整报表";
  $this->Nm_lang['lang_othr_grid_titl'] = "报表";
  $this->Nm_lang['lang_othr_prcs'] = "正在处理";
  $this->Nm_lang['lang_othr_prtc'] = "已上色";
  $this->Nm_lang['lang_othr_msec_capp'] = "应用";
  $this->Nm_lang['lang_othr_msec_cgrp'] = "角色";
  $this->Nm_lang['lang_othr_msec_cusr'] = "用户管理";
  $this->Nm_lang['lang_othr_msec_cpwd'] = "更改密码";
  $this->Nm_lang['lang_othr_msec_sync'] = "同步";
  $this->Nm_lang['lang_othr_msec_exit'] = "注销";
  $this->Nm_lang['lang_othr_msec_secr'] = "安全";
  $this->Nm_lang['lang_othr_prod_incp'] = "您正在使用与此应用程序不兼容的共用库文件，请联系技术支持。";
  $this->Nm_lang['lang_othr_prod_java'] = "未在服务器上发现Java运行环境，请联系技术支持。";
  $this->Nm_lang['lang_othr_prod_phpv'] = "PHP版本已过期，请联系技术支持。";
  $this->Nm_lang['lang_othr_prod_xtmb'] = "PHP需要激活mbsting扩展功能。请联系技术支持。";
  $this->Nm_lang['lang_othr_prod_xtzp'] = "PHP需要激活zip扩展功能。请联系技术支持。";
  $this->Nm_lang['lang_othr_reqr'] = "必填项!";
  $this->Nm_lang['lang_othr_rows'] = "记录";
  $this->Nm_lang['lang_othr_rtff_msg1'] = "RTF文件建立于：";
  $this->Nm_lang['lang_othr_rtff_msg2'] = "点击链接查看";
  $this->Nm_lang['lang_othr_rtrv'] = "从外部列表中选择数据";
  $this->Nm_lang['lang_othr_show_imgg'] = "显示图象";
  $this->Nm_lang['lang_othr_slct'] = "选择数据";
  $this->Nm_lang['lang_othr_smry_msge'] = "总结";
  $this->Nm_lang['lang_othr_smry_titl'] = "总结";
  $this->Nm_lang['lang_othr_smry_ppup_titl'] = "设置";
  $this->Nm_lang['lang_othr_smry_ppup_fild'] = "字段";
  $this->Nm_lang['lang_othr_smry_ppup_posi'] = "位置";
  $this->Nm_lang['lang_othr_smry_ppup_sort'] = "排序";
  $this->Nm_lang['lang_othr_smry_ppup_posi_labl'] = "字段作为标签";
  $this->Nm_lang['lang_othr_smry_ppup_posi_data'] = "字段作为数据";
  $this->Nm_lang['lang_othr_smry_ppup_sort_labl'] = "按标签";
  $this->Nm_lang['lang_othr_smry_ppup_sort_vlue'] = "按数值";
  $this->Nm_lang['lang_othr_smry_ppup_chek_tabu'] = "表格样式";
  $this->Nm_lang['lang_othr_smry_info'] = "?开始? 至 ?结束? 的 ?总共?";
  $this->Nm_lang['lang_othr_srch_titl'] = "搜索";
  $this->Nm_lang['lang_othr_srch_head'] = "保存过滤";
  $this->Nm_lang['lang_othr_totl'] = "总共";
  $this->Nm_lang['lang_othr_txto'] = "至";
  $this->Nm_lang['lang_othr_untl'] = "……";
  $this->Nm_lang['lang_othr_vrtc_nshw'] = "插入";
  $this->Nm_lang['lang_othr_vrtc_show'] = "删除";
  $this->Nm_lang['lang_othr_xlsf_msg1'] = "PDF文件创建于：";
  $this->Nm_lang['lang_othr_xlsf_msg2'] = "点击链接查看";
  $this->Nm_lang['lang_othr_xmlf_msg1'] = "XML文件建立于：";
  $this->Nm_lang['lang_othr_xmlf_msg2'] = "点击链接查看";
  $this->Nm_lang['lang_othr_zpcd'] = "";
  $this->Nm_lang['lang_othr_cepn'] = "无此邮编";
  $this->Nm_lang['lang_othr_slct_item'] = "选择";
  $this->Nm_lang['lang_othr_qk_watermark'] = "快捷搜索";
  $this->Nm_lang['lang_srch_curr_year'] = "今年";
  $this->Nm_lang['lang_srch_last_year'] = "去年";
  $this->Nm_lang['lang_srch_last_12mo'] = "前12个月";
  $this->Nm_lang['lang_srch_last_06mo'] = "前6个月";
  $this->Nm_lang['lang_srch_last_03mo'] = "前3个月";
  $this->Nm_lang['lang_errm_dele_rhcr'] = "存在依赖关系，不可删除";
  $this->Nm_lang['lang_errm_gd'] = "GD扩展未启用。";
  $this->Nm_lang['lang_btns_chrt_gantt'] = "甘特图";
  $this->Nm_lang['lang_btns_chrt_gantt_hint'] = "查看甘特图";
  $this->Nm_lang['lang_error_login_not_exist'] = "用户不存在！";
  $this->Nm_lang['lang_change_pswd'] = "更改密码";
  $this->Nm_lang['lang_flsh_chrt_gaug'] = "规";
  $this->Nm_lang['lang_flsh_chrt_gaug_shpe'] = "形状";
  $this->Nm_lang['lang_flsh_chrt_gaug_circ'] = "圆形";
  $this->Nm_lang['lang_flsh_chrt_gaug_semi'] = "半圆形";
  $this->Nm_lang['lang_flsh_chrt_polr'] = "极性";
  $this->Nm_lang['lang_retrieve_pswd'] = "取回密码";
  $this->Nm_lang['lang_error_login'] = "帐号或密码错误";
  $this->Nm_lang['lang_error_not_active'] = "帐号未被激活！";
  $this->Nm_lang['lang_srch_last_24mo'] = "过去24个月";
  $this->Nm_lang['lang_srch_last_18mo'] = "过去18个月";
  $this->Nm_lang['lang_errm_file_invl'] = "文件类型无效";
  $this->Nm_lang['lang_sec_app_name'] = "应用程序名称";
  $this->Nm_lang['lang_sec_app_description'] = "描述";
  $this->Nm_lang['lang_sec_priv_access'] = "查看";
  $this->Nm_lang['lang_sec_priv_print'] = "打印";
  $this->Nm_lang['lang_sec_priv_export'] = "导出";
  $this->Nm_lang['lang_sec_priv_insert'] = "插入";
  $this->Nm_lang['lang_sec_priv_update'] = "更新";
  $this->Nm_lang['lang_sec_priv_delete'] = "删除";
  $this->Nm_lang['lang_act_code_error'] = "激活码无效！";
  $this->Nm_lang['lang_sec_group'] = "角色";
  $this->Nm_lang['lang_sec_group_id'] = "角色ID";
  $this->Nm_lang['lang_sec_description'] = "描述";
  $this->Nm_lang['lang_sec_app_type'] = "应用类型";
  $this->Nm_lang['lang_send_actcode_newuser'] = "您好： <br>请访问此页面，激活%s的订阅。";
  $this->Nm_lang['lang_subject_new_user'] = "激活订阅。";
  $this->Nm_lang['lang_sended_active_code'] = "激活信息已发至新用户。";
  $this->Nm_lang['lang_subject_mail_new_user'] = "注册新用户";
  $this->Nm_lang['lang_chrt_angl_vals'] = "价值的角度";
  $this->Nm_lang['lang_chrt_anlt'] = "分析";
  $this->Nm_lang['lang_chrt_bars'] = "竖条";
  $this->Nm_lang['lang_chrt_bkgr_colr'] = "背景色";
  $this->Nm_lang['lang_chrt_crcl'] = "圆圈";
  $this->Nm_lang['lang_chrt_dots_type'] = "标记类型";
  $this->Nm_lang['lang_chrt_fill'] = "填充";
  $this->Nm_lang['lang_chrt_gtmd'] = "生成模式";
  $this->Nm_lang['lang_chrt_hgth'] = "高度（像素）";
  $this->Nm_lang['lang_chrt_horz_bars'] = "横条";
  $this->Nm_lang['lang_chrt_horz_line'] = "横线";
  $this->Nm_lang['lang_chrt_horz_puls'] = "水平脉冲";
  $this->Nm_lang['lang_chrt_horz_scat'] = "水平发散";
  $this->Nm_lang['lang_chrt_horz_squa'] = "长方形";
  $this->Nm_lang['lang_chrt_keep_aspc'] = "保持外观";
  $this->Nm_lang['lang_chrt_labl_colr'] = "标签颜色";
  $this->Nm_lang['lang_chrt_line'] = "竖线";
  $this->Nm_lang['lang_chrt_lzng'] = "菱形";
  $this->Nm_lang['lang_chrt_marg_colr'] = "页边颜色";
  $this->Nm_lang['lang_chrt_mrgn'] = "页边距（像素）";
  $this->Nm_lang['lang_chrt_nume_3pie'] = "立体饼图";
  $this->Nm_lang['lang_chrt_ordr'] = "数值排序";
  $this->Nm_lang['lang_chrt_ordr_none'] = "无";
  $this->Nm_lang['lang_chrt_ordr_asc'] = "升序";
  $this->Nm_lang['lang_chrt_ordr_desc'] = "降序";
  $this->Nm_lang['lang_chrt_nume_pies'] = "饼图";
  $this->Nm_lang['lang_chrt_perc_3pie'] = "百份比立体饼图";
  $this->Nm_lang['lang_chrt_prec_pies'] = "百份比饼图";
  $this->Nm_lang['lang_chrt_puls'] = "垂直脉冲";
  $this->Nm_lang['lang_chrt_scat'] = "垂直发散";
  $this->Nm_lang['lang_chrt_show_dots'] = "显示标志";
  $this->Nm_lang['lang_chrt_show_vals'] = "返回值";
  $this->Nm_lang['lang_chrt_smzd'] = "综合性";
  $this->Nm_lang['lang_chrt_sqre'] = "正方形";
  $this->Nm_lang['lang_chrt_squa'] = "正方形";
  $this->Nm_lang['lang_chrt_star'] = "星形";
  $this->Nm_lang['lang_chrt_titl'] = "图表选项";
  $this->Nm_lang['lang_chrt_trgl_down'] = "下三角形";
  $this->Nm_lang['lang_chrt_trgl_uppr'] = "上三角形";
  $this->Nm_lang['lang_chrt_type'] = "图表类型";
  $this->Nm_lang['lang_chrt_vals_colr'] = "字体颜色";
  $this->Nm_lang['lang_chrt_wdth'] = "宽度(像素)";
  $this->Nm_lang['lang_flsh_chrt_area'] = "区域";
  $this->Nm_lang['lang_flsh_chrt_area_area'] = "区域";
  $this->Nm_lang['lang_flsh_chrt_area_invi'] = "相反";
  $this->Nm_lang['lang_flsh_chrt_area_invn'] = "正常";
  $this->Nm_lang['lang_flsh_chrt_area_invr'] = "反向";
  $this->Nm_lang['lang_flsh_chrt_area_shpe'] = "形状";
  $this->Nm_lang['lang_flsh_chrt_area_spln'] = "曲线";
  $this->Nm_lang['lang_flsh_chrt_area_srbs'] = "按系列";
  $this->Nm_lang['lang_flsh_chrt_area_srgr'] = "系列组";
  $this->Nm_lang['lang_flsh_chrt_area_srst'] = "设定";
  $this->Nm_lang['lang_flsh_chrt_area_stak'] = "叠成";
  $this->Nm_lang['lang_flsh_chrt_area_stan'] = "关闭";
  $this->Nm_lang['lang_flsh_chrt_area_stap'] = "百份比";
  $this->Nm_lang['lang_flsh_chrt_area_stay'] = "开启";
  $this->Nm_lang['lang_flsh_chrt_bars'] = "条";
  $this->Nm_lang['lang_flsh_chrt_bars_2ddm'] = "2D";
  $this->Nm_lang['lang_flsh_chrt_bars_3ddm'] = "3D";
  $this->Nm_lang['lang_flsh_chrt_bars_3ont'] = "否";
  $this->Nm_lang['lang_flsh_chrt_bars_3ovr'] = "重叠";
  $this->Nm_lang['lang_flsh_chrt_bars_3oys'] = "是";
  $this->Nm_lang['lang_flsh_chrt_bars_bars'] = "条";
  $this->Nm_lang['lang_flsh_chrt_bars_cone'] = "圆锥形";
  $this->Nm_lang['lang_flsh_chrt_bars_cyld'] = "圆柱形";
  $this->Nm_lang['lang_flsh_chrt_bars_dmns'] = "面积";
  $this->Nm_lang['lang_flsh_chrt_bars_horz'] = "水平";
  $this->Nm_lang['lang_flsh_chrt_bars_invi'] = "相反";
  $this->Nm_lang['lang_flsh_chrt_bars_invn'] = "常规";
  $this->Nm_lang['lang_flsh_chrt_bars_invr'] = "反向";
  $this->Nm_lang['lang_flsh_chrt_bars_layo'] = "方位";
  $this->Nm_lang['lang_flsh_chrt_bars_pyrm'] = "金字塔形";
  $this->Nm_lang['lang_flsh_chrt_bars_shpe'] = "形状";
  $this->Nm_lang['lang_flsh_chrt_bars_srbs'] = "按系列";
  $this->Nm_lang['lang_flsh_chrt_bars_srgr'] = "系列组";
  $this->Nm_lang['lang_flsh_chrt_bars_srst'] = "设定";
  $this->Nm_lang['lang_flsh_chrt_bars_stan'] = "关闭";
  $this->Nm_lang['lang_flsh_chrt_bars_stap'] = "百份比";
  $this->Nm_lang['lang_flsh_chrt_bars_stay'] = "开启";
  $this->Nm_lang['lang_flsh_chrt_bars_stck'] = "叠加";
  $this->Nm_lang['lang_flsh_chrt_bars_vrtc'] = "垂直";
  $this->Nm_lang['lang_flsh_chrt_fpie'] = "饼图";
  $this->Nm_lang['lang_flsh_chrt_fpie_2ddm'] = "2D";
  $this->Nm_lang['lang_flsh_chrt_fpie_3ddm'] = "3D";
  $this->Nm_lang['lang_flsh_chrt_fpie_asrt'] = "升序";
  $this->Nm_lang['lang_flsh_chrt_fpie_axpl'] = "开";
  $this->Nm_lang['lang_flsh_chrt_fpie_cxpl'] = "点击";
  $this->Nm_lang['lang_flsh_chrt_fpie_dmns'] = "方位";
  $this->Nm_lang['lang_flsh_chrt_fpie_dnts'] = "圈饼";
  $this->Nm_lang['lang_flsh_chrt_fpie_dsrt'] = "降序";
  $this->Nm_lang['lang_flsh_chrt_fpie_dxpl'] = "关闭";
  $this->Nm_lang['lang_flsh_chrt_fpie_expl'] = "展开";
  $this->Nm_lang['lang_flsh_chrt_fpie_fpie'] = "饼图";
  $this->Nm_lang['lang_flsh_chrt_fpie_invi'] = "相反";
  $this->Nm_lang['lang_flsh_chrt_fpie_invn'] = "常规";
  $this->Nm_lang['lang_flsh_chrt_fpie_invr'] = "反向";
  $this->Nm_lang['lang_flsh_chrt_fpie_nsrt'] = "未分类";
  $this->Nm_lang['lang_flsh_chrt_fpie_shpe'] = "形状";
  $this->Nm_lang['lang_flsh_chrt_fpie_srtn'] = "排序";
  $this->Nm_lang['lang_flsh_chrt_fpie_fval'] = "数值格式";
  $this->Nm_lang['lang_flsh_chrt_fpie_fvlv'] = "值";
  $this->Nm_lang['lang_flsh_chrt_fpie_fvlp'] = "百分比";
  $this->Nm_lang['lang_flsh_chrt_line'] = "线条";
  $this->Nm_lang['lang_flsh_chrt_line_invi'] = "相反";
  $this->Nm_lang['lang_flsh_chrt_line_invn'] = "常规";
  $this->Nm_lang['lang_flsh_chrt_line_invr'] = "反向";
  $this->Nm_lang['lang_flsh_chrt_line_line'] = "线条";
  $this->Nm_lang['lang_flsh_chrt_line_shpe'] = "形状";
  $this->Nm_lang['lang_flsh_chrt_line_spln'] = "曲线";
  $this->Nm_lang['lang_flsh_chrt_line_srbs'] = "按系列";
  $this->Nm_lang['lang_flsh_chrt_line_srgr'] = "系列组";
  $this->Nm_lang['lang_flsh_chrt_line_srst'] = "设置";
  $this->Nm_lang['lang_flsh_chrt_line_step'] = "步骤";
  $this->Nm_lang['lang_flsh_chrt_mrks'] = "符号";
  $this->Nm_lang['lang_flsh_chrt_mrks_invi'] = "相反";
  $this->Nm_lang['lang_flsh_chrt_mrks_invn'] = "常规";
  $this->Nm_lang['lang_flsh_chrt_mrks_invr'] = "反向";
  $this->Nm_lang['lang_flsh_chrt_mrks_srbs'] = "按系列";
  $this->Nm_lang['lang_flsh_chrt_mrks_srgr'] = "系列组";
  $this->Nm_lang['lang_flsh_chrt_mrks_srst'] = "设置";
  $this->Nm_lang['lang_days_frid'] = "星期五";
  $this->Nm_lang['lang_days_mond'] = "星期一";
  $this->Nm_lang['lang_days_satd'] = "星期六";
  $this->Nm_lang['lang_days_sund'] = "星期日";
  $this->Nm_lang['lang_days_thud'] = "星期四";
  $this->Nm_lang['lang_days_tued'] = "星期二";
  $this->Nm_lang['lang_days_wend'] = "星期三";
  $this->Nm_lang['lang_mnth_apri'] = "四月";
  $this->Nm_lang['lang_mnth_augu'] = "八月";
  $this->Nm_lang['lang_mnth_dece'] = "十二月";
  $this->Nm_lang['lang_mnth_febr'] = "二月";
  $this->Nm_lang['lang_mnth_janu'] = "一月";
  $this->Nm_lang['lang_mnth_july'] = "七月";
  $this->Nm_lang['lang_mnth_june'] = "六月";
  $this->Nm_lang['lang_mnth_marc'] = "三月";
  $this->Nm_lang['lang_mnth_mayy'] = "五月";
  $this->Nm_lang['lang_mnth_nove'] = "十一月";
  $this->Nm_lang['lang_mnth_octo'] = "十月";
  $this->Nm_lang['lang_mnth_sept'] = "九月";
  $this->Nm_lang['lang_shrt_days_frid'] = "星期五";
  $this->Nm_lang['lang_shrt_days_mond'] = "星期一";
  $this->Nm_lang['lang_shrt_days_satd'] = "星期六";
  $this->Nm_lang['lang_shrt_days_sund'] = "星期日";
  $this->Nm_lang['lang_shrt_days_thud'] = "星期四";
  $this->Nm_lang['lang_shrt_days_tued'] = "星期二";
  $this->Nm_lang['lang_shrt_days_wend'] = "星期三";
  $this->Nm_lang['lang_shrt_mnth_apri'] = "四月";
  $this->Nm_lang['lang_shrt_mnth_augu'] = "八月";
  $this->Nm_lang['lang_shrt_mnth_dece'] = "十二月";
  $this->Nm_lang['lang_shrt_mnth_febr'] = "二月";
  $this->Nm_lang['lang_shrt_mnth_janu'] = "一月";
  $this->Nm_lang['lang_shrt_mnth_july'] = "七月";
  $this->Nm_lang['lang_shrt_mnth_june'] = "六月";
  $this->Nm_lang['lang_shrt_mnth_marc'] = "三月";
  $this->Nm_lang['lang_shrt_mnth_mayy'] = "五月";
  $this->Nm_lang['lang_shrt_mnth_nove'] = "十一月";
  $this->Nm_lang['lang_shrt_mnth_octo'] = "十月";
  $this->Nm_lang['lang_shrt_mnth_sept'] = "九月";
  $this->Nm_lang['lang_srch_days'] = "日";
  $this->Nm_lang['lang_srch_mint'] = "分钟";
  $this->Nm_lang['lang_srch_mnth'] = "月";
  $this->Nm_lang['lang_srch_scnd'] = "秒";
  $this->Nm_lang['lang_srch_time'] = "小时";
  $this->Nm_lang['lang_srch_year'] = "年";
  $this->Nm_lang['lang_recu_daily'] = "每日";
  $this->Nm_lang['lang_recu_weekly'] = "每周";
  $this->Nm_lang['lang_recu_monthly'] = "每月";
  $this->Nm_lang['lang_recu_annual'] = "周年";
  $this->Nm_lang['lang_peri_yes'] = "确认";
  $this->Nm_lang['lang_peri_no'] = "取消";
  $this->Nm_lang['lang_quck_srch'] = "快捷搜索";
  $this->Nm_lang['lang_srch_andd'] = "和";
  $this->Nm_lang['lang_srch_betw'] = "介于";
  $this->Nm_lang['lang_srch_betw_sevr'] = "在";
  $this->Nm_lang['lang_srch_butn'] = "搜索";
  $this->Nm_lang['lang_srch_butn_clea'] = "重置";
  $this->Nm_lang['lang_srch_butn_cncl'] = "取消";
  $this->Nm_lang['lang_srch_butn_dele'] = "删除过滤";
  $this->Nm_lang['lang_srch_butn_exit'] = "退出";
  $this->Nm_lang['lang_srch_butn_save'] = "保存过滤";
  $this->Nm_lang['lang_srch_cndt'] = "多个准则,请用:";
  $this->Nm_lang['lang_srch_dife'] = "不等于";
  $this->Nm_lang['lang_srch_diff'] = "不等于";
  $this->Nm_lang['lang_srch_equl'] = "等于";
  $this->Nm_lang['lang_srch_ever'] = "始终";
  $this->Nm_lang['lang_srch_exac'] = "等于";
  $this->Nm_lang['lang_srch_grtr'] = "大于";
  $this->Nm_lang['lang_srch_grtr_equl'] = "大于等于";
  $this->Nm_lang['lang_srch_head_mesg'] = "过滤";
  $this->Nm_lang['lang_srch_impt'] = "从外部列表中获取数据";
  $this->Nm_lang['lang_srch_last_mnth'] = "上月";
  $this->Nm_lang['lang_srch_less'] = "小于";
  $this->Nm_lang['lang_srch_less_equl'] = "小于等于";
  $this->Nm_lang['lang_srch_like'] = "包含";
  $this->Nm_lang['lang_srch_lst7'] = "前7天";
  $this->Nm_lang['lang_srch_lstw'] = "上周一至周日";
  $this->Nm_lang['lang_srch_lstw_bsnd'] = "上周一至周五";
  $this->Nm_lang['lang_srch_next_mnth'] = "下月";
  $this->Nm_lang['lang_srch_nnul'] = "内容不可为空";
  $this->Nm_lang['lang_srch_nrml'] = "简单";
  $this->Nm_lang['lang_srch_null'] = "空白内容";
  $this->Nm_lang['lang_srch_nx30'] = "后30天";
  $this->Nm_lang['lang_srch_nxt7'] = "后7天";
  $this->Nm_lang['lang_srch_nxtw_mond_frid'] = "下周一至周五";
  $this->Nm_lang['lang_srch_nxtw_mond_sund'] = "下周一至周日";
  $this->Nm_lang['lang_srch_orrr'] = "或者";
  $this->Nm_lang['lang_srch_outo'] = "不存在于";
  $this->Nm_lang['lang_srch_sett'] = "过滤条件";
  $this->Nm_lang['lang_srch_spec'] = "特别";
  $this->Nm_lang['lang_srch_strt'] = "以…开始";
  $this->Nm_lang['lang_srch_stts_with'] = "以…开始";
  $this->Nm_lang['lang_srch_tday'] = "今天";
  $this->Nm_lang['lang_srch_this_mnth'] = "本月";
  $this->Nm_lang['lang_srch_tomw'] = "明天";
  $this->Nm_lang['lang_srch_yest'] = "昨天";
  $this->Nm_lang['lang_srch_all_fields'] = "所有字段";
  $this->Nm_lang['lang_pdff_bkmk'] = "生成书签";
  $this->Nm_lang['lang_pdff_bndw'] = "黑白";
  $this->Nm_lang['lang_pdff_chrt'] = "显示图表";
  $this->Nm_lang['lang_pdff_chrt_nooo'] = "否";
  $this->Nm_lang['lang_pdff_chrt_yess'] = "是";
  $this->Nm_lang['lang_pdff_clck_mesg'] = "点击链接查看";
  $this->Nm_lang['lang_pdff_cncl'] = "取消";
  $this->Nm_lang['lang_pdff_colr'] = "颜色";
  $this->Nm_lang['lang_pdff_cstm'] = "自定义";
  $this->Nm_lang['lang_pdff_errg'] = "创建PDF档案错误，联系技术支持。";
  $this->Nm_lang['lang_pdff_file_loct'] = "PDF文件创建于：";
  $this->Nm_lang['lang_pdff_fnsh'] = "PDF已生成";
  $this->Nm_lang['lang_pdff_font'] = "字体大小";
  $this->Nm_lang['lang_pdff_frmt_page'] = "正在格式化页面";
  $this->Nm_lang['lang_pdff_gnrt'] = "正在生成PDF";
  $this->Nm_lang['lang_pdff_htlm_pdf'] = "正在建立用于生成PDF的HTML页面";
  $this->Nm_lang['lang_pdff_legl'] = "法定";
  $this->Nm_lang['lang_pdff_letr'] = "信件";
  $this->Nm_lang['lang_pdff_lnds'] = "横向";
  $this->Nm_lang['lang_pdff_pper'] = "页面布局";
  $this->Nm_lang['lang_pdff_pper_hgth'] = "高度";
  $this->Nm_lang['lang_pdff_pper_orie'] = "方向";
  $this->Nm_lang['lang_pdff_pper_wdth'] = "宽度";
  $this->Nm_lang['lang_pdff_prtr'] = "纵向";
  $this->Nm_lang['lang_pdff_rows'] = "正在格式化记录";
  $this->Nm_lang['lang_pdff_strt'] = "开始生成PDF";
  $this->Nm_lang['lang_pdff_titl'] = "PDF设置";
  $this->Nm_lang['lang_pdff_type'] = "打印设置";
  $this->Nm_lang['lang_pdff_wdth'] = "页面分辨率（像素）";
  $this->Nm_lang['lang_pdff_wrtg'] = "正在输出页面";
  $this->Nm_lang['lang_errm_ajax_data'] = "无效数据";
  $this->Nm_lang['lang_errm_ajax_rqrd'] = "必填项!";
  $this->Nm_lang['lang_errm_cfrm_remv'] = "确定删除选定的数据吗?";
  $this->Nm_lang['lang_errm_cmlb_nfnd'] = "找不到共用库文件：";
  $this->Nm_lang['lang_errm_dbas'] = "数据库访问";
  $this->Nm_lang['lang_errm_dbcn_conn'] = "数据库连接错误：";
  $this->Nm_lang['lang_errm_dbcn_data'] = "无法连接数据库：会话失效或连接参数错误。";
  $this->Nm_lang['lang_errm_dbcn_nfnd'] = "未找到数据库连接，请与系统管理员联系。连接：";
  $this->Nm_lang['lang_errm_dbcn_nspt'] = "本程序不支持连接该数据库，请与系统管理员联系。连接：";
  $this->Nm_lang['lang_errm_dber'] = "访问数据库时发生错误：";
  $this->Nm_lang['lang_errm_dcmt'] = "保存文件时发生错误：";
  $this->Nm_lang['lang_errm_dele'] = "删除时发生错误";
  $this->Nm_lang['lang_errm_dele_nfnd'] = "删除时发生错误：行找不到";
  $this->Nm_lang['lang_errm_empt'] = "无记录显示";
  $this->Nm_lang['lang_errm_errt'] = "发生错误";
  $this->Nm_lang['lang_errm_fkvi'] = "数据库错误：未删除记录";
  $this->Nm_lang['lang_errm_flds'] = "字段错误：";
  $this->Nm_lang['lang_errm_fnfd'] = "文件不存在";
  $this->Nm_lang['lang_errm_glob'] = "缺少全局变量：";
  $this->Nm_lang['lang_errm_inst'] = "插入错误";
  $this->Nm_lang['lang_errm_inst_uniq'] = "插入主键错误";
  $this->Nm_lang['lang_errm_ivch'] = "无效字符";
  $this->Nm_lang['lang_errm_ivdr'] = "无法创建目录";
  $this->Nm_lang['lang_errm_ivtp'] = "(无效类型)";
  $this->Nm_lang['lang_errm_line'] = "本行存在错误：";
  $this->Nm_lang['lang_errm_mnch'] = "至少";
  $this->Nm_lang['lang_errm_mxch'] = "至多";
  $this->Nm_lang['lang_errm_nchr'] = "字符";
  $this->Nm_lang['lang_errm_nfdr'] = "路径不存在或无保存权限";
  $this->Nm_lang['lang_errm_nfnd'] = "更新数据库时发生错误：行找不到";
  $this->Nm_lang['lang_errm_nfnd_extr'] = "数据库存取错误：行找不到";
  $this->Nm_lang['lang_errm_none_imge'] = "图象未被生成";
  $this->Nm_lang['lang_errm_pkey'] = "插入错误：记录已存在";
  $this->Nm_lang['lang_errm_remv'] = "确定删除记录?";
  $this->Nm_lang['lang_errm_reqd'] = "缺少字段：";
  $this->Nm_lang['lang_errm_size'] = "无效大小";
  $this->Nm_lang['lang_errm_time_outt'] = "程序已过期";
  $this->Nm_lang['lang_errm_tmeo'] = "记录数超出限制，更改搜索条件。";
  $this->Nm_lang['lang_errm_type_pswd'] = "输入访问密码";
  $this->Nm_lang['lang_errm_ukey'] = "更新数据库时主键错误：";
  $this->Nm_lang['lang_errm_unth_user'] = "未授权用户";
  $this->Nm_lang['lang_errm_unth_hwto'] = "开发环境中可关闭安全模式：菜单栏 (选项) - (我的ScriptCase)";
  $this->Nm_lang['lang_errm_updt'] = "数据库更新错误";
  $this->Nm_lang['lang_errm_upld'] = "(上传失败)";
  $this->Nm_lang['lang_log_id'] = "日志ID";
  $this->Nm_lang['lang_log_date_insert'] = "插入记录日期";
  $this->Nm_lang['lang_log_ip'] = "记录IP";
  $this->Nm_lang['lang_log_app'] = "记录应用";
  $this->Nm_lang['lang_log_creator'] = "记录创建者";
  $this->Nm_lang['lang_log_action'] = "记录行为";
  $this->Nm_lang['lang_log_updates'] = "日志更新";
  $this->Nm_lang['lang_log_title'] = "系统日志报告";
  $this->Nm_lang['lang_log_user'] = "记录用户";
  $this->Nm_lang['lang_log_description'] = "描述";
  $this->Nm_lang['lang_opt_yes'] = "确定";
  $this->Nm_lang['lang_opt_no'] = "取消";
  $this->Nm_lang['lang_exit'] = "退出";
  $this->Nm_lang['lang_error_confirm_pswd'] = "确认密码不匹配";
  $this->Nm_lang['lang_ret_pass'] = "取回密码";
  $this->Nm_lang['lang_error_pswd'] = "两次输入的新密码不一致";
  $this->Nm_lang['lang_syncronized_apps'] = "应用同步完成";
  $this->Nm_lang['lang_sync_apps'] = "您要同步应用吗？";
  $this->Nm_lang['lang_error_old_pswd'] = "旧密码不正确！";
  $this->Nm_lang['lang_new_user_sign_in'] = "用户注册成功！<br>  用户名： <b>%s</b><br> E-mail： <b>%s</b>";
  $this->Nm_lang['lang_new_user'] = "新建用户";
  $this->Nm_lang['lang_login_ok'] = "登陆成功";
  $this->Nm_lang['lang_send_new_pswd'] = "密码更改成功。<br>新密码：";
  $this->Nm_lang['lang_list_users'] = "用户管理";
  $this->Nm_lang['lang_send_activation_code'] = "发送激活码至电子邮箱：";
  $this->Nm_lang['lang_login_fail'] = "有人企图用您的帐号登陆！";
  $this->Nm_lang['lang_log_retrieve_pswd'] = "通过电​​子邮件取回密码";
  $this->Nm_lang['lang_menu_security'] = "安全";
  $this->Nm_lang['lang_list_apps_x_groups'] = "角色/应用";
  $this->Nm_lang['lang_list_groups'] = "角色";
  $this->Nm_lang['lang_list_apps'] = "应用";
  $this->Nm_lang['lang_list_users_groups'] = "用户/角色";
  $this->Nm_lang['lang_list_sync_apps'] = "同步应用";
  $this->Nm_lang['lang_msg_upd_pswd'] = "密码更新成功！";
  $this->Nm_lang['lang_old_pswd'] = "旧密码";
  $this->Nm_lang['lang_title_change_pswd'] = "更改密码";
  $this->Nm_lang['lang_send_act_code'] = "尊敬的用户：<br>请点击链接设置新密码。";
  $this->Nm_lang['lang_mail_sended_ok'] = "激活E-mail已发送，请检查您的邮箱！";
  $this->Nm_lang['lang_subject_mail'] = "取回密码";
  $this->Nm_lang['lang_send_pswd'] = "密码已重置，新密码为：";
  $this->Nm_lang['lang_list_apps_x_users'] = "用户/应用";
  $this->Nm_lang['lang_sec_users_fild_login'] = "用户名";
  $this->Nm_lang['lang_sec_users_fild_pswd'] = "密码";
  $this->Nm_lang['lang_sec_users_fild_name'] = "姓名";
  $this->Nm_lang['lang_sec_users_fild_email'] = "电子邮件";
  $this->Nm_lang['lang_sec_users_fild_active'] = "是否激活";
  $this->Nm_lang['lang_sec_users_fild_activation_code'] = "激活码";
  $this->Nm_lang['lang_sec_users_fild_priv_admin'] = "管理员权限";
  $this->Nm_lang['lang_sec_users_fild_pswd_confirm'] = "确认密码";
  $this->Nm_lang['lang_send_mail_admin'] = "新用户信息已发送至管理员的邮箱";
?>