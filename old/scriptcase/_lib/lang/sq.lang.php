<?php
  $this->Nm_lang = array();
  $this->Nm_lang['Nm_charset']     = "ISO-8859-2";
  $this->Nm_lang['lang_btns_dl_sel'] = "Delete Selected";
  $this->Nm_lang['lang_btns_dl_sel_hint'] = "Fshij të gjitha të dhënat e zgjedhura";
  $this->Nm_lang['lang_btns_expt'] = "Eksport";
  $this->Nm_lang['lang_btns_jump_page_hint'] = "Shko tek faqja";
  $this->Nm_lang['lang_btns_sv_sel'] = "Ruaj të zgjedhura";
  $this->Nm_lang['lang_btns_sv_sel_hint'] = "Ruaj të gjitha të dhënat e zgjedhura";
  $this->Nm_lang['lang_flsh_chrt_radr'] = "Radar";
  $this->Nm_lang['lang_flsh_chrt_bars_funl'] = "Vlerat në anën e";
  $this->Nm_lang['lang_flsh_chrt_funl'] = "Gyp";
  $this->Nm_lang['lang_shrt_days_sem'] = "Wk";
  $this->Nm_lang['lang_substr_days_frid'] = "Fr";
  $this->Nm_lang['lang_substr_days_mond'] = "Mo";
  $this->Nm_lang['lang_substr_days_satd'] = "Sa";
  $this->Nm_lang['lang_substr_days_sund'] = "Su";
  $this->Nm_lang['lang_substr_days_thud'] = "Th";
  $this->Nm_lang['lang_substr_days_tued'] = "Tu";
  $this->Nm_lang['lang_substr_days_wend'] = "We";
  $this->Nm_lang['lang_othr_qk_watermark'] = "Kërkim i shpejtë";
  $this->Nm_lang['lang_othr_slct_item'] = "Përzgjedh";
  $this->Nm_lang['lang_errm_dele_rhcr'] = "You can not delete the record, because it has dependencies.";
  $this->Nm_lang['lang_errm_gd'] = "Zgjerimi GD nuk është aktivizuar në PHP.";
  $this->Nm_lang['lang_btns_chrt_gantt'] = "Gantt";
  $this->Nm_lang['lang_btns_chrt_gantt_hint'] = "Shiko chart Gantt.";
  $this->Nm_lang['lang_change_pswd'] = "Ndryshimi juaj";
  $this->Nm_lang['lang_error_login_not_exist'] = "Login not exist!";
  $this->Nm_lang['lang_flsh_chrt_gaug'] = "Matës";
  $this->Nm_lang['lang_flsh_chrt_gaug_circ'] = "Qarkore";
  $this->Nm_lang['lang_flsh_chrt_gaug_semi'] = "Qarkorja Semi";
  $this->Nm_lang['lang_flsh_chrt_gaug_shpe'] = "Formë";
  $this->Nm_lang['lang_flsh_chrt_polr'] = "Polar";
  $this->Nm_lang['lang_error_login'] = "User / Password wrong.";
  $this->Nm_lang['lang_error_not_active'] = "Na vjen keq, por përdoruesi nuk është aktiv!";
  $this->Nm_lang['lang_retrieve_pswd'] = "Retrieve Password";
  $this->Nm_lang['lang_srch_last_18mo'] = "Të fundit 18 muaj";
  $this->Nm_lang['lang_srch_last_24mo'] = "Të fundit 24 muaj";
  $this->Nm_lang['lang_errm_file_invl'] = "Lloji i pavlefshëm fotografi";
  $this->Nm_lang['lang_act_code_error'] = "Kodi i pavlefshëm aktivizimi!";
  $this->Nm_lang['lang_sec_app_description'] = "Përshkrim";
  $this->Nm_lang['lang_sec_app_name'] = "Emri aplikimi";
  $this->Nm_lang['lang_sec_app_type'] = "Lloji i aplikimit";
  $this->Nm_lang['lang_sec_description'] = "Përshkrim";
  $this->Nm_lang['lang_sec_group'] = "Grup";
  $this->Nm_lang['lang_sec_group_id'] = "Grupi ID";
  $this->Nm_lang['lang_sec_priv_access'] = "Privilegj për Qasje";
  $this->Nm_lang['lang_sec_priv_delete'] = "Privilegj për Delete";
  $this->Nm_lang['lang_sec_priv_export'] = "Privilegj që të Export";
  $this->Nm_lang['lang_sec_priv_insert'] = "Privilegj për të futur";
  $this->Nm_lang['lang_sec_priv_print'] = "Privilegj për Print";
  $this->Nm_lang['lang_sec_priv_update'] = "Privilegj për Update";
  $this->Nm_lang['lang_sended_active_code'] = "Mesazhi aktivizimi dërguar për përdorues të ri.";
  $this->Nm_lang['lang_send_actcode_newuser'] = "Përshëndetje, <br/> Ju lutem, hyni në këtë faqe për të aktivizuar abonimin tuaj qind %s.";
  $this->Nm_lang['lang_subject_mail_new_user'] = "Përdorues i ri i regjistruar";
  $this->Nm_lang['lang_subject_new_user'] = "Aktivizo Abonimi.";
  $this->Nm_lang['lang_btns_copy'] = "Kopje";
  $this->Nm_lang['lang_btns_copy_hint'] = "Kopje të të dhënave të regjistrit për një të ri.";
  $this->Nm_lang['lang_btns_word'] = "WORD";
  $this->Nm_lang['lang_btns_word_hint'] = "Eksporti në dokumentet e Word (. Doc).";
  $this->Nm_lang['lang_log_action'] = "Identifikohu Action";
  $this->Nm_lang['lang_log_app'] = "Identifikohu Aplikimi";
  $this->Nm_lang['lang_log_creator'] = "Identifikohu Krijuesin";
  $this->Nm_lang['lang_log_date_insert'] = "Identifikohu Fut Data";
  $this->Nm_lang['lang_log_description'] = "Përshkrim";
  $this->Nm_lang['lang_log_id'] = "Identifikohu ID";
  $this->Nm_lang['lang_log_ip'] = "Identifikohu IP";
  $this->Nm_lang['lang_log_title'] = "Identifikohu sistem Raporti";
  $this->Nm_lang['lang_log_updates'] = "Identifikohu Updates";
  $this->Nm_lang['lang_log_user'] = "Identifikohu përdoruesin";
  $this->Nm_lang['lang_error_confirm_pswd'] = "Password confirmation does not equal";
  $this->Nm_lang['lang_error_old_pswd'] = "Old Password incorrect!";
  $this->Nm_lang['lang_error_pswd'] = "The password and confirmation do not match.";
  $this->Nm_lang['lang_exit'] = "Exit";
  $this->Nm_lang['lang_list_apps'] = "Applications";
  $this->Nm_lang['lang_list_apps_x_groups'] = "Groups/Applications";
  $this->Nm_lang['lang_list_apps_x_users'] = "Users/Applications";
  $this->Nm_lang['lang_list_groups'] = "Groups";
  $this->Nm_lang['lang_list_sync_apps'] = "Synchronize Applications";
  $this->Nm_lang['lang_list_users'] = "Users";
  $this->Nm_lang['lang_list_users_groups'] = "Users/Groups";
  $this->Nm_lang['lang_login_fail'] = "Dikush u përpoq të identifikoheni me antarit";
  $this->Nm_lang['lang_login_ok'] = "Login accomplished in the system";
  $this->Nm_lang['lang_log_retrieve_pswd'] = "Send email to retrieve password";
  $this->Nm_lang['lang_mail_sended_ok'] = "E-mail with activation key sent!";
  $this->Nm_lang['lang_menu_security'] = "Security";
  $this->Nm_lang['lang_msg_upd_pswd'] = "Password updated successfully!";
  $this->Nm_lang['lang_new_user'] = "New user";
  $this->Nm_lang['lang_new_user_sign_in'] = "New user registered in the system!<br/>  User: <b>%s</b><br/> E-mail: <b>%s</b>";
  $this->Nm_lang['lang_old_pswd'] = "Old Password";
  $this->Nm_lang['lang_opt_no'] = "No";
  $this->Nm_lang['lang_opt_yes'] = "Po";
  $this->Nm_lang['lang_ret_pass'] = "Retrieve password";
  $this->Nm_lang['lang_sec_users_fild_activation_code'] = "Kodi aktivizimi";
  $this->Nm_lang['lang_sec_users_fild_active'] = "Aktiv";
  $this->Nm_lang['lang_sec_users_fild_email'] = "Email";
  $this->Nm_lang['lang_sec_users_fild_login'] = "Hyrje";
  $this->Nm_lang['lang_sec_users_fild_name'] = "Emër";
  $this->Nm_lang['lang_sec_users_fild_priv_admin'] = "Privilege Administrator";
  $this->Nm_lang['lang_sec_users_fild_pswd'] = "Fjalëkalimi";
  $this->Nm_lang['lang_sec_users_fild_pswd_confirm'] = "Konfirmo fjalëkalimin";
  $this->Nm_lang['lang_send_activation_code'] = "Sended activation code to email:";
  $this->Nm_lang['lang_send_act_code'] = "Hi, <br/> You request to recover password, please access the link, to create a new password.";
  $this->Nm_lang['lang_send_mail_admin'] = "E-mail dërguar admin me përdoruesit e rinj";
  $this->Nm_lang['lang_send_new_pswd'] = "You request a new password.<br/> Your password is reseted to:";
  $this->Nm_lang['lang_send_pswd'] = "You requested to recover password. Your password is:";
  $this->Nm_lang['lang_subject_mail'] = "Retrieve password";
  $this->Nm_lang['lang_syncronized_apps'] = "Applications synchronized";
  $this->Nm_lang['lang_sync_apps'] = "Do you want to synchronize the applications?";
  $this->Nm_lang['lang_title_change_pswd'] = "Change Password";
?>