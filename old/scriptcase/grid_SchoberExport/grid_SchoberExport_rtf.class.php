<?php

class grid_SchoberExport_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $texto_tag;
   var $arquivo;
   var $tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_SchoberExport_rtf()
   {
      $this->nm_data   = new nm_data("en_us");
      $this->texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz . "grid_SchoberExport.php" ; 
      $this->arquivo    = "sc_rtf";
      $this->arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->arquivo   .= "_grid_SchoberExport";
      $this->arquivo   .= ".rtf";
      $this->tit_doc    = "grid_SchoberExport.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_SchoberExport']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_SchoberExport']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_SchoberExport']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']))
      { 
          $this->id = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['id']; 
          $tmp_pos = strpos($this->id, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id = substr($this->id, 0, $tmp_pos);
          }
          $this->sc_field_0 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_0']; 
          $tmp_pos = strpos($this->sc_field_0, "##@@");
          if ($tmp_pos !== false)
          {
              $this->sc_field_0 = substr($this->sc_field_0, 0, $tmp_pos);
          }
          $this->firmenname1 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['firmenname1']; 
          $tmp_pos = strpos($this->firmenname1, "##@@");
          if ($tmp_pos !== false)
          {
              $this->firmenname1 = substr($this->firmenname1, 0, $tmp_pos);
          }
          $this->firmenname2 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['firmenname2']; 
          $tmp_pos = strpos($this->firmenname2, "##@@");
          if ($tmp_pos !== false)
          {
              $this->firmenname2 = substr($this->firmenname2, 0, $tmp_pos);
          }
          $this->firmenname3 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['firmenname3']; 
          $tmp_pos = strpos($this->firmenname3, "##@@");
          if ($tmp_pos !== false)
          {
              $this->firmenname3 = substr($this->firmenname3, 0, $tmp_pos);
          }
          $this->strasse = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['strasse']; 
          $tmp_pos = strpos($this->strasse, "##@@");
          if ($tmp_pos !== false)
          {
              $this->strasse = substr($this->strasse, 0, $tmp_pos);
          }
          $this->plz = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['plz']; 
          $tmp_pos = strpos($this->plz, "##@@");
          if ($tmp_pos !== false)
          {
              $this->plz = substr($this->plz, 0, $tmp_pos);
          }
          $this->ort = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['ort']; 
          $tmp_pos = strpos($this->ort, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ort = substr($this->ort, 0, $tmp_pos);
          }
          $this->postfach = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['postfach']; 
          $tmp_pos = strpos($this->postfach, "##@@");
          if ($tmp_pos !== false)
          {
              $this->postfach = substr($this->postfach, 0, $tmp_pos);
          }
          $this->vorname = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['vorname']; 
          $tmp_pos = strpos($this->vorname, "##@@");
          if ($tmp_pos !== false)
          {
              $this->vorname = substr($this->vorname, 0, $tmp_pos);
          }
          $this->nachname = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['nachname']; 
          $tmp_pos = strpos($this->nachname, "##@@");
          if ($tmp_pos !== false)
          {
              $this->nachname = substr($this->nachname, 0, $tmp_pos);
          }
          $this->sc_field_1 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_1']; 
          $tmp_pos = strpos($this->sc_field_1, "##@@");
          if ($tmp_pos !== false)
          {
              $this->sc_field_1 = substr($this->sc_field_1, 0, $tmp_pos);
          }
          $this->bundesland = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['bundesland']; 
          $tmp_pos = strpos($this->bundesland, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bundesland = substr($this->bundesland, 0, $tmp_pos);
          }
          $this->regierungsbezirk = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['regierungsbezirk']; 
          $tmp_pos = strpos($this->regierungsbezirk, "##@@");
          if ($tmp_pos !== false)
          {
              $this->regierungsbezirk = substr($this->regierungsbezirk, 0, $tmp_pos);
          }
          $this->kreis = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['kreis']; 
          $tmp_pos = strpos($this->kreis, "##@@");
          if ($tmp_pos !== false)
          {
              $this->kreis = substr($this->kreis, 0, $tmp_pos);
          }
          $this->branchencode1 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['branchencode1']; 
          $tmp_pos = strpos($this->branchencode1, "##@@");
          if ($tmp_pos !== false)
          {
              $this->branchencode1 = substr($this->branchencode1, 0, $tmp_pos);
          }
          $this->branchencode2 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['branchencode2']; 
          $tmp_pos = strpos($this->branchencode2, "##@@");
          if ($tmp_pos !== false)
          {
              $this->branchencode2 = substr($this->branchencode2, 0, $tmp_pos);
          }
          $this->branchencode3 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['branchencode3']; 
          $tmp_pos = strpos($this->branchencode3, "##@@");
          if ($tmp_pos !== false)
          {
              $this->branchencode3 = substr($this->branchencode3, 0, $tmp_pos);
          }
          $this->sc_field_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_2']; 
          $tmp_pos = strpos($this->sc_field_2, "##@@");
          if ($tmp_pos !== false)
          {
              $this->sc_field_2 = substr($this->sc_field_2, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['rtf_name']))
      {
          $this->arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['rtf_name'];
          $this->tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT ID, Firmenname1, Firmenname2, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Vorwahl, Tel, `E-Mailadresse` as sc_field_1, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT ID, Firmenname1, Firmenname2, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Vorwahl, Tel, `E-Mailadresse` as sc_field_1, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT ID, Firmenname1, Firmenname2, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Vorwahl, Tel, `E-Mailadresse` as sc_field_1, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT ID, Firmenname1, Firmenname2, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Vorwahl, Tel, `E-Mailadresse` as sc_field_1, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT ID, Firmenname1, Firmenname2, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Vorwahl, Tel, `E-Mailadresse` as sc_field_1, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3 from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT ID, Firmenname1, Firmenname2, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Vorwahl, Tel, `E-Mailadresse` as sc_field_1, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3 from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->texto_tag .= "<table>\r\n";
      $this->texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['id'])) ? $this->New_label['id'] : "ID"; 
          if ($Cada_col == "id" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['firmenname1'])) ? $this->New_label['firmenname1'] : "Firmenname 1"; 
          if ($Cada_col == "firmenname1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['firmenname2'])) ? $this->New_label['firmenname2'] : "Firmenname 2"; 
          if ($Cada_col == "firmenname2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['strasse'])) ? $this->New_label['strasse'] : "Strasse"; 
          if ($Cada_col == "strasse" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['plz'])) ? $this->New_label['plz'] : "PLZ"; 
          if ($Cada_col == "plz" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ort'])) ? $this->New_label['ort'] : "Ort"; 
          if ($Cada_col == "ort" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['postfach'])) ? $this->New_label['postfach'] : "Postfach"; 
          if ($Cada_col == "postfach" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['plz_postfach'])) ? $this->New_label['plz_postfach'] : "PLZ Postfach"; 
          if ($Cada_col == "plz_postfach" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['titel'])) ? $this->New_label['titel'] : "Titel"; 
          if ($Cada_col == "titel" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['vorname'])) ? $this->New_label['vorname'] : "Vorname"; 
          if ($Cada_col == "vorname" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['nachname'])) ? $this->New_label['nachname'] : "Nachname"; 
          if ($Cada_col == "nachname" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['funktion'])) ? $this->New_label['funktion'] : "Funktion"; 
          if ($Cada_col == "funktion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['abteilungszusatz'])) ? $this->New_label['abteilungszusatz'] : "Abteilungszusatz"; 
          if ($Cada_col == "abteilungszusatz" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['vorwahl'])) ? $this->New_label['vorwahl'] : "Vorwahl"; 
          if ($Cada_col == "vorwahl" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['tel'])) ? $this->New_label['tel'] : "Tel"; 
          if ($Cada_col == "tel" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['sc_field_1'])) ? $this->New_label['sc_field_1'] : "E Mailadresse"; 
          if ($Cada_col == "sc_field_1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['homepage'])) ? $this->New_label['homepage'] : "Homepage"; 
          if ($Cada_col == "homepage" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['bundesland'])) ? $this->New_label['bundesland'] : "Bundesland"; 
          if ($Cada_col == "bundesland" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['regierungsbezirk'])) ? $this->New_label['regierungsbezirk'] : "Regierungsbezirk"; 
          if ($Cada_col == "regierungsbezirk" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['kreis'])) ? $this->New_label['kreis'] : "Kreis"; 
          if ($Cada_col == "kreis" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['beschaeftigtenklasse'])) ? $this->New_label['beschaeftigtenklasse'] : "Beschaeftigtenklasse"; 
          if ($Cada_col == "beschaeftigtenklasse" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['branchencode1'])) ? $this->New_label['branchencode1'] : "Branchencode 1"; 
          if ($Cada_col == "branchencode1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['branchencode2'])) ? $this->New_label['branchencode2'] : "Branchencode 2"; 
          if ($Cada_col == "branchencode2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['branchencode3'])) ? $this->New_label['branchencode3'] : "Branchencode 3"; 
          if ($Cada_col == "branchencode3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = mb_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->texto_tag .= "<tr>\r\n";
         $this->id = $rs->fields[0] ;  
         $this->id = (string)$this->id;
         $this->firmenname1 = $rs->fields[1] ;  
         $this->firmenname2 = $rs->fields[2] ;  
         $this->strasse = $rs->fields[3] ;  
         $this->plz = $rs->fields[4] ;  
         $this->ort = $rs->fields[5] ;  
         $this->postfach = $rs->fields[6] ;  
         $this->plz_postfach = $rs->fields[7] ;  
         $this->titel = $rs->fields[8] ;  
         $this->vorname = $rs->fields[9] ;  
         $this->nachname = $rs->fields[10] ;  
         $this->funktion = $rs->fields[11] ;  
         $this->abteilungszusatz = $rs->fields[12] ;  
         $this->vorwahl = $rs->fields[13] ;  
         $this->tel = $rs->fields[14] ;  
         $this->sc_field_1 = $rs->fields[15] ;  
         $this->homepage = $rs->fields[16] ;  
         $this->bundesland = $rs->fields[17] ;  
         $this->regierungsbezirk = $rs->fields[18] ;  
         $this->kreis = $rs->fields[19] ;  
         $this->beschaeftigtenklasse = $rs->fields[20] ;  
         $this->branchencode1 = $rs->fields[21] ;  
         $this->branchencode2 = $rs->fields[22] ;  
         $this->branchencode3 = $rs->fields[23] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- id
   function NM_export_id()
   {
         nmgp_Form_Num_Val($this->id, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id))
         {
             $this->id = mb_convert_encoding($this->id, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->id = str_replace('<', '&lt;', $this->id);
          $this->id = str_replace('>', '&gt;', $this->id);
         $this->texto_tag .= "<td>" . $this->id . "</td>\r\n";
   }
   //----- firmenname1
   function NM_export_firmenname1()
   {
         if (!NM_is_utf8($this->firmenname1))
         {
             $this->firmenname1 = mb_convert_encoding($this->firmenname1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->firmenname1 = str_replace('<', '&lt;', $this->firmenname1);
          $this->firmenname1 = str_replace('>', '&gt;', $this->firmenname1);
         $this->texto_tag .= "<td>" . $this->firmenname1 . "</td>\r\n";
   }
   //----- firmenname2
   function NM_export_firmenname2()
   {
         if (!NM_is_utf8($this->firmenname2))
         {
             $this->firmenname2 = mb_convert_encoding($this->firmenname2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->firmenname2 = str_replace('<', '&lt;', $this->firmenname2);
          $this->firmenname2 = str_replace('>', '&gt;', $this->firmenname2);
         $this->texto_tag .= "<td>" . $this->firmenname2 . "</td>\r\n";
   }
   //----- strasse
   function NM_export_strasse()
   {
         if (!NM_is_utf8($this->strasse))
         {
             $this->strasse = mb_convert_encoding($this->strasse, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->strasse = str_replace('<', '&lt;', $this->strasse);
          $this->strasse = str_replace('>', '&gt;', $this->strasse);
         $this->texto_tag .= "<td>" . $this->strasse . "</td>\r\n";
   }
   //----- plz
   function NM_export_plz()
   {
         if (!NM_is_utf8($this->plz))
         {
             $this->plz = mb_convert_encoding($this->plz, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->plz = str_replace('<', '&lt;', $this->plz);
          $this->plz = str_replace('>', '&gt;', $this->plz);
         $this->texto_tag .= "<td>" . $this->plz . "</td>\r\n";
   }
   //----- ort
   function NM_export_ort()
   {
         if (!NM_is_utf8($this->ort))
         {
             $this->ort = mb_convert_encoding($this->ort, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->ort = str_replace('<', '&lt;', $this->ort);
          $this->ort = str_replace('>', '&gt;', $this->ort);
         $this->texto_tag .= "<td>" . $this->ort . "</td>\r\n";
   }
   //----- postfach
   function NM_export_postfach()
   {
         if (!NM_is_utf8($this->postfach))
         {
             $this->postfach = mb_convert_encoding($this->postfach, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->postfach = str_replace('<', '&lt;', $this->postfach);
          $this->postfach = str_replace('>', '&gt;', $this->postfach);
         $this->texto_tag .= "<td>" . $this->postfach . "</td>\r\n";
   }
   //----- plz_postfach
   function NM_export_plz_postfach()
   {
         if (!NM_is_utf8($this->plz_postfach))
         {
             $this->plz_postfach = mb_convert_encoding($this->plz_postfach, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->plz_postfach = str_replace('<', '&lt;', $this->plz_postfach);
          $this->plz_postfach = str_replace('>', '&gt;', $this->plz_postfach);
         $this->texto_tag .= "<td>" . $this->plz_postfach . "</td>\r\n";
   }
   //----- titel
   function NM_export_titel()
   {
         if (!NM_is_utf8($this->titel))
         {
             $this->titel = mb_convert_encoding($this->titel, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->titel = str_replace('<', '&lt;', $this->titel);
          $this->titel = str_replace('>', '&gt;', $this->titel);
         $this->texto_tag .= "<td>" . $this->titel . "</td>\r\n";
   }
   //----- vorname
   function NM_export_vorname()
   {
         if (!NM_is_utf8($this->vorname))
         {
             $this->vorname = mb_convert_encoding($this->vorname, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->vorname = str_replace('<', '&lt;', $this->vorname);
          $this->vorname = str_replace('>', '&gt;', $this->vorname);
         $this->texto_tag .= "<td>" . $this->vorname . "</td>\r\n";
   }
   //----- nachname
   function NM_export_nachname()
   {
         if (!NM_is_utf8($this->nachname))
         {
             $this->nachname = mb_convert_encoding($this->nachname, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->nachname = str_replace('<', '&lt;', $this->nachname);
          $this->nachname = str_replace('>', '&gt;', $this->nachname);
         $this->texto_tag .= "<td>" . $this->nachname . "</td>\r\n";
   }
   //----- funktion
   function NM_export_funktion()
   {
         if (!NM_is_utf8($this->funktion))
         {
             $this->funktion = mb_convert_encoding($this->funktion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->funktion = str_replace('<', '&lt;', $this->funktion);
          $this->funktion = str_replace('>', '&gt;', $this->funktion);
         $this->texto_tag .= "<td>" . $this->funktion . "</td>\r\n";
   }
   //----- abteilungszusatz
   function NM_export_abteilungszusatz()
   {
         if (!NM_is_utf8($this->abteilungszusatz))
         {
             $this->abteilungszusatz = mb_convert_encoding($this->abteilungszusatz, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->abteilungszusatz = str_replace('<', '&lt;', $this->abteilungszusatz);
          $this->abteilungszusatz = str_replace('>', '&gt;', $this->abteilungszusatz);
         $this->texto_tag .= "<td>" . $this->abteilungszusatz . "</td>\r\n";
   }
   //----- vorwahl
   function NM_export_vorwahl()
   {
         if (!NM_is_utf8($this->vorwahl))
         {
             $this->vorwahl = mb_convert_encoding($this->vorwahl, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->vorwahl = str_replace('<', '&lt;', $this->vorwahl);
          $this->vorwahl = str_replace('>', '&gt;', $this->vorwahl);
         $this->texto_tag .= "<td>" . $this->vorwahl . "</td>\r\n";
   }
   //----- tel
   function NM_export_tel()
   {
         if (!NM_is_utf8($this->tel))
         {
             $this->tel = mb_convert_encoding($this->tel, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->tel = str_replace('<', '&lt;', $this->tel);
          $this->tel = str_replace('>', '&gt;', $this->tel);
         $this->texto_tag .= "<td>" . $this->tel . "</td>\r\n";
   }
   //----- sc_field_1
   function NM_export_sc_field_1()
   {
         if (!NM_is_utf8($this->sc_field_1))
         {
             $this->sc_field_1 = mb_convert_encoding($this->sc_field_1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->sc_field_1 = str_replace('<', '&lt;', $this->sc_field_1);
          $this->sc_field_1 = str_replace('>', '&gt;', $this->sc_field_1);
         $this->texto_tag .= "<td>" . $this->sc_field_1 . "</td>\r\n";
   }
   //----- homepage
   function NM_export_homepage()
   {
         if (!NM_is_utf8($this->homepage))
         {
             $this->homepage = mb_convert_encoding($this->homepage, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->homepage = str_replace('<', '&lt;', $this->homepage);
          $this->homepage = str_replace('>', '&gt;', $this->homepage);
         $this->texto_tag .= "<td>" . $this->homepage . "</td>\r\n";
   }
   //----- bundesland
   function NM_export_bundesland()
   {
         if (!NM_is_utf8($this->bundesland))
         {
             $this->bundesland = mb_convert_encoding($this->bundesland, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->bundesland = str_replace('<', '&lt;', $this->bundesland);
          $this->bundesland = str_replace('>', '&gt;', $this->bundesland);
         $this->texto_tag .= "<td>" . $this->bundesland . "</td>\r\n";
   }
   //----- regierungsbezirk
   function NM_export_regierungsbezirk()
   {
         if (!NM_is_utf8($this->regierungsbezirk))
         {
             $this->regierungsbezirk = mb_convert_encoding($this->regierungsbezirk, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->regierungsbezirk = str_replace('<', '&lt;', $this->regierungsbezirk);
          $this->regierungsbezirk = str_replace('>', '&gt;', $this->regierungsbezirk);
         $this->texto_tag .= "<td>" . $this->regierungsbezirk . "</td>\r\n";
   }
   //----- kreis
   function NM_export_kreis()
   {
         if (!NM_is_utf8($this->kreis))
         {
             $this->kreis = mb_convert_encoding($this->kreis, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->kreis = str_replace('<', '&lt;', $this->kreis);
          $this->kreis = str_replace('>', '&gt;', $this->kreis);
         $this->texto_tag .= "<td>" . $this->kreis . "</td>\r\n";
   }
   //----- beschaeftigtenklasse
   function NM_export_beschaeftigtenklasse()
   {
         if (!NM_is_utf8($this->beschaeftigtenklasse))
         {
             $this->beschaeftigtenklasse = mb_convert_encoding($this->beschaeftigtenklasse, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->beschaeftigtenklasse = str_replace('<', '&lt;', $this->beschaeftigtenklasse);
          $this->beschaeftigtenklasse = str_replace('>', '&gt;', $this->beschaeftigtenklasse);
         $this->texto_tag .= "<td>" . $this->beschaeftigtenklasse . "</td>\r\n";
   }
   //----- branchencode1
   function NM_export_branchencode1()
   {
         if (!NM_is_utf8($this->branchencode1))
         {
             $this->branchencode1 = mb_convert_encoding($this->branchencode1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->branchencode1 = str_replace('<', '&lt;', $this->branchencode1);
          $this->branchencode1 = str_replace('>', '&gt;', $this->branchencode1);
         $this->texto_tag .= "<td>" . $this->branchencode1 . "</td>\r\n";
   }
   //----- branchencode2
   function NM_export_branchencode2()
   {
         if (!NM_is_utf8($this->branchencode2))
         {
             $this->branchencode2 = mb_convert_encoding($this->branchencode2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->branchencode2 = str_replace('<', '&lt;', $this->branchencode2);
          $this->branchencode2 = str_replace('>', '&gt;', $this->branchencode2);
         $this->texto_tag .= "<td>" . $this->branchencode2 . "</td>\r\n";
   }
   //----- branchencode3
   function NM_export_branchencode3()
   {
         if (!NM_is_utf8($this->branchencode3))
         {
             $this->branchencode3 = mb_convert_encoding($this->branchencode3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
          $this->branchencode3 = str_replace('<', '&lt;', $this->branchencode3);
          $this->branchencode3 = str_replace('>', '&gt;', $this->branchencode3);
         $this->texto_tag .= "<td>" . $this->branchencode3 . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->arquivo;
      }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Ini->Nm_lang['lang_othr_grid_titl'] ?> - SchoberExport :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_SchoberExport_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="nm_tit_doc" value="<?php echo NM_encode_input($this->tit_doc); ?>"> 
<input type="hidden" name="nm_name_doc" value="<?php echo NM_encode_input($this->Ini->path_imag_temp . "/" . $this->arquivo) ?>"> 
</form>
<FORM name="F0" method=post action="grid_SchoberExport.php"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
