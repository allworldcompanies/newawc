<?php
//--- 
class grid_SchoberExport_det
{
   var $Ini;
   var $Erro;
   var $Db;
   var $nm_data;
   var $NM_raiz_img; 
   var $id;
   var $firmenname1;
   var $firmenname2;
   var $strasse;
   var $plz;
   var $ort;
   var $postfach;
   var $plz_postfach;
   var $titel;
   var $vorname;
   var $nachname;
   var $funktion;
   var $abteilungszusatz;
   var $vorwahl;
   var $tel;
   var $sc_field_1;
   var $homepage;
   var $bundesland;
   var $regierungsbezirk;
   var $kreis;
   var $beschaeftigtenklasse;
   var $branchencode1;
   var $branchencode2;
   var $branchencode3;
   var $sc_field_0;
   var $firmenname3;
   var $anrede;
   var $anredezeile;
   var $fax;
   var $branchencode4;
   var $branchencode5;
   var $branchencode6;
   var $sc_field_2;
   var $exportzaehler;
 function monta_det()
 {
    global 
           $nm_saida, $nm_lang, $nmgp_cor_print, $nmgp_tipo_pdf;
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']))
    { 
        $this->id = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['id']; 
        $tmp_pos = strpos($this->id, "##@@");
        if ($tmp_pos !== false)
        {
            $this->id = substr($this->id, 0, $tmp_pos);
        }
        $this->sc_field_0 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_0']; 
        $tmp_pos = strpos($this->sc_field_0, "##@@");
        if ($tmp_pos !== false)
        {
            $this->sc_field_0 = substr($this->sc_field_0, 0, $tmp_pos);
        }
        $this->firmenname1 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['firmenname1']; 
        $tmp_pos = strpos($this->firmenname1, "##@@");
        if ($tmp_pos !== false)
        {
            $this->firmenname1 = substr($this->firmenname1, 0, $tmp_pos);
        }
        $this->firmenname2 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['firmenname2']; 
        $tmp_pos = strpos($this->firmenname2, "##@@");
        if ($tmp_pos !== false)
        {
            $this->firmenname2 = substr($this->firmenname2, 0, $tmp_pos);
        }
        $this->firmenname3 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['firmenname3']; 
        $tmp_pos = strpos($this->firmenname3, "##@@");
        if ($tmp_pos !== false)
        {
            $this->firmenname3 = substr($this->firmenname3, 0, $tmp_pos);
        }
        $this->strasse = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['strasse']; 
        $tmp_pos = strpos($this->strasse, "##@@");
        if ($tmp_pos !== false)
        {
            $this->strasse = substr($this->strasse, 0, $tmp_pos);
        }
        $this->plz = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['plz']; 
        $tmp_pos = strpos($this->plz, "##@@");
        if ($tmp_pos !== false)
        {
            $this->plz = substr($this->plz, 0, $tmp_pos);
        }
        $this->ort = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['ort']; 
        $tmp_pos = strpos($this->ort, "##@@");
        if ($tmp_pos !== false)
        {
            $this->ort = substr($this->ort, 0, $tmp_pos);
        }
        $this->postfach = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['postfach']; 
        $tmp_pos = strpos($this->postfach, "##@@");
        if ($tmp_pos !== false)
        {
            $this->postfach = substr($this->postfach, 0, $tmp_pos);
        }
        $this->vorname = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['vorname']; 
        $tmp_pos = strpos($this->vorname, "##@@");
        if ($tmp_pos !== false)
        {
            $this->vorname = substr($this->vorname, 0, $tmp_pos);
        }
        $this->nachname = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['nachname']; 
        $tmp_pos = strpos($this->nachname, "##@@");
        if ($tmp_pos !== false)
        {
            $this->nachname = substr($this->nachname, 0, $tmp_pos);
        }
        $this->sc_field_1 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_1']; 
        $tmp_pos = strpos($this->sc_field_1, "##@@");
        if ($tmp_pos !== false)
        {
            $this->sc_field_1 = substr($this->sc_field_1, 0, $tmp_pos);
        }
        $this->bundesland = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['bundesland']; 
        $tmp_pos = strpos($this->bundesland, "##@@");
        if ($tmp_pos !== false)
        {
            $this->bundesland = substr($this->bundesland, 0, $tmp_pos);
        }
        $this->regierungsbezirk = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['regierungsbezirk']; 
        $tmp_pos = strpos($this->regierungsbezirk, "##@@");
        if ($tmp_pos !== false)
        {
            $this->regierungsbezirk = substr($this->regierungsbezirk, 0, $tmp_pos);
        }
        $this->kreis = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['kreis']; 
        $tmp_pos = strpos($this->kreis, "##@@");
        if ($tmp_pos !== false)
        {
            $this->kreis = substr($this->kreis, 0, $tmp_pos);
        }
        $this->branchencode1 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['branchencode1']; 
        $tmp_pos = strpos($this->branchencode1, "##@@");
        if ($tmp_pos !== false)
        {
            $this->branchencode1 = substr($this->branchencode1, 0, $tmp_pos);
        }
        $this->branchencode2 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['branchencode2']; 
        $tmp_pos = strpos($this->branchencode2, "##@@");
        if ($tmp_pos !== false)
        {
            $this->branchencode2 = substr($this->branchencode2, 0, $tmp_pos);
        }
        $this->branchencode3 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['branchencode3']; 
        $tmp_pos = strpos($this->branchencode3, "##@@");
        if ($tmp_pos !== false)
        {
            $this->branchencode3 = substr($this->branchencode3, 0, $tmp_pos);
        }
        $this->sc_field_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_2']; 
        $tmp_pos = strpos($this->sc_field_2, "##@@");
        if ($tmp_pos !== false)
        {
            $this->sc_field_2 = substr($this->sc_field_2, 0, $tmp_pos);
        }
    } 
    $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_orig'];
    $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_pesq'];
    $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_pesq_filtro'];
    $this->nm_field_dinamico = array();
    $this->nm_order_dinamico = array();
    $this->nm_data = new nm_data("en_us");
    $this->NM_raiz_img  = ""; 
    $this->sc_proc_grid = false; 
    include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
   $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
   $Lim   = strlen($Str_date);
   $Ult   = "";
   $Arr_D = array();
   for ($I = 0; $I < $Lim; $I++)
   {
       $Char = substr($Str_date, $I, 1);
       if ($Char != $Ult)
       {
           $Arr_D[] = $Char;
       }
       $Ult = $Char;
   }
   $Prim = true;
   $Str  = "";
   foreach ($Arr_D as $Cada_d)
   {
       $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
       $Str .= $Cada_d;
       $Prim = false;
   }
   $Str = str_replace("a", "Y", $Str);
   $Str = str_replace("y", "Y", $Str);
   $nm_data_fixa = date($Str); 
   $this->nm_data->SetaData(date("Y/m/d H:i:s"), "YYYY/MM/DD HH:II:SS"); 
   $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
   { 
       $nmgp_select = "SELECT ID, `EBI-Nr`, Firmenname1, Firmenname2, Firmenname3, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Anrede, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Anredezeile, Vorwahl, Tel, Fax, `E-Mailadresse`, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3, Branchencode4, Branchencode5, Branchencode6, `Nutzungsrecht bis`, Exportzaehler from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
   { 
       $nmgp_select = "SELECT ID, `EBI-Nr`, Firmenname1, Firmenname2, Firmenname3, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Anrede, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Anredezeile, Vorwahl, Tel, Fax, `E-Mailadresse`, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3, Branchencode4, Branchencode5, Branchencode6, `Nutzungsrecht bis`, Exportzaehler from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle)) 
   { 
       $nmgp_select = "SELECT ID, `EBI-Nr`, Firmenname1, Firmenname2, Firmenname3, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Anrede, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Anredezeile, Vorwahl, Tel, Fax, `E-Mailadresse`, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3, Branchencode4, Branchencode5, Branchencode6, `Nutzungsrecht bis`, Exportzaehler from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix)) 
   { 
       $nmgp_select = "SELECT ID, `EBI-Nr`, Firmenname1, Firmenname2, Firmenname3, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Anrede, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Anredezeile, Vorwahl, Tel, Fax, `E-Mailadresse`, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3, Branchencode4, Branchencode5, Branchencode6, `Nutzungsrecht bis`, Exportzaehler from " . $this->Ini->nm_tabela; 
   } 
   else 
   { 
       $nmgp_select = "SELECT ID, `EBI-Nr`, Firmenname1, Firmenname2, Firmenname3, Strasse, PLZ, Ort, Postfach, PLZ_Postfach, Anrede, Titel, Vorname, Nachname, Funktion, Abteilungszusatz, Anredezeile, Vorwahl, Tel, Fax, `E-Mailadresse`, Homepage, Bundesland, Regierungsbezirk, Kreis, Beschaeftigtenklasse, Branchencode1, Branchencode2, Branchencode3, Branchencode4, Branchencode5, Branchencode6, `Nutzungsrecht bis`, Exportzaehler from " . $this->Ini->nm_tabela; 
   } 
   $parms_det = explode(";", $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['chave_det']) ; 
   foreach ($parms_det as $key => $cada_par)
   {
       $parms_det[$key] = stripslashes($parms_det[$key]);
       $parms_det[$key] = $this->Db->qstr($parms_det[$key]);
       $parms_det[$key] = substr($parms_det[$key], 1, strlen($parms_det[$key]) - 2);
   } 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nmgp_select .= " where  ID = $parms_det[0]" ;  
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nmgp_select .= " where  ID = $parms_det[0]" ;  
   } 
   else 
   { 
       $nmgp_select .= " where  ID = $parms_det[0]" ;  
   } 
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
   $rs = $this->Db->Execute($nmgp_select) ; 
   if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
   { 
       $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit ; 
   }  
   $this->id = $rs->fields[0] ;  
   $this->id = (string)$this->id;
   $this->sc_field_0 = $rs->fields[1] ;  
   $this->firmenname1 = $rs->fields[2] ;  
   $this->firmenname2 = $rs->fields[3] ;  
   $this->firmenname3 = $rs->fields[4] ;  
   $this->strasse = $rs->fields[5] ;  
   $this->plz = $rs->fields[6] ;  
   $this->ort = $rs->fields[7] ;  
   $this->postfach = $rs->fields[8] ;  
   $this->plz_postfach = $rs->fields[9] ;  
   $this->anrede = $rs->fields[10] ;  
   $this->titel = $rs->fields[11] ;  
   $this->vorname = $rs->fields[12] ;  
   $this->nachname = $rs->fields[13] ;  
   $this->funktion = $rs->fields[14] ;  
   $this->abteilungszusatz = $rs->fields[15] ;  
   $this->anredezeile = $rs->fields[16] ;  
   $this->vorwahl = $rs->fields[17] ;  
   $this->tel = $rs->fields[18] ;  
   $this->fax = $rs->fields[19] ;  
   $this->sc_field_1 = $rs->fields[20] ;  
   $this->homepage = $rs->fields[21] ;  
   $this->bundesland = $rs->fields[22] ;  
   $this->regierungsbezirk = $rs->fields[23] ;  
   $this->kreis = $rs->fields[24] ;  
   $this->beschaeftigtenklasse = $rs->fields[25] ;  
   $this->branchencode1 = $rs->fields[26] ;  
   $this->branchencode2 = $rs->fields[27] ;  
   $this->branchencode3 = $rs->fields[28] ;  
   $this->branchencode4 = $rs->fields[29] ;  
   $this->branchencode5 = $rs->fields[30] ;  
   $this->branchencode6 = $rs->fields[31] ;  
   $this->sc_field_2 = $rs->fields[32] ;  
   $this->exportzaehler = $rs->fields[33] ;  
//--- 
   $nm_saida->saida(" <table align=\"center\" valign=\"top\" >\r\n");
   $nm_saida->saida(" <tr>\r\n");
   $nm_saida->saida("  <td class=\"scGridTabelaTd\" valign=\"top\">\r\n");
   $nm_saida->saida("<TABLE style=\"padding: 0px; spacing: 0px; border-width: 0px;\" class=\"scGridBorder\" align=\"center\" valign=\"top\" width=\"100%\">\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['id'])) ? $this->New_label['id'] : "ID"; 
          $conteudo = trim($this->id); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
          else    
          { 
              nmgp_Form_Num_Val($conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"  NOWRAP ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['sc_field_0'])) ? $this->New_label['sc_field_0'] : "EBI Nr"; 
          $conteudo = trim($this->sc_field_0); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['firmenname1'])) ? $this->New_label['firmenname1'] : "Firmenname 1"; 
          $conteudo = trim($this->firmenname1); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['firmenname2'])) ? $this->New_label['firmenname2'] : "Firmenname 2"; 
          $conteudo = trim($this->firmenname2); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['firmenname3'])) ? $this->New_label['firmenname3'] : "Firmenname 3"; 
          $conteudo = trim($this->firmenname3); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['strasse'])) ? $this->New_label['strasse'] : "Strasse"; 
          $conteudo = trim($this->strasse); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['plz'])) ? $this->New_label['plz'] : "PLZ"; 
          $conteudo = trim($this->plz); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['ort'])) ? $this->New_label['ort'] : "Ort"; 
          $conteudo = trim($this->ort); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['postfach'])) ? $this->New_label['postfach'] : "Postfach"; 
          $conteudo = trim($this->postfach); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['plz_postfach'])) ? $this->New_label['plz_postfach'] : "PLZ Postfach"; 
          $conteudo = trim($this->plz_postfach); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['anrede'])) ? $this->New_label['anrede'] : "Anrede"; 
          $conteudo = trim($this->anrede); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['titel'])) ? $this->New_label['titel'] : "Titel"; 
          $conteudo = trim($this->titel); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['vorname'])) ? $this->New_label['vorname'] : "Vorname"; 
          $conteudo = trim($this->vorname); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['nachname'])) ? $this->New_label['nachname'] : "Nachname"; 
          $conteudo = trim($this->nachname); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['funktion'])) ? $this->New_label['funktion'] : "Funktion"; 
          $conteudo = trim($this->funktion); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['abteilungszusatz'])) ? $this->New_label['abteilungszusatz'] : "Abteilungszusatz"; 
          $conteudo = trim($this->abteilungszusatz); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['anredezeile'])) ? $this->New_label['anredezeile'] : "Anredezeile"; 
          $conteudo = trim($this->anredezeile); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['vorwahl'])) ? $this->New_label['vorwahl'] : "Vorwahl"; 
          $conteudo = trim($this->vorwahl); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['tel'])) ? $this->New_label['tel'] : "Tel"; 
          $conteudo = trim($this->tel); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fax'])) ? $this->New_label['fax'] : "Fax"; 
          $conteudo = trim($this->fax); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['sc_field_1'])) ? $this->New_label['sc_field_1'] : "E Mailadresse"; 
          $conteudo = trim($this->sc_field_1); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['homepage'])) ? $this->New_label['homepage'] : "Homepage"; 
          $conteudo = trim($this->homepage); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bundesland'])) ? $this->New_label['bundesland'] : "Bundesland"; 
          $conteudo = trim($this->bundesland); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['regierungsbezirk'])) ? $this->New_label['regierungsbezirk'] : "Regierungsbezirk"; 
          $conteudo = trim($this->regierungsbezirk); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['kreis'])) ? $this->New_label['kreis'] : "Kreis"; 
          $conteudo = trim($this->kreis); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['beschaeftigtenklasse'])) ? $this->New_label['beschaeftigtenklasse'] : "Beschaeftigtenklasse"; 
          $conteudo = trim($this->beschaeftigtenklasse); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['branchencode1'])) ? $this->New_label['branchencode1'] : "Branchencode 1"; 
          $conteudo = trim($this->branchencode1); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['branchencode2'])) ? $this->New_label['branchencode2'] : "Branchencode 2"; 
          $conteudo = trim($this->branchencode2); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['branchencode3'])) ? $this->New_label['branchencode3'] : "Branchencode 3"; 
          $conteudo = trim($this->branchencode3); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['branchencode4'])) ? $this->New_label['branchencode4'] : "Branchencode 4"; 
          $conteudo = trim($this->branchencode4); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['branchencode5'])) ? $this->New_label['branchencode5'] : "Branchencode 5"; 
          $conteudo = trim($this->branchencode5); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['branchencode6'])) ? $this->New_label['branchencode6'] : "Branchencode 6"; 
          $conteudo = trim($this->branchencode6); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['sc_field_2'])) ? $this->New_label['sc_field_2'] : "Nutzungsrecht Bis"; 
          $conteudo = trim($this->sc_field_2); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['exportzaehler'])) ? $this->New_label['exportzaehler'] : "Exportzaehler"; 
          $conteudo = trim($this->exportzaehler); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont\"  ALIGN=\"left\" VALIGN=\"middle\">" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert\"   ALIGN=\"center\" VALIGN=\"top\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("</TABLE>\r\n");
   $rs->Close(); 
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
 }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}
