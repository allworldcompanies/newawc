<?php

class grid_SchoberExport_total
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;

   var $nm_data;

   //----- 
   function grid_SchoberExport_total($sc_page)
   {
      $this->sc_page = $sc_page;
      $this->nm_data = new nm_data("en_us");
      if (isset($_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']) && !empty($_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']))
      { 
          $this->id = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['id']; 
          $tmp_pos = strpos($this->id, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id = substr($this->id, 0, $tmp_pos);
          }
          $this->sc_field_0 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_0']; 
          $tmp_pos = strpos($this->sc_field_0, "##@@");
          if ($tmp_pos !== false)
          {
              $this->sc_field_0 = substr($this->sc_field_0, 0, $tmp_pos);
          }
          $this->firmenname1 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['firmenname1']; 
          $tmp_pos = strpos($this->firmenname1, "##@@");
          if ($tmp_pos !== false)
          {
              $this->firmenname1 = substr($this->firmenname1, 0, $tmp_pos);
          }
          $this->firmenname2 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['firmenname2']; 
          $tmp_pos = strpos($this->firmenname2, "##@@");
          if ($tmp_pos !== false)
          {
              $this->firmenname2 = substr($this->firmenname2, 0, $tmp_pos);
          }
          $this->firmenname3 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['firmenname3']; 
          $tmp_pos = strpos($this->firmenname3, "##@@");
          if ($tmp_pos !== false)
          {
              $this->firmenname3 = substr($this->firmenname3, 0, $tmp_pos);
          }
          $this->strasse = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['strasse']; 
          $tmp_pos = strpos($this->strasse, "##@@");
          if ($tmp_pos !== false)
          {
              $this->strasse = substr($this->strasse, 0, $tmp_pos);
          }
          $this->plz = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['plz']; 
          $tmp_pos = strpos($this->plz, "##@@");
          if ($tmp_pos !== false)
          {
              $this->plz = substr($this->plz, 0, $tmp_pos);
          }
          $this->ort = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['ort']; 
          $tmp_pos = strpos($this->ort, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ort = substr($this->ort, 0, $tmp_pos);
          }
          $this->postfach = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['postfach']; 
          $tmp_pos = strpos($this->postfach, "##@@");
          if ($tmp_pos !== false)
          {
              $this->postfach = substr($this->postfach, 0, $tmp_pos);
          }
          $this->vorname = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['vorname']; 
          $tmp_pos = strpos($this->vorname, "##@@");
          if ($tmp_pos !== false)
          {
              $this->vorname = substr($this->vorname, 0, $tmp_pos);
          }
          $this->nachname = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['nachname']; 
          $tmp_pos = strpos($this->nachname, "##@@");
          if ($tmp_pos !== false)
          {
              $this->nachname = substr($this->nachname, 0, $tmp_pos);
          }
          $this->sc_field_1 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_1']; 
          $tmp_pos = strpos($this->sc_field_1, "##@@");
          if ($tmp_pos !== false)
          {
              $this->sc_field_1 = substr($this->sc_field_1, 0, $tmp_pos);
          }
          $this->bundesland = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['bundesland']; 
          $tmp_pos = strpos($this->bundesland, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bundesland = substr($this->bundesland, 0, $tmp_pos);
          }
          $this->regierungsbezirk = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['regierungsbezirk']; 
          $tmp_pos = strpos($this->regierungsbezirk, "##@@");
          if ($tmp_pos !== false)
          {
              $this->regierungsbezirk = substr($this->regierungsbezirk, 0, $tmp_pos);
          }
          $this->kreis = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['kreis']; 
          $tmp_pos = strpos($this->kreis, "##@@");
          if ($tmp_pos !== false)
          {
              $this->kreis = substr($this->kreis, 0, $tmp_pos);
          }
          $this->branchencode1 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['branchencode1']; 
          $tmp_pos = strpos($this->branchencode1, "##@@");
          if ($tmp_pos !== false)
          {
              $this->branchencode1 = substr($this->branchencode1, 0, $tmp_pos);
          }
          $this->branchencode2 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['branchencode2']; 
          $tmp_pos = strpos($this->branchencode2, "##@@");
          if ($tmp_pos !== false)
          {
              $this->branchencode2 = substr($this->branchencode2, 0, $tmp_pos);
          }
          $this->branchencode3 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['branchencode3']; 
          $tmp_pos = strpos($this->branchencode3, "##@@");
          if ($tmp_pos !== false)
          {
              $this->branchencode3 = substr($this->branchencode3, 0, $tmp_pos);
          }
          $this->sc_field_2 = $_SESSION['sc_session'][$this->sc_page]['grid_SchoberExport']['campos_busca']['sc_field_2']; 
          $tmp_pos = strpos($this->sc_field_2, "##@@");
          if ($tmp_pos !== false)
          {
              $this->sc_field_2 = substr($this->sc_field_2, 0, $tmp_pos);
          }
      } 
   }

   //---- 
   function quebra_geral()
   {
      global $nada, $nm_lang ;
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['contr_total_geral'] == "OK") 
      { 
          return; 
      } 
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['tot_geral'] = array() ;  
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_comando = "select count(*) from " . $this->Ini->nm_tabela . " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_pesq']; 
      } 
      else 
      { 
          $nm_comando = "select count(*) from " . $this->Ini->nm_tabela . " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['where_pesq']; 
      } 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
      if (!$rt = $this->Db->Execute($nm_comando)) 
      { 
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit ; 
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['tot_geral'][0] = "" . $this->Ini->Nm_lang['lang_msgs_totl'] . ""; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['tot_geral'][1] = $rt->fields[0] ; 
      $rt->Close(); 
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_SchoberExport']['contr_total_geral'] = "OK";
   } 

   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
