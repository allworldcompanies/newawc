<?php

// STEP 1: Read POST data

// reading posted data from directly from $_POST causes serialization
// issues with array data in POST
// reading raw POST data from input stream instead.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
  $keyval = explode ('=', $keyval);
  if (count($keyval) == 2)
     $myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
   $get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
        $value = urlencode(stripslashes($value));
   } else {
        $value = urlencode($value);
   }
   $req .= "&$key=$value";
}


// STEP 2: Post IPN data back to paypal to validate

$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// In wamp like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below.
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
if( !($res = curl_exec($ch)) ) {
    // error_log("Got " . curl_error($ch) . " when processing IPN data");
    curl_close($ch);
    exit;
}
curl_close($ch);


// STEP 3: Inspect IPN validation result and act accordingly

if (strcmp ($res, "VERIFIED") == 0) {
    // check whether the payment_status is Completed
    // check that txn_id has not been previously processed
    // check that receiver_email is your Primary PayPal email
    // check that payment_amount/payment_currency are correct
    // process payment

    // assign posted variables to local variables
    $item_name = $_POST['item_name'];
    $item_number = $_POST['item_number'];
    $payment_status = $_POST['payment_status'];
    $payment_amount = $_POST['mc_gross'];
    $payment_currency = $_POST['mc_currency'];
    $txn_id = $_POST['txn_id'];
    $receiver_email = $_POST['receiver_email'];
    $payer_email = $_POST['payer_email'];
    $custom = $_POST['custom'];
    $txn_type = $_POST['txn_type'];
    $number_of_items = $_POST['num_cart_items'];





  $connection = mysql_connect("allworldcompanie.db.9113824.hostedresource.com", "allworldcompanie", "aA1!aA1!");
  if (!$connection) {
    die("Database connection failed: " . mysql_error());
  }
  
  $db_select = mysql_select_db("allworldcompanie", $connection);
  if (!$db_select) {
    die("Database selection failed: " . mysql_error());
  }






  $fields = mysql_query("SELECT * FROM email_to_all WHERE users_email = '$custom' ORDER BY id DESC LIMIT 1");

  $field = mysql_fetch_array($fields);



$arid = explode(', ', $field['users_email_all']);

    foreach ($arid as $keyemail) {

      $first = mysql_query("SELECT ID,company_name FROM companies WHERE email = '$keyemail' LIMIT 1");
      $second = mysql_fetch_array($first); 
      $keyname = mysql_real_escape_string($second["company_name"]);
      $keyid = $second['ID'];

        mysql_query('INSERT INTO added_to_cart(users_email, company_name, company_id, status)
                    VALUES("' . $custom . '", "' . $keyname . '", "' . $keyid . '", 1) ') or die(mysql_error());

     }



$to      =  $field['users_email_all'];
$subject =  $field['users_subject'];
$message =  $field['users_message'];
$headers = 'From: ' . $custom . "\r\n" .
    'Reply-To: ' . $custom . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);

  $fields = mysql_query("UPDATE email_to_all SET users_status = 1 WHERE users_email = '$custom' ORDER BY id DESC LIMIT 1");

 }

 else if (strcmp ($res, "INVALID") == 0) {
    // log for manual investigation
}
?>