var xmlHttp;
function searchby(str)
{ 	xmlHttp=GetXmlHttpObject();
	if(xmlHttp==null)
	{	alert ("Browser does not support HTTP Request");
		return;
	}
	var url="search.php";
	if(str=="field")
	{
		url=url+"?searchby="+document.getElementById("search_field").value;
	}
	xmlHttp.onreadystatechange=searchByAjax;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}
function searchByAjax()
{	if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{	document.getElementById("searchby").innerHTML=xmlHttp.responseText;
	}
}
function displayby(str)
{	xmlHttp=GetXmlHttpObject();
	if(xmlHttp==null)
	{	alert ("Browser does not support HTTP Request");
		return;
	}
	var url="search_display.php";
	url=url+"?displayby="+str;
	xmlHttp.onreadystatechange=displayByAjax;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}
function displayByAjax()
{	if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{	document.getElementById("displayby").innerHTML=xmlHttp.responseText;
	}
}
function GetXmlHttpObject()
{	var xmlHttp=null;
	try{ xmlHttp=new XMLHttpRequest(); }
	catch (e){ try { xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); }
			   catch (e) { xmlHttp=new ActiveXObject("Microsoft.XMLHTTP"); } }
	return xmlHttp;
}