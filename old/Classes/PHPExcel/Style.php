<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2013 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel_Style
 * @copyright  Copyright (c) 2006 - 2013 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version    ##VERSION##, ##DATE##
 */


/**
 * PHPExcel_Style
 *
 * @category   PHPExcel
 * @package    PHPExcel_Style
 * @copyright  Copyright (c) 2006 - 2013 PHPExcel (http://www.codeplex.com/PHPExcel)
 */
class PHPExcel_Style extends PHPExcel_Style_Supervisor implements PHPExcel_IComparable
{
    /**
     * Font
     *
     * @var PHPExcel_Style_Font
     */
    protected $_font;

    /**
     * Fill
     *
     * @var PHPExcel_Style_Fill
     */
    protected $_fill;

    /**
     * Borders
     *
     * @var PHPExcel_Style_Borders
     */
    protected $_borders;

    /**
     * Alignment
     *
     * @var PHPExcel_Style_Alignment
     */
    protected $_alignment;

    /**
     * Number Format
     *
     * @var PHPExcel_Style_NumberFormat
     */
    protected $_numberFormat;

    /**
     * Conditional styles
     *
     * @var PHPExcel_Style_Conditional[]
     */
    protected $_conditionalStyles;

    /**
     * Protection
     *
     * @var PHPExcel_Style_Protection
     */
    protected $_protection;

    /**
     * Index of style in collection. Only used for real style.
     *
     * @var int
     */
    protected $_index;

    /**
     * Use Quote Prefix when displaying in cell editor. Only used for real style.
     *
     * @var boolean
     */
    protected $_quotePrefix = false;

    /**
     * Create a new PHPExcel_Style
     *
     * @param boolean $isSupervisor Flag indicating if this is a supervisor or not
     * 		Leave this value at default unless you understand exactly what
     *    its ramifications are
     * @param boolean $isConditional Flag indicating if this is a conditional style or not
     *   	Leave this value at default unless you understand exactly what
     *    its ramifications are
     */
    public function __construct($isSupervisor = false, $isConditional = false)
    {
        // Supervisor?
        $this->_isSupervisor = $isSupervisor;

        // Initialise values
        $this->_conditionalStyles	= array();
        $this->_font              = new PHPExcel_Style_Font($isSupervisor, $isConditional);
        $this->_fill              = new PHPExcel_Style_Fill($isSupervisor, $isConditional);
        $this->_borders           = new PHPExcel_Style_Borders($isSupervisor, $isConditional);
        $this->_alignment         = new PHPExcel_Style_Alignment($isSupervisor, $isConditional);
        $this->_numberFormat      = new PHPExcel_Style_NumberFormat($isSupervisor, $isConditional);
        $this->_protection        = new PHPExcel_Style_Protection($isSupervisor, $isConditional);

        // bind parent if we are a supervisor
        if ($isSupervisor) {
            $this->_font->bindParent($this);
            $this->_fill->bindParent($this);
            $this->_borders->bindParent($this);
            $this->_alignment->bindParent($this);
            $this->_numberFormat->bindParent($this);
            $this->_protection->bindParent($this);
        }
    }

    /**
     * Get the shared style component for the currently active cell in currently active sheet.
     * Only used fo