<?php

include_once "logincheck.php";
include_once "myconnect.php";
include_once "../date_functions.php";
ini_set ( error_reporting, "" );

$id=(isset($_REQUEST["id"]))?(int)$_REQUEST["id"]:0;

$mem=mysql_fetch_array(mysql_query("select * from freetplbanners_advertisers where id=$id"));
if(!$mem)
{
	header("Location: advertisers.php?msg=".urlencode("There is no such advertiser, unable to continue"));
	die();
}

function main()
{
	global $id, $mem;
         
	$config=mysql_fetch_array(mysql_query("select * from freetplbanners_config"));
	$freetpl_null_char=$config["null_char"];

?><style>
.yescolor {
	background-color: #F5F5F5;
}
.titlestyle {
	font-family:Arial, Helvetica, sans-serif ;
	font-weight: bold;
	font-size: 13px;
	color: #FFFFFF;
	background-color: #004080;
}
</style>
<table width="100%" border="0" cellpadding="0" cellspacing="0" dwcopytype="CopyTableCell">
  <tr> 
    <td valign="top">&nbsp;</td>
  </tr>
  <tr align="center"> 
    <td align="right" valign="top"> <table width="90%" border="0" cellspacing="0" cellpadding="0" class="maintablestyle" align="center">
        <tr> 
          <td width="100%" valign="top"> <table width="100%" border="0" align="center" cellpadding="2" cellspacing="5">
              <tr> 
                <td height="25" colspan="2"  class="titlestyle"><div align="left">Aggregate 
                    Banner Statistics For Member <font color="#FFCC00"><? echo $mem["uname"];?></font></div></td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Total 
                    Number of Banners Posted</font></strong></div></td>
                <td width="47%"><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <?php 
	$freetplrow_ads=mysql_fetch_array(mysql_query("select count(*) as freetpltotal from freetplbanners_ads where adv_id=$id"));
	$freetpltotal=($freetplrow_ads and is_numeric($freetplrow_ads['freetpltotal']))?$freetplrow_ads['freetpltotal']:0;
	echo $freetpltotal;
		  ?>
                  </font></td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Approved</font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <?
	$freetplrow_ads=mysql_fetch_array(mysql_query("select count(*) as freetplapp_cnt from freetplbanners_ads where adv_id=$id and approved='yes'"));
	$freetplapp_cnt=($freetplrow_ads and is_numeric($freetplrow_ads['freetplapp_cnt']))?$freetplrow_ads['freetplapp_cnt']:0;
	echo $freetplapp_cnt;
?>
                  </font></td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Disapproved</font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"><? echo $freetpltotal-$freetplapp_cnt;?></font></td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Total 
                    Impressions Received</font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <? 
	$freetpl_display_cnt=0;
	$freetpl_click_cnt=0;
	$total=mysql_fetch_array(mysql_query("select sum(displays) as freetpld, sum(clicks) as freetplc from freetplbanners_ads where adv_id=$id"));
	if($total)
	{
		$freetpl_display_cnt=(is_numeric($total['freetpld']))?$total['freetpld']:0;
		$freetpl_click_cnt=(is_numeric($total['freetplc']))?$total['freetplc']:0;
	}
	echo ($freetpl_display_cnt>0)?$freetpl_display_cnt:$freetpl_null_char;
		  ?>
                  </font></td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Total 
                    Clicks Received</font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <? 
	echo ($freetpl_click_cnt>0)?$freetpl_click_cnt:$freetpl_null_char;
		  ?>
                  </font></td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Average 
                    Click Through Rate</font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <?php 
		if($freetpl_display_cnt>0)
			echo round(($freetpl_click_cnt/$freetpl_display_cnt)*100,2)."%";
		else
			echo $config["null_char"];
		  ?>
                  </font></td>
              </tr>
              <tr align="left"> 
                <td height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Latest 
                    Impression </font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <?php 
	$freetplq_latest="select DATE_FORMAT(ondate,'%D %b,%Y at %r') as t from freetplbanners_displays where adv_id=$id order by ondate desc limit 1";
//	echo "$freetplq_latest<br>";
	$latest_dis=mysql_fetch_array(mysql_query($freetplq_latest));
	if($latest_dis)
	{
		echo $latest_dis["t"];
	}
	else
	{
		echo $config["null_char"];
	}
				?>
                  </font></td>
              </tr>
              <tr align="left"> 
                <td height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Latest 
                    Click </font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <?php 
	$freetplq_latest="select DATE_FORMAT(ondate,'%D %b,%Y at %r') as t from freetplbanners_clicks where adv_id=$id order by ondate desc limit 1";
//	echo "$freetplq_latest<br>";
	$latest_clk=mysql_fetch_array(mysql_query($freetplq_latest));
	if($latest_clk)
		echo $latest_clk["t"];
	else
		echo $config["null_char"];
				?>
                  </font></td>
              </tr>
                  <? 
	$freetplq_most="select * from freetplbanners_ads where adv_id=$id and paid<>'no' and displays > 0 order by displays desc limit 1";
//	echo "$freetplq_most<br>";
	$most_viewed=mysql_fetch_array(mysql_query($freetplq_most));
	if($most_viewed)
	{

				?>
              <tr align="center"> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr align="center"> 
                <td colspan="2"><?php 
		$size=mysql_fetch_array(mysql_query("select * from freetplbanner_sizes where id=".$most_viewed["size_id"]));
		if($most_viewed["banner_type"]!="flash")
		{
				?>
                  <img src="banners/<? echo $most_viewed["bannerurl"];?>" width=<? echo $size["width"];?> height=<? echo $size["height"];?>> 
                  <?php
		}
		else
		{	?><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="<? echo $size["width"];?>" height="<? echo $size["height"];?>">
                    <param name="movie" value="">
                    <param name="quality" value="high">
                    <embed src="<? echo "banners/". $most_viewed["bannerurl"]; ?>" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="<? echo $size["width"];?>" height="<? echo $size["height"];?>"></embed></object><?php
		}	?><strong><font size="2" face="Arial, Helvetica, sans-serif"><br>
                  Most Displayed Banner</font></strong></td>
              </tr> 
                  <?
	}	
	
	$freetplq_most="select * from freetplbanners_ads where adv_id=$id and paid<>'no' and clicks > 0 order by clicks desc limit 1";
//	echo "$freetplq_most<br>";
	$most_clicked=mysql_fetch_array(mysql_query($freetplq_most));
		 
	if($most_clicked)
	{
				?>
              <tr align="center"> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr align="center"> 
                <td colspan="2"><?php
		$size=mysql_fetch_array(mysql_query("select * from freetplbanner_sizes where id=".$most_clicked["size_id"]));
		if($most_clicked["banner_type"]!="flash")
		{ 
			?><img src="banners/<? echo $most_clicked["bannerurl"];?>" width=<? echo $size["width"];?> height=<? echo $size["height"];?>><?php
		}
		else
		{	?><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="<? echo $size["width"];?>" height="<? echo $size["height"];?>">
                    <param name="movie" value="">
                    <param name="quality" value="high">
                    <embed src="<? echo "banners/". $most_clicked["bannerurl"]; ?>" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="<? echo $size["width"];?>" height="<? echo $size["height"];?>"></embed></object><?php
		}	?> 
                  <strong><font size="2" face="Arial, Helvetica, sans-serif"><br>
                  Most Clicked Banner</font></strong></td>
              </tr> 
                  <?
	}		?>
                
            </table></td>
        </tr>
        <tr> 
          <td valign="top">&nbsp;</td>
        </tr>
        <tr> 
          <td valign="top"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="5">
              <tr> 
                <td height="25" colspan="2"  class="titlestyle"><div align="center"></div>
                  Stats: Clicks / Impressions</td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">Today</font></strong></div></td>
                <td width="47%"><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <?php 
	$freetplq_disp="select sum(freetpl_count) as freetpl_displays from freetplbanners_displays where adv_id=$id and TO_DAYS(NOW())-TO_DAYS(ondate)=0";
	$freetplrow_disp=mysql_fetch_array(mysql_query($freetplq_disp));
	
	$freetpl_total_disp=($freetplrow_disp and is_numeric($freetplrow_disp['freetpl_displays']) and $freetplrow_disp['freetpl_displays'] > 0)?$freetplrow_disp['freetpl_displays']:$freetpl_null_char;
	
	$freetplq_clk="select sum(freetpl_count) as freetpl_clicks from freetplbanners_clicks where adv_id=$id and TO_DAYS(NOW())-TO_DAYS(ondate)=0";
	$freetplrow_clk=mysql_fetch_array(mysql_query($freetplq_clk));
	
	$freetpl_total_clk=($freetplrow_clk and is_numeric($freetplrow_clk['freetpl_clicks']) and $freetplrow_clk['freetpl_clicks'] > 0)?$freetplrow_clk['freetpl_clicks']:$freetpl_null_char;
	
	echo "$freetpl_total_clk / $freetpl_total_disp";		
				?>
                  </font></td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Yesterday</font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"> <?php
	$freetplq_disp="select sum(freetpl_count) as freetpl_displays from freetplbanners_displays where adv_id=$id and TO_DAYS(NOW())-TO_DAYS(ondate)=1";
	$freetplrow_disp=mysql_fetch_array(mysql_query($freetplq_disp));
	
	$freetpl_total_disp=($freetplrow_disp and is_numeric($freetplrow_disp['freetpl_displays']) and $freetplrow_disp['freetpl_displays'] > 0)?$freetplrow_disp['freetpl_displays']:$freetpl_null_char;
	
	$freetplq_clk="select sum(freetpl_count) as freetpl_clicks from freetplbanners_clicks where adv_id=$id and TO_DAYS(NOW())-TO_DAYS(ondate)=1";
	$freetplrow_clk=mysql_fetch_array(mysql_query($freetplq_clk));
	
	$freetpl_total_clk=($freetplrow_clk and is_numeric($freetplrow_clk['freetpl_clicks']) and $freetplrow_clk['freetpl_clicks'] > 0)?$freetplrow_clk['freetpl_clicks']:$freetpl_null_char;
	
	echo "$freetpl_total_clk / $freetpl_total_disp";		
?>                  </font></td>
              </tr>
              <tr> 
                <td width="40%" height="25" class="yescolor"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;Last 
                    7 Days</font></strong></div></td>
                <td><font size="2" face="Arial, Helvetica, sans-serif"> 
                  <?php 
	$freetplq_disp="select sum(freetpl_count) as freetpl_displays from freetplbanners_displays where adv_id=$id and TO_DAYS(NOW())-TO_DAYS(ondate)<=7";
	$freetplrow_disp=mysql_fetch_array(mysql_query($freetplq_disp));
	
	$freetpl_total_disp=($freetplrow_disp and is_numeric($freetplrow_disp['freetpl_displays']) and $freetplrow_disp['freetpl_displays'] > 0)?$freetplrow_disp['freetpl_displays']:$freetpl_null_char;
	
	$freetplq_clk="select sum(freetpl_count) as freetpl_clicks from freetplbanners_clicks where adv_id=$id and TO_DAYS(NOW())-TO_DAYS(ondate)<=7";
	$freetplrow_clk=mysql_fetch_array(mysql_query($freetplq_clk));
	
	$freetpl_total_clk=($freetplrow_clk and is_numeric($freetplrow_clk['freetpl_clicks']) and $freetplrow_clk['freetpl_clicks'] > 0)?$freetplrow_clk['freetpl_clicks']:$freetpl_null_char;
	
	echo "$freetpl_total_clk / $freetpl_total_disp";		
	?>
                  </font></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td valign="top">&nbsp;</td>
        </tr>
        <tr> 
          <td valign="top"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="5">
              <tr> 
                <td height="25" colspan="2"  class="titlestyle"><div align="center"></div>
                  This Year: Clicks / Impressions </td>
              </tr>
              <?
	$freetpl_month_clk=0;
	$freetpl_month=0;
	$freetpl_pdate=date("Y",time());
	$freetplq_clk="select sum(freetpl_count) as freetpl_clicks, MONTH(ondate) as freetpl_month from freetplbanners_clicks where adv_id=$id and DATE_FORMAT(ondate,'%Y')=$freetpl_pdate group by freetpl_month order by freetpl_month";
//	echo $freetplq_disp;
	$freetplrs_clk=mysql_query($freetplq_clk);
	$freetplprev_found_clk=true;
	$freetplprev_used_clk=true;

	$freetplq_disp="select sum(freetpl_count) as freetpl_displays, MONTH(ondate) as freetpl_month from freetplbanners_displays where adv_id=$id and DATE_FORMAT(ondate,'%Y')=$freetpl_pdate group by freetpl_month order by freetpl_month";
//	echo $freetplq_disp;
	$freetplrs_disp=mysql_query($freetplq_disp);
	$freetplprev_found=true;
	$freetplprev_used=true;
	for($i=1;$i<=12;$i++)
	{
		if($freetplprev_used_clk)
		{
			if($freetplprev_found_clk and $freetplrow_clk=mysql_fetch_array($freetplrs_clk))
			{
				$freetpl_clicks=$freetplrow_clk['freetpl_clicks'];
				$freetpl_month_clk=$freetplrow_clk['freetpl_month'];
				$freetplprev_found_clk=true;
				$freetplprev_used_clk=false;
			}
			else
				$freetplprev_found_clk=false;
		}
//		echo "$freetpl_clicks, $freetpl_month_clk<br>";

		if($freetplprev_used)
		{
			if($freetplprev_found and $freetplrow_disp=mysql_fetch_array($freetplrs_disp))
			{
				$freetpl_displays=$freetplrow_disp['freetpl_displays'];
				$freetpl_month=$freetplrow_disp['freetpl_month'];
				$freetplprev_found=true;
				$freetplprev_used=false;
			}
			else
				$freetplprev_found=false;
		}
	//	echo "$freetpl_displays, $freetpl_month<br>";

                ?>
  <tr> 
	<td width="40%" align="right" class="yescolor"><font size="2" face="Arial, Helvetica, sans-serif" > 
                  <strong> 
                  <?
		switch($i)
		{
		case 1: echo "January";break;
		case 2: echo "Febuary";break;
		case 3: echo "March";break;
		case 4: echo "April";break;
		case 5: echo "May";break;
		case 6: echo "June";break;
		case 7: echo "July";break;
		case 8: echo "August";break;
		case 9: echo "September";break;
		case 10: echo "October";break;
		case 11: echo "November";break;
		case 12: echo "December";break;
		}
				?>
                  </strong> </font></td>
		<td width="47%"><font size="2" face="Arial, Helvetica, sans-serif">
		<?php 
		if($i==$freetpl_month_clk)
		{
			echo "$freetpl_clicks";
			$freetplprev_used_clk=true;
		}
		else
			echo $freetpl_null_char;
		?>
				 /
		<?php 
		if($i==$freetpl_month)
		{
			echo "$freetpl_displays";
			$freetplprev_used=true;
		}
		else
			echo $freetpl_null_char;
		?></font></td>
		  </tr>
				  <?
	}	//end for
            ?>
            </table></td>
        </tr>
        <tr> 
          <td valign="top">&nbsp;</td>
        </tr>
        <tr> 
          <td valign="top"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td colspan="4"> 
                  <table width="100%" border="0" cellspacing="5" cellpadding="2">
                    <tr>
                      <td height="25"  class="titlestyle"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>This 
                        Month: Clicks / Impressions </strong></font></td>
                    </tr>
                  </table> </td>
              </tr>
			<tr> 
              <?php
	$freetpl_day_clk=0;
	$freetpl_day=0;
	$freetpl_pdate=date("Ym",time());
	$freetplq_clk="select sum(freetpl_count) as freetpl_clicks, DAYOFMONTH(ondate) as freetpl_day from freetplbanners_clicks where adv_id=$id and DATE_FORMAT(ondate,'%Y%m')=$freetpl_pdate group by freetpl_day order by freetpl_day";
//	echo $freetplq_clk;
	$freetplrs_clk=mysql_query($freetplq_clk);
	$freetplprev_found_clk=true;
	$freetplprev_used_clk=true;

	$freetplq_disp="select sum(freetpl_count) as freetpl_displays, DAYOFMONTH(ondate) as freetpl_day from freetplbanners_displays where adv_id=$id and DATE_FORMAT(ondate,'%Y%m')=$freetpl_pdate  group by freetpl_day order by freetpl_day";
	$freetplrs_disp=mysql_query($freetplq_disp);
	$freetplprev_found=true;
	$freetplprev_used=true;
	
	$freetplday_cnt=freetpldays_in_month(time());
	for($i=1;$i<=$freetplday_cnt;$i++)
	{
		if($freetplprev_used_clk)
		{
			if($freetplprev_found_clk and $freetplrow_clk=mysql_fetch_array($freetplrs_clk))
			{
				$freetpl_clicks=$freetplrow_clk['freetpl_clicks'];
				$freetpl_day_clk=$freetplrow_clk['freetpl_day'];
				$freetplprev_found_clk=true;
				$freetplprev_used_clk=false;
			}
			else
				$freetplprev_found_clk=false;
		}
//		echo "$freetpl_clicks, $freetpl_day_clk<br>";

		if($freetplprev_used)
		{
			if($freetplprev_found and $freetplrow_disp=mysql_fetch_array($freetplrs_disp))
			{
				$freetpl_displays=$freetplrow_disp['freetpl_displays'];
				$freetpl_day=$freetplrow_disp['freetpl_day'];
				$freetplprev_found=true;
				$freetplprev_used=false;
			}
			else
				$freetplprev_found=false;
		}
//		echo "$freetpl_displays, $freetpl_day<br>";
		if(($i-1)%11==0)
		{
?>			
                <td valign="top" width="33%">
<table width="100%" border="0" cellspacing="5" cellpadding="2">
                    <?php 	}		//fi	?>
                    <tr>
                      <td width="45%" height="25" align="right" class="yescolor"><font size="2" face="Arial, Helvetica, sans-serif"><strong> 
                        <?php 
		echo $i; ?>
                        </strong></font></td>
                      <td><font size="2" face="Arial, Helvetica, sans-serif"> 
                        <?php 
		if($i==$freetpl_day_clk)
		{
			echo "$freetpl_clicks";
			$freetplprev_used_clk=true;
		}
		else
			echo $freetpl_null_char;
		
		echo " / ";
		
		if($i==$freetpl_day)
		{
			echo "$freetpl_displays";
			$freetplprev_used=true;
		}
		else
			echo $freetpl_null_char;
		?>
                        </font></td>
                    </tr a>
<?php	if(($i)%11==0)
		{		?>
                  </table> 
                   </td>
              <?
		}	//fi
	}		//end for
	
	echo '</table></td>';	//padding as internal table will never be ended
            ?>
              </tr>
            </table></td>
        </tr>
      </table>
     </td>
  </tr>
</table>
<?
          
}// end main
include_once "template.php";
?>