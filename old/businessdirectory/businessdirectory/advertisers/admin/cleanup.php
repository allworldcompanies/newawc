<?php

include_once("logincheck.php");
include_once("myconnect.php");

function main()
{
       #testcode#        			  
?>
<script language="JavaScript" type="text/javascript">
function validate_frm(frm)
{
	for (var i=0;i<frm.elements.length;i++)
	{
		var e = frm.elements[i];
		if (e.type == 'checkbox' && e.checked==true)
			return check_months(frm);			//if atleast one box has been checked 
	}
	alert("Please select atleast one option");
	return false;
}

function check_months(frm)
{
	if(frm.delete_stats.checked==true && (frm.freetpl_months.value=='' || frm.freetpl_months.value < 1 || isNaN(frm.freetpl_months.value)))
	{
		alert("Please specify valid value for months");
		frm.freetpl_months.select();
		frm.freetpl_months.select();
		return false;
	}
	return true;
}
</script>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="10">
      <?

$errcnt=0;
$showform="";
$msg="";

if ( count($_POST)>0 )
{	?>
  <tr> 
    <td align="center" valign="top"> 
<?php

	if (  isset( $_REQUEST["deleteimages"])  )
	{
		$cnt=0;
		$dir = opendir("banners");
		while($item = readdir($dir))
		{
			$used=0;
			$banner_query=mysql_query("select bannerurl from freetplbanners_ads");
			while ( $banner=mysql_fetch_array($banner_query))
			{
				if ($banner["bannerurl"]==$item)
					$used=1;
			}
		
			if ($used==0 && $item!="." && $item!=".."  )
			{
				@unlink("banners/".$item);
				$cnt++;
			}
		}
		closedir($dir);

		$dir = opendir("uploadedimages");
		while($item = readdir($dir))
		{
			$used=0;
			$def_banner_query=mysql_query("select default_banner from freetplbanner_sizes");
			while ( $def_banner=mysql_fetch_array($def_banner_query))
			{
				if ($def_banner["default_banner"]==$item)
					$used=1;
			}
			
			if ($used==0 && $item!="." && $item!=".."  )
			{
				@unlink("uploadedimages/".$item);
				$cnt++;
			}
		}
		closedir($dir);
		$msg .= $cnt . " Images Removed <br>";
	}
//////////////////////////////////////////////////////////////
//Delete All expired///////////////////////////////
//////////////////////////////////////////////////////////////

	if (  isset( $_REQUEST["deleteexp"])  )
	{
		$banner_query=mysql_query("select *,DATE_FORMAT(date_submitted,'%Y') as y from freetplbanners_ads where (credits-displays)<=0");
		$items_removed=0;
		while ( $banner=mysql_fetch_array($banner_query))
		{
			$num=mysql_num_rows(mysql_query("select * from freetplbanners_displays where banner_id=".$banner["id"]." order by id desc"));
			if($num>0)
			{
				$last_displayed=mysql_fetch_array(mysql_query("select * from freetplbanners_displays where banner_id=".$banner["id"]." and DATE_FORMAT(NOW(),'%Y')>DATE_FORMAT(ondate,'%Y') order by id desc"));
				if($last_displayed)
				{
					$items_removed++;
					unlink("banners/".$banner["bannerurl"]);
					mysql_query("delete from freetplbanners_clicks where banner_id=".$banner["id"]);
					mysql_query("delete from freetplbanners_displays where banner_id=".$banner["id"]);			
					mysql_query("delete from freetplbanners_ads where id=".$banner["id"]);
				}
			}
			else
			{
				if( $banner["y"] < date("Y",time()) )
				{
					$items_removed++;
					unlink("banners/".$banner["bannerurl"]);
					mysql_query("delete from freetplbanners_clicks where banner_id=".$banner["id"]);
					mysql_query("delete from freetplbanners_displays where banner_id=".$banner["id"]);			
					mysql_query("delete from freetplbanners_ads where id=".$banner["id"]);
				}
			}
		}//End of while
		$msg .= $items_removed . " Banners Removed<br>";
	}// End if...favorites to delete?

	if (  isset( $_REQUEST["delete_stats"])  )
	{
		$freetpl_months=(int)$_POST['freetpl_months'];
		$freetplcnt=0;
		if($freetpl_months >= 1)
		{	//only delete when months >= 1
			$freetplq_date="select DATE_FORMAT(DATE_SUB(NOW(),INTERVAL $freetpl_months MONTH), '%Y%m%d') as freetpl_cut_off";
			$freetplrow_date=mysql_fetch_array(mysql_query($freetplq_date)); 		//getting cutoff date
			$freetpl_cut_off_date=$freetplrow_date['freetpl_cut_off'];
//			echo $freetpl_cut_off_date;
//			echo "<br>select * from freetplbanners_displays where ondate < $freetpl_cut_off_date<br>";
			mysql_query("delete from freetplbanners_displays where ondate < $freetpl_cut_off_date");
			$freetplcnt+=mysql_affected_rows();
			mysql_query("delete from freetplbanners_clicks where ondate < $freetpl_cut_off_date");			
			$freetplcnt+=mysql_affected_rows();
		}
		$msg .= $freetplcnt . " Banner Stats Removed<br>";
	}
	
?>
      <font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Cleanup 
      has been completed<br>
      <?
		if (isset($msg) )
		{
		echo $msg;
		}
		?>
      </font> 
</td>
  </tr>      <?
}		//fi posted
?>
    
  <tr> 
    <td height="25" bgcolor="#004080"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>&nbsp;Cleanup 
      Routines </strong></font></td>
  </tr>
  <form name="form1" method="post" action="cleanup.php" onSubmit="javascript:return validate_frm(this);">
    <tr> 
      <td align="right" valign="top" bgcolor="#F5F5F5"><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"> 
          <input type="checkbox" name="deleteimages" value="Yes">
          Delete all unused uploaded banner image files. </font> </div></td>
    </tr>
    <tr> 
      <td align="right" valign="top" bgcolor="#F5F5F5"><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"> 
          <input name="deleteexp" type="checkbox" id="deleteexp" value="Yes">
          Delete all banners that expired 1 year ago. </font></div></td>
    </tr>
    <tr> 
      <td align="right" valign="top" bgcolor="#F5F5F5"><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"> 
          <input name="delete_stats" type="checkbox" id="delete_stats" value="Yes">
          Delete 
          <input name="freetpl_months" type="text" id="freetpl_months" size="5">
          months older banner stats. </font></div></td>
    </tr>
    <tr> 
      <td align="right" valign="top" bgcolor="#F5F5F5"><div align="left"><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Once 
          deleted, Information can't be recovered LATER.</font><font size="2" face="Arial, Helvetica, sans-serif"><br>
          <input type="submit" name="Submit2" value="Start the Cleanup">
          </font></div></td>
    </tr>
  </form>
</table>
<?
       #testcode#        			  
}  //End of main
include_once("template.php");
?>