<table width="174" height="100%" border="0" align="left" cellpadding="1" cellspacing="0">
  <tr> 
    <td width="172" valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td valign="top" bgcolor="#F5F5F5"><table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><strong>Admin 
                  Settings</strong></font></td>
              </tr><?PHP
			          
              ?><tr> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="adminhome.php" class="sidelink">Home</a></font></td>
              </tr>
              <tr> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="config.php">Configure 
                  Site</a></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="billing_account.php">Billing 
                    Account</a></font></div></td>
              </tr>
              <tr> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="changepassword.php">Change 
                  Password</a></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="siteemails.php">Config 
                    Emails</a></font></div></td>
              </tr>
              <tr bgcolor="">
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="cleanup.php">Cleanup 
                  Routines </a></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="logout.php">Logout</a></font></div></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr><?PHP
          
 
  ?><tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td valign="top" bgcolor="#F5F5F5"><table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><strong> 
                  Advertisers</strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="advertisers.php">Manage 
                    Advertisers</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="view_adv_transactions.php">Billing 
                  Transactions</a></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="add_adv_transaction.php">Make 
                  a Transaction </a></font></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td bgcolor="F5F5F5" ></td>
        </tr>
        <tr> 
          <td align="left" valign="top" bgcolor="F5F5F5"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="90%"><font size="2" face="Arial, Helvetica, sans-serif"><strong>Banner 
                  Options </strong></font></td>
              </tr>
              <tr bgcolor="">
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="getcode.php">Get 
                  Code </a></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="plans.php">Banner 
                    Puchase Packages</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="banner_sizes.php">Banner 
                  Sizes</a></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="ads.php">Banners</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="addad.php">Add 
                  a Banner Ad</a></font></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td bgcolor="F5F5F5" ></td>
        </tr>
        <tr> 
          <td align="left" valign="top" bgcolor="F5F5F5"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="90%"><font size="2" face="Arial, Helvetica, sans-serif"><strong>Mailing 
                  Options </strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="email.php">Email 
                    an Advertiser</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="emailall.php">Email 
                    All</a></font></div></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td bgcolor="F5F5F5" ></td>
        </tr>
        <tr> 
          <td align="left" valign="top" bgcolor="F5F5F5"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="90%"><font size="2" face="Arial, Helvetica, sans-serif"><strong>Public 
                  view </strong></font></td>
              </tr>
              <tr> 
                <td><div align="left"><font size="2" face="Arial, Helvetica, sans-serif"><a href="header_footer.php">HTML 
                    Header/ Footer</a></font></div></td>
              </tr>
              <tr> 
                <td><font size="2" face="Arial, Helvetica, sans-serif"><a href="site_styles.php"></a></font></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="100%" valign="top" >&nbsp;</td>
  </tr>
</table>
