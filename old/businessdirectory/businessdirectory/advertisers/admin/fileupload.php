<?php
include_once "session.php";
?>
<html>
<head>
<Title>Upload Image File</Title>
<link href="styles.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function checkFile(form1)
{
	if (form1.userfile.value == "")
	{
		alert("Please choose a file to upload");
		form1.userfile.focus();
		return (false);
	}
	if ( !form1.userfile.value.match(/(\.jpg|\.png|\.gif|\.bmp|\.jpeg)$/i) )
	{
		alert("Please upload .gif/.jpg/.jpeg/.bmp/.png files only");
		form1.userfile.focus();
		return (false);
	}
	return(true);
}


function submit_frm(frm)
{
	if(checkFile(frm))
	{
		frm.action="doupload1.php?box=<?php echo $_REQUEST["box"]; ?>";
		frm.submit();
	}
}

</script>


</head>

<body>
<FORM ENCTYPE="multipart/form-data" METHOD=post ID=form1 NAME=form1 onSubmit="javscript:return checkFile(this);"> 
  <table bgcolor="#F5F5F5">
    <tr>
      <td height="25" bgcolor="#004080"><b><font size="3" face="Arial, Helvetica, sans-serif">&nbsp;<font color="#FFFFFF">Upload 
        Image</font></font></b></TD>
    </TR>
    <tr> 
      <td> <b><font size="3" color="#FFFFFF"><u></u></font></b> <table width="100%">
          <tr> 
            <td valign="top" width="15"><font color="#000000">1.</font></td>
            <td width="470"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">To 
              add an image, click the 'Browse' button &amp; select the file, or 
              type the path to the file in the 'Text-box' below.</font></td>
          </tr>
          <tr> 
            <td valign="top" width="15"><font color="#000000">2.</font></td>
            <td width="470"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Then 
              click 'Upload' button to complete the process.</font></td>
          </tr>
          <tr> 
            <td valign="top" width="15"><font color="#000000">3.</font></td>
            <td width="470" valign="top"><font color="#990000" size="2" face="Arial, Helvetica, sans-serif">NOTE</font><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">: 
              The file transfer can take from a few seconds to a few minutes depending 
              on the size of the file. Please have patience while the file is 
              being uploaded.</font></td>
          </tr>
          <tr> 
            <td valign="top" width="15"><font color="#000000">4.</font></td>
            <td width="470"><font color="#990000" size="2" face="Arial, Helvetica, sans-serif">NOTE</font><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">: 
              The file will be renamed if the file with the same name is already 
              present.</font></td>
          </tr>
        </table></TD>
    </TR>
    <TR>
      <TD><font size="2" face="Arial, Helvetica, sans-serif"><STRONG>Hit the [Browse] button 
        to find the file on your computer.</STRONG></font><BR></TD>
    </TR>
    <TR>
      <TD><font size="2" face="Arial, Helvetica, sans-serif"><strong>Image</strong></font> 
        <INPUT NAME=userfile SIZE=30 TYPE=file   MaxFileSize="1000000"> 
        <input type="hidden" name="MAX_FILE_SIZE" value="1000000"> </TD>
    </TR>
    <TR>
      <TD>&nbsp;</TD>
    </TR>
    <TR>
      <TD><input type="button" value="Upload" name="uploadfile" onClick="javascript:submit_frm(this.form);"></TD>
    </TR>
    <TR> 
      <TD><font color="#990000" size="2" face="Arial, Helvetica, sans-serif">NOTE:</font><font size="2" face="Arial, Helvetica, sans-serif"> 
        Please have<font color="#006699"> </font> patience, you will not receive 
        any notification until the file is completely transferred.</font><BR></TD>
    </TR>
  </table>

</FORM>

   
</body>

</html>