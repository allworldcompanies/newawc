<?php
include_once "logincheck.php";
include_once "myconnect.php";


function RTESafe($strText) {
	//returns safe code for preloading in the RTE
	$tmpString = trim($strText);
	
	//convert all types of single quotes
	$tmpString = str_replace(chr(145), chr(39), $tmpString);
	$tmpString = str_replace(chr(146), chr(39), $tmpString);
	$tmpString = str_replace("'", "&#39;", $tmpString);
	
	//convert all types of double quotes
	$tmpString = str_replace(chr(147), chr(34), $tmpString);
	$tmpString = str_replace(chr(148), chr(34), $tmpString);
//	$tmpString = str_replace("\"", "\"", $tmpString);
	
	//replace carriage returns & line feeds
	$tmpString = str_replace(chr(10), " ", $tmpString);
	$tmpString = str_replace(chr(13), " ", $tmpString);
	
	return $tmpString;
}
function main()
{

$config=mysql_fetch_array(mysql_query("select * from freetplclassified_config"));

$freetplshow_extra_shipping=$config["freetplshow_extra_shipping"];

$cur=mysql_fetch_array(mysql_query("select * from freetplclassified_currency where id=".$config["cur_id"]));

$rs0=mysql_fetch_array(mysql_query("select * from freetplclassified_products where id=".$_REQUEST["id"]." and uid=".$_SESSION["freetpl_clsplus_userid"]));
if(!$rs0)
{
echo "<p>&nbsp;</p><p>&nbsp;</p><br><br><br><div align='center'><font size=2 color='#333333' face='Arial, Helvetica, sans-serif'>Classified Not Found. Click <a href='myclassified.php' class='insidelink'>here</a> to continue</font></div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";
return;
}

if($rs0["freetplextra_shipping"]>=0)
{
	$freetplenable_ship='yes';
	$freetplextra_shipping=$rs0["freetplextra_shipping"];
}
else
{
	$freetplenable_ship='no';
	$freetplextra_shipping="";
}

$rs1=mysql_fetch_array(mysql_query("select * from freetplclassified_categories where id=".$rs0["cid"]));
$state=$rs0["state"];
$country=$rs0["country"];
                        
?>
<script language="JavaScript" type="text/javascript" src="richtext.js"></script>
<script language="javascript">
   //<!--
      function category(box)
{

str="choosecategory.php?box="  + box;

window.open(str,"Category","top=5,left=30,toolbars=no,maximize=yes,resize=yes,width=550,height=450,location=no,directories=no,scrollbars=yes");


}
	  
	  function emailCheck (emailStr) {
var emailPat=/^(.+)@(.+)$/
var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
var validChars="\[^\\s" + specialChars + "\]"
var quotedUser="(\"[^\"]*\")"
var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
var atom=validChars + '+'
var word="(" + atom + "|" + quotedUser + ")"
var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
var matchArray=emailStr.match(emailPat)
if (matchArray==null) {
	alert("Paypal ID seems incorrect (check @ and .'s)")
	return false
}
var user=matchArray[1]
var domain=matchArray[2]
if (user.match(userPat)==null) {
    alert("The username doesn't seem to be valid.")
    return false
}
var IPArray=domain.match(ipDomainPat)
if (IPArray!=null) {
    // this is an IP address
	  for (var i=1;i<=4;i++) {
	    if (IPArray[i]>255) {
	        alert("Destination IP address is invalid!")
		return false
	    }
    }
    return true
}
var domainArray=domain.match(domainPat)
if (domainArray==null) {
	alert("The domain name doesn't seem to be valid.")
    return false
}
var atomPat=new RegExp(atom,"g")
var domArr=domain.match(atomPat)
var len=domArr.length
if (domArr[domArr.length-1].length<2 || 
			domArr[domArr.length-1].length>4) {
		   alert("The address must end in a valid domain, or two letter country.")
   return false
}
if (len<2) {
   var errStr="This address is missing a hostname!"
   alert(errStr)
   return false
}
return true;
}
function Validate(form) {

         updateRTEs();
		 if(form.product_name.value == "") {
            alert('Please Enter Classified Name.');
			form.product_name.focus();
            return false;
         }

		if(form.product_name.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Classified Name (e.g. &  < >)");
			form.product_name.focus();
			return(false);
		}

		 if(form.location.value == "") {
            alert('Please specify city.');
			form.location.focus();
            return false;
         } 

		if(form.location.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from city (e.g. &  < >)");
			form.location.focus();
			return(false);
		}
	if ( (form.state.selectedIndex == 0 ) && (form.other_state.value == "") ) {
       	   alert('Please specify State.');
		   form.state.focus();
	   return false;
	   }
		if(form.other_state.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from state (e.g. &  < >)");
			form.other_state.focus();
			return(false);
		}
        if(form.country.selectedIndex == 0)
		{
  	    alert('Please specify country.');
			form.country.focus();
        return false; 
        }
         		 if(form.rte1.value == "") {
            alert('Please enter Classified description.');
            return false;
         }

        ////////////////////additional fields 
		<?php
                        
		$freetplq_t_f="select * from freetplclassified_types_fields where freetpl_type_id=".$rs0["cid"];
		$freetplrs_t_f=mysql_query($freetplq_t_f);
		$freetpl_field_ids='-1';
		while($freetplrow_t_f=mysql_fetch_array($freetplrs_t_f))
			$freetpl_field_ids.=','.$freetplrow_t_f["freetpl_field_id"];

//			  	  $field_q=mysql_query("select * from freetplclassified_additional_fields order by freetpl_id"); 
  	  $field_q=mysql_query("select * from freetplclassified_additional_fields where freetpl_id in ($freetpl_field_ids) or freetpl_common='yes' order by freetpl_id"); 
				  while($field=mysql_fetch_array($field_q))
				  {
				  $js_str="";
				  
				  if($field["freetpl_req"]=="yes")
				  {
				  switch ($field["freetpl_type"])
				  {
				  	case "text":
					case "textarea":
						$field_name="form.".$field["freetpl_name"].".value";
				  		$msg="Please specify ".$field["freetpl_label"];
						$js_str="if($field_name=='')
								{ 
								alert('".$msg."'); 
								form.".$field["freetpl_name"].".focus();
								return false;
								 }";
						if($field["freetpl_is_num"]=="yes")
						{
				  		$msg="Please enter a numeric value for ".$field["freetpl_label"];
						$js_str.="if(isNaN($field_name))
								{ 
								alert('".$msg."'); 
								form.".$field["freetpl_name"].".focus();
								return false;
								 }";
						}		 
						break;
				  	
				  	case "radio":
						$cond_str="";
						$radio_arr=explode("|",$field["freetpl_int_value"]);
						$cnt=0;
						if(count($radio_arr)>1)
						{
							foreach($radio_arr as $value)
							{
								$field_name="form.".$field["freetpl_name"]."[$cnt].checked";
								$cond_str=($cond_str=="")?"$field_name==false":$cond_str."&&"."$field_name==false";
								$cnt++;
							}
							$msg="Please choose atleast one ".$field["freetpl_label"];
							$field_name="form.".$field["freetpl_name"]."[0]";
						}
						else
						{
								$field_name="form.".$field["freetpl_name"].".checked";
								$cond_str="$field_name==false";
								$msg="Please choose ".$field["freetpl_label"];
								$field_name="form.".$field["freetpl_name"];
						}
						$js_str="if($cond_str)
								{ 
								alert('".$msg."'); 
								".$field_name.".focus();
								return false;
								 }";
						break;
						
				  }	
				  }//end if
				  echo $js_str;
				  }// end while
                   ?>
		////////////////////////////////////////
		 
				 <?php 
	if($rs0["paypal_id"]!="")
	{		?>
				////////////////////////////////////////////////////////////
		if(form.buy_price.value!="")
		{
			if(isNaN(form.buy_price.value) || (form.buy_price.value<=0))
			{
				alert('Please specify positive value for Buy Now Price');
				form.buy_price.focus();
				return false;
			}
		}
				 
		if(form.paypal_id.value!="")
		{
			if (!emailCheck (form.paypal_id.value) )
			{
				form.paypal_id.focus();
				return (false);
			}
		}
		if(form.buy_price.value!="" && form.paypal_id.value=="")
		{
			alert('Please specify Paypal Id to post a buy now offer');
			form.paypal_id.focus();
			return false;
		}
			
		if(form.paypal_id.value!="" && form.buy_price.value=="")
		{
			alert('Please specify Buy Now Price for classified');
			form.buy_price.focus();
			return false;
		}

<?php  	if( ($freetplshow_extra_shipping=='yes') )
		{?>
			if(form.freetplenable_ship.checked==true)
			{
				if(form.buy_price.value=="")
				{ 
					form.buy_price.focus();
					alert('Please specify Buy Now Price for classified');
					return false;
				}
				if(form.paypal_id.value=="")
				{ 
					form.paypal_id.focus();
					alert('Please specify Paypal Id to post a buy now offer');
					return false;
				}
				if(form.freetplextra_shipping.value=="" || isNaN(form.freetplextra_shipping.value) || form.freetplextra_shipping.value<0)
				{ 
					form.freetplextra_shipping.focus();
					alert('Please specify positive value for Shipping Cost');
					return false;
				}
			}
<?php	}	//end if php ($freetplshow_extra_shipping=='yes')
?>					
<?php	
	}		//end if paypal id there	?>
    return true;
}
	  
function change_shipping_cost(frm)
{
	if(frm.freetplenable_ship.checked==true)
	{
		frm.freetplextra_shipping.disabled=false;
		frm.freetplextra_shipping.focus();
	}
	else
	{
		frm.freetplextra_shipping.disabled=true;
	}
}			  
	  
   //-->
</script>



 <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="onepxtable" >
  <tr class="titlestyle"> 
    <td  valign="top">&nbsp;Edit Listing </td>
  </tr>
  <tr> 
    <td  valign="top"> 
        <table width="100%" border="0" cellspacing="5" cellpadding="2">
        <form name="frm1" onSubmit="return Validate(this);" action="update_product.php" method="post">
          <tr> 
            <td width="50%" height="25" align="left" valign="top" class="innertablestyle"><font class="normal">&nbsp;<b>Listing 
              Name 
                    <input type="hidden" name=id value="<? echo $_REQUEST["id"];?>">
              </b></font></td>
            <TD align=left valign="top"><FONT class="red">*&nbsp;</FONT></TD>
            <td> <font class="normal"> 
              <input type="text" name="product_name" MAXLENGTH="120" SIZE="35" value="<? echo $rs0["product_name"];?>">
              </font></td>
          </tr>
          <?php	/*	?>
          <!--tr> 
            <td width="50%" height="25" align="left" valign="top" class="innertablestyle"><font class="normal">&nbsp;<b>Category</b></font></td>
            <TD align=left valign="top"><FONT class="red">*&nbsp;</FONT></TD>
            <TD valign="center"> 
              <?php										
			 if($config["cat_choosen"]=="drop")
			 {
                ?>
              <select name="cat1" id="cat1" >
                <option value="">Select a category</option>
                <?
				  $rs_query=mysql_query("select * from freetplclassified_categories order by pid");
				  
				  while($rst=mysql_fetch_array($rs_query))
				  {
				  
				  $cat_path="";
		$child=mysql_fetch_array(mysql_query("select * from freetplclassified_categories where pid=".$rst["id"]));
									  if($child)
									  {
									  continue;
									  }
									  $cat_path.=$rst["cat_name"];
		 $par=mysql_query("select * from freetplclassified_categories where id=".$rst["pid"]);
		 				while($parent=mysql_fetch_array($par))
		 				{
						$cat_path=$parent["cat_name"].">".$cat_path;
						$par=mysql_query("select * from freetplclassified_categories where id=".$parent["pid"]);
						}
                                      ?>
                <option value="<? echo $rst["id"];?>" <? if($rs1["id"]==$rst["id"])
				  { echo " selected";}
				  ?>><? echo $cat_path;?></option>
                <?
									  }
									  ?>
              </select> 
              <?php
				}// if drop list
				else
				{
				?>
              <input  name = "cat_name1" type = "text" id="cat_name1" value="<? echo $rs1["cat_name"];?>"  size="35" readOnly > 
              <input name = "cat1" type = "hidden" id="cat1" value="<? echo $rs1["id"];?>" readonly > 
              <input type=BUTTON name=btn_name22 value="Select A Category" onClick=category('1')> 
              <?php
				}
				?>
            </TD>
          </tr-->
          <?php	*/	?>
          <?php
                        
          ?>
          <tr> 
            <td width="50%" height="25" align="left" valign="top" class="innertablestyle"><font class="normal">&nbsp;<b>City</b></font></td>
            <TD align=left valign="top"><FONT class="red">*&nbsp;</FONT></TD>
            <td> <font class="normal"> 
              <input type="text" name="location" MAXLENGTH="255" SIZE="35" value="<? echo $rs0["location"]?>">
              <input name="aucid" type="hidden" id="aucid" value="4">
              </font></td>
          </tr>
          <tr> 
            <td height="25" align="left" valign="top" class="innertablestyle"><font class="normal">&nbsp;<b>State</b></font></td>
            <TD align=left valign="top"><FONT class="red">*&nbsp;</FONT></TD>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td><font class='normal'>US</font></td>
                  <td> <select name="state" >
                      <option value="" selected >Select State</option>
                      <? 
						  $state1=mysql_query("select * from freetplclassified_us_states order by freetpl_state");
						  while($rst= mysql_fetch_array($state1))
						  {
						  ?>
                      <option value='<? echo $rst["freetpl_state"];?>' <? if($state==$rst["freetpl_state"]) {echo " selected ";}?>><? echo $rst["freetpl_state"];?></option>
                      <?
							   } // wend
							   ?>
                    </select> </td>
                </tr>
                <tr> 
                  <td><font class='normal'>Non US</font></td>
                  <td><input name="other_state" type="text" id="other_state" value="<?php echo $state;?>"></td>
                </tr>
              </table></td>
          </tr>
          <tr> 
            <td height="25" align="left" valign="top" class="innertablestyle"><font class="normal">&nbsp;<b>Country</b></font></td>
            <TD align=left valign="top"><FONT class="red">*&nbsp;</FONT></TD>
            <td><SELECT name=country>
                <OPTION value=0
                          <? if($country==0) {echo " selected ";}?>>Select a Value 
                <? 
						  $country1=mysql_query("select * from freetplclassified_country order by country");
						  while($rst= mysql_fetch_array($country1))
						  {
						  ?>
                <OPTION value=<? echo $rst["id"];?> <? if($rst["id"]==$country) {echo " selected ";}?>><? echo $rst["country"];?></OPTION>
                <?
							   } // wend
							   ?>
              </SELECT></td>
          </tr>
          <?php
                        
          ?>
          <tr> 
            <td width="50%" height="25" align="left" valign="top" class="innertablestyle"><font class="normal">&nbsp;<b>Listing 
              Description</b></font></td>
            <TD align=left valign="top"><FONT class="red">*&nbsp;</FONT></TD>
            <TD valign="middle"><font class="normal"> 
              <script language="JavaScript" type="text/javascript">
<!--


<?
$content = $rs0["product_desc"];
$content = RTESafe($content);
?>//Usage: initRTE(imagesPath, includesPath, cssFile)
initRTE("images/", "", "");

//Usage: writeRichText(fieldname, html, width, height, buttons)
writeRichText('rte1', '<?=$content?>', 450, 200, true, false);

//uncomment the following to see a demo of multiple RTEs on one page
//document.writeln('<br><br>');
//writeRichText('rte2', 'read-only text', 450, 100, true, false);
//-->
</script>
              <noscript>
              <p><b>Javascript must be enabled to use this form.</b></p>
              </noscript>
              </font> </TD>
          </tr>
          <?php
		  		  $add_fields=explode(";",$rs0["additional_info"]);
				  	
//			  	  $field_q=mysql_query("select * from freetplclassified_additional_fields order by freetpl_id"); 
  	  $field_q=mysql_query("select * from freetplclassified_additional_fields where freetpl_id in ($freetpl_field_ids) or freetpl_common='yes' order by freetpl_id"); 
				  while($field=mysql_fetch_array($field_q))
				  {
				  $value="";
				   foreach($add_fields as $add_val)
					 {
					 	$val=explode("|",$add_val);
						if($val[0]==$field["freetpl_name"])
						{
						$value=$val[1];
						}
					 }

				  $field_str="";
				  switch ($field["freetpl_type"])
				  {
				  	case "text":
						$field_str="<input type='".$field["freetpl_type"]."' width='".$field["freetpl_width"]."' 
				  		value='".$value."' name='".$field["freetpl_name"]."'>";
						break;
				  	case "textarea":
						$field_str="<textarea cols='".$field["freetpl_width"]."' rows='".$field["freetpl_num_lines"].
						"' name='".$field["freetpl_name"]."'>".$value."</textarea>";
						break;
				  	case "checkbox":
							$val=explode("+",$field["freetpl_int_value"]);
							$init_val=$val[0];
							if(count($val)>1)
							{
							$init_val=$val[1];
							}
							
						if(($value==$init_val))
						$field_str="<input type='".$field["freetpl_type"]."' value='".$init_val."'
						name='".$field["freetpl_name"]."' checked>".$init_val;
						else
						$field_str="<input type='".$field["freetpl_type"]."' value='".$init_val."'
						name='".$field["freetpl_name"]."'>".$init_val;
						break;
				  	case "radio":
						$radio_arr=explode("|",$field["freetpl_int_value"]);
						foreach($radio_arr as $radio)
						{
							$val=explode("+",$radio);
							$init_val=$val[0];
							if(count($val)>1)
							{
							$init_val=$val[1];
							}
							
							if(($value==$init_val))
							$field_str.="<input type='".$field["freetpl_type"]."' value='".$init_val."'
							name='".$field["freetpl_name"]."' checked> ".$init_val;
							else
							$field_str.="<input type='".$field["freetpl_type"]."' value='".$init_val."'
							name='".$field["freetpl_name"]."' > ".$init_val;
							
						}
						break;
				  	case "select":
						$options_arr=explode("|",$field["freetpl_int_value"]);
						$field_str="<select name='".$field["freetpl_name"]."'>";
						foreach($options_arr as $select)
						{
							$val=explode("+",$select);
							$init_val=$val[0];
							if(count($val)>1)
							{
							$init_val=$val[1];
							}
							
							if(($value==$init_val))
							$field_str.="<option value='".$init_val."' selected>".$init_val."</option>";
							else
							$field_str.="<option value='".$init_val."' >".$init_val."</option>";
						}
						break;
						
				  }	
				  
                   ?>
          <tr> 
            <td height="25" align="left" valign="top" class="innertablestyle"> 
              <font class="normal">&nbsp;<b><?php echo $field["freetpl_label"];?></b></font></td>
            <td align="left" valign="top"><font class="red"> 
              <? if(($field["freetpl_req"]=="yes")&&($field["freetpl_type"]<>"checkbox")) echo "*";?>
              </font></td>
            <td><font class="normal"><?php echo $field_str;?></font></td>
          </tr>
          <?php
				  }// end if additional 
                               ?>
          <? 
					  if($rs0["paypal_id"]<>"")
					  {
					 
                      
          ?>
          <tr valign="top"> 
            <td height="25" class="innertablestyle"><font class="normal">&nbsp;<b>Buy 
              Now Price</b></font></td>
            <td align="left" valign="top">&nbsp;</td>
            <td><font class="normal"><?php echo $cur["cur_name"];?> 
              <input name="buy_price" type="text" id="buy_price" value="<?  echo $rs0["buy_price"];?>" SIZE="10" MAXLENGTH="120">
              </font></td>
          </tr>
          <tr valign="top"> 
            <td height="25" class="innertablestyle"><font class="normal">&nbsp;<b>Paypal 
              ID</b></font></td>
            <td align="left" valign="top">&nbsp;</td>
            <td><font class="normal"> 
              <input name="paypal_id" type="text" id="paypal_id"  value="<?  echo $rs0["paypal_id"];?>" SIZE="25" MAXLENGTH="120">
              </font></td>
          </tr>
                                  <?php 
		if($freetplshow_extra_shipping=='yes')
		{		?>
          <tr valign="top"> 
            <td height="25" class="innertablestyle"><font class="normal">&nbsp;<b>Extra 
              Shipping Cost</b></font></td>
            <td align="left" valign="top">&nbsp;</td>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="2"><input name="freetplenable_ship" type="checkbox" id="freetplenable_ship" value="yes" onClick="javascript:change_shipping_cost(this.form);" <?php echo(	$freetplenable_ship=='yes')?'checked':''; ?>> 
                    <font class="normal"><b>Enable Extra Shipping Cost</b></font></td>
                </tr>
                <tr> 
                  <td width="34%"><font class="normal"><b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shipping 
                    Cost</b></font></td>
                  <td width="66%"><font class="normal"> 
                    <input name="freetplextra_shipping" type="text" id="freetplextra_shipping"  value="<?php echo $freetplextra_shipping; ?>" size="10" maxlength="120" <?php echo($freetplenable_ship=='yes')?'':'disabled'; ?> >
                    </font></td>
                </tr>
                <tr> 
                  <td colspan="2"><font class="normal">Extra shipping cost facilitates 
                    you to define various shipping costs depending for example 
                    on the region/state. You can define those shipping costs in 
                    the Listing Description, the buyer will be provided with 
                    a text-box asking for applicable shipping from the description. 
                    The above given shipping cost will be treated as default shipping.</font></td>
                </tr>
              </table></td>
          </tr>
<?php
		}	//end 	if($freetplshow_extra_shipping=='yes')
?>
          <?
					  }
					  else
					  {
					  ?>
          <input type="hidden" name="buy_price" value=0>
          <input type="hidden" name="paypal_id" value="">
          <?
					  }
                      ?>
          <tr valign="top"> 
            <td height="25" class="innertablestyle"><font class="normal">&nbsp;<b>Hit 
              Counter </b></font></td>
            <td align="left">&nbsp;</td>
            <td> <table border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <TD align=left valign="middle"> <input type="radio" name="radio" value="0" <? if($rs0["counter_id"]==0) { echo " checked";} ?>></TD>
                  <td colspan="10"><font class="normal"><b>None</b></font></td>
                </tr>
                <? $sql=mysql_query("select * from freetplclassified_counters");
							$cnt=1;
			while($counter=mysql_fetch_array($sql))
			{?>
                <TR> 
                  <TD align=left valign="middle"><input type="radio" name="radio" value="<? echo $cnt;?>" <? if($rs0["counter_id"]==$cnt) { echo " checked";} ?>></TD>
                  <TD align=left valign="middle"><img src="admin/uploadedimages/<? echo $counter["zero"];?>"></TD>
                  <TD valign="middle"><img src="admin/uploadedimages/<? echo $counter["one"];?>"></TD>
                  <TD valign="middle"><img src="admin/uploadedimages/<? echo $counter["two"];?>"></TD>
                  <TD valign="middle"><img src="admin/uploadedimages/<? echo $counter["three"];?>"></TD>
                </TR>
                <? $cnt++;}?>
              </table></td>
          </tr>
          <tr> 
            <td height="25" class="innertablestyle"><font class="normal">&nbsp;</font></td>
            <td align="left" valign="top"><font class="normal">&nbsp;</font></td>
            <td><font class="normal"> 
              <INPUT type=submit value=Update name=submit>
              </font></td>
          </tr>
        </form>
      </table>
      </td>
  </tr>
</table><BR> 
<?
}// end main
include "template.php";
?>