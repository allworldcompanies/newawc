<?php 
include_once "logincheck.php";
include_once "myconnect.php";

if(isset($_REQUEST["pid"]))
	$pid=(int)$_REQUEST["pid"];
else
	$pid=0;

$product_q=mysql_query("select * from freetplclassified_products where id=$pid and status='open' and uid=".$_SESSION["freetpl_clsplus_userid"]);
$product=mysql_fetch_array($product_q);

if(!$product)
{
	include_once "styles.php";
	echo "<font class='normal'><font class='red'><b>Classified not found. Click <a href='#' onclick='javascript:window.close();'>here</a> to close this window</b></font></font>";
	die();
}

$sql=mysql_query("select count(id) as freetplnum from freetplclassified_images where pid=$pid");
$num_row=mysql_fetch_array($sql);
$num=($num_row and is_numeric($num_row["freetplnum"]))?$num_row["freetplnum"]:0;

$config=mysql_fetch_array(mysql_query("select * from freetplclassified_config"));

if (is_uploaded_file($_FILES['userfile']['tmp_name'])) 
{
	$realname = $_FILES['userfile']['name'];
	///////--------chking extension	
	if(!preg_match("/(\.jpg|\.png|\.gif|\.bmp|\.jpeg)$/i",$realname))
		die();
	///////--------end chking extension	
	if ($_FILES['userfile']['size']>($config["image_size"]))
		$mess="Uploaded files must be less than ".($config["image_size"]/1000)."k. Please try again";
	elseif($_FILES['userfile']['size']<=0)
		$mess="File could not be uploaded. Please try again";
	else
	{
		$insert="no";
		switch($_FILES['userfile']['error'])
		{ case 0: $mess = "Image has been uploaded successfully"; $insert="yes";	  break;
		  case 1:
		  case 2: $mess = "Error : File size more than maximum size allowed by server";break;
		  case 3: $mess = "Error : File partially uploaded"; break;
		  case 4: $mess = "Error : No File Uploaded";
		  break;
		}
                        

		mt_srand((double)microtime()*1000000);
		$randvar =  mt_rand(1,10000000);
		settype($randvar,"string");
		
		$newfilename = "uploadedimages/" . $randvar. str_replace(" ","_",$realname);
		$shortfname = $randvar. str_replace(" ","_",$realname);
		while ( file_exists($newfilename) != FALSE )
		{
			$randvar =  mt_rand(1,10000000);
			settype($randvar,"string");
			$newfilename = "uploadedimages/" . $randvar. str_replace(" ","_",$realname);
			$shortfname =  $randvar. str_replace(" ","_",$realname);
		}

		copy($_FILES['userfile']['tmp_name'], $newfilename);

		if($insert=="yes")
		{
			$image_rate=(-1)*$config["image_rate"];
			$balance=mysql_fetch_array(mysql_query("select sum(amount) from freetplclassified_transactions where uid=".$_SESSION["freetpl_clsplus_userid"]));
			$insert_image="yes";
			$url=$shortfname;

			$product=mysql_fetch_array(mysql_query("select * from freetplclassified_products where id=$pid"));
			$product_name=$product["product_name"];
			if($num>=($config["free_images"]+$product["purchased_images"]))
			{
				if(($balance[0]>$config["image_rate"])||($config["image_rate"]<=0))
				{
					if($config["image_rate"]>0)
					{
						mysql_query("insert into freetplclassified_transactions (uid,amount,description,date_submitted) values (".$_SESSION["freetpl_clsplus_userid"].",$image_rate,'Purchased ".$config["buy_images"]." images for Classified ''$product_name''.','".date("YmdHis",time())."')");
					}
					mysql_query("update freetplclassified_products set purchased_images=purchased_images+".$config["buy_images"]." where id=$pid");
				}
				else
				{
					$insert_image="no";
					$mess="You do not have sufficient balance, please add some money first";
				}
			}
			if($insert_image=="yes")
			{
				mysql_query("Insert into `freetplclassified_images` (pid,url) VALUES ($pid,'$url')");
				if($config["image_magik"]=="enable")
				{
					$size_str=$config["th_width"] . "x" . $config["th_width"];
					$size_str2=$config["th_width2"] . "x" . $config["th_width2"];
					$path1="uploadedimages" . '/' . $url;
					$path2="thumbs1" . '/' . $url;
					$path3="thumbs2" . '/' . $url;
					if($config["water_marking"]=="enable")
						exec("composite -dissolve 20 images/watermark.gif $path1 $path1");
					exec("convert $path1 -resize $size_str $path2");
					exec("convert $path1 -resize $size_str2 $path3");
				}
			}
		}
	}// Else fr more than 60k
} 
else 
{	
	$mess="Some error occurred, please try again";
}

header("Location: view_images.php?id=$pid&msg=".urlencode($mess));
die();

?>