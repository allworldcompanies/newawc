﻿-- phpMyAdmin SQL Dump
-- version 2.11.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 18, 2008 at 01:54 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `Database Name`
--

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_admin`
--

CREATE TABLE `freetplbanners_admin` (
  `id` bigint(20) NOT NULL auto_increment,
  `admin_name` varchar(255) collate latin1_general_ci default NULL,
  `pwd` varchar(255) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `freetplbanners_admin`
--

INSERT INTO `freetplbanners_admin` VALUES(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_ads`
--

CREATE TABLE `freetplbanners_ads` (
  `id` bigint(20) NOT NULL auto_increment,
  `url` varchar(255) collate latin1_general_ci NOT NULL default '',
  `bannerurl` varchar(255) collate latin1_general_ci NOT NULL default '',
  `adv_id` bigint(20) NOT NULL default '0',
  `credits` bigint(20) NOT NULL default '0',
  `displays` bigint(20) NOT NULL default '0',
  `paid` varchar(255) collate latin1_general_ci NOT NULL default '',
  `approved` varchar(255) collate latin1_general_ci NOT NULL default '',
  `clicks` bigint(20) default '0',
  `date_submitted` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `size_id` bigint(20) default '1',
  `banner_type` varchar(255) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=38 ;

--
-- Dumping data for table `freetplbanners_ads`
--

INSERT INTO `freetplbanners_ads` VALUES(36, 'http://www.pozscripts.com', '937903.gif', 0, 999999999999999, 1, 'yes', 'yes', 0, '2008-01-18 01:48:51', 2, 'image');
INSERT INTO `freetplbanners_ads` VALUES(37, 'http://www.iwebsglobal.com', '3928419.gif', 0, 999999999999, 1, 'yes', 'yes', 0, '2008-01-18 01:50:56', 2, 'image');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_advertisers`
--

CREATE TABLE `freetplbanners_advertisers` (
  `id` bigint(20) NOT NULL auto_increment,
  `uname` varchar(255) collate latin1_general_ci default NULL,
  `email` varchar(255) collate latin1_general_ci default NULL,
  `pwd` varchar(255) collate latin1_general_ci default NULL,
  `add1` varchar(255) collate latin1_general_ci default NULL,
  `city` varchar(255) collate latin1_general_ci default NULL,
  `state` varchar(255) collate latin1_general_ci default NULL,
  `country` bigint(20) default NULL,
  `url` varchar(255) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `freetplbanners_advertisers`
--

INSERT INTO `freetplbanners_advertisers` VALUES(2, 'vine', 'demo@demo.com', '0001', 'ldh', 'ldh', 'pb', 89, 'http://freetpl.com');
INSERT INTO `freetplbanners_advertisers` VALUES(4, 'savvy', 'savvy@ww.com', 'a', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `freetplbanners_advertisers` VALUES(6, 'vikas', 'vikk@vik.com', 'aaaa', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `freetplbanners_advertisers` VALUES(7, 'vin', 'vin@qq1.com', 'vini', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `freetplbanners_advertisers` VALUES(9, 'Evita', 'evita@evita.com', 'evita', 'address', 'city', 'state', 222, 'http://www.evita.com');
INSERT INTO `freetplbanners_advertisers` VALUES(11, 'vita', 'vita@vita.com', 'vita', 'south extension', 'delhi', 'delhi', 89, 'http://www.dd.com');
INSERT INTO `freetplbanners_advertisers` VALUES(12, 'test@test.com', 'test@test.com', '123456', 'test@test.com', 'test@test.com', 'test@test.com', 210, 'http://www.freetpl.com');
INSERT INTO `freetplbanners_advertisers` VALUES(13, 'CCS', 'support@pozscripts.com', '888888', 'test', 'Singapore', 'singapore', 173, 'http://www.emall.sg');
INSERT INTO `freetplbanners_advertisers` VALUES(14, 'Alan', 'alanbaby@singnet.com.sg', '888888', 'rrrrrrrrrrrrrrrrr', 'Bangkok', 'Singapore', 26, 'http://www.88db.com');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_adv_transactions`
--

CREATE TABLE `freetplbanners_adv_transactions` (
  `id` bigint(20) NOT NULL auto_increment,
  `description` varchar(255) collate latin1_general_ci default NULL,
  `amount` decimal(10,2) default NULL,
  `adv_id` bigint(20) default NULL,
  `date_submitted` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `freetplbanners_adv_transactions`
--

INSERT INTO `freetplbanners_adv_transactions` VALUES(8, 'Banner Posting Charges', '-9.00', 2, '2004-03-27 16:17:46');
INSERT INTO `freetplbanners_adv_transactions` VALUES(9, 'Money added through paypal', '10.00', 2, '2004-03-27 04:45:04');
INSERT INTO `freetplbanners_adv_transactions` VALUES(10, 'Purchased More Impressions', '-9.00', 2, '2004-03-27 05:00:22');
INSERT INTO `freetplbanners_adv_transactions` VALUES(16, 'Added by admin', '10.00', 2, '2004-03-29 05:11:38');
INSERT INTO `freetplbanners_adv_transactions` VALUES(17, 'Banner Posting Charges', '-9.00', 2, '2004-03-29 05:11:56');
INSERT INTO `freetplbanners_adv_transactions` VALUES(7, 'Added Money', '10.00', 2, '2004-03-27 15:57:03');
INSERT INTO `freetplbanners_adv_transactions` VALUES(18, 'Added by admin', '10.00', 2, '2004-04-10 09:42:43');
INSERT INTO `freetplbanners_adv_transactions` VALUES(19, 'Added by admin', '1.00', 2, '2004-04-10 05:59:04');
INSERT INTO `freetplbanners_adv_transactions` VALUES(20, 'Banner Posting Charges', '-7.00', 2, '2004-04-13 11:22:55');
INSERT INTO `freetplbanners_adv_transactions` VALUES(21, 'Added by admin', '10.00', 2, '2004-04-13 11:25:14');
INSERT INTO `freetplbanners_adv_transactions` VALUES(25, 'Banner Posting Charges', '-7.00', 2, '2004-04-13 12:43:34');
INSERT INTO `freetplbanners_adv_transactions` VALUES(26, 'Banner Posting Charges', '-7.00', 2, '2004-04-15 09:26:02');
INSERT INTO `freetplbanners_adv_transactions` VALUES(27, 'Added by admin', '10.00', 11, '2004-04-16 09:43:05');
INSERT INTO `freetplbanners_adv_transactions` VALUES(28, 'Added by admin', '10.00', 10, '2004-04-16 09:43:28');
INSERT INTO `freetplbanners_adv_transactions` VALUES(29, 'Added by admin', '10.00', 2, '2004-04-16 10:29:16');
INSERT INTO `freetplbanners_adv_transactions` VALUES(30, 'test', '100000.00', 12, '2007-08-23 02:11:35');
INSERT INTO `freetplbanners_adv_transactions` VALUES(31, 'Banner Posting Charges', '0.00', 13, '2007-09-04 12:47:01');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_clicks`
--

CREATE TABLE `freetplbanners_clicks` (
  `id` bigint(20) NOT NULL auto_increment,
  `adv_id` bigint(20) default NULL,
  `banner_id` bigint(20) default NULL,
  `ondate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `freetpl_count` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `freetplbanners_clicks`
--

INSERT INTO `freetplbanners_clicks` VALUES(1, 2, 13, '2004-04-14 11:14:15', 0);
INSERT INTO `freetplbanners_clicks` VALUES(2, 2, 18, '2004-04-18 18:31:08', 0);
INSERT INTO `freetplbanners_clicks` VALUES(3, 2, 18, '2004-04-18 18:31:27', 0);
INSERT INTO `freetplbanners_clicks` VALUES(4, 2, 18, '2007-09-04 00:00:00', 1);
INSERT INTO `freetplbanners_clicks` VALUES(5, 0, 32, '2007-12-16 00:00:00', 1);
INSERT INTO `freetplbanners_clicks` VALUES(6, 0, 33, '2007-12-16 00:00:00', 1);
INSERT INTO `freetplbanners_clicks` VALUES(7, 0, 34, '2007-12-17 00:00:00', 1);
INSERT INTO `freetplbanners_clicks` VALUES(8, 0, 33, '2007-12-17 00:00:00', 1);
INSERT INTO `freetplbanners_clicks` VALUES(9, 0, 32, '2007-12-19 00:00:00', 1);
INSERT INTO `freetplbanners_clicks` VALUES(10, 0, 32, '2008-01-04 00:00:00', 1);
INSERT INTO `freetplbanners_clicks` VALUES(11, 0, 34, '2008-01-06 00:00:00', 1);
INSERT INTO `freetplbanners_clicks` VALUES(12, 0, 34, '2008-01-09 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_config`
--

CREATE TABLE `freetplbanners_config` (
  `id` bigint(20) NOT NULL auto_increment,
  `admin_email` varchar(255) collate latin1_general_ci default NULL,
  `site_name` varchar(255) collate latin1_general_ci default NULL,
  `null_char` varchar(255) collate latin1_general_ci default NULL,
  `site_root` varchar(255) collate latin1_general_ci default NULL,
  `approval_type` varchar(10) collate latin1_general_ci default NULL,
  `html_header` longtext collate latin1_general_ci,
  `html_footer` longtext collate latin1_general_ci,
  `style_list` bigint(20) default NULL,
  `pwd_len` bigint(20) default NULL,
  `recperpage` int(11) default '10',
  `pay_pal` varchar(255) collate latin1_general_ci default NULL,
  `freetpl_cur_symbol` varchar(255) collate latin1_general_ci NOT NULL default '$',
  `freetpl_cur_code` varchar(255) collate latin1_general_ci NOT NULL default 'USD',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `freetplbanners_config`
--

INSERT INTO `freetplbanners_config` VALUES(1, 'demo@biglobangs.com', 'Banner Ads Management', '-', 'http://www.biglobangs.com/linkdirectory/advertisers/', 'admin', '<table width="100%" height="125" cellpadding="0" cellspacing="0">\r\n  <tr>\r\n    <td background="images/whmcslogobg.png">    <div align="left"><a href="index.html"><img src="images/whmcslogo.png" width="395" height="120" border="0"></a></div>\r\n    </tr>\r\n</table>\r\n', '<div align="center">\r\n  <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><font size="1">Banner Ads Management \r\n  Copyright 2007 - 2008</font></font> </div>', 2, 4, 10, 'demo@biglobangs.com', '$', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_country`
--

CREATE TABLE `freetplbanners_country` (
  `country` varchar(255) collate latin1_general_ci NOT NULL default '',
  `id` bigint(20) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=223 ;

--
-- Dumping data for table `freetplbanners_country`
--

INSERT INTO `freetplbanners_country` VALUES('Afghanistan', 1);
INSERT INTO `freetplbanners_country` VALUES('Albania', 2);
INSERT INTO `freetplbanners_country` VALUES('Algeria', 3);
INSERT INTO `freetplbanners_country` VALUES('Andorra', 4);
INSERT INTO `freetplbanners_country` VALUES('Angola', 5);
INSERT INTO `freetplbanners_country` VALUES('Anguilla', 6);
INSERT INTO `freetplbanners_country` VALUES('Antigua & Barbuda', 7);
INSERT INTO `freetplbanners_country` VALUES('Argentina', 8);
INSERT INTO `freetplbanners_country` VALUES('Armenia', 9);
INSERT INTO `freetplbanners_country` VALUES('Austria', 10);
INSERT INTO `freetplbanners_country` VALUES('Azerbaijan', 11);
INSERT INTO `freetplbanners_country` VALUES('Bahamas', 12);
INSERT INTO `freetplbanners_country` VALUES('Bahrain', 13);
INSERT INTO `freetplbanners_country` VALUES('Bangladesh', 14);
INSERT INTO `freetplbanners_country` VALUES('Barbados', 15);
INSERT INTO `freetplbanners_country` VALUES('Belarus', 16);
INSERT INTO `freetplbanners_country` VALUES('Belgium', 17);
INSERT INTO `freetplbanners_country` VALUES('Belize', 18);
INSERT INTO `freetplbanners_country` VALUES('Belize', 19);
INSERT INTO `freetplbanners_country` VALUES('Bermuda', 20);
INSERT INTO `freetplbanners_country` VALUES('Bhutan', 21);
INSERT INTO `freetplbanners_country` VALUES('Bolivia', 22);
INSERT INTO `freetplbanners_country` VALUES('Bosnia and Herzegovina', 23);
INSERT INTO `freetplbanners_country` VALUES('Botswana', 24);
INSERT INTO `freetplbanners_country` VALUES('Brazil', 25);
INSERT INTO `freetplbanners_country` VALUES('Brunei', 26);
INSERT INTO `freetplbanners_country` VALUES('Bulgaria', 27);
INSERT INTO `freetplbanners_country` VALUES('Burkina Faso', 28);
INSERT INTO `freetplbanners_country` VALUES('Burundi', 29);
INSERT INTO `freetplbanners_country` VALUES('Cambodia', 30);
INSERT INTO `freetplbanners_country` VALUES('Cameroon', 31);
INSERT INTO `freetplbanners_country` VALUES('Canada', 32);
INSERT INTO `freetplbanners_country` VALUES('Cape Verde', 33);
INSERT INTO `freetplbanners_country` VALUES('Cayman Islands', 34);
INSERT INTO `freetplbanners_country` VALUES('Central African Republic', 35);
INSERT INTO `freetplbanners_country` VALUES('Chad', 36);
INSERT INTO `freetplbanners_country` VALUES('Chile', 37);
INSERT INTO `freetplbanners_country` VALUES('China', 38);
INSERT INTO `freetplbanners_country` VALUES('Colombia', 39);
INSERT INTO `freetplbanners_country` VALUES('Comoros', 40);
INSERT INTO `freetplbanners_country` VALUES('Congo', 41);
INSERT INTO `freetplbanners_country` VALUES('Congo (DRC)', 42);
INSERT INTO `freetplbanners_country` VALUES('Cook Islands', 43);
INSERT INTO `freetplbanners_country` VALUES('Costa Rica', 44);
INSERT INTO `freetplbanners_country` VALUES('Cote d''Ivoire', 45);
INSERT INTO `freetplbanners_country` VALUES('Croatia (Hrvatska)', 46);
INSERT INTO `freetplbanners_country` VALUES('Cuba', 47);
INSERT INTO `freetplbanners_country` VALUES('Cyprus', 48);
INSERT INTO `freetplbanners_country` VALUES('Czech Republic', 49);
INSERT INTO `freetplbanners_country` VALUES('Denmark', 50);
INSERT INTO `freetplbanners_country` VALUES('Djibouti', 51);
INSERT INTO `freetplbanners_country` VALUES('Dominica', 52);
INSERT INTO `freetplbanners_country` VALUES('Dominican Republic', 53);
INSERT INTO `freetplbanners_country` VALUES('East Timor', 54);
INSERT INTO `freetplbanners_country` VALUES('Ecuador', 55);
INSERT INTO `freetplbanners_country` VALUES('Egypt', 56);
INSERT INTO `freetplbanners_country` VALUES('El Salvador', 57);
INSERT INTO `freetplbanners_country` VALUES('Equatorial Guinea', 58);
INSERT INTO `freetplbanners_country` VALUES('Eritrea', 59);
INSERT INTO `freetplbanners_country` VALUES('Estonia', 60);
INSERT INTO `freetplbanners_country` VALUES('Ethiopia', 61);
INSERT INTO `freetplbanners_country` VALUES('Falkland Islands', 62);
INSERT INTO `freetplbanners_country` VALUES('Faroe Islands', 63);
INSERT INTO `freetplbanners_country` VALUES('Fiji Islands', 64);
INSERT INTO `freetplbanners_country` VALUES('Finland', 65);
INSERT INTO `freetplbanners_country` VALUES('France', 66);
INSERT INTO `freetplbanners_country` VALUES('French Guiana', 67);
INSERT INTO `freetplbanners_country` VALUES('French Polynesia', 68);
INSERT INTO `freetplbanners_country` VALUES('Gabon', 69);
INSERT INTO `freetplbanners_country` VALUES('Gambia', 70);
INSERT INTO `freetplbanners_country` VALUES('Georgia', 71);
INSERT INTO `freetplbanners_country` VALUES('Germany', 72);
INSERT INTO `freetplbanners_country` VALUES('Ghana', 73);
INSERT INTO `freetplbanners_country` VALUES('Gibraltar', 74);
INSERT INTO `freetplbanners_country` VALUES('Greece', 75);
INSERT INTO `freetplbanners_country` VALUES('Greenland', 76);
INSERT INTO `freetplbanners_country` VALUES('Grenada', 77);
INSERT INTO `freetplbanners_country` VALUES('Guadeloupe', 78);
INSERT INTO `freetplbanners_country` VALUES('Guam', 79);
INSERT INTO `freetplbanners_country` VALUES('Guatemala', 80);
INSERT INTO `freetplbanners_country` VALUES('Guinea', 81);
INSERT INTO `freetplbanners_country` VALUES('Guinea-Bissau', 82);
INSERT INTO `freetplbanners_country` VALUES('Guyana', 83);
INSERT INTO `freetplbanners_country` VALUES('Haiti', 84);
INSERT INTO `freetplbanners_country` VALUES('Honduras', 85);
INSERT INTO `freetplbanners_country` VALUES('Hong Kong SAR', 86);
INSERT INTO `freetplbanners_country` VALUES('Hungary', 87);
INSERT INTO `freetplbanners_country` VALUES('Iceland', 88);
INSERT INTO `freetplbanners_country` VALUES('India', 89);
INSERT INTO `freetplbanners_country` VALUES('Indonesia', 90);
INSERT INTO `freetplbanners_country` VALUES('Iran', 91);
INSERT INTO `freetplbanners_country` VALUES('Iraq', 92);
INSERT INTO `freetplbanners_country` VALUES('Ireland', 93);
INSERT INTO `freetplbanners_country` VALUES('Israel', 94);
INSERT INTO `freetplbanners_country` VALUES('Italy', 95);
INSERT INTO `freetplbanners_country` VALUES('Jamaica', 96);
INSERT INTO `freetplbanners_country` VALUES('Japan', 97);
INSERT INTO `freetplbanners_country` VALUES('Jordan', 98);
INSERT INTO `freetplbanners_country` VALUES('Kazakhstan', 99);
INSERT INTO `freetplbanners_country` VALUES('Kenya', 100);
INSERT INTO `freetplbanners_country` VALUES('Kiribati', 101);
INSERT INTO `freetplbanners_country` VALUES('Korea', 102);
INSERT INTO `freetplbanners_country` VALUES('Kuwait', 103);
INSERT INTO `freetplbanners_country` VALUES('Kyrgyzstan', 104);
INSERT INTO `freetplbanners_country` VALUES('Laos', 105);
INSERT INTO `freetplbanners_country` VALUES('Latvia', 106);
INSERT INTO `freetplbanners_country` VALUES('Lebanon', 107);
INSERT INTO `freetplbanners_country` VALUES('Lesotho', 108);
INSERT INTO `freetplbanners_country` VALUES('Liberia', 109);
INSERT INTO `freetplbanners_country` VALUES('Libya', 110);
INSERT INTO `freetplbanners_country` VALUES('Liechtenstein', 111);
INSERT INTO `freetplbanners_country` VALUES('Lithuania', 112);
INSERT INTO `freetplbanners_country` VALUES('Luxembourg', 113);
INSERT INTO `freetplbanners_country` VALUES('Macao SAR', 114);
INSERT INTO `freetplbanners_country` VALUES('Macedonia', 115);
INSERT INTO `freetplbanners_country` VALUES('Madagascar', 116);
INSERT INTO `freetplbanners_country` VALUES('Malawi', 117);
INSERT INTO `freetplbanners_country` VALUES('Malaysia', 118);
INSERT INTO `freetplbanners_country` VALUES('Maldives', 119);
INSERT INTO `freetplbanners_country` VALUES('Mali', 120);
INSERT INTO `freetplbanners_country` VALUES('Malta', 121);
INSERT INTO `freetplbanners_country` VALUES('Martinique', 122);
INSERT INTO `freetplbanners_country` VALUES('Mauritania', 123);
INSERT INTO `freetplbanners_country` VALUES('Mauritius', 124);
INSERT INTO `freetplbanners_country` VALUES('Mayotte', 125);
INSERT INTO `freetplbanners_country` VALUES('Mexico', 126);
INSERT INTO `freetplbanners_country` VALUES('Micronesia', 127);
INSERT INTO `freetplbanners_country` VALUES('Moldova', 128);
INSERT INTO `freetplbanners_country` VALUES('Monaco', 129);
INSERT INTO `freetplbanners_country` VALUES('Mongolia', 130);
INSERT INTO `freetplbanners_country` VALUES('Montserrat', 131);
INSERT INTO `freetplbanners_country` VALUES('Morocco', 132);
INSERT INTO `freetplbanners_country` VALUES('Mozambique', 133);
INSERT INTO `freetplbanners_country` VALUES('Myanmar', 134);
INSERT INTO `freetplbanners_country` VALUES('Namibia', 135);
INSERT INTO `freetplbanners_country` VALUES('Nauru', 136);
INSERT INTO `freetplbanners_country` VALUES('Nepal', 137);
INSERT INTO `freetplbanners_country` VALUES('Netherlands', 138);
INSERT INTO `freetplbanners_country` VALUES('Netherlands Antilles', 139);
INSERT INTO `freetplbanners_country` VALUES('New Caledonia', 140);
INSERT INTO `freetplbanners_country` VALUES('New Zealand', 141);
INSERT INTO `freetplbanners_country` VALUES('Nicaragua', 142);
INSERT INTO `freetplbanners_country` VALUES('Niger', 143);
INSERT INTO `freetplbanners_country` VALUES('Nigeria', 144);
INSERT INTO `freetplbanners_country` VALUES('Niue', 145);
INSERT INTO `freetplbanners_country` VALUES('Norfolk Island', 146);
INSERT INTO `freetplbanners_country` VALUES('North Korea', 147);
INSERT INTO `freetplbanners_country` VALUES('Norway', 148);
INSERT INTO `freetplbanners_country` VALUES('Oman', 149);
INSERT INTO `freetplbanners_country` VALUES('Pakistan', 150);
INSERT INTO `freetplbanners_country` VALUES('Panama', 151);
INSERT INTO `freetplbanners_country` VALUES('Papua New Guinea', 152);
INSERT INTO `freetplbanners_country` VALUES('Paraguay', 153);
INSERT INTO `freetplbanners_country` VALUES('Peru', 154);
INSERT INTO `freetplbanners_country` VALUES('Philippines', 155);
INSERT INTO `freetplbanners_country` VALUES('Pitcairn Islands', 156);
INSERT INTO `freetplbanners_country` VALUES('Poland', 157);
INSERT INTO `freetplbanners_country` VALUES('Portugal', 158);
INSERT INTO `freetplbanners_country` VALUES('Puerto Rico', 159);
INSERT INTO `freetplbanners_country` VALUES('Qatar', 160);
INSERT INTO `freetplbanners_country` VALUES('Reunion', 161);
INSERT INTO `freetplbanners_country` VALUES('Romania', 162);
INSERT INTO `freetplbanners_country` VALUES('Russia', 163);
INSERT INTO `freetplbanners_country` VALUES('Rwanda', 164);
INSERT INTO `freetplbanners_country` VALUES('Samoa', 165);
INSERT INTO `freetplbanners_country` VALUES('San Marino', 166);
INSERT INTO `freetplbanners_country` VALUES('Sao Tome and Principe', 167);
INSERT INTO `freetplbanners_country` VALUES('Saudi Arabia', 168);
INSERT INTO `freetplbanners_country` VALUES('Senegal', 169);
INSERT INTO `freetplbanners_country` VALUES('Serbia and Montenegro', 170);
INSERT INTO `freetplbanners_country` VALUES('Seychelles', 171);
INSERT INTO `freetplbanners_country` VALUES('Sierra Leone', 172);
INSERT INTO `freetplbanners_country` VALUES('Singapore', 173);
INSERT INTO `freetplbanners_country` VALUES('Slovakia', 174);
INSERT INTO `freetplbanners_country` VALUES('Slovenia', 175);
INSERT INTO `freetplbanners_country` VALUES('Solomon Islands', 176);
INSERT INTO `freetplbanners_country` VALUES('Somalia', 177);
INSERT INTO `freetplbanners_country` VALUES('South Africa', 178);
INSERT INTO `freetplbanners_country` VALUES('Spain', 179);
INSERT INTO `freetplbanners_country` VALUES('Sri Lanka', 180);
INSERT INTO `freetplbanners_country` VALUES('St. Helena', 181);
INSERT INTO `freetplbanners_country` VALUES('St. Kitts and Nevis', 182);
INSERT INTO `freetplbanners_country` VALUES('St. Lucia', 183);
INSERT INTO `freetplbanners_country` VALUES('St. Pierre and Miquelon', 184);
INSERT INTO `freetplbanners_country` VALUES('St. Vincent & Grenadines', 185);
INSERT INTO `freetplbanners_country` VALUES('Sudan', 186);
INSERT INTO `freetplbanners_country` VALUES('Suriname', 187);
INSERT INTO `freetplbanners_country` VALUES('Swaziland', 188);
INSERT INTO `freetplbanners_country` VALUES('Sweden', 189);
INSERT INTO `freetplbanners_country` VALUES('Switzerland', 190);
INSERT INTO `freetplbanners_country` VALUES('Syria', 191);
INSERT INTO `freetplbanners_country` VALUES('Taiwan', 192);
INSERT INTO `freetplbanners_country` VALUES('Tajikistan', 193);
INSERT INTO `freetplbanners_country` VALUES('Tanzania', 194);
INSERT INTO `freetplbanners_country` VALUES('Thailand', 195);
INSERT INTO `freetplbanners_country` VALUES('Togo', 196);
INSERT INTO `freetplbanners_country` VALUES('Tokelau', 197);
INSERT INTO `freetplbanners_country` VALUES('Tonga', 198);
INSERT INTO `freetplbanners_country` VALUES('Trinidad and Tobago', 199);
INSERT INTO `freetplbanners_country` VALUES('Tunisia', 200);
INSERT INTO `freetplbanners_country` VALUES('Turkey', 201);
INSERT INTO `freetplbanners_country` VALUES('Turkmenistan', 202);
INSERT INTO `freetplbanners_country` VALUES('Turks and Caicos Islands', 203);
INSERT INTO `freetplbanners_country` VALUES('Tuvalu', 204);
INSERT INTO `freetplbanners_country` VALUES('Uganda', 205);
INSERT INTO `freetplbanners_country` VALUES('Ukraine', 206);
INSERT INTO `freetplbanners_country` VALUES('United Arab Emirates', 207);
INSERT INTO `freetplbanners_country` VALUES('United Kingdom', 208);
INSERT INTO `freetplbanners_country` VALUES('Uruguay', 209);
INSERT INTO `freetplbanners_country` VALUES('USA', 210);
INSERT INTO `freetplbanners_country` VALUES('Uzbekistan', 211);
INSERT INTO `freetplbanners_country` VALUES('Vanuatu', 212);
INSERT INTO `freetplbanners_country` VALUES('Venezuela', 213);
INSERT INTO `freetplbanners_country` VALUES('Vietnam', 214);
INSERT INTO `freetplbanners_country` VALUES('Virgin Islands', 215);
INSERT INTO `freetplbanners_country` VALUES('Virgin Islands (British)', 216);
INSERT INTO `freetplbanners_country` VALUES('Wallis and Futuna', 217);
INSERT INTO `freetplbanners_country` VALUES('Yemen', 218);
INSERT INTO `freetplbanners_country` VALUES('Yugoslavia', 219);
INSERT INTO `freetplbanners_country` VALUES('Zambia', 220);
INSERT INTO `freetplbanners_country` VALUES('Zimbabwe', 221);
INSERT INTO `freetplbanners_country` VALUES('Australia', 222);

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_currency`
--

CREATE TABLE `freetplbanners_currency` (
  `id` bigint(20) NOT NULL auto_increment,
  `cur_name` varchar(255) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `freetplbanners_currency`
--

INSERT INTO `freetplbanners_currency` VALUES(1, '$');
INSERT INTO `freetplbanners_currency` VALUES(2, 'Rs.');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_displays`
--

CREATE TABLE `freetplbanners_displays` (
  `id` bigint(20) NOT NULL auto_increment,
  `adv_id` bigint(20) default NULL,
  `banner_id` bigint(20) default NULL,
  `ondate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `freetpl_count` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=312 ;

--
-- Dumping data for table `freetplbanners_displays`
--

INSERT INTO `freetplbanners_displays` VALUES(1, 0, 22, '2004-04-14 11:13:37', 0);
INSERT INTO `freetplbanners_displays` VALUES(2, 0, 28, '2004-04-14 11:13:37', 0);
INSERT INTO `freetplbanners_displays` VALUES(3, 0, 22, '2004-04-14 11:13:56', 0);
INSERT INTO `freetplbanners_displays` VALUES(4, 0, 28, '2004-04-14 11:13:56', 0);
INSERT INTO `freetplbanners_displays` VALUES(5, 2, 13, '2004-04-14 11:13:57', 0);
INSERT INTO `freetplbanners_displays` VALUES(6, 0, 28, '2004-04-14 11:13:57', 0);
INSERT INTO `freetplbanners_displays` VALUES(7, 2, 23, '2004-04-14 13:22:48', 0);
INSERT INTO `freetplbanners_displays` VALUES(8, 0, 28, '2004-04-14 13:22:48', 0);
INSERT INTO `freetplbanners_displays` VALUES(9, 0, 22, '2004-04-15 14:44:37', 0);
INSERT INTO `freetplbanners_displays` VALUES(10, 2, 26, '2004-04-15 14:44:37', 0);
INSERT INTO `freetplbanners_displays` VALUES(11, 0, 28, '2004-04-15 14:44:38', 0);
INSERT INTO `freetplbanners_displays` VALUES(12, 0, 21, '2004-04-15 15:54:03', 0);
INSERT INTO `freetplbanners_displays` VALUES(13, 2, 26, '2004-04-15 15:54:03', 0);
INSERT INTO `freetplbanners_displays` VALUES(14, 0, 28, '2004-04-15 15:54:03', 0);
INSERT INTO `freetplbanners_displays` VALUES(15, 0, 21, '2004-04-15 15:54:06', 0);
INSERT INTO `freetplbanners_displays` VALUES(16, 2, 27, '2004-04-15 15:54:06', 0);
INSERT INTO `freetplbanners_displays` VALUES(17, 0, 30, '2004-04-15 15:54:06', 0);
INSERT INTO `freetplbanners_displays` VALUES(18, 0, 21, '2004-04-15 15:54:06', 0);
INSERT INTO `freetplbanners_displays` VALUES(19, 2, 27, '2004-04-15 15:54:06', 0);
INSERT INTO `freetplbanners_displays` VALUES(20, 0, 28, '2004-04-15 15:54:07', 0);
INSERT INTO `freetplbanners_displays` VALUES(21, 0, 21, '2004-04-15 15:54:07', 0);
INSERT INTO `freetplbanners_displays` VALUES(22, 2, 27, '2004-04-15 15:54:07', 0);
INSERT INTO `freetplbanners_displays` VALUES(23, 0, 30, '2004-04-15 15:54:07', 0);
INSERT INTO `freetplbanners_displays` VALUES(24, 0, 21, '2004-04-15 15:54:08', 0);
INSERT INTO `freetplbanners_displays` VALUES(25, 2, 27, '2004-04-15 15:54:08', 0);
INSERT INTO `freetplbanners_displays` VALUES(26, 0, 30, '2004-04-15 15:54:08', 0);
INSERT INTO `freetplbanners_displays` VALUES(27, 2, 27, '2004-04-15 15:54:08', 0);
INSERT INTO `freetplbanners_displays` VALUES(28, 0, 19, '2004-04-15 15:54:08', 0);
INSERT INTO `freetplbanners_displays` VALUES(29, 0, 28, '2004-04-15 15:54:08', 0);
INSERT INTO `freetplbanners_displays` VALUES(30, 2, 26, '2004-04-15 15:54:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(31, 0, 22, '2004-04-15 15:54:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(32, 0, 28, '2004-04-15 15:54:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(33, 0, 22, '2004-04-15 15:54:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(34, 2, 26, '2004-04-15 15:54:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(35, 0, 30, '2004-04-15 15:54:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(36, 2, 27, '2004-04-15 15:54:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(37, 0, 22, '2004-04-15 15:54:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(38, 0, 28, '2004-04-15 15:54:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(39, 0, 22, '2004-04-15 15:54:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(40, 2, 27, '2004-04-15 15:54:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(41, 0, 28, '2004-04-15 15:54:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(42, 2, 27, '2004-04-15 15:54:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(43, 0, 21, '2004-04-15 15:54:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(44, 0, 30, '2004-04-15 15:54:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(45, 2, 27, '2004-04-15 15:54:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(46, 0, 21, '2004-04-15 15:54:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(47, 0, 28, '2004-04-15 15:54:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(48, 0, 21, '2004-04-15 15:54:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(49, 2, 27, '2004-04-15 15:54:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(50, 0, 30, '2004-04-15 15:54:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(51, 0, 21, '2004-04-15 15:54:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(52, 2, 26, '2004-04-15 15:54:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(53, 0, 30, '2004-04-15 15:54:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(54, 2, 27, '2004-04-15 15:54:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(55, 0, 21, '2004-04-15 15:54:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(56, 0, 28, '2004-04-15 15:54:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(57, 2, 20, '2004-04-15 15:55:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(58, 2, 26, '2004-04-15 15:55:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(59, 0, 28, '2004-04-15 15:55:09', 0);
INSERT INTO `freetplbanners_displays` VALUES(60, 0, 19, '2004-04-15 15:55:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(61, 2, 29, '2004-04-15 15:55:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(62, 0, 28, '2004-04-15 15:55:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(63, 0, 22, '2004-04-15 15:55:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(64, 2, 27, '2004-04-15 15:55:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(65, 0, 30, '2004-04-15 15:55:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(66, 2, 29, '2004-04-15 15:55:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(67, 0, 21, '2004-04-15 15:55:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(68, 0, 30, '2004-04-15 15:55:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(69, 2, 29, '2004-04-15 15:55:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(70, 2, 13, '2004-04-15 15:55:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(71, 0, 28, '2004-04-15 15:55:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(72, 2, 27, '2004-04-15 15:55:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(73, 2, 13, '2004-04-15 15:55:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(74, 0, 30, '2004-04-15 15:55:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(75, 2, 13, '2004-04-15 15:55:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(76, 2, 29, '2004-04-15 15:55:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(77, 0, 30, '2004-04-15 15:55:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(78, 2, 26, '2004-04-15 15:55:17', 0);
INSERT INTO `freetplbanners_displays` VALUES(79, 0, 19, '2004-04-15 15:55:17', 0);
INSERT INTO `freetplbanners_displays` VALUES(80, 0, 28, '2004-04-15 15:55:17', 0);
INSERT INTO `freetplbanners_displays` VALUES(81, 0, 21, '2004-04-15 15:55:17', 0);
INSERT INTO `freetplbanners_displays` VALUES(82, 2, 27, '2004-04-15 15:55:17', 0);
INSERT INTO `freetplbanners_displays` VALUES(83, 0, 30, '2004-04-15 15:55:17', 0);
INSERT INTO `freetplbanners_displays` VALUES(84, 2, 27, '2004-04-15 15:55:18', 0);
INSERT INTO `freetplbanners_displays` VALUES(85, 0, 21, '2004-04-15 15:55:18', 0);
INSERT INTO `freetplbanners_displays` VALUES(86, 0, 28, '2004-04-15 15:55:18', 0);
INSERT INTO `freetplbanners_displays` VALUES(87, 0, 19, '2004-04-15 15:55:18', 0);
INSERT INTO `freetplbanners_displays` VALUES(88, 2, 26, '2004-04-15 15:55:18', 0);
INSERT INTO `freetplbanners_displays` VALUES(89, 0, 28, '2004-04-15 15:55:18', 0);
INSERT INTO `freetplbanners_displays` VALUES(90, 2, 27, '2004-04-15 15:55:19', 0);
INSERT INTO `freetplbanners_displays` VALUES(91, 2, 13, '2004-04-15 15:55:19', 0);
INSERT INTO `freetplbanners_displays` VALUES(92, 0, 30, '2004-04-15 15:55:19', 0);
INSERT INTO `freetplbanners_displays` VALUES(93, 2, 29, '2004-04-15 15:55:19', 0);
INSERT INTO `freetplbanners_displays` VALUES(94, 0, 19, '2004-04-15 15:55:19', 0);
INSERT INTO `freetplbanners_displays` VALUES(95, 0, 28, '2004-04-15 15:55:19', 0);
INSERT INTO `freetplbanners_displays` VALUES(96, 2, 29, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(97, 0, 28, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(98, 0, 22, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(99, 0, 19, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(100, 2, 27, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(101, 0, 30, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(102, 2, 13, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(103, 2, 27, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(104, 0, 28, '2004-04-15 15:55:20', 0);
INSERT INTO `freetplbanners_displays` VALUES(105, 2, 26, '2004-04-15 15:55:21', 0);
INSERT INTO `freetplbanners_displays` VALUES(106, 0, 19, '2004-04-15 15:55:21', 0);
INSERT INTO `freetplbanners_displays` VALUES(107, 0, 28, '2004-04-15 15:55:21', 0);
INSERT INTO `freetplbanners_displays` VALUES(108, 2, 26, '2004-04-15 15:55:21', 0);
INSERT INTO `freetplbanners_displays` VALUES(109, 2, 20, '2004-04-15 15:55:21', 0);
INSERT INTO `freetplbanners_displays` VALUES(110, 0, 30, '2004-04-15 15:55:21', 0);
INSERT INTO `freetplbanners_displays` VALUES(111, 2, 13, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(112, 2, 26, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(113, 0, 30, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(114, 0, 22, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(115, 2, 29, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(116, 0, 30, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(117, 2, 20, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(118, 2, 26, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(119, 0, 30, '2004-04-15 15:55:22', 0);
INSERT INTO `freetplbanners_displays` VALUES(120, 2, 27, '2004-04-15 15:55:23', 0);
INSERT INTO `freetplbanners_displays` VALUES(121, 2, 13, '2004-04-15 15:55:23', 0);
INSERT INTO `freetplbanners_displays` VALUES(122, 0, 28, '2004-04-15 15:55:23', 0);
INSERT INTO `freetplbanners_displays` VALUES(123, 2, 26, '2004-04-15 15:55:23', 0);
INSERT INTO `freetplbanners_displays` VALUES(124, 2, 13, '2004-04-15 15:55:23', 0);
INSERT INTO `freetplbanners_displays` VALUES(125, 0, 30, '2004-04-15 15:55:23', 0);
INSERT INTO `freetplbanners_displays` VALUES(126, 0, 22, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(127, 2, 26, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(128, 0, 28, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(129, 2, 20, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(130, 2, 26, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(131, 0, 30, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(132, 0, 21, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(133, 2, 27, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(134, 0, 30, '2004-04-15 15:55:24', 0);
INSERT INTO `freetplbanners_displays` VALUES(135, 2, 29, '2004-04-15 15:55:25', 0);
INSERT INTO `freetplbanners_displays` VALUES(136, 0, 22, '2004-04-15 15:55:25', 0);
INSERT INTO `freetplbanners_displays` VALUES(137, 0, 30, '2004-04-15 15:55:25', 0);
INSERT INTO `freetplbanners_displays` VALUES(138, 2, 20, '2004-04-15 15:55:25', 0);
INSERT INTO `freetplbanners_displays` VALUES(139, 2, 29, '2004-04-15 15:55:25', 0);
INSERT INTO `freetplbanners_displays` VALUES(140, 0, 28, '2004-04-15 15:55:25', 0);
INSERT INTO `freetplbanners_displays` VALUES(141, 2, 20, '2004-04-15 15:55:25', 0);
INSERT INTO `freetplbanners_displays` VALUES(142, 2, 26, '2004-04-15 15:55:25', 0);
INSERT INTO `freetplbanners_displays` VALUES(143, 0, 30, '2004-04-15 15:55:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(144, 2, 13, '2004-04-15 15:55:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(145, 2, 26, '2004-04-15 15:55:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(146, 0, 28, '2004-04-15 15:55:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(147, 2, 20, '2004-04-15 15:55:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(148, 2, 26, '2004-04-15 15:55:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(149, 0, 30, '2004-04-15 15:55:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(150, 0, 19, '2004-04-15 15:55:27', 0);
INSERT INTO `freetplbanners_displays` VALUES(151, 2, 29, '2004-04-15 15:55:27', 0);
INSERT INTO `freetplbanners_displays` VALUES(152, 0, 30, '2004-04-15 15:55:27', 0);
INSERT INTO `freetplbanners_displays` VALUES(153, 0, 19, '2004-04-15 15:55:27', 0);
INSERT INTO `freetplbanners_displays` VALUES(154, 2, 27, '2004-04-15 15:55:27', 0);
INSERT INTO `freetplbanners_displays` VALUES(155, 0, 30, '2004-04-15 15:55:27', 0);
INSERT INTO `freetplbanners_displays` VALUES(156, 2, 26, '2004-04-15 15:55:28', 0);
INSERT INTO `freetplbanners_displays` VALUES(157, 0, 21, '2004-04-15 15:55:28', 0);
INSERT INTO `freetplbanners_displays` VALUES(158, 0, 30, '2004-04-15 15:55:28', 0);
INSERT INTO `freetplbanners_displays` VALUES(159, 0, 22, '2004-04-15 15:55:28', 0);
INSERT INTO `freetplbanners_displays` VALUES(160, 2, 27, '2004-04-15 15:55:28', 0);
INSERT INTO `freetplbanners_displays` VALUES(161, 0, 30, '2004-04-15 15:55:28', 0);
INSERT INTO `freetplbanners_displays` VALUES(162, 2, 27, '2004-04-15 15:55:29', 0);
INSERT INTO `freetplbanners_displays` VALUES(163, 2, 20, '2004-04-15 15:55:29', 0);
INSERT INTO `freetplbanners_displays` VALUES(164, 0, 28, '2004-04-15 15:55:29', 0);
INSERT INTO `freetplbanners_displays` VALUES(165, 2, 13, '2004-04-15 15:55:29', 0);
INSERT INTO `freetplbanners_displays` VALUES(166, 2, 26, '2004-04-15 15:55:29', 0);
INSERT INTO `freetplbanners_displays` VALUES(167, 0, 28, '2004-04-15 15:55:29', 0);
INSERT INTO `freetplbanners_displays` VALUES(168, 0, 19, '2004-04-15 15:55:30', 0);
INSERT INTO `freetplbanners_displays` VALUES(169, 2, 27, '2004-04-15 15:55:30', 0);
INSERT INTO `freetplbanners_displays` VALUES(170, 0, 28, '2004-04-15 15:55:30', 0);
INSERT INTO `freetplbanners_displays` VALUES(171, 2, 26, '2004-04-15 15:55:30', 0);
INSERT INTO `freetplbanners_displays` VALUES(172, 2, 13, '2004-04-15 15:55:30', 0);
INSERT INTO `freetplbanners_displays` VALUES(173, 0, 30, '2004-04-15 15:55:30', 0);
INSERT INTO `freetplbanners_displays` VALUES(174, 2, 13, '2004-04-18 16:49:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(175, 2, 13, '2004-04-18 16:49:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(176, 0, 22, '2004-04-18 16:49:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(177, 0, 22, '2004-04-18 16:49:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(178, 2, 13, '2004-04-18 16:49:51', 0);
INSERT INTO `freetplbanners_displays` VALUES(179, 2, 13, '2004-04-18 16:49:52', 0);
INSERT INTO `freetplbanners_displays` VALUES(180, 2, 20, '2004-04-18 16:49:52', 0);
INSERT INTO `freetplbanners_displays` VALUES(181, 2, 20, '2004-04-18 16:49:53', 0);
INSERT INTO `freetplbanners_displays` VALUES(182, 2, 20, '2004-04-18 16:49:53', 0);
INSERT INTO `freetplbanners_displays` VALUES(183, 2, 20, '2004-04-18 16:49:54', 0);
INSERT INTO `freetplbanners_displays` VALUES(184, 2, 20, '2004-04-18 16:49:54', 0);
INSERT INTO `freetplbanners_displays` VALUES(185, 2, 20, '2004-04-18 16:49:55', 0);
INSERT INTO `freetplbanners_displays` VALUES(186, 2, 20, '2004-04-18 16:49:55', 0);
INSERT INTO `freetplbanners_displays` VALUES(187, 2, 23, '2004-04-18 16:51:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(188, 2, 23, '2004-04-18 16:51:27', 0);
INSERT INTO `freetplbanners_displays` VALUES(189, 2, 18, '2004-04-18 16:51:27', 0);
INSERT INTO `freetplbanners_displays` VALUES(190, 2, 18, '2004-04-18 16:51:28', 0);
INSERT INTO `freetplbanners_displays` VALUES(191, 2, 23, '2004-04-18 16:51:28', 0);
INSERT INTO `freetplbanners_displays` VALUES(192, 2, 13, '2004-04-18 16:51:29', 0);
INSERT INTO `freetplbanners_displays` VALUES(193, 2, 13, '2004-04-18 16:51:30', 0);
INSERT INTO `freetplbanners_displays` VALUES(194, 2, 23, '2004-04-18 16:51:30', 0);
INSERT INTO `freetplbanners_displays` VALUES(195, 2, 13, '2004-04-18 16:51:31', 0);
INSERT INTO `freetplbanners_displays` VALUES(196, 2, 18, '2004-04-18 16:56:10', 0);
INSERT INTO `freetplbanners_displays` VALUES(197, 2, 20, '2004-04-18 16:56:12', 0);
INSERT INTO `freetplbanners_displays` VALUES(198, 2, 18, '2004-04-18 16:56:14', 0);
INSERT INTO `freetplbanners_displays` VALUES(199, 2, 20, '2004-04-18 16:56:15', 0);
INSERT INTO `freetplbanners_displays` VALUES(200, 2, 18, '2004-04-18 16:56:17', 0);
INSERT INTO `freetplbanners_displays` VALUES(201, 2, 23, '2004-04-18 16:56:49', 0);
INSERT INTO `freetplbanners_displays` VALUES(202, 2, 20, '2004-04-18 17:09:08', 0);
INSERT INTO `freetplbanners_displays` VALUES(203, 2, 13, '2004-04-18 17:09:11', 0);
INSERT INTO `freetplbanners_displays` VALUES(204, 2, 18, '2004-04-18 17:09:13', 0);
INSERT INTO `freetplbanners_displays` VALUES(205, 2, 13, '2004-04-18 17:09:14', 0);
INSERT INTO `freetplbanners_displays` VALUES(206, 2, 23, '2004-04-18 17:09:33', 0);
INSERT INTO `freetplbanners_displays` VALUES(207, 2, 18, '2004-04-18 18:25:02', 0);
INSERT INTO `freetplbanners_displays` VALUES(208, 2, 23, '2004-04-18 18:26:15', 0);
INSERT INTO `freetplbanners_displays` VALUES(209, 2, 20, '2004-04-18 18:26:58', 0);
INSERT INTO `freetplbanners_displays` VALUES(210, 2, 20, '2004-04-18 18:27:03', 0);
INSERT INTO `freetplbanners_displays` VALUES(211, 2, 20, '2004-04-18 18:27:21', 0);
INSERT INTO `freetplbanners_displays` VALUES(212, 2, 13, '2004-04-18 18:27:26', 0);
INSERT INTO `freetplbanners_displays` VALUES(213, 2, 20, '2004-04-18 18:28:57', 0);
INSERT INTO `freetplbanners_displays` VALUES(214, 2, 20, '2004-04-18 18:29:03', 0);
INSERT INTO `freetplbanners_displays` VALUES(215, 2, 18, '2004-04-18 18:30:23', 0);
INSERT INTO `freetplbanners_displays` VALUES(216, 2, 18, '2004-04-18 18:30:44', 0);
INSERT INTO `freetplbanners_displays` VALUES(217, 2, 23, '2004-04-18 18:33:05', 0);
INSERT INTO `freetplbanners_displays` VALUES(218, 2, 13, '2004-04-18 18:33:32', 0);
INSERT INTO `freetplbanners_displays` VALUES(219, 2, 29, '2004-04-29 18:35:37', 0);
INSERT INTO `freetplbanners_displays` VALUES(220, 0, 28, '2004-04-29 18:35:37', 0);
INSERT INTO `freetplbanners_displays` VALUES(221, 2, 23, '2007-09-04 00:00:00', 1);
INSERT INTO `freetplbanners_displays` VALUES(222, 2, 29, '2007-09-04 00:00:00', 4);
INSERT INTO `freetplbanners_displays` VALUES(223, 2, 13, '2007-09-04 00:00:00', 5);
INSERT INTO `freetplbanners_displays` VALUES(224, 2, 18, '2007-09-04 00:00:00', 4);
INSERT INTO `freetplbanners_displays` VALUES(225, 13, 31, '2007-09-04 00:00:00', 3);
INSERT INTO `freetplbanners_displays` VALUES(226, 2, 13, '2007-12-16 00:00:00', 1);
INSERT INTO `freetplbanners_displays` VALUES(227, 2, 18, '2007-12-16 00:00:00', 1);
INSERT INTO `freetplbanners_displays` VALUES(228, 0, 32, '2007-12-16 00:00:00', 50);
INSERT INTO `freetplbanners_displays` VALUES(229, 0, 34, '2007-12-16 00:00:00', 48);
INSERT INTO `freetplbanners_displays` VALUES(230, 0, 33, '2007-12-16 00:00:00', 42);
INSERT INTO `freetplbanners_displays` VALUES(231, 0, 34, '2007-12-17 00:00:00', 30);
INSERT INTO `freetplbanners_displays` VALUES(232, 0, 33, '2007-12-17 00:00:00', 25);
INSERT INTO `freetplbanners_displays` VALUES(233, 0, 32, '2007-12-17 00:00:00', 26);
INSERT INTO `freetplbanners_displays` VALUES(234, 0, 32, '2007-12-18 00:00:00', 31);
INSERT INTO `freetplbanners_displays` VALUES(235, 0, 34, '2007-12-18 00:00:00', 31);
INSERT INTO `freetplbanners_displays` VALUES(236, 0, 33, '2007-12-18 00:00:00', 23);
INSERT INTO `freetplbanners_displays` VALUES(237, 0, 32, '2007-12-19 00:00:00', 34);
INSERT INTO `freetplbanners_displays` VALUES(238, 0, 34, '2007-12-19 00:00:00', 36);
INSERT INTO `freetplbanners_displays` VALUES(239, 0, 33, '2007-12-19 00:00:00', 40);
INSERT INTO `freetplbanners_displays` VALUES(240, 0, 33, '2007-12-20 00:00:00', 77);
INSERT INTO `freetplbanners_displays` VALUES(241, 0, 34, '2007-12-20 00:00:00', 59);
INSERT INTO `freetplbanners_displays` VALUES(242, 0, 32, '2007-12-20 00:00:00', 79);
INSERT INTO `freetplbanners_displays` VALUES(243, 0, 34, '2007-12-21 00:00:00', 27);
INSERT INTO `freetplbanners_displays` VALUES(244, 0, 33, '2007-12-21 00:00:00', 35);
INSERT INTO `freetplbanners_displays` VALUES(245, 0, 32, '2007-12-21 00:00:00', 35);
INSERT INTO `freetplbanners_displays` VALUES(246, 0, 34, '2007-12-22 00:00:00', 20);
INSERT INTO `freetplbanners_displays` VALUES(247, 0, 33, '2007-12-22 00:00:00', 29);
INSERT INTO `freetplbanners_displays` VALUES(248, 0, 32, '2007-12-22 00:00:00', 18);
INSERT INTO `freetplbanners_displays` VALUES(249, 0, 34, '2007-12-23 00:00:00', 37);
INSERT INTO `freetplbanners_displays` VALUES(250, 0, 32, '2007-12-23 00:00:00', 29);
INSERT INTO `freetplbanners_displays` VALUES(251, 0, 33, '2007-12-23 00:00:00', 23);
INSERT INTO `freetplbanners_displays` VALUES(252, 0, 33, '2007-12-24 00:00:00', 38);
INSERT INTO `freetplbanners_displays` VALUES(253, 0, 32, '2007-12-24 00:00:00', 38);
INSERT INTO `freetplbanners_displays` VALUES(254, 0, 34, '2007-12-24 00:00:00', 33);
INSERT INTO `freetplbanners_displays` VALUES(255, 0, 34, '2007-12-25 00:00:00', 18);
INSERT INTO `freetplbanners_displays` VALUES(256, 0, 33, '2007-12-25 00:00:00', 29);
INSERT INTO `freetplbanners_displays` VALUES(257, 0, 32, '2007-12-25 00:00:00', 23);
INSERT INTO `freetplbanners_displays` VALUES(258, 0, 33, '2007-12-26 00:00:00', 26);
INSERT INTO `freetplbanners_displays` VALUES(259, 0, 34, '2007-12-26 00:00:00', 25);
INSERT INTO `freetplbanners_displays` VALUES(260, 0, 32, '2007-12-26 00:00:00', 24);
INSERT INTO `freetplbanners_displays` VALUES(261, 0, 33, '2007-12-27 00:00:00', 28);
INSERT INTO `freetplbanners_displays` VALUES(262, 0, 32, '2007-12-27 00:00:00', 32);
INSERT INTO `freetplbanners_displays` VALUES(263, 0, 34, '2007-12-27 00:00:00', 28);
INSERT INTO `freetplbanners_displays` VALUES(264, 0, 34, '2007-12-28 00:00:00', 13);
INSERT INTO `freetplbanners_displays` VALUES(265, 0, 32, '2007-12-28 00:00:00', 23);
INSERT INTO `freetplbanners_displays` VALUES(266, 0, 33, '2007-12-28 00:00:00', 18);
INSERT INTO `freetplbanners_displays` VALUES(267, 0, 33, '2007-12-29 00:00:00', 24);
INSERT INTO `freetplbanners_displays` VALUES(268, 0, 34, '2007-12-29 00:00:00', 24);
INSERT INTO `freetplbanners_displays` VALUES(269, 0, 32, '2007-12-29 00:00:00', 17);
INSERT INTO `freetplbanners_displays` VALUES(270, 0, 32, '2007-12-30 00:00:00', 12);
INSERT INTO `freetplbanners_displays` VALUES(271, 0, 33, '2007-12-30 00:00:00', 3);
INSERT INTO `freetplbanners_displays` VALUES(272, 0, 34, '2007-12-30 00:00:00', 6);
INSERT INTO `freetplbanners_displays` VALUES(273, 0, 32, '2007-12-31 00:00:00', 12);
INSERT INTO `freetplbanners_displays` VALUES(274, 0, 33, '2007-12-31 00:00:00', 12);
INSERT INTO `freetplbanners_displays` VALUES(275, 0, 34, '2007-12-31 00:00:00', 16);
INSERT INTO `freetplbanners_displays` VALUES(276, 0, 34, '2008-01-01 00:00:00', 7);
INSERT INTO `freetplbanners_displays` VALUES(277, 0, 32, '2008-01-01 00:00:00', 8);
INSERT INTO `freetplbanners_displays` VALUES(278, 0, 33, '2008-01-01 00:00:00', 6);
INSERT INTO `freetplbanners_displays` VALUES(279, 0, 33, '2008-01-02 00:00:00', 25);
INSERT INTO `freetplbanners_displays` VALUES(280, 0, 32, '2008-01-02 00:00:00', 24);
INSERT INTO `freetplbanners_displays` VALUES(281, 0, 34, '2008-01-02 00:00:00', 12);
INSERT INTO `freetplbanners_displays` VALUES(282, 0, 32, '2008-01-03 00:00:00', 20);
INSERT INTO `freetplbanners_displays` VALUES(283, 0, 34, '2008-01-03 00:00:00', 14);
INSERT INTO `freetplbanners_displays` VALUES(284, 0, 33, '2008-01-03 00:00:00', 17);
INSERT INTO `freetplbanners_displays` VALUES(285, 0, 34, '2008-01-04 00:00:00', 14);
INSERT INTO `freetplbanners_displays` VALUES(286, 0, 33, '2008-01-04 00:00:00', 24);
INSERT INTO `freetplbanners_displays` VALUES(287, 0, 32, '2008-01-04 00:00:00', 14);
INSERT INTO `freetplbanners_displays` VALUES(288, 0, 34, '2008-01-05 00:00:00', 22);
INSERT INTO `freetplbanners_displays` VALUES(289, 0, 33, '2008-01-05 00:00:00', 21);
INSERT INTO `freetplbanners_displays` VALUES(290, 0, 32, '2008-01-05 00:00:00', 16);
INSERT INTO `freetplbanners_displays` VALUES(291, 0, 32, '2008-01-06 00:00:00', 19);
INSERT INTO `freetplbanners_displays` VALUES(292, 0, 34, '2008-01-06 00:00:00', 25);
INSERT INTO `freetplbanners_displays` VALUES(293, 0, 33, '2008-01-06 00:00:00', 14);
INSERT INTO `freetplbanners_displays` VALUES(294, 0, 33, '2008-01-07 00:00:00', 37);
INSERT INTO `freetplbanners_displays` VALUES(295, 0, 34, '2008-01-07 00:00:00', 41);
INSERT INTO `freetplbanners_displays` VALUES(296, 0, 32, '2008-01-07 00:00:00', 38);
INSERT INTO `freetplbanners_displays` VALUES(297, 0, 33, '2008-01-08 00:00:00', 39);
INSERT INTO `freetplbanners_displays` VALUES(298, 0, 34, '2008-01-08 00:00:00', 28);
INSERT INTO `freetplbanners_displays` VALUES(299, 0, 32, '2008-01-08 00:00:00', 31);
INSERT INTO `freetplbanners_displays` VALUES(300, 0, 33, '2008-01-09 00:00:00', 17);
INSERT INTO `freetplbanners_displays` VALUES(301, 0, 32, '2008-01-09 00:00:00', 17);
INSERT INTO `freetplbanners_displays` VALUES(302, 0, 34, '2008-01-09 00:00:00', 22);
INSERT INTO `freetplbanners_displays` VALUES(303, 0, 34, '2008-01-10 00:00:00', 29);
INSERT INTO `freetplbanners_displays` VALUES(304, 0, 32, '2008-01-10 00:00:00', 26);
INSERT INTO `freetplbanners_displays` VALUES(305, 0, 33, '2008-01-10 00:00:00', 21);
INSERT INTO `freetplbanners_displays` VALUES(306, 0, 33, '2008-01-11 00:00:00', 8);
INSERT INTO `freetplbanners_displays` VALUES(307, 0, 34, '2008-01-11 00:00:00', 11);
INSERT INTO `freetplbanners_displays` VALUES(308, 0, 32, '2008-01-11 00:00:00', 15);
INSERT INTO `freetplbanners_displays` VALUES(309, 0, 33, '2008-01-16 00:00:00', 1);
INSERT INTO `freetplbanners_displays` VALUES(310, 0, 35, '2008-01-16 00:00:00', 1);
INSERT INTO `freetplbanners_displays` VALUES(311, 0, 35, '2008-01-18 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_mails`
--

CREATE TABLE `freetplbanners_mails` (
  `id` bigint(20) NOT NULL auto_increment,
  `mailid` bigint(20) NOT NULL default '0',
  `fromid` varchar(255) collate latin1_general_ci NOT NULL default '',
  `subject` varchar(255) collate latin1_general_ci NOT NULL default '',
  `mail` longtext collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `freetplbanners_mails`
--

INSERT INTO `freetplbanners_mails` VALUES(2, 2, 'support@biglobangs.com', 'Your password', 'Hi <fname>,\r\n\r\nHere is your login information:\r\n\r\nPassword:        <password>\r\nEmail:           <email>\r\n\r\n\r\nThanks for being part of our website. Click <loginurl> to login.\r\n\r\nRegards,\r\nAdmin\r\nBiglobangs Advertisement');
INSERT INTO `freetplbanners_mails` VALUES(1, 1, 'demo@demo.com', 'Welcome to Biglobangs Advertisement', 'Hi <fname>,\r\n\r\nWelcome to Biglobangs Advertisement.\r\n\r\nYour registration information is as follows:\r\n\r\nPassword:        <password>\r\nEmail:           <email>\r\n\r\n\r\nThanks for being part of our website.\r\nClick <loginurl> to login.\r\n\r\nRegards,\r\nAdmin\r\nBiglobangs Advertisement');
INSERT INTO `freetplbanners_mails` VALUES(3, 3, 'support@biglobangs.com', 'Stats of your banner on Biglobangs Advertisement', 'Hi <fname>,\r\n\r\nStatistical Information for your banner is as follows:\r\n\r\nBanner::<bannerurl>\r\nCredits::<credits>\r\nDisplays::<displays>\r\nBalance::<balance>\r\n\r\nRegards,\r\nAdmin\r\nBiglobangs Advertisement');
INSERT INTO `freetplbanners_mails` VALUES(4, 4, 'demo@demo.com', 'Biglobangs Advertisement Banner Approval Mail', 'Hi <fname>\r\n\r\nYour Banner <bannerurl> has been approved by the admin\r\n\r\nRegards,\r\nBiglobangs Advertisement');
INSERT INTO `freetplbanners_mails` VALUES(5, 5, 'info@freetpl.com', 'Banner Disapproval Mail', 'Hi <fname>\r\n\r\nYour Banner <bannerurl> has been disapproved by admin.\r\n\r\nThanks \r\nRegards,\r\nfreetplbanners');
INSERT INTO `freetplbanners_mails` VALUES(6, 6, 'info@freetpl.com', 'New member signed up', 'Hi admin,\r\n\r\nA new member has just signed up at FreeTplbanners.com. Details are :\r\n\r\nName:<fname>\r\nEmail:<email>\r\nPassword:<password>\r\n\r\nRegards,\r\nAutomail');
INSERT INTO `freetplbanners_mails` VALUES(8, 8, 'support@biglobangs.com', 'Banner has been updated and awaiting approval', 'Hi admin,\r\n\r\nA member has updated a banner and is awiting approval. The details are as following\r\n\r\nName:<fname>\r\nEmail:<email>\r\nBanner URL:<bannerurl>\r\n\r\nRegards,\r\nautomail\r\nBiglobangs Advertisement');
INSERT INTO `freetplbanners_mails` VALUES(7, 7, 'demo@demo.com', 'New banner waiting approval', 'Hi admin,\r\n\r\nA member has posted a banner and is awiting approval. The details are as following\r\n\r\nName:<fname>\r\nEmail:<email>\r\nBanner URL:<bannerurl>\r\n\r\nRegards,\r\nautomail\r\nBiglobangs Advertisement');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_plans`
--

CREATE TABLE `freetplbanners_plans` (
  `id` bigint(20) NOT NULL auto_increment,
  `credits` bigint(20) NOT NULL default '0',
  `price` bigint(20) NOT NULL default '0',
  `size_id` bigint(20) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `freetplbanners_plans`
--

INSERT INTO `freetplbanners_plans` VALUES(10, 100000000, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_sizes`
--

CREATE TABLE `freetplbanners_sizes` (
  `id` bigint(20) NOT NULL auto_increment,
  `height` int(11) default NULL,
  `width` int(11) default NULL,
  `default_banner` varchar(255) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `freetplbanners_sizes`
--

INSERT INTO `freetplbanners_sizes` VALUES(2, 60, 468, '31647831.jpg');
INSERT INTO `freetplbanners_sizes` VALUES(1, 60, 234, '78742786.jpg');
INSERT INTO `freetplbanners_sizes` VALUES(4, 578, 160, '585256.gif');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanners_styles`
--

CREATE TABLE `freetplbanners_styles` (
  `id` bigint(20) NOT NULL auto_increment,
  `title` varchar(255) collate latin1_general_ci default NULL,
  `page_bg` varchar(255) collate latin1_general_ci default NULL,
  `table_bg` varchar(255) collate latin1_general_ci default NULL,
  `seperator` varchar(255) collate latin1_general_ci default NULL,
  `title_bg` varchar(255) collate latin1_general_ci default NULL,
  `title_font_color` varchar(255) collate latin1_general_ci default NULL,
  `title_font` varchar(255) collate latin1_general_ci default NULL,
  `title_font_size` varchar(255) collate latin1_general_ci default NULL,
  `normal_font_color` varchar(255) collate latin1_general_ci default NULL,
  `normal_font` varchar(255) collate latin1_general_ci default NULL,
  `normal_font_size` varchar(255) collate latin1_general_ci default NULL,
  `normal_table_bg` varchar(255) collate latin1_general_ci default NULL,
  `tb_font_size` varchar(255) collate latin1_general_ci default NULL,
  `tb_font` varchar(255) collate latin1_general_ci default NULL,
  `tb_font_color` varchar(255) collate latin1_general_ci default NULL,
  `search_font` varchar(255) collate latin1_general_ci default NULL,
  `search_font_size` varchar(255) collate latin1_general_ci default NULL,
  `search_font_color` varchar(255) collate latin1_general_ci default NULL,
  `stat_yes_color` varchar(255) collate latin1_general_ci default NULL,
  `stat_no_color` varchar(255) collate latin1_general_ci default NULL,
  `link_font` varchar(255) collate latin1_general_ci NOT NULL default '',
  `link_font_color` varchar(255) collate latin1_general_ci NOT NULL default '',
  `link_font_size` varchar(255) collate latin1_general_ci NOT NULL default '',
  `highlight_bg_color` varchar(255) collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `freetplbanners_styles`
--

INSERT INTO `freetplbanners_styles` VALUES(4, 'Red', 'ffffff', 'F5F5F5', 'C33100', 'C33100', 'ffffff', 'Arial', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'F5F5F5', '12', 'Arial', '003333', 'Arial', '12', '003333', 'FFE8E8', 'ff0000', 'Arial, Helvetica, sans-serif', '990000', '12', 'FFD5D5');
INSERT INTO `freetplbanners_styles` VALUES(2, 'Blue', 'ECF5FF', 'FFFFFF', '0068C3', '0068C3', 'ffffff', 'Arial, Helvetica, sans-serif', '18', '000000', 'Arial, Helvetica, sans-serif', '12', 'FFFFFF', '11', 'Arial, Helvetica, sans-serif', '990000', 'Arial, Helvetica, sans-serif', '10', '000033', 'F0F8FF', 'E1002D', 'Arial, Helvetica, sans-serif', '0068C3', '12', '9DDFFF');
INSERT INTO `freetplbanners_styles` VALUES(3, 'Gray - Black', 'ffffff', 'FAFAFA', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', '12', 'Arial, Helvetica, sans-serif', '000000', 'Arial, Helvetica, sans-serif', '12', '000000', 'F5F5F5', 'FFFFFF', 'Arial, Helvetica, sans-serif', '262626', '12', 'EEEEEE');
INSERT INTO `freetplbanners_styles` VALUES(8, 'Green', 'ffffff', 'F4FFFA', '0E6900', '0E6900', 'ffffff', NULL, NULL, '000000', 'Arial, Helvetica, sans-serif', '12', 'F4FFFA', NULL, NULL, NULL, NULL, NULL, NULL, 'E9FFEE', '00B9C3', 'Arial, Helvetica, sans-serif', '0E6900', '12', 'F4FFFA');
INSERT INTO `freetplbanners_styles` VALUES(9, 'Orange', 'ffffff', 'FFEFE1', 'FF7300', 'FF7300', 'ffffff', NULL, NULL, '000000', 'Arial, Helvetica, sans-serif', '12', 'FFEFE1', NULL, NULL, NULL, NULL, NULL, NULL, 'FFDDCC', 'FF6600', 'Arial, Helvetica, sans-serif', 'FF7300', '12', 'FFCCBB');

-- --------------------------------------------------------

--
-- Table structure for table `freetplbanner_sizes`
--

CREATE TABLE `freetplbanner_sizes` (
  `id` bigint(20) NOT NULL auto_increment,
  `height` int(11) default NULL,
  `width` int(11) default NULL,
  `default_banner` varchar(255) collate latin1_general_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `freetplbanner_sizes`
--

INSERT INTO `freetplbanner_sizes` VALUES(2, 60, 468, '31647831.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_additional_fields`
--

CREATE TABLE `freetplclassified_additional_fields` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_type` varchar(255) default NULL,
  `freetpl_name` varchar(255) default NULL,
  `freetpl_label` varchar(255) default NULL,
  `freetpl_int_value` longtext,
  `freetpl_req` varchar(255) default NULL,
  `freetpl_display` varchar(255) default NULL,
  `freetpl_width` int(11) default NULL,
  `freetpl_num_lines` int(11) default NULL,
  `freetpl_is_num` varchar(255) default 'no',
  `freetpl_common` varchar(255) NOT NULL default 'no',
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `freetplclassified_additional_fields`
--

INSERT INTO `freetplclassified_additional_fields` VALUES(9, 'text', 'freetpl_quantity', 'Quantity Available', '', 'yes', 'yes', 10, 0, 'yes', 'no');
INSERT INTO `freetplclassified_additional_fields` VALUES(10, 'text', 'freetpl_ship_cost', 'Shipping Cost', '', 'no', 'yes', 10, 0, 'yes', 'no');
INSERT INTO `freetplclassified_additional_fields` VALUES(11, 'select', 'freetpl_who_pay_sc', 'Who will pay shipping cost?', '+Buyer|Seller', 'yes', 'yes', 0, 0, 'no', 'no');
INSERT INTO `freetplclassified_additional_fields` VALUES(8, 'text', 'freetpl_price', 'Price', '', 'yes', 'yes', 10, 0, 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_admin`
--

CREATE TABLE `freetplclassified_admin` (
  `id` bigint(20) NOT NULL auto_increment,
  `admin_name` varchar(255) default NULL,
  `pwd` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `freetplclassified_admin`
--

INSERT INTO `freetplclassified_admin` VALUES(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_ads`
--

CREATE TABLE `freetplclassified_ads` (
  `id` bigint(20) NOT NULL auto_increment,
  `url` varchar(255) NOT NULL default '',
  `bannerurl` varchar(255) NOT NULL default '',
  `adv_id` bigint(20) NOT NULL default '0',
  `credits` bigint(20) NOT NULL default '0',
  `displays` bigint(20) NOT NULL default '0',
  `paid` varchar(255) NOT NULL default '',
  `approved` varchar(255) NOT NULL default '',
  `clicks` bigint(20) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `freetplclassified_ads`
--

INSERT INTO `freetplclassified_ads` VALUES(1, 'sdds', 'dss', 0, 1221, 1039, 'yes', 'no', 1);
INSERT INTO `freetplclassified_ads` VALUES(3, 'http://www.vin.edu', 'http://www.v.e/ee.gif', 2, 1000, 1000, 'yes', 'yes', 4);

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_advertisers`
--

CREATE TABLE `freetplclassified_advertisers` (
  `id` bigint(20) NOT NULL auto_increment,
  `uname` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `pwd` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `freetplclassified_advertisers`
--

INSERT INTO `freetplclassified_advertisers` VALUES(2, 'vine', 'vin@qq.com', 'vin');
INSERT INTO `freetplclassified_advertisers` VALUES(4, 'richie', 'richie@r.com', 'a');
INSERT INTO `freetplclassified_advertisers` VALUES(6, 'vikings', 'vikings@v.com', 'aaaa');
INSERT INTO `freetplclassified_advertisers` VALUES(7, 'graf', 'graf@g.com', 'g');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_adv_transactions`
--

CREATE TABLE `freetplclassified_adv_transactions` (
  `id` bigint(20) NOT NULL auto_increment,
  `description` varchar(255) default NULL,
  `amount` decimal(10,2) default NULL,
  `adv_id` bigint(20) default NULL,
  `date_submitted` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `freetplclassified_adv_transactions`
--

INSERT INTO `freetplclassified_adv_transactions` VALUES(8, 'Banner Posting Charges', '-9.00', 2, '2004-03-27 16:17:46');
INSERT INTO `freetplclassified_adv_transactions` VALUES(9, 'Money added through paypal', '10.00', 2, '2004-03-27 04:45:04');
INSERT INTO `freetplclassified_adv_transactions` VALUES(10, 'Purchased More Impressions', '-9.00', 2, '2004-03-27 05:00:22');
INSERT INTO `freetplclassified_adv_transactions` VALUES(16, 'Added by admin', '10.00', 2, '2004-03-29 05:11:38');
INSERT INTO `freetplclassified_adv_transactions` VALUES(17, 'Banner Posting Charges', '-6.00', 2, '2004-07-13 17:39:17');
INSERT INTO `freetplclassified_adv_transactions` VALUES(7, 'Added Money', '10.00', 2, '2004-03-27 15:57:03');
INSERT INTO `freetplclassified_adv_transactions` VALUES(18, 'Added by Admin', '1000.00', 2, '2004-07-15 18:30:23');
INSERT INTO `freetplclassified_adv_transactions` VALUES(19, 'Banner Posting Charges', '-9.00', 2, '2004-07-15 06:30:48');
INSERT INTO `freetplclassified_adv_transactions` VALUES(20, 'Banner Posting Charges', '-9.00', 2, '2004-07-15 06:46:50');
INSERT INTO `freetplclassified_adv_transactions` VALUES(21, 'Banner Posting Charges', '-9.00', 2, '2004-09-06 03:29:27');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_auction_types`
--

CREATE TABLE `freetplclassified_auction_types` (
  `id` bigint(20) NOT NULL auto_increment,
  `auction_name` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `freetplclassified_auction_types`
--

INSERT INTO `freetplclassified_auction_types` VALUES(3, 'fixed');
INSERT INTO `freetplclassified_auction_types` VALUES(2, 'dutch');
INSERT INTO `freetplclassified_auction_types` VALUES(1, 'auction');
INSERT INTO `freetplclassified_auction_types` VALUES(4, 'classified');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_blocked`
--

CREATE TABLE `freetplclassified_blocked` (
  `id` bigint(20) NOT NULL auto_increment,
  `uid` bigint(20) default NULL,
  `blocked_user` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `freetplclassified_blocked`
--


-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_categories`
--

CREATE TABLE `freetplclassified_categories` (
  `id` bigint(20) NOT NULL auto_increment,
  `cat_name` varchar(255) default NULL,
  `pid` bigint(20) default NULL,
  `clicks` bigint(20) default NULL,
  `freetpl_fee` decimal(10,2) NOT NULL default '0.00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=265 ;

--
-- Dumping data for table `freetplclassified_categories`
--

INSERT INTO `freetplclassified_categories` VALUES(118, 'Aerospace And Defense', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(117, 'Accounting', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(119, 'Automotive', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(120, 'Commercial Goods and Services ', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(131, 'Agriculture', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(132, 'Chemicals', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(133, 'Computers and Software', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(134, 'Energy and Environment ', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(135, 'Food and Beverage', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(121, 'Electronics and Semiconductors', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(122, 'Financial Services ', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(123, 'Government and Trade', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(124, 'Human Resources', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(125, 'Law ', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(126, 'Media and Entertainment', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(127, 'Pharmaceuticals and Biotechnology', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(128, 'Retail and Consumer Services', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(129, 'Telecommunications', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(130, 'Advertising and Marketing ', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(136, 'Healthcare', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(137, 'Internet and Online', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(138, 'Management', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(139, 'Merchandise', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(140, 'Real Estate and Construction', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(141, 'Small Business', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(142, 'Transportation and Logistics', 0, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(143, 'Accounts Payable', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(144, 'Asset Analysis', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(145, 'Budgeting & Forecasting', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(146, 'CPAs/Firms', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(147, 'Liability Analysis', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(148, 'Payroll Accounting', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(149, 'Tax', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(150, 'Accounts Receivable', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(151, 'Auditing & Fraud', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(152, 'Consulting Services', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(153, 'Financial Statements', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(154, 'Managerial Accounting ', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(155, 'Software', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(156, 'Unclaimed Property & Escheat ', 117, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(157, 'Advertising', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(158, 'Copywriting ', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(159, 'Customer Service', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(160, 'Event & Conference Planning', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(161, 'Online Marketing', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(162, 'Promotional Products', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(163, 'Public Relations', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(164, 'Segmentation & Targeting', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(165, 'Strategic Planning', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(166, 'Consulting Services', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(167, 'Corporate Branding & Identity ', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(168, 'Direct Marketing ', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(169, 'Market Research', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(170, 'Product Development & Management', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(171, 'Promotions', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(172, 'Sales', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(173, 'Software', 130, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(174, 'Aircraft ', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(175, 'B2B Markets', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(176, 'Electronics', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(177, 'Military ', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(178, 'Research & Development', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(179, 'Space', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(180, 'Aviation/Aerospace Medicine', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(181, 'Consulting & Engineering Services', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(182, 'Equipment & Supplies', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(183, 'Navigation, Guidance & Detection', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(184, 'Software', 118, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(185, 'Aquaculture ', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(186, 'Barns & Structures', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(187, 'Commodities Markets', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(188, 'Cooperatives', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(189, 'Equipment & Supplies', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(190, 'Fertilizers & Pesticides', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(191, 'Forestry ', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(192, 'Insects ', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(193, 'Precision Agriculture', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(194, 'Biotechnology', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(195, 'Dairy ', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(196, 'Farm Management', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(197, 'Horticulture & Field Crops', 131, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(198, 'Aftermarket ', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(199, 'Dealerships', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(200, 'Fleet Management', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(201, 'Manufacturers & Distributors', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(202, 'Repair & Restoration ', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(203, 'Valet Parking Services', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(204, 'Finance ', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(205, 'Internet Buying Services', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(206, 'Parts & Equipment', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(207, 'Telematics', 119, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(208, 'Commodity Chemicals ', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(209, 'Distributors & Wholesalers', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(210, 'Fine & Specialty Chemicals', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(211, 'Petrochemicals', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(212, 'Polymers', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(213, 'Catalysts ', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(214, 'Equipment & Supplies', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(215, 'Gases ', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(216, 'Pharmaceuticals', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(217, 'Process Services', 132, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(218, 'Janitorial Services', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(219, 'Manufacturing & Process', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(220, 'Materials', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(221, 'Paper Shredding', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(222, 'Machinery & Tools', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(223, 'Material Handling', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(224, 'Packaging & Containers', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(225, 'Photography', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(226, 'Testing Laboratories & Services', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(227, 'Safety Consulting', 120, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(228, 'Graphic Design', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(229, 'Networking ', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(230, 'Software Applications', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(231, 'Computer Services', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(232, 'Desktop & Portable Computers', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(233, 'Embedded Systems', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(234, 'Hardware & Accessories', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(235, 'Security', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(236, 'Software Development', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(237, 'Wireless', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(238, 'Web Hosting', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(239, 'Reseller Web Hosting', 133, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(240, 'Application Service Providers', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(241, 'Directories & Portals', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(242, 'Ecommerce', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(243, 'Internet Service Providers', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(244, 'Software ', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(245, 'Web Site Hosting', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(246, 'Reseller Web Hosting', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(247, 'Domain Services', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(248, 'Personal Web Tools', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(249, 'Site Management', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(250, 'Streaming Media ', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(251, 'Web Site Development', 137, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(252, 'Career Management & Planning', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(253, 'Employee Development & Training', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(254, 'Expatriate Services ', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(255, 'Labor & Employment Law', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(256, 'Outsourcing ', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(257, 'Workplace Health & Safety', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(258, 'Compensation & Benefits', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(259, 'Diversity', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(260, 'Employment Services', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(261, 'Hiring & Retention ', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(262, 'Workforce Management', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(263, 'Software', 124, NULL, '0.00');
INSERT INTO `freetplclassified_categories` VALUES(264, 'Beauty Care', 0, NULL, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_config`
--

CREATE TABLE `freetplclassified_config` (
  `id` bigint(20) NOT NULL auto_increment,
  `admin_email` varchar(255) default NULL,
  `freetpl_site_name` varchar(255) default NULL,
  `freetpl_site_keywords` varchar(255) default NULL,
  `freetpl_logo` varchar(255) default NULL,
  `recperpage` int(11) default NULL,
  `freetpl_paypal_id` varchar(255) default NULL,
  `freetpl_seller_id` varchar(255) default '0',
  `freetpl_enable_paypal` varchar(255) default NULL,
  `freetpl_enable_checkout` varchar(255) default NULL,
  `freetpl_enable_offline` varchar(255) default NULL,
  `freetpl_billing_terms` longtext,
  `freetpl_offline_details` longtext,
  `no_of_images` int(11) default NULL,
  `agreement` longtext,
  `recinpanel` int(11) default NULL,
  `null_char` varchar(255) default NULL,
  `privacy` longtext,
  `legal` longtext,
  `terms` longtext,
  `username_len` int(11) default NULL,
  `pwd_len` int(11) default NULL,
  `site_root` varchar(255) default NULL,
  `featured_items` int(11) default NULL,
  `fp_images` int(11) default '0',
  `bonus` decimal(10,2) default NULL,
  `bold_rate` decimal(10,2) default NULL,
  `featured_rate` decimal(10,2) default NULL,
  `highlight_rate` decimal(10,2) default NULL,
  `free_images` int(10) default NULL,
  `image_rate` decimal(10,2) default NULL,
  `buy_images` int(10) default NULL,
  `welcome_msg` longtext,
  `max_period` int(10) default NULL,
  `fp_featured_rate` decimal(10,2) default NULL,
  `gallery_featured_rate` decimal(10,2) default NULL,
  `item_fees` decimal(10,2) default '0.00',
  `image_size` bigint(20) default '60',
  `buy_now` decimal(10,2) default NULL,
  `cur_id` bigint(20) default '1',
  `max_ext_period` int(11) default '10',
  `ext_cost` decimal(10,2) default '0.00',
  `banner_approval` varchar(50) NOT NULL default 'admin',
  `contact_info` varchar(50) default 'no',
  `freetpl_date_format` bigint(20) default '5',
  `freetpl_time_format` bigint(20) default '2',
  `freetpl_style_list` bigint(20) default '2',
  `freetpl_icon_list` bigint(20) default '2',
  `featured_cnt` bigint(20) default NULL,
  `elapsed_date` varchar(255) default 'yes',
  `show_stat` varchar(255) default 'no',
  `cat_choosen` varchar(255) default 'drop',
  `image_magik` varchar(255) default NULL,
  `water_marking` varchar(255) default NULL,
  `th_width` bigint(20) default NULL,
  `th_width2` bigint(20) default NULL,
  `premium_cnt` int(11) default '5',
  `layout_listing` int(11) default '1',
  `freetpl_show_color` varchar(50) default 'yes',
  `freetpl_default_image` varchar(255) default NULL,
  `freetpl_prod_approval` varchar(255) default 'auto',
  `freetpl_mem_approval` varchar(255) default 'auto',
  `freetpl_same_fee` varchar(255) NOT NULL default 'yes',
  `freetpl_signup_verification` varchar(255) NOT NULL default 'no',
  `freetplshow_extra_shipping` varchar(255) NOT NULL default 'no',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `freetplclassified_config`
--

INSERT INTO `freetplclassified_config` VALUES(1, 'demo@biglobangs.com', 'Biglobangs Business Listing Directorys', 'business listing, classified ads,classifieds, business ads, ads directory script', '4397043.gif', 5, 'sales@pozscripts.com', '1235676', 'yes', 'no', 'yes', 'Billing Terms Here Billing Terms HereBilling Terms HereBilling Terms HereBilling Terms HereBilling Terms HereBilling Terms HereBilling Terms HereBilling Terms HereBilling Terms HereBilling Terms HereBilling Terms HereBilling Terms Here', 'Please send DD/Cheque at the following address:<b>\r\nA/c No. -XXXXXXX \r\nXYZ Bank\r\nABC City</b>\r\nFunds will be transferred to your account as soon as payment has been confirmed, Thanks', 6, 'Your Agreement Here Your Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\nYour Agreement Here\r\n\r\n', 10, '--', 'coming soon..........', 'coming soon..........', 'coming soon..........', 3, 4, 'http://www.biglobangs.com/linkdirectory/', 9, 2, '20.00', '0.50', '0.75', '0.60', 3, '1.00', 5, '<font size="2" face="Arial, Helvetica, sans-serif"><strong>Business Listing Directory</strong></font> Online\r\n <p><font size="2" face="Arial, Helvetica, sans-serif">Business Listing Directory \r\n  Onlilne comes with  features below:</font></p>\r\n <table width="616" border="0" align="left" bordercolor="#9ab9ee">\r\n  <tr>\r\n    <td width="295" bgcolor="#9ab9ee" scope="col"><font color="#000000" size="2" face="Tahoma">Favorites management</font></td>\r\n    <td width="278" bgcolor="#9ab9ee" scope="col"><font color="#000000" size="2" face="Tahoma">Secure gateway integration</font></td>\r\n  </tr>\r\n  <tr>\r\n    <td bgcolor="#9ab9ee"><font color="#000000" size="2" face="Tahoma">Support for contact \r\n    list, block list</font></td>\r\n    <td bgcolor="#9ab9ee"><font color="#000000" size="2" face="Tahoma">Full Internal Messaging \r\n    system</font></td>\r\n  </tr>\r\n  <tr>\r\n    <td bgcolor="#9ab9ee"><font color="#000000" size="2" face="Tahoma">Preconfigured for paypal, \r\n    2Checkout and offline payments</font></td>\r\n    <td bgcolor="#9ab9ee"><font color="#000000" size="2" face="Tahoma">WYSIWYG editor for ad \r\n    posting</font></td>\r\n  </tr>\r\n  <tr>\r\n    <td bgcolor="#9ab9ee"><font color="#000000" size="2" face="Tahoma">Direct selling through \r\n    paypal for sellers</font></td>\r\n    <td bgcolor="#9ab9ee"><font color="#000000" size="2" face="Tahoma">Seller Stores</font></td>\r\n  </tr>\r\n  <tr>\r\n    <td bgcolor="#9ab9ee"><font color="#000000" size="2" face="Tahoma">Support for featured, \r\n    highlightes and gallery featured</font></td>\r\n    <td bgcolor="#9ab9ee"><font size="2" face="Tahoma">Rotation Banner Ads System ( See Top Banner)</font>  </td>\r\n  </tr>\r\n</table>\r\n', 1000, '0.70', '0.80', '1.00', 60000, '1.00', 1, 5, '0.05', 'admin', 'yes', 6, 1, 0, 2, 6, 'yes', 'yes', 'pop', 'disable', 'disable', 80, 70, 0, 1, 'no', '7238557.gif', 'auto', 'auto', 'yes', 'no', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_contacts`
--

CREATE TABLE `freetplclassified_contacts` (
  `id` bigint(20) NOT NULL auto_increment,
  `uid` bigint(20) default NULL,
  `contact_user` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `freetplclassified_contacts`
--

INSERT INTO `freetplclassified_contacts` VALUES(21, 59, 58);

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_counters`
--

CREATE TABLE `freetplclassified_counters` (
  `id` bigint(20) NOT NULL auto_increment,
  `zero` varchar(255) default NULL,
  `one` varchar(255) default NULL,
  `two` varchar(255) default NULL,
  `three` varchar(255) default NULL,
  `four` varchar(255) default NULL,
  `five` varchar(255) default NULL,
  `six` varchar(255) default NULL,
  `seven` varchar(255) default NULL,
  `eight` varchar(255) default NULL,
  `nine` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `freetplclassified_counters`
--

INSERT INTO `freetplclassified_counters` VALUES(1, '0.gif', '1.gif', '2.gif', '3.gif', '4.gif', '5.gif', '6.gif', '7.gif', '8.gif', '9.gif');
INSERT INTO `freetplclassified_counters` VALUES(2, 'a0.gif', 'a1.gif', 'a2.gif', 'a3.gif', 'a4.gif', 'a5.gif', 'a6.gif', 'a7.gif', 'a8.gif', 'a9.gif');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_country`
--

CREATE TABLE `freetplclassified_country` (
  `country` varchar(255) NOT NULL default '',
  `id` bigint(20) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=223 ;

--
-- Dumping data for table `freetplclassified_country`
--

INSERT INTO `freetplclassified_country` VALUES('Afghanistan', 1);
INSERT INTO `freetplclassified_country` VALUES('Albania', 2);
INSERT INTO `freetplclassified_country` VALUES('Algeria', 3);
INSERT INTO `freetplclassified_country` VALUES('Andorra', 4);
INSERT INTO `freetplclassified_country` VALUES('Angola', 5);
INSERT INTO `freetplclassified_country` VALUES('Anguilla', 6);
INSERT INTO `freetplclassified_country` VALUES('Antigua & Barbuda', 7);
INSERT INTO `freetplclassified_country` VALUES('Argentina', 8);
INSERT INTO `freetplclassified_country` VALUES('Armenia', 9);
INSERT INTO `freetplclassified_country` VALUES('Austria', 10);
INSERT INTO `freetplclassified_country` VALUES('Azerbaijan', 11);
INSERT INTO `freetplclassified_country` VALUES('Bahamas', 12);
INSERT INTO `freetplclassified_country` VALUES('Bahrain', 13);
INSERT INTO `freetplclassified_country` VALUES('Bangladesh', 14);
INSERT INTO `freetplclassified_country` VALUES('Barbados', 15);
INSERT INTO `freetplclassified_country` VALUES('Belarus', 16);
INSERT INTO `freetplclassified_country` VALUES('Belgium', 17);
INSERT INTO `freetplclassified_country` VALUES('Belize', 18);
INSERT INTO `freetplclassified_country` VALUES('Belize', 19);
INSERT INTO `freetplclassified_country` VALUES('Bermuda', 20);
INSERT INTO `freetplclassified_country` VALUES('Bhutan', 21);
INSERT INTO `freetplclassified_country` VALUES('Bolivia', 22);
INSERT INTO `freetplclassified_country` VALUES('Bosnia and Herzegovina', 23);
INSERT INTO `freetplclassified_country` VALUES('Botswana', 24);
INSERT INTO `freetplclassified_country` VALUES('Brazil', 25);
INSERT INTO `freetplclassified_country` VALUES('Brunei', 26);
INSERT INTO `freetplclassified_country` VALUES('Bulgaria', 27);
INSERT INTO `freetplclassified_country` VALUES('Burkina Faso', 28);
INSERT INTO `freetplclassified_country` VALUES('Burundi', 29);
INSERT INTO `freetplclassified_country` VALUES('Cambodia', 30);
INSERT INTO `freetplclassified_country` VALUES('Cameroon', 31);
INSERT INTO `freetplclassified_country` VALUES('Canada', 32);
INSERT INTO `freetplclassified_country` VALUES('Cape Verde', 33);
INSERT INTO `freetplclassified_country` VALUES('Cayman Islands', 34);
INSERT INTO `freetplclassified_country` VALUES('Central African Republic', 35);
INSERT INTO `freetplclassified_country` VALUES('Chad', 36);
INSERT INTO `freetplclassified_country` VALUES('Chile', 37);
INSERT INTO `freetplclassified_country` VALUES('China', 38);
INSERT INTO `freetplclassified_country` VALUES('Colombia', 39);
INSERT INTO `freetplclassified_country` VALUES('Comoros', 40);
INSERT INTO `freetplclassified_country` VALUES('Congo', 41);
INSERT INTO `freetplclassified_country` VALUES('Congo (DRC)', 42);
INSERT INTO `freetplclassified_country` VALUES('Cook Islands', 43);
INSERT INTO `freetplclassified_country` VALUES('Costa Rica', 44);
INSERT INTO `freetplclassified_country` VALUES('Cote d''Ivoire', 45);
INSERT INTO `freetplclassified_country` VALUES('Croatia (Hrvatska)', 46);
INSERT INTO `freetplclassified_country` VALUES('Cuba', 47);
INSERT INTO `freetplclassified_country` VALUES('Cyprus', 48);
INSERT INTO `freetplclassified_country` VALUES('Czech Republic', 49);
INSERT INTO `freetplclassified_country` VALUES('Denmark', 50);
INSERT INTO `freetplclassified_country` VALUES('Djibouti', 51);
INSERT INTO `freetplclassified_country` VALUES('Dominica', 52);
INSERT INTO `freetplclassified_country` VALUES('Dominican Republic', 53);
INSERT INTO `freetplclassified_country` VALUES('East Timor', 54);
INSERT INTO `freetplclassified_country` VALUES('Ecuador', 55);
INSERT INTO `freetplclassified_country` VALUES('Egypt', 56);
INSERT INTO `freetplclassified_country` VALUES('El Salvador', 57);
INSERT INTO `freetplclassified_country` VALUES('Equatorial Guinea', 58);
INSERT INTO `freetplclassified_country` VALUES('Eritrea', 59);
INSERT INTO `freetplclassified_country` VALUES('Estonia', 60);
INSERT INTO `freetplclassified_country` VALUES('Ethiopia', 61);
INSERT INTO `freetplclassified_country` VALUES('Falkland Islands', 62);
INSERT INTO `freetplclassified_country` VALUES('Faroe Islands', 63);
INSERT INTO `freetplclassified_country` VALUES('Fiji Islands', 64);
INSERT INTO `freetplclassified_country` VALUES('Finland', 65);
INSERT INTO `freetplclassified_country` VALUES('France', 66);
INSERT INTO `freetplclassified_country` VALUES('French Guiana', 67);
INSERT INTO `freetplclassified_country` VALUES('French Polynesia', 68);
INSERT INTO `freetplclassified_country` VALUES('Gabon', 69);
INSERT INTO `freetplclassified_country` VALUES('Gambia', 70);
INSERT INTO `freetplclassified_country` VALUES('Georgia', 71);
INSERT INTO `freetplclassified_country` VALUES('Germany', 72);
INSERT INTO `freetplclassified_country` VALUES('Ghana', 73);
INSERT INTO `freetplclassified_country` VALUES('Gibraltar', 74);
INSERT INTO `freetplclassified_country` VALUES('Greece', 75);
INSERT INTO `freetplclassified_country` VALUES('Greenland', 76);
INSERT INTO `freetplclassified_country` VALUES('Grenada', 77);
INSERT INTO `freetplclassified_country` VALUES('Guadeloupe', 78);
INSERT INTO `freetplclassified_country` VALUES('Guam', 79);
INSERT INTO `freetplclassified_country` VALUES('Guatemala', 80);
INSERT INTO `freetplclassified_country` VALUES('Guinea', 81);
INSERT INTO `freetplclassified_country` VALUES('Guinea-Bissau', 82);
INSERT INTO `freetplclassified_country` VALUES('Guyana', 83);
INSERT INTO `freetplclassified_country` VALUES('Haiti', 84);
INSERT INTO `freetplclassified_country` VALUES('Honduras', 85);
INSERT INTO `freetplclassified_country` VALUES('Hong Kong SAR', 86);
INSERT INTO `freetplclassified_country` VALUES('Hungary', 87);
INSERT INTO `freetplclassified_country` VALUES('Iceland', 88);
INSERT INTO `freetplclassified_country` VALUES('India', 89);
INSERT INTO `freetplclassified_country` VALUES('Indonesia', 90);
INSERT INTO `freetplclassified_country` VALUES('Iran', 91);
INSERT INTO `freetplclassified_country` VALUES('Iraq', 92);
INSERT INTO `freetplclassified_country` VALUES('Ireland', 93);
INSERT INTO `freetplclassified_country` VALUES('Israel', 94);
INSERT INTO `freetplclassified_country` VALUES('Italy', 95);
INSERT INTO `freetplclassified_country` VALUES('Jamaica', 96);
INSERT INTO `freetplclassified_country` VALUES('Japan', 97);
INSERT INTO `freetplclassified_country` VALUES('Jordan', 98);
INSERT INTO `freetplclassified_country` VALUES('Kazakhstan', 99);
INSERT INTO `freetplclassified_country` VALUES('Kenya', 100);
INSERT INTO `freetplclassified_country` VALUES('Kiribati', 101);
INSERT INTO `freetplclassified_country` VALUES('Korea', 102);
INSERT INTO `freetplclassified_country` VALUES('Kuwait', 103);
INSERT INTO `freetplclassified_country` VALUES('Kyrgyzstan', 104);
INSERT INTO `freetplclassified_country` VALUES('Laos', 105);
INSERT INTO `freetplclassified_country` VALUES('Latvia', 106);
INSERT INTO `freetplclassified_country` VALUES('Lebanon', 107);
INSERT INTO `freetplclassified_country` VALUES('Lesotho', 108);
INSERT INTO `freetplclassified_country` VALUES('Liberia', 109);
INSERT INTO `freetplclassified_country` VALUES('Libya', 110);
INSERT INTO `freetplclassified_country` VALUES('Liechtenstein', 111);
INSERT INTO `freetplclassified_country` VALUES('Lithuania', 112);
INSERT INTO `freetplclassified_country` VALUES('Luxembourg', 113);
INSERT INTO `freetplclassified_country` VALUES('Macao SAR', 114);
INSERT INTO `freetplclassified_country` VALUES('Macedonia', 115);
INSERT INTO `freetplclassified_country` VALUES('Madagascar', 116);
INSERT INTO `freetplclassified_country` VALUES('Malawi', 117);
INSERT INTO `freetplclassified_country` VALUES('Malaysia', 118);
INSERT INTO `freetplclassified_country` VALUES('Maldives', 119);
INSERT INTO `freetplclassified_country` VALUES('Mali', 120);
INSERT INTO `freetplclassified_country` VALUES('Malta', 121);
INSERT INTO `freetplclassified_country` VALUES('Martinique', 122);
INSERT INTO `freetplclassified_country` VALUES('Mauritania', 123);
INSERT INTO `freetplclassified_country` VALUES('Mauritius', 124);
INSERT INTO `freetplclassified_country` VALUES('Mayotte', 125);
INSERT INTO `freetplclassified_country` VALUES('Mexico', 126);
INSERT INTO `freetplclassified_country` VALUES('Micronesia', 127);
INSERT INTO `freetplclassified_country` VALUES('Moldova', 128);
INSERT INTO `freetplclassified_country` VALUES('Monaco', 129);
INSERT INTO `freetplclassified_country` VALUES('Mongolia', 130);
INSERT INTO `freetplclassified_country` VALUES('Montserrat', 131);
INSERT INTO `freetplclassified_country` VALUES('Morocco', 132);
INSERT INTO `freetplclassified_country` VALUES('Mozambique', 133);
INSERT INTO `freetplclassified_country` VALUES('Myanmar', 134);
INSERT INTO `freetplclassified_country` VALUES('Namibia', 135);
INSERT INTO `freetplclassified_country` VALUES('Nauru', 136);
INSERT INTO `freetplclassified_country` VALUES('Nepal', 137);
INSERT INTO `freetplclassified_country` VALUES('Netherlands', 138);
INSERT INTO `freetplclassified_country` VALUES('Netherlands Antilles', 139);
INSERT INTO `freetplclassified_country` VALUES('New Caledonia', 140);
INSERT INTO `freetplclassified_country` VALUES('New Zealand', 141);
INSERT INTO `freetplclassified_country` VALUES('Nicaragua', 142);
INSERT INTO `freetplclassified_country` VALUES('Niger', 143);
INSERT INTO `freetplclassified_country` VALUES('Nigeria', 144);
INSERT INTO `freetplclassified_country` VALUES('Niue', 145);
INSERT INTO `freetplclassified_country` VALUES('Norfolk Island', 146);
INSERT INTO `freetplclassified_country` VALUES('North Korea', 147);
INSERT INTO `freetplclassified_country` VALUES('Norway', 148);
INSERT INTO `freetplclassified_country` VALUES('Oman', 149);
INSERT INTO `freetplclassified_country` VALUES('Pakistan', 150);
INSERT INTO `freetplclassified_country` VALUES('Panama', 151);
INSERT INTO `freetplclassified_country` VALUES('Papua New Guinea', 152);
INSERT INTO `freetplclassified_country` VALUES('Paraguay', 153);
INSERT INTO `freetplclassified_country` VALUES('Peru', 154);
INSERT INTO `freetplclassified_country` VALUES('Philippines', 155);
INSERT INTO `freetplclassified_country` VALUES('Pitcairn Islands', 156);
INSERT INTO `freetplclassified_country` VALUES('Poland', 157);
INSERT INTO `freetplclassified_country` VALUES('Portugal', 158);
INSERT INTO `freetplclassified_country` VALUES('Puerto Rico', 159);
INSERT INTO `freetplclassified_country` VALUES('Qatar', 160);
INSERT INTO `freetplclassified_country` VALUES('Reunion', 161);
INSERT INTO `freetplclassified_country` VALUES('Romania', 162);
INSERT INTO `freetplclassified_country` VALUES('Russia', 163);
INSERT INTO `freetplclassified_country` VALUES('Rwanda', 164);
INSERT INTO `freetplclassified_country` VALUES('Samoa', 165);
INSERT INTO `freetplclassified_country` VALUES('San Marino', 166);
INSERT INTO `freetplclassified_country` VALUES('Sao Tome and Principe', 167);
INSERT INTO `freetplclassified_country` VALUES('Saudi Arabia', 168);
INSERT INTO `freetplclassified_country` VALUES('Senegal', 169);
INSERT INTO `freetplclassified_country` VALUES('Serbia and Montenegro', 170);
INSERT INTO `freetplclassified_country` VALUES('Seychelles', 171);
INSERT INTO `freetplclassified_country` VALUES('Sierra Leone', 172);
INSERT INTO `freetplclassified_country` VALUES('Singapore', 173);
INSERT INTO `freetplclassified_country` VALUES('Slovakia', 174);
INSERT INTO `freetplclassified_country` VALUES('Slovenia', 175);
INSERT INTO `freetplclassified_country` VALUES('Solomon Islands', 176);
INSERT INTO `freetplclassified_country` VALUES('Somalia', 177);
INSERT INTO `freetplclassified_country` VALUES('South Africa', 178);
INSERT INTO `freetplclassified_country` VALUES('Spain', 179);
INSERT INTO `freetplclassified_country` VALUES('Sri Lanka', 180);
INSERT INTO `freetplclassified_country` VALUES('St. Helena', 181);
INSERT INTO `freetplclassified_country` VALUES('St. Kitts and Nevis', 182);
INSERT INTO `freetplclassified_country` VALUES('St. Lucia', 183);
INSERT INTO `freetplclassified_country` VALUES('St. Pierre and Miquelon', 184);
INSERT INTO `freetplclassified_country` VALUES('St. Vincent & Grenadines', 185);
INSERT INTO `freetplclassified_country` VALUES('Sudan', 186);
INSERT INTO `freetplclassified_country` VALUES('Suriname', 187);
INSERT INTO `freetplclassified_country` VALUES('Swaziland', 188);
INSERT INTO `freetplclassified_country` VALUES('Sweden', 189);
INSERT INTO `freetplclassified_country` VALUES('Switzerland', 190);
INSERT INTO `freetplclassified_country` VALUES('Syria', 191);
INSERT INTO `freetplclassified_country` VALUES('Taiwan', 192);
INSERT INTO `freetplclassified_country` VALUES('Tajikistan', 193);
INSERT INTO `freetplclassified_country` VALUES('Tanzania', 194);
INSERT INTO `freetplclassified_country` VALUES('Thailand', 195);
INSERT INTO `freetplclassified_country` VALUES('Togo', 196);
INSERT INTO `freetplclassified_country` VALUES('Tokelau', 197);
INSERT INTO `freetplclassified_country` VALUES('Tonga', 198);
INSERT INTO `freetplclassified_country` VALUES('Trinidad and Tobago', 199);
INSERT INTO `freetplclassified_country` VALUES('Tunisia', 200);
INSERT INTO `freetplclassified_country` VALUES('Turkey', 201);
INSERT INTO `freetplclassified_country` VALUES('Turkmenistan', 202);
INSERT INTO `freetplclassified_country` VALUES('Turks and Caicos Islands', 203);
INSERT INTO `freetplclassified_country` VALUES('Tuvalu', 204);
INSERT INTO `freetplclassified_country` VALUES('Uganda', 205);
INSERT INTO `freetplclassified_country` VALUES('Ukraine', 206);
INSERT INTO `freetplclassified_country` VALUES('United Arab Emirates', 207);
INSERT INTO `freetplclassified_country` VALUES('United Kingdom', 208);
INSERT INTO `freetplclassified_country` VALUES('Uruguay', 209);
INSERT INTO `freetplclassified_country` VALUES('USA', 210);
INSERT INTO `freetplclassified_country` VALUES('Uzbekistan', 211);
INSERT INTO `freetplclassified_country` VALUES('Vanuatu', 212);
INSERT INTO `freetplclassified_country` VALUES('Venezuela', 213);
INSERT INTO `freetplclassified_country` VALUES('Vietnam', 214);
INSERT INTO `freetplclassified_country` VALUES('Virgin Islands', 215);
INSERT INTO `freetplclassified_country` VALUES('Virgin Islands (British)', 216);
INSERT INTO `freetplclassified_country` VALUES('Wallis and Futuna', 217);
INSERT INTO `freetplclassified_country` VALUES('Yemen', 218);
INSERT INTO `freetplclassified_country` VALUES('Yugoslavia', 219);
INSERT INTO `freetplclassified_country` VALUES('Zambia', 220);
INSERT INTO `freetplclassified_country` VALUES('Zimbabwe', 221);
INSERT INTO `freetplclassified_country` VALUES('Australia', 222);

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_currency`
--

CREATE TABLE `freetplclassified_currency` (
  `id` bigint(20) NOT NULL auto_increment,
  `cur_name` varchar(255) default NULL,
  `paypal_code` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `freetplclassified_currency`
--

INSERT INTO `freetplclassified_currency` VALUES(1, '$', 'USD');
INSERT INTO `freetplclassified_currency` VALUES(2, '&pound;', 'GBP');
INSERT INTO `freetplclassified_currency` VALUES(3, '&euro;', 'EUR');
INSERT INTO `freetplclassified_currency` VALUES(4, '$', 'CAD');
INSERT INTO `freetplclassified_currency` VALUES(5, '&yen;', 'JPY');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_dateformats`
--

CREATE TABLE `freetplclassified_dateformats` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_format` varchar(255) default NULL,
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `freetplclassified_dateformats`
--

INSERT INTO `freetplclassified_dateformats` VALUES(1, '2004-06-29');
INSERT INTO `freetplclassified_dateformats` VALUES(2, '06-29-2004');
INSERT INTO `freetplclassified_dateformats` VALUES(3, '29-06-2004');
INSERT INTO `freetplclassified_dateformats` VALUES(4, '29 Jun 2004');
INSERT INTO `freetplclassified_dateformats` VALUES(5, '29 June 2004');
INSERT INTO `freetplclassified_dateformats` VALUES(6, 'Jun 29th,2004');
INSERT INTO `freetplclassified_dateformats` VALUES(7, 'Tue Jun 29th,2004');
INSERT INTO `freetplclassified_dateformats` VALUES(8, 'Tuesday Jun 29th,2004');
INSERT INTO `freetplclassified_dateformats` VALUES(9, 'Tuesday June 29th,2004');
INSERT INTO `freetplclassified_dateformats` VALUES(10, '29 June 2004 Tuesday');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_email_id`
--

CREATE TABLE `freetplclassified_email_id` (
  `id` bigint(20) NOT NULL auto_increment,
  `fname` varchar(255) default NULL,
  `lname` varchar(255) default NULL,
  `useremail` varchar(255) default NULL,
  `friend_email` longtext,
  `no_of_friends` int(11) default NULL,
  `sid` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `freetplclassified_email_id`
--

INSERT INTO `freetplclassified_email_id` VALUES(5, NULL, NULL, 'vv@vv.com', 'friend1@vv.com,friend2@vv.com', 2, 14);
INSERT INTO `freetplclassified_email_id` VALUES(6, NULL, NULL, 'asasa@www.com', 'sadasd@ww.com,ssss@www.cvom', 2, 14);
INSERT INTO `freetplclassified_email_id` VALUES(7, NULL, NULL, 'aa@www.com', ',', 2, 14);
INSERT INTO `freetplclassified_email_id` VALUES(8, NULL, NULL, '23423423', '23423,4234,23423,423423,4234', 5, 14);
INSERT INTO `freetplclassified_email_id` VALUES(9, 'fred', 'dedo', 'freddedo@g.com', 'friend1@vv.com,friend2@vv.com,friend3@vv.com,friend1@vv.com,friend1@vv.com', 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_favourites`
--

CREATE TABLE `freetplclassified_favourites` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) default NULL,
  `uid` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `freetplclassified_favourites`
--

INSERT INTO `freetplclassified_favourites` VALUES(16, 158, 59);

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_icons`
--

CREATE TABLE `freetplclassified_icons` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_title` varchar(255) default NULL,
  `freetpl_mem_opt` varchar(255) default NULL,
  `freetpl_contact_seller` varchar(255) default NULL,
  `freetpl_store` varchar(255) default NULL,
  `freetpl_add_fav` varchar(255) default NULL,
  `freetpl_buy_now` varchar(255) default NULL,
  `freetpl_bar` varchar(255) default NULL,
  `freetpl_details` varchar(255) default NULL,
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `freetplclassified_icons`
--

INSERT INTO `freetplclassified_icons` VALUES(2, 'icon2', '74225399.gif', '85377896.gif', '60973420.gif', '90224797.gif', '57991888.gif', '15482334.gif', '58496072.gif');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_images`
--

CREATE TABLE `freetplclassified_images` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) default NULL,
  `url` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=161 ;

--
-- Dumping data for table `freetplclassified_images`
--

INSERT INTO `freetplclassified_images` VALUES(5, 19, 'preimage1.gif');
INSERT INTO `freetplclassified_images` VALUES(4, 19, 'preimage.jpg');
INSERT INTO `freetplclassified_images` VALUES(7, 22, '615733COINS.jpg');
INSERT INTO `freetplclassified_images` VALUES(16, 36, '615733COINS.jpg');
INSERT INTO `freetplclassified_images` VALUES(151, 158, '6990412partners.jpg');
INSERT INTO `freetplclassified_images` VALUES(152, 159, '884462goto_advertisewithus.gif');
INSERT INTO `freetplclassified_images` VALUES(153, 160, '7048107777.jpg');
INSERT INTO `freetplclassified_images` VALUES(147, 154, '5736368333.jpg');
INSERT INTO `freetplclassified_images` VALUES(148, 155, '1626714444.jpg');
INSERT INTO `freetplclassified_images` VALUES(149, 156, '3934641555.jpg');
INSERT INTO `freetplclassified_images` VALUES(150, 157, '7172093666.jpg');
INSERT INTO `freetplclassified_images` VALUES(144, 151, '6170153logo-2b.png');
INSERT INTO `freetplclassified_images` VALUES(145, 152, '50617111.jpg');
INSERT INTO `freetplclassified_images` VALUES(146, 153, '6555844222.jpg');
INSERT INTO `freetplclassified_images` VALUES(154, 161, '3914876img_taketour.gif');
INSERT INTO `freetplclassified_images` VALUES(155, 162, '654355HELIOS-10-340.jpg');
INSERT INTO `freetplclassified_images` VALUES(156, 163, '4698525908811.gif');
INSERT INTO `freetplclassified_images` VALUES(157, 167, '6615368mango_raw.JPG');
INSERT INTO `freetplclassified_images` VALUES(158, 167, '4007702mango_raw.JPG');
INSERT INTO `freetplclassified_images` VALUES(159, 167, '4650150mango2.jpg');
INSERT INTO `freetplclassified_images` VALUES(160, 168, '4089415IPHONE_BLK.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_mails`
--

CREATE TABLE `freetplclassified_mails` (
  `id` bigint(20) NOT NULL auto_increment,
  `mailid` bigint(20) NOT NULL default '0',
  `fromid` varchar(255) NOT NULL default '',
  `subject` varchar(255) NOT NULL default '',
  `mail` longtext NOT NULL,
  `freetpl_status` varchar(255) default 'no',
  `freetpl_html_format` varchar(255) default 'no',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `freetplclassified_mails`
--

INSERT INTO `freetplclassified_mails` VALUES(2, 2, 'support@biglobangs.com', 'Your product has been Approved', '<P>Hi %fname% %lname%, <BR><BR>Your product %productname% has been Approved for inclusion on the site. <BR>You can view this product on %producturl% <BR><BR>Regards, <BR>Admin </P>\r\n<P>Biglobangs Classified Ads Online</P>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(3, 3, 'disapproval@classified.com', 'Your product has been disapproved', '<FONT face="Arial, Helvetica, sans-serif" size=2>Hi %fname% %lname%, <BR><BR>Your product %productname% has been disapproved from inclusion on the site. <BR><BR><BR>Regards, <BR>Admin</FONT> ', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(4, 4, 'support@biglobangs.com', 'Your password', 'Hi %fname% %lname%, <BR><BR>Here is your login information: <BR><BR>Username: %username% <BR>Password: %password% <BR>Email: %email% <BR><BR><BR>Thanks for being part of our website. <BR><BR>Regards, <BR>Admin <BR>For login click %loginurl% ', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(1, 1, 'support@biglobangs.com', 'Welcome to Biglobangs Classified Ads Online', '<P><FONT face="Arial, Helvetica, sans-serif" size=2>Hi %fname% %lname%, <BR><BR>Welcome to Biglobangs Classified Ads Online.<BR></FONT><FONT face="Arial, Helvetica, sans-serif" size=2></FONT></P>\r\n<P><FONT face="Arial, Helvetica, sans-serif" size=2>Welcome bonus of $20 has been added into your account. Your registration information is as follows: <BR><BR>Username: %username% <BR>Password: %password% <BR>Email: %email% <BR><BR>Thanks for being part of our website. <BR><BR>Regards, <BR>Admin <BR>Biglobangs Classified Ads</FONT></P>\r\n<P><FONT face="Arial, Helvetica, sans-serif" size=2>http://www.biglobangs.com<BR></P></FONT>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(5, 5, 'support@biglobangs.com', 'Your product has been Posted', '<P>Hi %fname%%lname%, <BR><BR>Your product %productname% has been Posted to our site.You can view this product at %producturl%. <BR><BR><BR>Regards, <BR>Admin </P>\r\n<P>Biglobangs Classified Ads Online</P>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(11, 11, 'admin@classified.com', 'Send Stat Mail', 'Hi \r<br>\r<br>There are statistics information regarding your banner display on our site::\r<br>Banner ::%bannerurl%\r<br>Displays::%displays%\r<br>credits ::%credits%\r<br>balance ::%balance%\r<br>\r<br>Regards,\r<br>Admin', 'yes', 'no');
INSERT INTO `freetplclassified_mails` VALUES(8, 8, 'support@biglobangs.com', 'You have Message from user at Biglobangs Classified Ads Online', '<P><FONT face="Arial, Helvetica, sans-serif" size=2>Hi %fname% %lname%, <BR><BR>You have been recieved a message <BR>From :: %sender_username% <BR>Title :: %message_title% <BR>Message :: %message_text% <BR>Time :: %message_time% <BR>Date :: %message_date% <BR><BR><BR>Regards, <BR>Admin</FONT> </P>\r\n<P>Biglobangs Classified Ads Online</P>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(6, 6, 'support@biglobangs.com', 'Purchase Request', '<P>Hi <FNAME><LNAME>, A purchase request for your product <PRODUCTNAME>, cost <PRODUCTCOST>has been Posted to our site.You can view this product at <PRODUCTURL>. </P>\r\n<P>Regards, </P>\r\n<P>Admin</P>\r\n<P>Biglobangs Classified Ads Online&nbsp;</P>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(7, 7, 'adv_password@classified.com', 'Your password', 'Hi %fname% , <BR><BR>Here is your login information: <BR><BR>Password: %password% <BR>Email: %email% <BR><BR><BR>Thanks for being part of our website. <BR><BR>Regards, <BR>Admin <BR>For login click %loginurl% ', 'yes', 'no');
INSERT INTO `freetplclassified_mails` VALUES(9, 9, 'info@freetpl.com', 'offline payment of members', '<P align=left>Hi admin,<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; this user has&nbsp;selected offline payment mode for payment<BR>Details of user :<BR>Username&nbsp;: %username%<BR>Email : <FONT face=Arial size=2>%email%</FONT><BR>Password : %password%<BR>Payment method :Offline<BR>Amount : %payment%<BR><BR>Thanks</P>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(10, 10, 'offline@classified.com', 'offline payment of advertisers', '<P align=left>Hi admin,<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; this advertiser has&nbsp;selected offline payment mode for payment<BR>Details of user :<BR>Username&nbsp;: %username%<BR>Email : <FONT face=Arial size=2>%email%</FONT><BR>Password : %password%<BR>Payment method :Offline<BR>Amount : %payment%<BR><BR>Thanks</P>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(25, 25, 'info@freetpl.com', 'Email verification mail', '<P>Hi, <BR><BR>Your link for proceeding the signup is given below, please follow the link to complete the registration process, </P>\r\n<P>Thanks for being part of the site.</P>\r\n<P>Email::%email% Link is %signup_url% <BR><BR><BR>Regards, <BR>Admin </P>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(26, 26, 'info@freetpl.com', 'New member signup waiting your approval', '<P>Hi admin, <BR><BR>A new member has signed-up and awaiting your approval. <BR>Full name: %lname%, %fname% <BR>Username: %username% <BR>Password: %password% <BR>Email Id: %email% <BR><BR>Regards, </P>\r\n<P>Business Directory Listing&nbsp;Automail </P>', 'yes', 'yes');
INSERT INTO `freetplclassified_mails` VALUES(21, 21, 'info@freetpl', 'A newly posted/updated item is awaiting your approval', '<P>Admin, </P>\r\n<P>A newly posted/updated item is awaiting your approval. </P>\r\n<P>The details are: </P>\r\n<P>Name::%fname% %lname% </P>\r\n<P>Username::%username% </P>\r\n<P>Password::%password% </P>\r\n<P>Email::%email% </P>\r\n<P>Item Title::%productname% </P>\r\n<P>Item <A href=":%producturl%">URL::%producturl%</A> </P>\r\n<P>Login URL:: %loginurl% </P>\r\n<P>FreeTpl Automail </P>', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_members`
--

CREATE TABLE `freetplclassified_members` (
  `id` bigint(20) NOT NULL auto_increment,
  `c_name` varchar(255) default NULL,
  `add1` varchar(255) default NULL,
  `add2` varchar(255) default NULL,
  `city` varchar(255) default NULL,
  `state` varchar(255) default NULL,
  `zip` varchar(50) default NULL,
  `country` varchar(255) default NULL,
  `home_phone` varchar(50) default NULL,
  `email` varchar(255) default NULL,
  `username` varchar(255) default NULL,
  `pwd` varchar(255) default NULL,
  `fname` varchar(255) default NULL,
  `lname` varchar(255) default NULL,
  `work_phone` varchar(50) default NULL,
  `logo_url` varchar(255) default NULL,
  `store_title` varchar(255) default NULL,
  `store_desc` longtext,
  `small_logo` varchar(255) default NULL,
  `premium_status` varchar(255) default 'no',
  `suspended` varchar(255) default 'no',
  `freetpltemp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `freetpl_signup_on` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `freetplclassified_members`
--

INSERT INTO `freetplclassified_members` VALUES(58, 'Biglobangs', 'Biglobangs.Com', '', 'USA', 'USA', '34566', '210', 'www.biglobangs.com', 'admin@iwebsglobal.com', 'admin', '888888', 'Admin', 'Admin', '888888', '5202329.jpg', 'Admin Store', 'Welcome To Admin Store', '8847436.jpg', 'no', 'no', '2007-11-10 01:03:32', '2007-11-08 01:05:56');
INSERT INTO `freetplclassified_members` VALUES(59, '', 'MYDVD68.Com', '', 'USA', 'USA', '34566', '210', 'www.biglobangs.com', 'supporDt@pozscripts.com', 'alan888', '888888', 'poo', 'Shop', '', '', '', '', NULL, 'no', 'no', '2007-11-09 20:44:11', '0000-00-00 00:00:00');
INSERT INTO `freetplclassified_members` VALUES(60, '', 'none', '', 'Malaysia', 'Malaysia', '34566', '26', '021548587', 'admin@singoon.com', 'aaa77', '888888', 'Alan', 'Admin', '', '', '', '', '', 'no', 'no', '2007-11-09 18:43:11', '2007-11-09 18:43:11');
INSERT INTO `freetplclassified_members` VALUES(61, '', 'singapore', '', 'Singapore', 'singapore', '34566', '173', 'www.biglobangs.com', 'alanbabf@singnet.com.sg', 'kerrynbaby', '888888', 'Mabel', 'Admin', '95874587', '', '', '', '', 'no', 'no', '2007-11-09 19:02:12', '2007-11-09 19:02:12');
INSERT INTO `freetplclassified_members` VALUES(62, '', 'Klang', '', 'Banting', 'Selangor', '42500', '118', 'www.wangxtra.com', 'wangxtra@yahoo.com', 'wangxtra', 'kambingg', 'Adi Surya', 'Umri', '', '', '', '', '', 'no', 'no', '2007-11-13 17:57:04', '2007-11-13 17:57:04');
INSERT INTO `freetplclassified_members` VALUES(63, '', '123', '', '123', 'ca', '12333', '205', 'http://www.yahoo.com', '123@YAHOO.COM', '123', '1234', '123', '123', '', '', '', '', '', 'no', 'no', '2007-11-29 06:49:59', '2007-11-29 06:49:59');
INSERT INTO `freetplclassified_members` VALUES(64, '', '1234  Alan Lane', '', 'tucson', 'az', '85706', '210', 'www.zclassifieds.biz', 'mccamacho@yahoo.com', 'solotu1', '1111', 'Michel ', 'Camacho', '', '', '', '', '', 'no', 'no', '2007-12-01 07:28:40', '2007-12-01 07:28:40');
INSERT INTO `freetplclassified_members` VALUES(65, '', '123 Paseo del Verano', '', 'San Diego', 'CA', '92128', '210', 'www.sleeptrader.com', 'time4sleeping@yahoo.com', 'time4sleeping', 'cassie1latte2', 'John', 'Garvey', '888-888-8888', '2812854.jpg', 'eKidsMart', 'The only online source for school websites and a family marketplace.', '4051129.jpg', 'no', 'no', '2007-12-14 10:36:58', '2007-12-14 10:26:03');
INSERT INTO `freetplclassified_members` VALUES(66, 'Above1', '51 Ubi Avenue 1, #05-08 (8B)', 'Paya Ubi Industrial Park', 'Singapore', 'Singapore', '408933', '173', 'http://www.above1.com', 'marklee64@gmail.com', 'marklee', 'plano123', 'Mark', 'Lee', '(65) 6846 2594', '', '', '', '', 'no', 'no', '2007-12-14 14:14:35', '2007-12-14 14:14:35');
INSERT INTO `freetplclassified_members` VALUES(67, 'Maisvalias', 'Porto', '', 'Porto', '4200', '4200', '158', 'http://www.sapo.pt', 'barrosrenato@gmail.com', 'barrosrenato', '12345678', 'Renato', 'Barros', '', '', 'Mias Loja', '&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; sadkljasdlkjasdlkjaklsd\r\n\r\n', '', 'no', 'no', '2007-12-14 22:02:37', '2007-12-14 21:58:24');
INSERT INTO `freetplclassified_members` VALUES(68, '', 'gfdfg', '', 'dfgd', 'KL', '45645', '118', 'htthjkhjkhjk', 'rashuz@gmail.com', 'pakos', 'panamas', 'Pakos', 'sss', '', '9558581.jpg', 'MaxTrion Web Services', '<P align=justify><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2><STRONG><FONT style="BACKGROUND-COLOR: #ffffff" color=#cc0000 size=1>Located in Kuala Lumpur, Malaysia, we have been creating and promoting Web sites since the 2006. </FONT></STRONG></P>\r\n<P align=justify><FONT style="BACKGROUND-COLOR: #ffffff" face="Verdana, Arial, Helvetica, sans-serif" color=#cc0000 size=1><STRONG>We are a full-service studio to businesses of all sizes from locations around the world. We develop an understanding of your business goals and your Internet goals. We review your content and respond with a written quotation. We register your domain name. We design and program a unique, custom Web site for your business. We advise on how to successfully integrate the Internet technology and your Web site into your business operation. We help you develop an on-going maintenance and enhancement plan for your Web site. We promote and market your Web site on the Internet.</STRONG></FONT><BR></P></FONT>', '2231243.jpg', 'no', 'no', '2007-12-19 21:15:13', '2007-12-19 21:12:18');
INSERT INTO `freetplclassified_members` VALUES(69, '', 'dfg', '', 'edf', '1112', '112232', '88', 'www.google.com', 'sorrynomail@gmail.com', 'maky', '12345', 'sorry', 'sdfsd', '', '4174042.JPG', 'Mango Store', 'OK I am just testiing this script.<br><br>This is link <a href="www.google.com">Mango</a> <br><br>Here is the image of my mangoes.&nbsp; <img src="http://www003.upp.so-net.ne.jp/beech-forest/plant/mango2.jpg"><br><br><img src="http://www.hort.purdue.edu/ext/senior/fruits/images/large/mango3.jpg">&nbsp;One more image.<br><br>\r\n\r\n', '6881680.jpg', 'no', 'no', '2007-12-29 19:43:18', '2007-12-29 19:21:49');
INSERT INTO `freetplclassified_members` VALUES(70, 'harrow lesier center', 'hgfhfghfg', 'gffgfgnfgn', 'london', 'uk', 'ha9 6ll', '208', 'www.center.com', 'Jblack51@msn.com', 'Jason841', 'Jamaican', 'john', 'clarke', '', '', 'hjkjhkhjkhj', 'khjkhjkhjjhkjhk\r\n\r\n', '1873507.jpg', 'no', 'no', '2008-01-05 17:49:21', '2008-01-05 17:39:19');
INSERT INTO `freetplclassified_members` VALUES(71, '', '333nowhere', '', 'chicago', 'Il', '55467', '208', 'www.tonoehere.infp', 'maz@4maz2.com', 'maz22', 'me22', 'me', 'maz', '', '', '', '', '', 'no', 'no', '2008-01-08 06:12:49', '2008-01-08 06:12:49');
INSERT INTO `freetplclassified_members` VALUES(72, '', 'asd nr 45', '', 'LA', 'CA', '90210', '210', 'http://www.google.com', 'asd@aasd.com', 'asd', '12345', 'asdf', 'asd', '', '', '', '', '', 'no', 'no', '2008-01-09 20:21:46', '2008-01-09 20:21:46');
INSERT INTO `freetplclassified_members` VALUES(73, '', '123 45th', '', 'Las Vegas', ' NV', '12345', '210', 'cvgh.nn', 'jay2-ray2@usa.net', 'jay', 'randy', 'Jay', 'Ray', '', '', '', '', '', 'no', 'no', '2008-01-10 15:50:19', '2008-01-10 15:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_member_feedback`
--

CREATE TABLE `freetplclassified_member_feedback` (
  `id` bigint(20) NOT NULL auto_increment,
  `fname` varchar(255) default NULL,
  `lname` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `url` varchar(255) default NULL,
  `title` varchar(255) default NULL,
  `comment` longtext,
  `uid` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `freetplclassified_member_feedback`
--

INSERT INTO `freetplclassified_member_feedback` VALUES(1, 'ss', 'sss', 'sss', 'ddd', NULL, 'this is try for the submission of members request', 7);
INSERT INTO `freetplclassified_member_feedback` VALUES(2, 'fsdfsd', 'fsdfsdf', 'sdfsdf', 'sdfsdf', NULL, 'sdfsdfsdfsdf\r\nsdf\r\nsdf\r\nsdf', 7);
INSERT INTO `freetplclassified_member_feedback` VALUES(3, 'dodi', 'dodi', 'fdsfdfdfd', 'dssdsd', 'ssdd', 'dsdsdsddsds', 7);
INSERT INTO `freetplclassified_member_feedback` VALUES(4, 'ASa', 'sASa', 'sASa', 's', 'asAS', 'asASas\r\nAS\r\nas\r\nASa\r\nsA\r\nSA\r\nS', 7);
INSERT INTO `freetplclassified_member_feedback` VALUES(5, 'VVVV', 'FFFFF', 'dddd', 'ddddd', 'ddddd', 'ddddddd', 9);
INSERT INTO `freetplclassified_member_feedback` VALUES(6, 'cc', 'cc', 'cc@cv', '', 'title', 'this is no joke', 16);
INSERT INTO `freetplclassified_member_feedback` VALUES(11, 'wwww', 'qqqqq', 'qqqq@qq.com', 'qwe', 'qwe', 'fdgdgfd dfg dghf hgd\r\ngdhgd\r\ngdhd\r\ndjd\r\nkjdk', 8);
INSERT INTO `freetplclassified_member_feedback` VALUES(13, 'ygh', 'hggh', 'ghgh@gfg.com', 'hghg', 'ghgh', 'ghhg', 8);
INSERT INTO `freetplclassified_member_feedback` VALUES(14, 'xzc', 'zxzx', 'xz@gg.vv', 'zx', '', 'zxxzxc', 8);
INSERT INTO `freetplclassified_member_feedback` VALUES(15, 'cxcx', 'cxc', 'c@cc.com', ';;', 'sd', 'sdsdsd\r\n dsds sdsdds ds  ds sd sdsd sd \r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n', 8);
INSERT INTO `freetplclassified_member_feedback` VALUES(16, 'jz', 'zabo', 'user@mycarfs.com', '', 'Scripts and business opportunities', 'Are your scripts written in english?\r\nWhat business opportunties do you have? \r\nCan I easily modify your  scripts to my liking?\r\n\r\nThank you\r\njz', 0);
INSERT INTO `freetplclassified_member_feedback` VALUES(17, 'Kat', 'Izfat', 'katizfat@gmail.com', 'www.LetsWork5to9.com', 'just testing', 'test test test', 0);
INSERT INTO `freetplclassified_member_feedback` VALUES(18, 'Mirza', 'Baig', 'mabcox@yahoo.co.in', '', 'Mr', 'Hello,\r\n1-I am interested in your software, could you please confirm if the software has option for RTL languages? \r\n2- Is there any limitation on usage of the software? or it is full and with ownership right?\r\n3- I tried to see the terms and conditions but window opened has nothing but "Terms and condition" written all over the page', 0);

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_messages`
--

CREATE TABLE `freetplclassified_messages` (
  `id` bigint(20) NOT NULL auto_increment,
  `fid` bigint(20) NOT NULL default '0',
  `tid` bigint(20) NOT NULL default '0',
  `subject` mediumtext NOT NULL,
  `message` longtext NOT NULL,
  `tempdate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `onstamp` timestamp NOT NULL default '0000-00-00 00:00:00',
  `msg_read` varchar(10) NOT NULL default '',
  `f_del` varchar(10) NOT NULL default '',
  `t_del` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `freetplclassified_messages`
--

INSERT INTO `freetplclassified_messages` VALUES(40, 59, 58, 'Billing', 'tESTING', '2007-11-08 02:34:29', '2007-11-08 02:30:50', 'Yes', 'No', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_online`
--

CREATE TABLE `freetplclassified_online` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_ip` varchar(255) NOT NULL default '',
  `freetpl_ontime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `freetpl_uid` bigint(20) default '0',
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5320 ;

--
-- Dumping data for table `freetplclassified_online`
--

INSERT INTO `freetplclassified_online` VALUES(5319, '202.156.8.9', '2008-01-18 01:46:51', -1);

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_plans`
--

CREATE TABLE `freetplclassified_plans` (
  `id` bigint(20) NOT NULL auto_increment,
  `credits` bigint(20) NOT NULL default '0',
  `price` decimal(10,2) NOT NULL default '0.00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `freetplclassified_plans`
--

INSERT INTO `freetplclassified_plans` VALUES(10, 1000, '9.00');
INSERT INTO `freetplclassified_plans` VALUES(7, 10000, '35.00');
INSERT INTO `freetplclassified_plans` VALUES(6, 5000, '20.00');
INSERT INTO `freetplclassified_plans` VALUES(9, 20000, '69.00');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_products`
--

CREATE TABLE `freetplclassified_products` (
  `id` bigint(20) NOT NULL auto_increment,
  `uid` bigint(20) NOT NULL default '0',
  `product_name` varchar(255) default NULL,
  `cid` bigint(20) default NULL,
  `aucid` bigint(20) default NULL,
  `auction_period` varchar(255) default NULL,
  `featured` varchar(10) default NULL,
  `approved` varchar(10) default NULL,
  `tmpdate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `date_submitted` timestamp NOT NULL default '0000-00-00 00:00:00',
  `product_desc` longtext,
  `location` varchar(255) default NULL,
  `no_of_views` bigint(20) default NULL,
  `status` varchar(255) default NULL,
  `winner` varchar(255) default NULL,
  `bold` varchar(10) default NULL,
  `highlight` varchar(10) default NULL,
  `purchased_images` int(10) default '0',
  `fp_featured` varchar(10) default NULL,
  `gallery_featured` varchar(10) default NULL,
  `paypal_id` varchar(255) default NULL,
  `counter_id` bigint(20) default '0',
  `buy_price` decimal(10,2) default '0.00',
  `additional_info` longtext,
  `state` varchar(255) default NULL,
  `country` bigint(20) default NULL,
  `freetplextra_shipping` decimal(10,2) NOT NULL default '-1.00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=170 ;

--
-- Dumping data for table `freetplclassified_products`
--

INSERT INTO `freetplclassified_products` VALUES(152, 58, 'Elite Creative ', 228, 4, '606', 'yes', 'yes', '2008-01-10 12:32:28', '2007-11-08 01:12:03', '<P><B>Elite Creative &amp; Services</B> provide a wide range of creative services and colourful products tailored to your needs!<BR>Just creative, Nothing excess.<BR><BR><BR>Our services and products include:</P>\r\n<DIV align=left>1. Creative design e.g brochure, corporate profile, annual report, promotional collaterals, corporate identity<BR></DIV>\r\n<DIV align=left>2. Calendars<BR>3. Diary &amp; notebooks<BR>4. Product packaging<BR>5. Premium gifts<BR>6. Digital print displays</DIV>\r\n<DIV align=left>7. Offset printing</DIV>\r\n<DIV align=left><BR>Feel free to reach us at:<BR>Tel: 67785663<BR>Fax: 67786552<BR>Email: sales@elite-creative.com</DIV>\r\n<DIV align=left>Website : <A href="http://www.elite-creative.com/" target=_blank rel=nofollow><FONT color=#0073d5>http://www.elite-creative.com</FONT></A><BR></DIV>\r\n<DIV align=left><FONT color=#0073d5></FONT></DIV>\r\n<DIV align=left>15 West Coast Highway<BR>#04-10 Pasir Panjang Building<BR>Singapore 117861<BR></DIV>', 'Singapore', 45, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 173, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(151, 58, 'Tampa CPA Firm', 143, 4, '1770', 'yes', 'yes', '2008-01-10 12:30:54', '2007-11-08 01:07:52', '<DIV id=Text3><SPAN class=text><FONT color=#000000>Our firm <DIV">, <STRONG>SK Financial Services, P.A.</STRONG> was founded in 2002. We believe in the value of relationships. We view every client relationship like a partnership, and <STRONG>truly believe that our success is a result of your success.&nbsp;</STRONG> <DIV"></DIV"></DIV">\r\n<DIV>&nbsp;</DIV>\r\n<DIV><FONT color=#ff0000>What differentiates us from other CPA firms is <STRONG>our unique approach to our clients</STRONG>, which includes the following <STRONG>three steps</STRONG>:</FONT> <BR></DIV>\r\n<OL type=1><LI">1.&nbsp;&nbsp; Understanding your business and determining your goals, <LI"></LI"></LI">\r\n<DIV>2.&nbsp;&nbsp; Identifying the problems that are obstacles to achieving your goals, and&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <LI">3.&nbsp;&nbsp; Providing solutions that will address the problems and helping you achieve your goals. <BR></LI"></DIV></OL><DIV">The above three steps are accomplished by using the latest technology. This not only helps us to do our job more efficiently but also helps us to reduce costs, hence we can provide our services at lower fees<STRONG>. &nbsp;The value we provide to your business is significantly greater than our fees. </STRONG></DIV">To know more about us click <A href="http://d171334.u27.hsphere.clientsource.com/custom.html"><FONT color=#000099>here</FONT></A></FONT><FONT color=#000099>.&nbsp;<FONT color=#000000>You can also meet <A href="http://d171334.u27.hsphere.clientsource.com/custom2.html"><FONT color=#000099>Our Accountants</FONT></A>.</FONT>&nbsp;</FONT></SPAN></DIV>', 'USA', 97, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'usa', 210, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(153, 58, 'TeliWave Graphic Design', 228, 4, '1000', 'yes', 'yes', '2008-01-10 12:33:29', '2007-11-08 01:16:27', '<SPAN style="FONT-SIZE: 14pt; FONT-FAMILY: ''Times New Roman''"><STRONG><U>About Us<?XML:NAMESPACE PREFIX = O /><O:P></O:P></U></STRONG></SPAN><SPAN style="FONT-FAMILY: ''Times New Roman''">&nbsp;<O:P></O:P></SPAN><SPAN style="FONT-FAMILY: ''Times New Roman''">Teliwave aims to provide users with seamless communication in devices and networks, across geographical boundaries.<O:P></O:P></SPAN> \r\n<H2 style="MARGIN: 0in 0in 0pt"><U><FONT face="Times New Roman">&nbsp;<O:P></O:P></FONT></U></H2>\r\n<H2 style="MARGIN: 0in 0in 0pt"><U><FONT face="Times New Roman">Service Provided</FONT></U></H2><SPAN style="FONT-FAMILY: ''Times New Roman''">&nbsp;<O:P></O:P></SPAN><SPAN style="FONT-FAMILY: ''Times New Roman''">Website designing and IT communication Services<O:P></O:P></SPAN> \r\n<H2 style="MARGIN: 0in 0in 0pt"><U><FONT face="Times New Roman">&nbsp;<O:P></O:P></FONT></U></H2>\r\n<DIV><FONT face="Times New Roman" size=5><U></U></FONT></DIV>\r\n<H2 style="MARGIN: 0in 0in 0pt"><U><FONT face="Times New Roman">Contact Us</FONT></U></H2><FONT face="Times New Roman">&nbsp;<O:P></O:P></FONT><PRE><SPAN style="FONT-SIZE: 12pt; FONT-FAMILY: ''Times New Roman''">Seah Yong Wah, Sean<O:P></O:P></SPAN></PRE><PRE><SPAN style="FONT-SIZE: 12pt; FONT-FAMILY: ''Times New Roman''">Project Manager<O:P></O:P></SPAN></PRE><PRE><SPAN style="FONT-SIZE: 12pt; FONT-FAMILY: ''Times New Roman''">Teliwave Pte Ltd<O:P></O:P></SPAN></PRE><PRE><SPAN style="FONT-SIZE: 12pt; FONT-FAMILY: ''Times New Roman''">Tel: (65) 6848 4177<BR><BR><O:P><BR><BR><BR><BR>graphic designer(s), graphic and web designing, graphic design(s), cheap web designing, web designing companies, graphic design singapore</SPAN></PRE></O:P>', 'Singapore', 64, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 173, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(154, 58, 'Iventure Management', 122, 4, '1000', 'yes', 'yes', '2008-01-10 11:43:51', '2007-11-08 01:21:30', '<TABLE cellSpacing=0 cellPadding=0 border=0>\r\n<TBODY>\r\n<TR>\r\n<TD class=contentText colSpan=2>Protecting individual and Corporation Assets &amp; Liabilities before &amp; after a loss is our forte. Our client’s financial health is our duty and our business. We do not compromise our client’s financial health with cost saving. <BR><BR>Our ability to create value and manage risk at each stage of our customers growth, has delivered rapid growth over the past few years which has raise our profile in the Industry today. <BR><BR>We take pride in providing solutions that are innovative, practical, cost saving and work accordingly to the requirements of our clients. All without compromising our impeccable standards.</TD></TR>\r\n<TR>\r\n<TD class=contentHeader width=525><A title=1 target=_blank name=1></A>Our Mission</TD>\r\n<TD width=29>&nbsp;</TD></TR>\r\n<TR>\r\n<TD class=contentText colSpan=2>We will be the Successful symbol of Insurance Intermediaries for the New Business Economy of the 21st Century.</TD></TR>\r\n<TR>\r\n<TD class=contentHeader><A title=2 target=_blank name=2></A>Our Networking Strategy </TD>\r\n<TD>&nbsp;</TD></TR>\r\n<TR>\r\n<TD class=contentText colSpan=2>Our notion in enhancing value to our network partners, we develop a superior capability in increasing our customer value.</TD></TR>\r\n<TR>\r\n<TD class=contentHeader><A title=3 target=_blank name=3></A>Our Customer’s Strategy</TD>\r\n<TD>&nbsp;</TD></TR>\r\n<TR>\r\n<TD class=contentText colSpan=2>We believe that the things that drives everything in business is creating genuine value for our customers and we believe the relationship with our customers is the key to the company’s future. Our core is we are relentlessly Customer driven. <BR><BR><SPAN style="FONT-WEIGHT: bold; COLOR: #000000">Our Customers Extraordinary Value &amp; Security Come first!</SPAN></TD></TR>\r\n<TR>\r\n<TD class=contentHeader><A title=4 target=_blank name=4></A>Our People Strategy</TD>\r\n<TD>&nbsp;</TD></TR>\r\n<TR>\r\n<TD class=contentText colSpan=2>Our employees work with a desire to serve our client, and with utmost professionalism. Their knowledge and experience also allow them to constantly understand the client’s needs. <BR><BR><SPAN style="FONT-WEIGHT: bold; COLOR: #000000">Our Staff are Inspirational, Culturally rich &amp; Customer Orientated.</SPAN></TD></TR></TBODY></TABLE>\r\n<P>&nbsp;</P>\r\n<P>\r\n<TABLE cellSpacing=0 cellPadding=0 border=0>\r\n<TBODY>\r\n<TR>\r\n<TD class=contentHeader width=493>Mailing Address</TD>\r\n<TD width=29>&nbsp;</TD></TR>\r\n<TR>\r\n<TD class=contentText colSpan=2>Inorder to get in touch with us, you can visit us at : <BR><BR>10 Anson Road #26-06<BR>International Plaza<BR>Singapore 079903 <BR><BR>Tel: 6226 2622<BR>Fax: 6223 8371<BR>Email: <A href="mailto:hello@iventure.com.sg" target=_blank rel=nofollow><FONT color=#0073d5>hello@iventure.com.sg</FONT></A></TD></TR></TBODY></TABLE></P>', 'Singapore', 83, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 173, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(155, 58, ' ABle viSual ', 157, 4, '1000', 'yes', 'yes', '2008-01-10 15:33:21', '2007-11-08 01:23:15', '<FONT size=2><FONT face=Verdana><STRONG><FONT color=#999999>ABle viSual</FONT></STRONG><FONT color=#666666> has specialised in designing logos, corporate identities and brand images since 1994, working with private companies in a wide range of different markets, and also with government funded projects in education and leisure. </FONT><BR></FONT></FONT><FONT color=#336699><BR></FONT><FONT face="Arial, Helvetica, sans-serif" color=#336699><I><FONT face="Times New Roman, Times, serif" size=2>more than design...</FONT></I></FONT><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#333333><B><FONT color=#666666><BR></FONT></B><FONT color=#666666 size=2>In addition to the design team this includes people who can write your copy, develop your strategic marketing plan, sort out an IT problem, create your website or buy your print.</FONT></FONT><FONT size=2> </FONT><FONT color=#ffcc00><I><FONT face="Times New Roman, Times, serif" color=#cccc66 size=2><BR>meeting your needs...</FONT></I></FONT><FONT face="Times New Roman, Times, serif" color=#cccc66><BR><FONT size=2><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666>W</FONT><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666>e will be competent to <B><FONT color=#ff9999>discover</FONT></B> the solution, <B><FONT color=#336699>create</FONT></B> the impossibilites and <FONT color=#999999><B><FONT color=#cccc66>explore</FONT></B></FONT> the unexpected. <BR><BR>Our Services:<BR><BR><EM><FONT color=#0066cc size=4>advertising &amp; promotions<BR></FONT></EM><FONT size=1>- Posters, Brochures, Banners, Leaflets, etc.</FONT></FONT></FONT></FONT><FONT face="Times New Roman, Times, serif" color=#cccc66><FONT size=2><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666> \r\n<P><FONT face="Times New Roman, Times, serif" color=#666666 size=5><I><FONT color=#cc3399 size=4>corporate identities</FONT></I></FONT><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666 size=1><BR>- Logo designs, namecards, letterheads, envelopes, etc<BR></FONT></P>\r\n<P><FONT face="Times New Roman, Times, serif" color=#666666 size=4><I><FONT color=#009999>computer illustration</FONT></I></FONT><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666 size=1><BR>- Mascot (Cartoon images), digital images<BR><BR><I><FONT face="Times New Roman, Times, serif" color=#996600 size=4>web design </FONT></I><BR>- Domain name &amp; Hosting Packages<BR><BR><FONT face="Times New Roman, Times, serif"><FONT color=#339933 size=4><EM><FONT color=#0000ff>printing</FONT> <BR><BR>Visit Our Site at </EM><A href="http://www.ablevisual.com/" target=_blank rel=nofollow><EM><FONT color=#0073d5>www.ablevisual.com</FONT></EM></A><EM>&nbsp;to view our portfolios.<BR><BR>Or For Enquires&nbsp;<BR><BR>Contact us at <BR></EM><FONT size=1><FONT face=Verdana color=#666666>405A Sembawang Road<BR>Singapore 758385<BR><BR>T</FONT></FONT><FONT color=#666666><FONT face=Verdana><FONT size=1>el/Fax • 6314 8154<BR><BR>Email: </FONT><FONT size=1><B><FONT size=1><A href="mailto:info@ablevisual.com" target=_blank rel=nofollow><FONT color=#0073d5>info@ablevisual.com</FONT></A><BR></FONT></B></FONT></FONT></FONT><BR></FONT></FONT></FONT></P></FONT></FONT></FONT>', 'Singapore', 111, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 221, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(156, 58, 'Smartpages', 251, 4, '1000', 'yes', 'yes', '2008-01-10 23:46:25', '2007-11-08 01:25:43', '<TABLE height=65 cellSpacing=0 cellPadding=3 width="96%" border=0>\r\n<TBODY>\r\n<TR>\r\n<TD>\r\n<DIV align=center><FONT face="Arial, Helvetica, sans-serif" color=#cc0000 size=4><STRONG>Select a Professional Template to Build Your Website and Have it Customized</STRONG></FONT></DIV></TD></TR>\r\n<TR>\r\n<TD>\r\n<DIV align=center><FONT face="Verdana, Arial, Helvetica, sans-serif" size=2><BR>The fastest and easiest way to set up a new website is to start with a pre-made template that has the general look and feel that you would like. You pick the template, and we customize it with your logo, contents, graphics and set it up for you. <BR><STRONG><FONT color=#6633cc size=4><BR>Professional web design + Domain name + web hosting = ???</FONT></STRONG><BR><BR><BR>\r\n<TABLE cellSpacing=0 cellPadding=5 align=center border=0>\r\n<TBODY>\r\n<TR>\r\n<TD>\r\n<TABLE height=190 cellSpacing=1 cellPadding=3 width=150 bgColor=#cccccc border=0>\r\n<TBODY>\r\n<TR>\r\n<TD vAlign=bottom bgColor=#ffffff>\r\n<TABLE height=170 cellSpacing=0 cellPadding=2 width="100%" border=0>\r\n<TBODY>\r\n<TR>\r\n<TD><IMG style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" height=149 src="http://www.smartpages.com.my/webdesign/template_images/16264-m.jpg" width=145 border=0></TD></TR>\r\n<TR>\r\n<TD bgColor=#5e8c01>\r\n<DIV align=right><FONT face="Arial, Helvetica, sans-serif" size=2><B><FONT color=#ffffff></FONT></B></FONT></DIV></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD>\r\n<TD></TD>\r\n<TD>\r\n<TABLE height=190 cellSpacing=1 cellPadding=3 width=150 bgColor=#cccccc border=0>\r\n<TBODY>\r\n<TR>\r\n<TD vAlign=bottom bgColor=#ffffff>\r\n<TABLE height=170 cellSpacing=0 cellPadding=2 width="100%" border=0>\r\n<TBODY>\r\n<TR>\r\n<TD><IMG style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" height=149 src="http://www.smartpages.com.my/webdesign/template_images/16310-m.jpg" width=145 border=0></TD></TR>\r\n<TR>\r\n<TD bgColor=#5e8c01>\r\n<DIV align=right><STRONG><FONT size=2></FONT></STRONG></DIV></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD>\r\n<TD>\r\n<TABLE height=190 cellSpacing=1 cellPadding=3 width=150 bgColor=#cccccc border=0>\r\n<TBODY>\r\n<TR>\r\n<TD vAlign=bottom bgColor=#ffffff>\r\n<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>\r\n<TBODY>\r\n<TR>\r\n<TD><IMG style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" height=149 src="http://www.smartpages.com.my/webdesign/template_images/16315-m.jpg" width=145 border=0></TD></TR>\r\n<TR>\r\n<TD bgColor=#5e8c01>\r\n<DIV align=right><FONT face="Arial, Helvetica, sans-serif" size=2><B><FONT color=#ffffff>&nbsp;&nbsp;</FONT></B></FONT></DIV></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD>\r\n<TD>\r\n<TABLE height=190 cellSpacing=1 cellPadding=3 width=150 bgColor=#cccccc border=0>\r\n<TBODY>\r\n<TR>\r\n<TD vAlign=bottom bgColor=#ffffff>\r\n<TABLE cellSpacing=0 cellPadding=2 width="100%" border=0>\r\n<TBODY>\r\n<TR>\r\n<TD><IMG style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" height=142 src="http://www.smartpages.com.my/webdesign/template_images/16324-m.jpg" width=145 border=0></TD></TR>\r\n<TR>\r\n<TD bgColor=#5e8c01>\r\n<DIV align=right><FONT face="Arial, Helvetica, sans-serif" size=2><B><FONT color=#ffffff></FONT></B></FONT></DIV></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE><BR>Visit Our Site: <A href="http://www.smartpages.com.my/webdesign" target=_blank rel=nofollow><FONT color=#0073d5>http://www.smartpages.com.my/webdesign</FONT></A><BR><STRONG><FONT face=Arial color=#ff0000 size=3><BR>Website design, development, maintenance, hosting, e-commerce and domain management specialist.<BR></FONT></STRONG><FONT size=2><FONT face=Arial>We always available by email to answer any questions you may have. </FONT><A href="mailto:customercare@smartpages.com.my" target=_blank rel=nofollow><FONT face=Arial color=#0073d5>customercare@smartpages.com.my</FONT></A></FONT><STRONG><FONT face=Arial color=#ff0000 size=3> </FONT></STRONG><BR></FONT></DIV></TD></TR></TBODY></TABLE><FONT size=2><FONT face=Verdana><STRONG>\r\n<DIV align=center><FONT size=2><FONT face=Verdana><STRONG>&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;We Are Looking For&nbsp; Part Time agents.&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;</STRONG> </FONT></FONT></DIV></STRONG></FONT></FONT>\r\n<P class=text_Ultra align=left><B></B></P><BR>\r\n<DIV style="TEXT-ALIGN: center"><A href="mailto:infotrex@yahoo.com" target=_blank rel=nofollow><IMG style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" height=42 src="http://estorecatalog.p8market.com/images/i_messenger.gif" width=408 border=0></A></DIV>', 'Malaysia', 115, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Malaysia', 118, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(157, 58, 'Slimming and Wellness', 264, 4, '1000', 'yes', 'yes', '2008-01-11 01:01:00', '2007-11-08 01:39:05', '<DIV id=_ctl0_DB88ContentHolder_PostPanel1_PostDetailContend1_postContent style="FONT-SIZE: 12pt; FONT-FAMILY: Arial"><SPAN class=font12>My Slimming &amp; Wellness Sanctuary has now been opened for 4 months and the response has been excellent. For the past 4 months, we have seen many men and women alike, shed pounds off their bodies. The record weight loss till date is 5kg in merely 3 days, which impressed almost everyone of us here at the Sanctuary. This further strengths our belief in the services that we are providing here at the Sanctuary.<BR><BR>As a matter of fact, we are so confident of our services, that we will now be offering a 90-DAYS MONEY BACK GUARANTEE* to all our customers who sign up for our slimming packages. Meaning if you fail to slim down and shed pounds off your body after 3 months, we will refund you the FULL AMOUNT which you have paid!!!<BR><BR>Therefore, for those of you who:<BR>- are determined to slim down (must slim down); or<BR>- are thinking of slimming down (like to slim down); or<BR>- have tried but failed to slim down; or<BR>- have people around you who wants to slim down<BR><BR>I am confident enough to say that my Slimming &amp; Wellness Sanctuary is the place which will make you and the people around you a very happy, and also a slim man or woman. Most importantly, to regain the confidence in you!!!<BR><BR>And with this 90-DAYS MONEY BACK GUARANTEE*, its a sure win scenario for our customers. If you manage to slim down (which we are 100% confident you will), you would have achieved your aim. If you fail to slim down (which is almost impossible), you will be able to get the full refund back.<BR><BR>So why wait??? We are eager to help you achieve the figure and body that you have always dreamed of!!!<BR><BR>If you have any questions or need further information, please feel free to drop me a message here or to call me at (65) 9875 5093. Otherwise, will be looking forward to see YOU at my Slimming &amp; Wellness Sanctuary.<BR><BR>Cheers,<BR>Gilbert Goh<BR>Mobile: (65) 9875 5093<BR><BR>(*Terms and conditions apply)<BR></SPAN></DIV>', 'Singapore', 121, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 173, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(158, 58, ' Manpower Sourcing', 262, 4, '1000', 'yes', 'yes', '2008-01-10 12:00:18', '2007-11-08 01:41:34', '<DIV style="BORDER-RIGHT: #c40000 1pt solid; PADDING-RIGHT: 4pt; BORDER-TOP: #c40000 1pt solid; PADDING-LEFT: 4pt; PADDING-BOTTOM: 1pt; BORDER-LEFT: #c40000 1pt solid; PADDING-TOP: 1pt; BORDER-BOTTOM: #c40000 1pt solid"><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">Human Resource is the key ingredient for organizational success and a glitch in having the right personnel may halt your company’s progress. <?XML:NAMESPACE PREFIX = O /><O:P></O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P>&nbsp;</O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">ACETECH<SPAN style="COLOR: black"> was established in 1997. &nbsp;Our Aim is to provide fast, efficient, responsive service and offer the best staffing solutions to you.</SPAN></SPAN></B><SPAN style="FONT-SIZE: 8.5pt; COLOR: black; FONT-FAMILY: Tahoma">&nbsp;</SPAN><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"> Our team of highly capable, skilful and experienced Consultants in recruitment services, are dedicated to assist in putting your company into gear and propel it towards success through the recruitment and selection of well-qualified candidates.<O:P></O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P>&nbsp;</O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">ACETECH believes that every business, like you, is an individual and unique in its very own way. To gain a better understanding of your requirements, we will discuss and analyse your manpower needs and recommend the right candidate with relevant skills and knowledge who will meet your expectation and blend into your corporate culture.<O:P></O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P>&nbsp;<BR></O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><BR>SERVICES WE PROVIDE:<O:P></O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P>&nbsp;<BR></O:P></SPAN></B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN><BR>· </SPAN></SPAN><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">IT Recruitment &amp; Outsourcing<BR></SPAN></I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN>· </SPAN></SPAN><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">General Recruitment<BR><O:P></O:P></SPAN></I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN>· </SPAN></SPAN><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">Oversea Recruitment<BR><O:P></O:P></SPAN></I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN>·<SPAN style="FONT: 7pt ''Times New Roman''"> </SPAN></SPAN></SPAN><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">Executive Search<BR><O:P></O:P></SPAN></I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN>· </SPAN></SPAN><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">Payroll &amp; Headcount Maintenance<BR><O:P></O:P></SPAN></I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN>·<SPAN style="FONT: 7pt ''Times New Roman''"> </SPAN></SPAN></SPAN><?XML:NAMESPACE PREFIX = ST1 /><ST1:PLACE w:st="on"><ST1:PLACENAME w:st="on"><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">Employment</SPAN></I></ST1:PLACENAME><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"> <ST1:PLACETYPE w:st="on">Passes</ST1:PLACETYPE></SPAN></I></ST1:PLACE><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"> &amp; Work Permit Application<O:P></O:P></SPAN></I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P>&nbsp;<BR></O:P></SPAN><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><BR>WE PROVIDE CANDIDATE FOR:<O:P></O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P>&nbsp;<BR></O:P></SPAN></B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN>· </SPAN></SPAN><I><SPAN style="FONT-SIZE: 8.5pt; COLOR: black; FONT-FAMILY: Tahoma">Contractual<BR></SPAN></I><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P></O:P></SPAN></I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN>· </SPAN></SPAN><I><SPAN style="FONT-SIZE: 8.5pt; COLOR: black; FONT-FAMILY: Tahoma">Temporary Basis<BR></SPAN></I><I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P></O:P></SPAN></I><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Symbol"><SPAN>·<SPAN style="FONT: 7pt ''Times New Roman''">&nbsp;</SPAN></SPAN></SPAN><I><SPAN style="FONT-SIZE: 8.5pt; COLOR: black; FONT-FAMILY: Tahoma">Permanent Placement<BR><BR></SPAN></I><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">We believe in total customer satisfactory with our professionalism, quality and speed. We have a large database of candidates, wide referrals and extensive networking contacts that allow us to discover the best talents for you in the shortest time.<O:P></O:P></SPAN></B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P>&nbsp;</O:P></SPAN><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">Allow us to take the Manpower-sourcing burden off your shoulders so that you can concentrate on your core business. <O:P></O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma"><O:P>&nbsp;</O:P></SPAN></B><B><SPAN style="FONT-SIZE: 8.5pt; FONT-FAMILY: Tahoma">Contact our professional Consultant now to discover a whole new convergence of Technology and executive search/ recruitment services that would spur your esteemed organization towards success!<O:P></O:P></SPAN></B></DIV>', 'Singapore', 102, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 173, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(159, 58, 'Ads Space', 252, 4, '1000', 'yes', 'yes', '2007-11-09 02:58:13', '2007-11-08 01:43:55', '<TABLE class=UserAd width="100%">\r\n<TBODY>\r\n<TR>\r\n<TD width=940>\r\n<DIV style="PADDING-RIGHT: 2px; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px"></DIV>\r\n<SCRIPT type=text/javascript>\r\nwindow.onload=getTdWidth;\r\n\r\nfunction SetBoder()\r\n{\r\n    var DivId=''_ctl0_DB88ContentHolder_PostPanel1_PostDetailContend1_postContent''\r\n    if(document.getElementById(DivId)!=null)\r\n    {\r\n       var imgs= document.getElementById(DivId).getElementsByTagName("IMG");\r\n       for(var i=0;i<imgs.length;i++)\r\n       {\r\n       imgs[i].style.borderWidth ="0px";\r\n       }\r\n    }\r\n}\r\n\r\nif (window.addEventListener)\r\nwindow.addEventListener("load", SetBoder, false)\r\nelse if (window.attachEvent)\r\nwindow.attachEvent("onload", SetBoder)\r\nelse\r\nwindow.onload=SetBoder\r\n</SCRIPT>\r\n\r\n<DIV id=_ctl0_DB88ContentHolder_PostPanel1_PostDetailContend1_postContent style="FONT-SIZE: 12pt; FONT-FAMILY: Arial">\r\n<P align=left><FONT color=#a52a2a size=5>PATRICK ADs SPACE</FONT></P>\r\n<P align=left>&nbsp;</P>\r\n<P align=left><FONT color=#ff0000 size=4>FOR SERVICE IN HR OPERATION / CONSULTING JOB ROLE</FONT></P>\r\n<P align=left>"Describe your personal qualities that suit this position" </P>\r\n<P align=left>&nbsp;</P>\r\n<UL>\r\n<LI>\r\n<DIV align=left>Article Related To Change Management: Received International HR Professional Recognition. </DIV></LI></UL>\r\n<P align=left>&nbsp;</P>\r\n<UL>\r\n<LI>\r\n<DIV align=left>SUMMARY OF KEY SKILLS </DIV></LI></UL>\r\n<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">\r\n<P align=left>1. Management Skills Sets </P>\r\n<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">\r\n<P align=justify>Professional Human Resource Manager with over 3 years HR management experience, and over 5 years generalist role with both local &amp; corporate/international firms. Excellent analytical and interpersonal skills have enhanced my success at managing people and a busy HR Department. </P></BLOCKQUOTE>\r\n<P align=left>2. Administration Skills Sets </P>\r\n<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">\r\n<P align=justify>Extensive administration experience coupled with organizational skills and ability to multitask effectively form an integral part of management style. </P></BLOCKQUOTE>\r\n<P align=left>3. Knowledge Base HR, HRIS, ISO, Job Design, etc. </P>\r\n<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">\r\n<P align=left>• Outsourcing, HRIS Implementation as HR Solution to the business plan </P>\r\n<P align=justify>• ISO framework – procedures, KPI, Mapping, Work Design, Survey etc. for continuing improvement to the business needs. </P>\r\n<P align=justify>• Staff counseling, welfare, career counseling, recruitment/selection, training, benefits &amp; compensation packages, IR/Union, OHS-Security Manual, HR Manual, Training Manual, Employee Manual, Staff/Deployment Management Reports, Employment Law, Hotel Act, Change management, staff incidence/workplace accident reports , Benchmarking – Customer Service &amp; HR Best Practice with international hotel chain namely, Beaufort Hotel, Le-Meridien Hotel, etc. </P>\r\n<P align=justify>• Career Exhibition, Marketing of HR &amp; Accounting Software presentation. </P>\r\n<P align=justify>• Strong planning and facilitation skills, team building and ability to diagnostic organizational problems and needs. </P>\r\n<P align=justify>• Business proposal, video presentation, recruitment ad, head office reports Achievement &amp; Seminars: </P></BLOCKQUOTE></BLOCKQUOTE>\r\n<P align=left>&nbsp;</P>\r\n<UL>\r\n<LI>\r\n<DIV align=left>MILESTONES:</DIV></LI></UL>\r\n<P align=left>&nbsp;</P>\r\n<OL>\r\n<OL>\r\n<LI>\r\n<DIV align=left>Letter of Appreciation - Ministry of Manpower (Best HR Practice) </DIV>\r\n<LI>\r\n<DIV align=left>Student Excellent Praise </DIV>\r\n<LI>\r\n<DIV align=left>Tan Tock Seng Hosp - Stress Management Seminar </DIV>\r\n<LI>\r\n<DIV align=left>Shook Lin &amp; Bok, Drew &amp; Napier - Employment Law </DIV>\r\n<LI>\r\n<DIV align=left>WUJI - Hotel Seminar (Trends) </DIV>\r\n<LI>\r\n<DIV align=left>Others Hotel seminar - IR, etc. </DIV>\r\n<LI>\r\n<DIV align=left>Profssional HR Member Certification </DIV></LI></OL></OL>\r\n<P align=left>Many thanks for viewing this ad</P></DIV></TD></TR></TBODY></TABLE>', 'Singapore', 9, 'closed', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 173, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(160, 58, 'Lock+Store', 142, 4, '1000', 'yes', 'yes', '2008-01-10 11:41:30', '2007-11-08 01:46:56', '<DIV align=justify><STRONG>Lock+Store Self Storage offers storage space of various sizes with minimum rental of only one month. <BR><BR>The services we offer include: - Various sizes from 16sqft to 200sqft (24 hours Air-Con units also available) - <BR><BR>Unlimited access to your storage room 24-hours a day, 7-days a week.<BR><BR>Each room is individually compartmentalized with your own personal door lock.<BR>All the hallways and corridors in the entire facility is supervised by more than 250 CCTV cameras.<BR>Clean, bright and safe storage environment.<BR>Ample trolleys provided. <BR><BR></STRONG><BR><STRONG>We are located at 37 Keppel Road #01-03 Tanjong Pagar Distripark. <BR><BR>For a map of our location, please see <U><FONT color=#3366ff>http://www.lockandstore.com.sg/contact/index.html</FONT></U></STRONG></DIV>', 'Singapore', 165, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 173, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(161, 58, 'Ads Space', 173, 4, '1000', 'yes', 'yes', '2008-01-11 02:36:46', '2007-11-09 03:12:37', '<FONT size=2><FONT face=Verdana><STRONG><FONT color=#999999>ABle viSual</FONT></STRONG><FONT color=#666666> has specialised in designing logos, corporate identities and brand images since 1994, working with private companies in a wide range of different markets, and also with government funded projects in education and leisure. </FONT><BR></FONT></FONT><FONT color=#336699><BR></FONT><FONT face="Arial, Helvetica, sans-serif" color=#336699><I><FONT face="Times New Roman, Times, serif" size=2>more than design...</FONT></I></FONT><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#333333><B><FONT color=#666666><BR></FONT></B><FONT color=#666666 size=2>In addition to the design team this includes people who can write your copy, develop your strategic marketing plan, sort out an IT problem, create your website or buy your print.</FONT></FONT><FONT face=Arial size=2> </FONT><FONT color=#ffcc00><I><FONT face="Times New Roman, Times, serif" color=#cccc66 size=2><BR>meeting your needs...</FONT></I></FONT><FONT face="Times New Roman, Times, serif" color=#cccc66><BR><FONT size=2><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666>W</FONT><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666>e will be competent to <B><FONT color=#ff9999>discover</FONT></B> the solution, <B><FONT color=#336699>create</FONT></B> the impossibilites and <FONT color=#999999><B><FONT color=#cccc66>explore</FONT></B></FONT> the unexpected. <BR><BR>Our Services:<BR><BR><EM><FONT color=#0066cc size=4>advertising &amp; promotions<BR></FONT></EM><FONT size=1>- Posters, Brochures, Banners, Leaflets, etc.</FONT></FONT></FONT></FONT><FONT face="Times New Roman, Times, serif" color=#cccc66><FONT size=2><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666> \r\n<P><FONT face="Times New Roman, Times, serif" color=#666666 size=5><I><FONT color=#cc3399 size=4>corporate identities</FONT></I></FONT><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666 size=1><BR>- Logo designs, namecards, letterheads, envelopes, etc<BR></FONT></P>\r\n<P><FONT face="Times New Roman, Times, serif" color=#666666 size=4><I><FONT color=#009999>computer illustration</FONT></I></FONT><FONT face="Verdana, Arial, Helvetica, sans-serif" color=#666666 size=1><BR>- Mascot (Cartoon images), digital images<BR><BR><I><FONT face="Times New Roman, Times, serif" color=#996600 size=4>web design </FONT></I><BR>- Domain name &amp; Hosting Packages<BR><BR><FONT face="Times New Roman, Times, serif"><FONT color=#339933 size=4><EM><FONT color=#0000ff>printing</FONT> <BR><BR>Visit Our Site at </EM><A href="http://www.ablevisual.com/" target=_blank rel=nofollow><EM><FONT face=Arial color=#0073d5 size=2>www.ablevisual.com</FONT></EM></A><EM>&nbsp;to view our portfolios.<BR><BR>Or For Enquires&nbsp;<BR><BR>Contact us at <BR></EM><FONT size=1><FONT face=Verdana color=#666666>405A Sembawang Road<BR>Singapore 758385<BR><BR>T</FONT></FONT><FONT color=#666666><FONT face=Verdana><FONT size=1>el/Fax • 6314 8154<BR><BR>Email: </FONT><FONT size=1><FONT size=1><A href="mailto:info@ablevisual.com" target=_blank rel=nofollow><FONT face=Arial color=#0073d5 size=2>info@ablevisual.com</FONT></A><BR></FONT></FONT></FONT></FONT></FONT></FONT></FONT></P></FONT></FONT></FONT>', 'Singapore', 136, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Singapore', 3, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(162, 59, 'Air Craft ', 174, 4, '1000', 'yes', 'yes', '2008-01-10 15:57:05', '2007-11-09 16:23:27', '<TABLE id=AutoNumber7 style="BORDER-COLLAPSE: collapse" borderColor=#111111 cellSpacing=0 width=696 border=0>\r\n<TBODY>\r\n<TR>\r\n<TD width=694 colSpan=6 height=62><B><FONT face=Verdana size=4>Welcome to Air Craft </FONT><FONT face=Verdana size=5><BR></FONT><FONT face=Verdana size=2>R/C World Shop</FONT><FONT face=Verdana color=#cc0000 size=2> </FONT><FONT face=Verdana color=#cc0000 size=1>Updated November 6<SPAN lang=en-us>,</SPAN> 2007</FONT></B> \r\n<P><FONT size=1><B>Arrival Information : HYPERION </B>(<SPAN lang=en-us>LBA10, 0606i-AD, TITAN-30A) </SPAN>- Nov. 20 <SPAN lang=en-us><B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Waypoint </B>(servo) - Nov. 20</SPAN><B><BR>Aeronaut </B>- Arrived.&nbsp;&nbsp; <B>APC</B>-Arrived. <B>&nbsp; Graupner </B>- middle of Nov. <BR></FONT></P></TD></TR>\r\n<TR><BR>\r\n<TD align=left width=694 colSpan=6 height=30>\r\n<P align=center><B><FONT size=5><BR>GWS SuperSale! Discounted</FONT><FONT color=#cc0000 size=5> 30%~70%</FONT><FONT color=#cc0000 size=4> <A href="http://aircraft-world.com/shopquery.asp?cname=GW-">Click Here!</A><BR></FONT><FONT color=#222c57>Almost all items </B><FONT size=2>(except Props and Ducted Fans)</FONT><B> are now unbelievable prices! </B></FONT><FONT color=#222c57 size=1><BR></FONT><FONT size=1><FONT color=#cc0000>Servos and Receivers are 50%~80% off, Gearboxes and motors are 70% off, Others are 50% off. Can''t miss it!<BR></FONT><B>NO WARRANTY, NO RETURNS PLEASE!<BR><BR></B></FONT></P></TD></TR>\r\n<TR>\r\n<TD align=left width=694 colSpan=6 height=30>\r\n<P align=center><B><FONT color=#0000ff size=5>Kits Blowout! Final Bargain Sale! </FONT><FONT color=#cc0000 size=4><A href="http://aircraft-world.com/bargain2/ACW%20Kit%20Bargain-070830.htm">Click Here!</A><BR></FONT></B><FONT color=#222c57>GWS, Hacker Models CZ, UltraFly, Windrider HK, and Align<BR><B><FONT size=4>Get yours before they are gone!</FONT></B></FONT></P></TD></TR></TBODY></TABLE>', 'USA', 206, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'usa', 210, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(163, 61, 'Testing', 129, 4, '1000', 'yes', 'yes', '2008-01-10 18:54:10', '2007-11-09 19:03:09', 'Testing', 'Malaysia', 207, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'Malaysia', 169, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(164, 63, 'hoistudio', 251, 4, '1', 'yes', 'yes', '2007-12-23 11:02:36', '2007-11-29 06:52:53', '\r\n\r\n', '123', 7, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '123@yahoo.com', 1, '1.00', '', 'California', 210, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(165, 65, 'Sandman 2 Bed Sleep Diagnostic System', 136, 4, '7', 'no', 'yes', '2008-01-10 11:54:53', '2007-12-14 10:34:37', 'Best 2 Bed Sleep System on the Market - No Sleep Lab should be without one!!!!', 'Phoenix', 32, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', 'time4sleeping@yahoo.com', 0, '9000.00', '', 'Arizona', 210, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(166, 67, 'Mais', 234, 4, '15', 'yes', 'yes', '2008-01-05 14:48:51', '2007-12-14 22:04:21', 'sdasdasdasskhsdf<br>dg<br>sg<br>h<br>s<br>hsg<br>pdgçlasdkfçlaksdlfdq<br>\r\n\r\n', 'bla bla', 24, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', 'blabla@bla.com', 2, '10.00', '', 'dasldk', 158, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(167, 69, 'mango', 135, 4, '1', 'yes', 'yes', '2008-01-10 11:15:44', '2007-12-29 19:50:32', 'Ok just testing how does this work.<br><br>There is no way to see the stores. <br>\r\n\r\n', 'dallas', 7, 'open', '0', 'no', 'no', 0, 'yes', 'yes', 'abc212asd@paypal.com', 1, '200.00', '', 'Alabama', 3, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(168, 70, ',,nm,nm,', 121, 4, '40', 'yes', 'yes', '2008-01-11 07:15:02', '2008-01-05 17:45:54', '\r\n\r\n', 'ihkhlhl', 17, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', '', 1, '0.00', '', 'hjlhjlhjljlhjl', 208, '-1.00');
INSERT INTO `freetplclassified_products` VALUES(169, 73, 'Bag', 143, 4, '1', 'yes', 'yes', '2008-01-11 09:30:40', '2008-01-10 15:54:14', '<P align=center><STRONG>Description</STRONG></P>\r\n<P align=center><STRONG>e plurius unum</STRONG></P>', 'LV', 4, 'open', '0', 'yes', 'yes', 0, 'yes', 'yes', 'sales@meme.com', 0, '4500.00', '', 'Nevada', 210, '-1.00');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_purchased`
--

CREATE TABLE `freetplclassified_purchased` (
  `id` bigint(20) NOT NULL auto_increment,
  `pid` bigint(20) default NULL,
  `uid` bigint(20) default NULL,
  `quantity` bigint(20) default NULL,
  `date_submitted` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `email` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `freetplclassified_purchased`
--

INSERT INTO `freetplclassified_purchased` VALUES(15, 42, 9, 10, '2004-02-12 13:17:55', 'ddddddd');
INSERT INTO `freetplclassified_purchased` VALUES(23, 42, 27, 10, '2004-09-09 12:25:09', 'harison@h.com');
INSERT INTO `freetplclassified_purchased` VALUES(14, 43, 9, 15, '2004-02-12 13:17:35', 'ddddddd');
INSERT INTO `freetplclassified_purchased` VALUES(13, 43, 8, 12, '2004-02-12 13:17:24', 'vv@vv.com');
INSERT INTO `freetplclassified_purchased` VALUES(12, 42, 12, 12, '2004-09-09 12:26:23', 'viking@v.com');
INSERT INTO `freetplclassified_purchased` VALUES(11, 42, 12, 10, '2004-09-09 12:26:23', 'viking@v.com');
INSERT INTO `freetplclassified_purchased` VALUES(19, 43, 8, 12, '2004-02-12 11:42:21', 'vv@vv.com');
INSERT INTO `freetplclassified_purchased` VALUES(22, 65, 27, 1, '2004-09-09 12:25:09', 'harison@h.com');
INSERT INTO `freetplclassified_purchased` VALUES(21, 43, 8, 11, '2004-02-12 03:40:28', 'vv@vv.com');
INSERT INTO `freetplclassified_purchased` VALUES(24, 42, 27, 10, '2004-09-09 12:25:09', 'harison@h.com');
INSERT INTO `freetplclassified_purchased` VALUES(25, 74, 12, 10, '2004-09-09 12:26:23', 'viking@v.com');
INSERT INTO `freetplclassified_purchased` VALUES(26, 74, 9, 12, '2004-01-24 08:45:03', 'ddddddd');
INSERT INTO `freetplclassified_purchased` VALUES(27, 74, 9, 12, '2004-01-24 08:49:07', 'ddddddd');
INSERT INTO `freetplclassified_purchased` VALUES(28, 74, 9, 12, '2004-01-24 08:49:36', 'ddddddd');
INSERT INTO `freetplclassified_purchased` VALUES(29, 74, 9, 12, '2004-01-24 08:50:18', 'ddddddd');
INSERT INTO `freetplclassified_purchased` VALUES(30, 80, 12, 10, '2004-09-09 12:26:23', 'viking@v.com');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_ratings`
--

CREATE TABLE `freetplclassified_ratings` (
  `id` bigint(20) NOT NULL auto_increment,
  `sid` bigint(20) default NULL,
  `rating` int(11) default NULL,
  `ip` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `freetplclassified_ratings`
--

INSERT INTO `freetplclassified_ratings` VALUES(1, 1, 10, NULL);
INSERT INTO `freetplclassified_ratings` VALUES(2, 2, 5, NULL);
INSERT INTO `freetplclassified_ratings` VALUES(3, 3, 7, NULL);
INSERT INTO `freetplclassified_ratings` VALUES(4, 1, 5, NULL);
INSERT INTO `freetplclassified_ratings` VALUES(5, 14, 5, '127.0.0.1');
INSERT INTO `freetplclassified_ratings` VALUES(15, 14, 7, '127.0.0.1');
INSERT INTO `freetplclassified_ratings` VALUES(14, 14, 6, '127.0.0.1');
INSERT INTO `freetplclassified_ratings` VALUES(13, 2, 9, '127.0.0.1');
INSERT INTO `freetplclassified_ratings` VALUES(16, 14, 10, '127.0.0.1');
INSERT INTO `freetplclassified_ratings` VALUES(17, 14, 5, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_signups`
--

CREATE TABLE `freetplclassified_signups` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_rnum` varchar(255) default '',
  `freetpl_email` varchar(255) default '',
  `freetpl_onstamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `freetplclassified_signups`
--


-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_styles`
--

CREATE TABLE `freetplclassified_styles` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_title` varchar(255) default NULL,
  `freetpl_page_bg` varchar(255) default NULL,
  `freetpl_table_bg` varchar(255) default NULL,
  `freetpl_inner_table_bg` varchar(255) default NULL,
  `freetpl_seperator` varchar(255) default NULL,
  `freetpl_title_bg` varchar(255) default NULL,
  `freetpl_title_font_color` varchar(255) default NULL,
  `freetpl_title_font` varchar(255) default NULL,
  `freetpl_title_font_size` varchar(255) default NULL,
  `freetpl_normal_font_color` varchar(255) default NULL,
  `freetpl_normal_font` varchar(255) default NULL,
  `freetpl_normal_font_size` varchar(255) default NULL,
  `freetpl_normal_table_bg` varchar(255) default NULL,
  `freetpl_link_font` varchar(255) default NULL,
  `freetpl_link_font_color` varchar(255) default NULL,
  `freetpl_link_font_size` varchar(255) default NULL,
  `freetpl_side_title_bg` varchar(255) default NULL,
  `freetpl_side_title_font` varchar(255) default NULL,
  `freetpl_side_title_font_color` varchar(255) default NULL,
  `freetpl_side_title_font_size` varchar(255) default NULL,
  `freetpl_sub_title_bg` varchar(255) default NULL,
  `freetpl_highlighted` varchar(255) default 'FFFFCC',
  `freetpl_highlighted1` varchar(255) default 'FFFFDD',
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `freetplclassified_styles`
--

INSERT INTO `freetplclassified_styles` VALUES(4, 'Red', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(13, 'Australia Theme', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(14, 'Green', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(15, 'Gray - Black', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(16, 'Blue', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(17, 'Blue Red', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(18, 'Yellow (Default)', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(19, 'Canada,Dames,Swiss', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(20, 'Germany Theme', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(21, 'US Theme', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(22, 'UK Theme', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(23, 'Netherlands Theme', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(24, 'Sweden Theme', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(25, 'New Zealand Theme', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');
INSERT INTO `freetplclassified_styles` VALUES(26, 'France Theme', 'FFFFFF', 'FAFAFA', 'F5F5F5', 'cccccc', '363636', 'ffffff', 'Arial, Helvetica, sans-serif', '12', '000000', 'Arial, Helvetica, sans-serif', '12', 'FAFAFA', 'Arial, Helvetica, sans-serif', '990000', '12', '333333', NULL, 'FFFFFF', NULL, 'EEEEEE', 'FFFFCC', 'FFFFDD');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_timeformats`
--

CREATE TABLE `freetplclassified_timeformats` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_format` varchar(255) default NULL,
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `freetplclassified_timeformats`
--

INSERT INTO `freetplclassified_timeformats` VALUES(1, '06:20 pm');
INSERT INTO `freetplclassified_timeformats` VALUES(2, '06:20 PM');
INSERT INTO `freetplclassified_timeformats` VALUES(3, '18:20');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_transactions`
--

CREATE TABLE `freetplclassified_transactions` (
  `id` bigint(20) NOT NULL auto_increment,
  `uid` bigint(20) default NULL,
  `amount` decimal(10,2) default NULL,
  `date_submitted` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `description` longtext,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=559 ;

--
-- Dumping data for table `freetplclassified_transactions`
--

INSERT INTO `freetplclassified_transactions` VALUES(3, 8, '20.00', '2004-02-09 11:38:05', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(10, 8, '-2.00', '2004-02-10 11:43:57', 'Made Your Product ''Digital camera'' to be Appeared as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(11, 8, '-5.00', '2004-02-10 11:43:58', 'Made Your Product ''Digital camera'' to be Appeared as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(12, 8, '-7.00', '2004-02-10 11:43:58', 'Made Your Product ''Digital camera'' to be Appeared as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(19, 8, '-5.00', '2004-02-10 01:15:37', 'Purchased 3 images for Product ''Digital camera''.');
INSERT INTO `freetplclassified_transactions` VALUES(18, 8, '2.00', '2004-02-10 01:15:14', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(17, 8, '-5.00', '2004-02-10 01:03:35', 'Purchased 3 images for Product ''Digital camera''.');
INSERT INTO `freetplclassified_transactions` VALUES(16, 8, '2.00', '2004-02-10 12:29:28', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(28, 8, '20.00', '2004-02-12 04:00:32', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(29, 8, '-2.00', '2004-02-12 04:33:39', 'Made Your Product ''keyboard'' to be Appeared as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(30, 8, '-5.00', '2004-02-12 04:33:39', 'Made Your Product ''keyboard'' to be Appeared as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(31, 8, '-5.00', '2004-02-12 04:33:39', 'Made Your Product ''keyboard'' to be Appeared  as   Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(32, 8, '-7.00', '2004-02-12 04:33:39', 'Made Your Product ''keyboard'' to be Appeared as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(33, 8, '20.00', '2004-02-12 05:36:30', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(34, 8, '-2.00', '2004-02-12 06:26:24', 'Made Your Product ''keys2'' to be Appeared as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(35, 8, '-5.00', '2004-02-12 06:26:24', 'Made Your Product ''keys2'' to be Appeared as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(36, 8, '-5.00', '2004-02-12 06:26:24', 'Made Your Product ''keys2'' to be Appeared  as   Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(37, 8, '-5.00', '2004-02-12 06:26:24', 'Made Your Product ''keys2'' to be Appeared as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(38, 8, '-3.00', '2004-02-12 08:17:06', 'Made Your Product ''keys'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(39, 8, '10.00', '2004-02-12 08:20:45', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(40, 8, '-6.00', '2004-02-12 08:22:20', 'Made Your Product ''keys'' to be Appeared as gallery  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(41, 8, '-2.00', '2004-02-12 08:25:15', 'Made Your Product ''keys'' to be Appeared as bold item');
INSERT INTO `freetplclassified_transactions` VALUES(42, 8, '5.00', '2004-02-12 08:29:12', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(43, 8, '-7.00', '2004-02-12 08:29:18', 'Made Your Product ''keys'' to be Appeared as highlighted item');
INSERT INTO `freetplclassified_transactions` VALUES(45, 8, '20.00', '2004-02-14 11:51:00', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(46, 8, '-3.00', '2004-02-14 11:51:12', 'Made Your Product ''testzzzzz3'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(47, 8, '-3.00', '2004-02-14 11:51:14', 'Made Your Product ''relist test'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(48, 8, '-3.00', '2004-02-14 11:51:22', 'Made Your Product ''pen'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(49, 8, '-3.00', '2004-02-14 11:51:29', 'Made Your Product ''Digital camera'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(50, 8, '-3.00', '2004-02-14 11:51:34', 'Made Your Product ''testzzzz'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(51, 8, '-6.00', '2004-02-14 11:51:37', 'Made Your Product ''relist test'' to be Appeared as gallery  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(52, 8, '10.00', '2004-02-14 06:09:28', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(53, 8, '-3.00', '2004-02-14 06:09:36', 'Made Your Product ''Coffee Maker'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(57, 8, '-3.00', '2004-01-21 08:12:51', 'Made Your Product ''testing counter'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(58, 8, '-3.00', '2004-01-22 12:01:02', 'Made Your Product ''testing formating'' to be Appeared  as   Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(65, 8, '2.00', '2004-01-24 06:02:18', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(66, 8, '-2.00', '2004-01-24 06:48:32', 'Uploaded product ''eee''');
INSERT INTO `freetplclassified_transactions` VALUES(67, 8, '10.00', '2004-01-24 07:02:44', 'Added Money To Your Account');
INSERT INTO `freetplclassified_transactions` VALUES(68, 8, '-2.00', '2004-01-24 07:03:58', 'Uploaded product ''aaa''');
INSERT INTO `freetplclassified_transactions` VALUES(69, 8, '-2.00', '2004-01-24 07:06:28', 'Uploaded product ''test''');
INSERT INTO `freetplclassified_transactions` VALUES(70, 8, '-2.00', '2004-01-24 07:10:29', 'Uploaded product ''saa''');
INSERT INTO `freetplclassified_transactions` VALUES(71, 8, '-2.00', '2004-01-24 08:39:53', 'Uploaded product ''fixxxx''');
INSERT INTO `freetplclassified_transactions` VALUES(72, 8, '-2.00', '2004-01-24 08:39:53', 'Made Your Product ''fixxxx'' to be Appeared as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(74, 8, '10.00', '2004-01-29 14:52:16', 'Added to your Account\r\n');
INSERT INTO `freetplclassified_transactions` VALUES(75, 8, '-5.00', '2004-01-29 02:52:29', 'Purchased 3 images for Product ''testing formating''.');
INSERT INTO `freetplclassified_transactions` VALUES(78, 8, '-2.00', '2004-02-04 02:22:20', 'Made Your Product ''classified1'' to be Appeared as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(79, 8, '-1.00', '2004-02-04 02:22:20', 'Made Your Product ''classified1'' to be Appeared as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(80, 8, '-3.00', '2004-02-04 02:22:20', 'Made Your Product ''classified1'' to be Appeared  as   Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(81, 8, '12.00', '2004-03-02 10:54:01', 'uyt');
INSERT INTO `freetplclassified_transactions` VALUES(83, 8, '-2.00', '2004-03-08 07:46:49', 'Made Your Product ''p1'' to be Appeared as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(84, 8, '-1.00', '2004-03-08 07:46:49', 'Made Your Product ''p1'' to be Appeared as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(85, 16, '50.00', '2004-03-08 09:28:25', 'Added by admin');
INSERT INTO `freetplclassified_transactions` VALUES(86, 8, '-2.00', '2004-03-17 11:36:38', 'Made Your Product ''testing total price'' to be Appeared as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(87, 8, '-1.00', '2004-03-17 11:36:38', 'Made Your Product ''testing total price'' to be Appeared as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(93, 8, '0.30', '2004-03-23 11:37:01', 'Extend duration of classified ''bp3/b''');
INSERT INTO `freetplclassified_transactions` VALUES(94, 8, '0.30', '2004-03-23 11:37:06', 'Extend duration of classified ''bp3/b''');
INSERT INTO `freetplclassified_transactions` VALUES(95, 8, '0.25', '2004-03-23 11:38:59', 'Extend duration of classified ''bp3/b''');
INSERT INTO `freetplclassified_transactions` VALUES(96, 8, '-0.25', '2004-03-23 12:01:38', 'Extend duration of classified ''bp3/b''');
INSERT INTO `freetplclassified_transactions` VALUES(91, 8, '3.00', '2004-03-20 15:07:13', 'Made Your Product ''testing_buynow_option'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(98, 8, '-1.00', '2004-03-23 05:17:37', 'Activate Buy Now option for Classified ''p3''');
INSERT INTO `freetplclassified_transactions` VALUES(101, 8, '-3.00', '2004-04-03 04:02:10', 'Made Your Classified ''bp3/b'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(102, 8, '-0.50', '2004-04-03 04:03:16', 'Extend duration of classified ''bp3/b''');
INSERT INTO `freetplclassified_transactions` VALUES(103, 8, '-3.00', '2004-04-03 04:04:19', 'Made Your Classified ''p3'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(104, 8, '10.00', '2004-04-03 05:19:24', 'Added by admin');
INSERT INTO `freetplclassified_transactions` VALUES(105, 8, '-3.00', '2004-04-03 05:19:31', 'Made Your Classified ''p1'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(111, 8, '-0.05', '2004-04-03 08:43:01', 'Extend duration of classified ''proviney1''');
INSERT INTO `freetplclassified_transactions` VALUES(107, 8, '-2.00', '2004-04-03 05:29:19', 'Made Your Classified ''bp3/b'' to be Appeared as gallery  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(108, 8, '-2.00', '2004-04-03 05:29:24', 'Made Your Classified ''p3'' to be Appeared as gallery  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(109, 8, '-3.00', '2004-04-03 05:53:40', 'Purchased 3 images for Classified ''p3''.');
INSERT INTO `freetplclassified_transactions` VALUES(112, 8, '-1.00', '2004-04-11 11:17:27', 'Made Your Classified ''SEO Guide:Google'' to be Appeared as gallery  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(113, 8, '-1.00', '2004-07-10 14:30:35', 'Activate Buy Now option for Classified ''SEO Guide:Google''');
INSERT INTO `freetplclassified_transactions` VALUES(114, 8, '1000.00', '2004-07-12 09:34:36', 'Added by Admin');
INSERT INTO `freetplclassified_transactions` VALUES(115, 8, '-0.05', '2004-07-12 15:35:59', 'Extend duration of classified ''Testin Classified 12''');
INSERT INTO `freetplclassified_transactions` VALUES(116, 8, '-0.05', '2004-07-12 15:36:25', 'Extend duration of classified ''Testin Classified 12''');
INSERT INTO `freetplclassified_transactions` VALUES(117, 8, '-1.00', '2004-07-12 16:33:43', 'Made Your Classified ''Testin Classified 12'' to be Appeared as category featured');
INSERT INTO `freetplclassified_transactions` VALUES(118, 8, '-1.00', '2004-07-12 16:34:02', 'Made Your Classified ''Testin Classified 12'' to be Appeared as front page  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(119, 8, '-1.00', '2004-07-12 16:34:13', 'Made Your Classified ''Testin Classified 12'' to be Appeared as gallery  featured item');
INSERT INTO `freetplclassified_transactions` VALUES(120, 8, '-1.00', '2004-07-12 16:34:20', 'Made Your Classified ''Testin Classified 12'' to be Appeared as highlighted item');
INSERT INTO `freetplclassified_transactions` VALUES(121, 8, '-1.00', '2004-07-12 16:34:30', 'Made Your Classified ''Testin Classified 12'' to be Appeared as highlighted item');
INSERT INTO `freetplclassified_transactions` VALUES(122, 8, '-1.00', '2004-07-12 16:35:46', 'Made Your Classified ''Testin Classified 12'' to be Appeared as highlighted item');
INSERT INTO `freetplclassified_transactions` VALUES(125, 8, '-1.00', '2004-07-23 17:22:32', 'Made Your Classified ''Classified Script'' to be Appeared as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(126, 8, '-1.00', '2004-07-23 17:22:32', 'Made Your Classified ''Classified Script'' to be Appeared  as   Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(127, 8, '-1.00', '2004-07-23 17:22:32', 'Made Your Classified ''Classified Script'' to be Appeared as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(128, 8, '-1.00', '2004-09-06 10:59:15', 'Activate Buy Now option for Classified ''test posted''');
INSERT INTO `freetplclassified_transactions` VALUES(129, 8, '-1.00', '2004-09-06 10:59:15', 'Activate Buy Now option for Classified ''test posted''');
INSERT INTO `freetplclassified_transactions` VALUES(130, 8, '-1.00', '2004-09-06 11:14:42', 'Enabled buynow option for Your Classified ''testing''');
INSERT INTO `freetplclassified_transactions` VALUES(131, 8, '-1.00', '2004-09-06 11:16:53', 'Activate Buy Now option for Classified ''Classified Script''');
INSERT INTO `freetplclassified_transactions` VALUES(132, 8, '-0.50', '2004-09-06 12:19:51', 'Extend duration of classified ''testing''');
INSERT INTO `freetplclassified_transactions` VALUES(133, 8, '-1.00', '2004-09-06 12:53:12', 'Posted classified ''testing again''');
INSERT INTO `freetplclassified_transactions` VALUES(134, 8, '-1.00', '2004-09-06 12:56:29', 'Posted classified ''df''');
INSERT INTO `freetplclassified_transactions` VALUES(135, 8, '-1.00', '2004-09-06 14:14:00', 'Posted classified ''testing 2''');
INSERT INTO `freetplclassified_transactions` VALUES(136, 8, '-3.00', '2004-09-06 17:15:26', 'Purchased 0 images for Classified ''testing 2''.');
INSERT INTO `freetplclassified_transactions` VALUES(139, 8, '-1.00', '2004-09-07 11:53:50', 'Posted classified ''testing''');
INSERT INTO `freetplclassified_transactions` VALUES(140, 8, '-1.00', '2004-09-07 12:05:58', 'Posted classified ''testing''');
INSERT INTO `freetplclassified_transactions` VALUES(141, 8, '-1.00', '2004-09-07 12:08:45', 'Posted classified ''testing''');
INSERT INTO `freetplclassified_transactions` VALUES(142, 8, '-1.00', '2004-09-07 12:15:09', 'Posted classified ''testing''');
INSERT INTO `freetplclassified_transactions` VALUES(143, 8, '-1.00', '2004-09-07 17:49:13', 'Posted classified ''testing''');
INSERT INTO `freetplclassified_transactions` VALUES(144, 8, '-1.00', '2004-09-07 17:55:24', 'Posted classified ''testing''');
INSERT INTO `freetplclassified_transactions` VALUES(145, 27, '100.00', '2007-07-08 16:04:33', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(146, 27, '-1.00', '2007-07-08 16:06:03', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(147, 27, '-0.50', '2007-07-08 16:06:03', 'Made classifed ''Testing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(148, 27, '-0.75', '2007-07-08 16:06:03', 'Made classified ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(149, 27, '-0.70', '2007-07-08 16:06:03', 'Made classified ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(150, 27, '-0.80', '2007-07-08 16:06:03', 'Made classified ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(151, 27, '-0.60', '2007-07-08 16:06:03', 'Made classified ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(152, 27, '-1.00', '2007-07-08 16:06:03', 'Enabled buynow option for classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(153, 27, '-1.00', '2007-07-08 16:18:14', 'Posted classified ''Adult Toys''');
INSERT INTO `freetplclassified_transactions` VALUES(154, 27, '-0.50', '2007-07-08 16:18:14', 'Made classifed ''Adult Toys'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(155, 27, '-0.75', '2007-07-08 16:18:14', 'Made classified ''Adult Toys'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(156, 27, '-0.70', '2007-07-08 16:18:14', 'Made classified ''Adult Toys'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(157, 27, '-0.80', '2007-07-08 16:18:14', 'Made classified ''Adult Toys'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(158, 27, '-0.60', '2007-07-08 16:18:14', 'Made classified ''Adult Toys'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(159, 27, '-1.00', '2007-07-08 16:18:14', 'Enabled buynow option for classified ''Adult Toys''');
INSERT INTO `freetplclassified_transactions` VALUES(160, 27, '-1.00', '2007-07-08 16:27:13', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(161, 27, '-0.50', '2007-07-08 16:27:13', 'Made classifed ''Testing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(162, 27, '-1.00', '2007-07-08 16:27:13', 'Enabled buynow option for classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(163, 28, '100.00', '2007-07-13 09:47:16', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(164, 28, '-1.00', '2007-07-13 09:51:52', 'Posted classified ''AWerwer''');
INSERT INTO `freetplclassified_transactions` VALUES(165, 28, '-0.50', '2007-07-13 09:51:52', 'Made classifed ''AWerwer'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(166, 28, '-0.75', '2007-07-13 09:51:52', 'Made classified ''AWerwer'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(167, 28, '-0.70', '2007-07-13 09:51:52', 'Made classified ''AWerwer'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(168, 28, '-1.00', '2007-07-13 09:51:52', 'Enabled buynow option for classified ''AWerwer''');
INSERT INTO `freetplclassified_transactions` VALUES(169, 29, '100.00', '2007-07-13 20:35:58', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(170, 30, '100.00', '2007-07-13 22:01:30', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(171, 31, '100.00', '2007-07-14 15:58:07', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(172, 32, '100.00', '2007-07-17 10:31:12', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(173, 32, '-1.00', '2007-07-17 10:32:45', 'Posted classified ''Honda Civic''');
INSERT INTO `freetplclassified_transactions` VALUES(174, 32, '-0.50', '2007-07-17 10:32:45', 'Made classifed ''Honda Civic'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(175, 32, '-0.75', '2007-07-17 10:32:45', 'Made classified ''Honda Civic'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(176, 32, '-0.70', '2007-07-17 10:32:45', 'Made classified ''Honda Civic'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(177, 32, '-0.80', '2007-07-17 10:32:45', 'Made classified ''Honda Civic'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(178, 32, '-0.60', '2007-07-17 10:32:45', 'Made classified ''Honda Civic'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(179, 32, '-1.00', '2007-07-17 10:32:45', 'Enabled buynow option for classified ''Honda Civic''');
INSERT INTO `freetplclassified_transactions` VALUES(180, 32, '-1.00', '2007-07-17 10:34:19', 'Purchased 3 images for Classified ''Honda Civic''.');
INSERT INTO `freetplclassified_transactions` VALUES(181, 27, '-1.00', '2007-07-17 14:18:58', 'Posted classified ''Deluxe Membership''');
INSERT INTO `freetplclassified_transactions` VALUES(182, 27, '-0.50', '2007-07-17 14:18:58', 'Made classifed ''Deluxe Membership'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(183, 27, '-0.75', '2007-07-17 14:18:58', 'Made classified ''Deluxe Membership'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(184, 27, '-0.70', '2007-07-17 14:18:58', 'Made classified ''Deluxe Membership'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(185, 27, '-0.80', '2007-07-17 14:18:58', 'Made classified ''Deluxe Membership'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(186, 27, '-0.60', '2007-07-17 14:18:58', 'Made classified ''Deluxe Membership'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(187, 27, '-1.00', '2007-07-17 14:18:58', 'Enabled buynow option for classified ''Deluxe Membership''');
INSERT INTO `freetplclassified_transactions` VALUES(188, 33, '100.00', '2007-07-19 21:23:56', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(189, 34, '100.00', '2007-07-22 08:51:18', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(190, 35, '100.00', '2007-07-25 04:52:12', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(191, 35, '-1.00', '2007-07-25 05:01:52', 'Posted classified ''Up for sales''');
INSERT INTO `freetplclassified_transactions` VALUES(192, 35, '-0.50', '2007-07-25 05:01:52', 'Made classifed ''Up for sales'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(193, 35, '-0.75', '2007-07-25 05:01:52', 'Made classified ''Up for sales'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(194, 35, '-0.70', '2007-07-25 05:01:52', 'Made classified ''Up for sales'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(195, 35, '-0.80', '2007-07-25 05:01:52', 'Made classified ''Up for sales'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(196, 35, '-0.60', '2007-07-25 05:01:52', 'Made classified ''Up for sales'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(197, 36, '100.00', '2007-08-01 09:32:25', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(198, 37, '100.00', '2007-08-06 20:26:16', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(199, 38, '100.00', '2007-08-08 00:16:41', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(200, 39, '100.00', '2007-08-08 00:20:23', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(201, 40, '100.00', '2007-08-09 22:01:53', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(202, 40, '-1.00', '2007-08-09 22:07:15', 'Posted classified ''Car for Sale''');
INSERT INTO `freetplclassified_transactions` VALUES(203, 41, '100.00', '2007-08-18 12:39:36', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(204, 42, '100.00', '2007-08-21 15:51:46', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(205, 27, '-1.00', '2007-08-29 07:47:02', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(206, 27, '-0.50', '2007-08-29 07:47:02', 'Made classifed ''Testing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(207, 27, '-0.75', '2007-08-29 07:47:02', 'Made classified ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(208, 27, '-0.70', '2007-08-29 07:47:02', 'Made classified ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(209, 27, '-0.80', '2007-08-29 07:47:02', 'Made classified ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(210, 27, '-0.60', '2007-08-29 07:47:02', 'Made classified ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(211, 27, '-1.00', '2007-08-29 07:47:02', 'Enabled buynow option for classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(212, 27, '-1.00', '2007-08-29 07:48:33', 'Posted classified ''xx''');
INSERT INTO `freetplclassified_transactions` VALUES(213, 27, '-0.50', '2007-08-29 07:48:33', 'Made classifed ''xx'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(214, 27, '-0.75', '2007-08-29 07:48:33', 'Made classified ''xx'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(215, 27, '-0.70', '2007-08-29 07:48:33', 'Made classified ''xx'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(216, 27, '-0.80', '2007-08-29 07:48:33', 'Made classified ''xx'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(217, 27, '-0.60', '2007-08-29 07:48:33', 'Made classified ''xx'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(218, 27, '-1.00', '2007-08-29 07:48:33', 'Enabled buynow option for classified ''xx''');
INSERT INTO `freetplclassified_transactions` VALUES(219, 27, '-1.00', '2007-08-29 07:49:37', 'Posted classified ''Deluxe Membership''');
INSERT INTO `freetplclassified_transactions` VALUES(220, 27, '-0.50', '2007-08-29 07:49:37', 'Made classifed ''Deluxe Membership'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(221, 27, '-0.75', '2007-08-29 07:49:37', 'Made classified ''Deluxe Membership'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(222, 27, '-0.70', '2007-08-29 07:49:37', 'Made classified ''Deluxe Membership'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(223, 27, '-0.80', '2007-08-29 07:49:37', 'Made classified ''Deluxe Membership'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(224, 27, '-0.60', '2007-08-29 07:49:37', 'Made classified ''Deluxe Membership'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(225, 27, '-1.00', '2007-08-29 07:49:37', 'Enabled buynow option for classified ''Deluxe Membership''');
INSERT INTO `freetplclassified_transactions` VALUES(226, 43, '100.00', '2007-08-31 04:05:20', 'Sign Up Bonus');
INSERT INTO `freetplclassified_transactions` VALUES(227, 44, '100.00', '2007-08-31 15:11:02', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(228, 44, '-1.00', '2007-08-31 15:14:57', 'Posted classified ''High-fi''');
INSERT INTO `freetplclassified_transactions` VALUES(229, 44, '-0.50', '2007-08-31 15:14:57', 'Made classifed ''High-fi'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(230, 44, '-0.75', '2007-08-31 15:14:57', 'Made classified ''High-fi'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(231, 44, '-0.70', '2007-08-31 15:14:57', 'Made classified ''High-fi'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(232, 44, '-0.80', '2007-08-31 15:14:57', 'Made classified ''High-fi'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(233, 44, '-0.60', '2007-08-31 15:14:57', 'Made classified ''High-fi'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(234, 44, '-1.00', '2007-08-31 15:14:57', 'Enabled buynow option for classified ''High-fi''');
INSERT INTO `freetplclassified_transactions` VALUES(235, 45, '100.00', '2007-09-04 04:16:10', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(236, 45, '-1.00', '2007-09-04 04:19:43', 'Posted classified ''Samsung WEP 410 Bluetooth version 2.0''');
INSERT INTO `freetplclassified_transactions` VALUES(237, 45, '-0.50', '2007-09-04 04:19:43', 'Made classifed ''Samsung WEP 410 Bluetooth version 2.0'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(238, 45, '-0.75', '2007-09-04 04:19:43', 'Made classified ''Samsung WEP 410 Bluetooth version 2.0'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(239, 45, '-0.70', '2007-09-04 04:19:43', 'Made classified ''Samsung WEP 410 Bluetooth version 2.0'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(240, 45, '-0.80', '2007-09-04 04:19:43', 'Made classified ''Samsung WEP 410 Bluetooth version 2.0'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(241, 45, '-0.60', '2007-09-04 04:19:43', 'Made classified ''Samsung WEP 410 Bluetooth version 2.0'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(242, 45, '-1.00', '2007-09-04 04:19:43', 'Enabled buynow option for classified ''Samsung WEP 410 Bluetooth version 2.0''');
INSERT INTO `freetplclassified_transactions` VALUES(243, 46, '100.00', '2007-09-04 04:26:18', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(244, 46, '-1.00', '2007-09-04 04:28:52', 'Posted classified ''Wireless House Alarm System''');
INSERT INTO `freetplclassified_transactions` VALUES(245, 46, '-0.50', '2007-09-04 04:28:52', 'Made classifed ''Wireless House Alarm System'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(246, 46, '-0.75', '2007-09-04 04:28:52', 'Made classified ''Wireless House Alarm System'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(247, 46, '-0.70', '2007-09-04 04:28:52', 'Made classified ''Wireless House Alarm System'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(248, 46, '-0.80', '2007-09-04 04:28:52', 'Made classified ''Wireless House Alarm System'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(249, 46, '-0.60', '2007-09-04 04:28:52', 'Made classified ''Wireless House Alarm System'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(250, 46, '-1.00', '2007-09-04 04:28:52', 'Enabled buynow option for classified ''Wireless House Alarm System''');
INSERT INTO `freetplclassified_transactions` VALUES(251, 47, '100.00', '2007-09-04 04:32:35', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(252, 47, '-1.00', '2007-09-04 04:34:21', 'Posted classified ''E-Sky Belt CP Helicopter with Futaba Transmitter''');
INSERT INTO `freetplclassified_transactions` VALUES(253, 47, '-0.50', '2007-09-04 04:34:21', 'Made classifed ''E-Sky Belt CP Helicopter with Futaba Transmitter'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(254, 47, '-0.75', '2007-09-04 04:34:21', 'Made classified ''E-Sky Belt CP Helicopter with Futaba Transmitter'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(255, 47, '-0.70', '2007-09-04 04:34:21', 'Made classified ''E-Sky Belt CP Helicopter with Futaba Transmitter'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(256, 47, '-0.80', '2007-09-04 04:34:21', 'Made classified ''E-Sky Belt CP Helicopter with Futaba Transmitter'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(257, 47, '-0.60', '2007-09-04 04:34:21', 'Made classified ''E-Sky Belt CP Helicopter with Futaba Transmitter'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(258, 47, '-1.00', '2007-09-04 04:34:21', 'Enabled buynow option for classified ''E-Sky Belt CP Helicopter with Futaba Transmitter''');
INSERT INTO `freetplclassified_transactions` VALUES(259, 48, '100.00', '2007-09-04 04:39:57', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(260, 48, '-1.00', '2007-09-04 04:41:29', 'Posted classified ''BACKBONE ALIGNMENT ADJUSTER''');
INSERT INTO `freetplclassified_transactions` VALUES(261, 48, '-0.50', '2007-09-04 04:41:29', 'Made classifed ''BACKBONE ALIGNMENT ADJUSTER'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(262, 48, '-0.75', '2007-09-04 04:41:29', 'Made classified ''BACKBONE ALIGNMENT ADJUSTER'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(263, 48, '-0.70', '2007-09-04 04:41:29', 'Made classified ''BACKBONE ALIGNMENT ADJUSTER'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(264, 48, '-0.80', '2007-09-04 04:41:29', 'Made classified ''BACKBONE ALIGNMENT ADJUSTER'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(265, 48, '-0.60', '2007-09-04 04:41:29', 'Made classified ''BACKBONE ALIGNMENT ADJUSTER'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(266, 48, '-1.00', '2007-09-04 04:41:29', 'Enabled buynow option for classified ''BACKBONE ALIGNMENT ADJUSTER''');
INSERT INTO `freetplclassified_transactions` VALUES(267, 49, '100.00', '2007-09-04 04:44:44', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(268, 49, '-1.00', '2007-09-04 04:46:41', 'Posted classified ''SOUL by HUGO BOSS''');
INSERT INTO `freetplclassified_transactions` VALUES(269, 49, '-0.50', '2007-09-04 04:46:41', 'Made classifed ''SOUL by HUGO BOSS'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(270, 49, '-0.75', '2007-09-04 04:46:41', 'Made classified ''SOUL by HUGO BOSS'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(271, 49, '-0.70', '2007-09-04 04:46:41', 'Made classified ''SOUL by HUGO BOSS'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(272, 49, '-0.80', '2007-09-04 04:46:41', 'Made classified ''SOUL by HUGO BOSS'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(273, 49, '-0.60', '2007-09-04 04:46:41', 'Made classified ''SOUL by HUGO BOSS'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(274, 49, '-1.00', '2007-09-04 04:46:41', 'Enabled buynow option for classified ''SOUL by HUGO BOSS''');
INSERT INTO `freetplclassified_transactions` VALUES(275, 50, '100.00', '2007-09-04 04:50:02', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(276, 50, '-1.00', '2007-09-04 04:53:40', 'Posted classified ''316L Stainless Steel Magnetic Bracelet with roller ball^^Design 13 ''');
INSERT INTO `freetplclassified_transactions` VALUES(277, 50, '-0.50', '2007-09-04 04:53:40', 'Made classifed ''316L Stainless Steel Magnetic Bracelet with roller ball^^Design 13 '' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(278, 50, '-0.75', '2007-09-04 04:53:40', 'Made classified ''316L Stainless Steel Magnetic Bracelet with roller ball^^Design 13 '' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(279, 50, '-0.70', '2007-09-04 04:53:40', 'Made classified ''316L Stainless Steel Magnetic Bracelet with roller ball^^Design 13 '' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(280, 50, '-0.80', '2007-09-04 04:53:40', 'Made classified ''316L Stainless Steel Magnetic Bracelet with roller ball^^Design 13 '' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(281, 50, '-0.60', '2007-09-04 04:53:40', 'Made classified ''316L Stainless Steel Magnetic Bracelet with roller ball^^Design 13 '' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(282, 50, '-1.00', '2007-09-04 04:53:40', 'Enabled buynow option for classified ''316L Stainless Steel Magnetic Bracelet with roller ball^^Design 13 ''');
INSERT INTO `freetplclassified_transactions` VALUES(283, 51, '100.00', '2007-09-04 10:19:24', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(284, 52, '20.00', '2007-09-04 12:25:38', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(285, 53, '20.00', '2007-11-07 03:24:58', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(286, 53, '-1.00', '2007-11-07 03:26:37', 'Posted classified ''Web Hosting ''');
INSERT INTO `freetplclassified_transactions` VALUES(287, 53, '-0.50', '2007-11-07 03:26:37', 'Made classifed ''Web Hosting '' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(288, 53, '-0.75', '2007-11-07 03:26:37', 'Made classified ''Web Hosting '' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(289, 53, '-0.70', '2007-11-07 03:26:37', 'Made classified ''Web Hosting '' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(290, 53, '-0.80', '2007-11-07 03:26:37', 'Made classified ''Web Hosting '' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(291, 53, '-0.60', '2007-11-07 03:26:37', 'Made classified ''Web Hosting '' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(292, 53, '-1.00', '2007-11-07 04:22:36', 'Posted classified '' Small Storerooms ''');
INSERT INTO `freetplclassified_transactions` VALUES(293, 53, '-0.50', '2007-11-07 04:22:36', 'Made classifed '' Small Storerooms '' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(294, 53, '-0.75', '2007-11-07 04:22:36', 'Made classified '' Small Storerooms '' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(295, 53, '-0.70', '2007-11-07 04:22:36', 'Made classified '' Small Storerooms '' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(296, 53, '-0.80', '2007-11-07 04:22:36', 'Made classified '' Small Storerooms '' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(297, 53, '-0.60', '2007-11-07 04:22:36', 'Made classified '' Small Storerooms '' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(298, 54, '20.00', '2007-11-07 04:38:14', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(299, 54, '-1.00', '2007-11-07 04:46:06', 'Posted classified ''Arrow Consulting''');
INSERT INTO `freetplclassified_transactions` VALUES(300, 54, '-0.50', '2007-11-07 04:46:06', 'Made classifed ''Arrow Consulting'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(301, 54, '-0.75', '2007-11-07 04:46:06', 'Made classified ''Arrow Consulting'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(302, 54, '-0.70', '2007-11-07 04:46:06', 'Made classified ''Arrow Consulting'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(303, 54, '-0.80', '2007-11-07 04:46:06', 'Made classified ''Arrow Consulting'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(304, 54, '-0.60', '2007-11-07 04:46:06', 'Made classified ''Arrow Consulting'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(305, 54, '-1.00', '2007-11-07 04:50:16', 'Posted classified ''Deluxe Membership''');
INSERT INTO `freetplclassified_transactions` VALUES(306, 54, '-0.50', '2007-11-07 04:50:16', 'Made classifed ''Deluxe Membership'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(307, 54, '-0.75', '2007-11-07 04:50:16', 'Made classified ''Deluxe Membership'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(308, 54, '-0.70', '2007-11-07 04:50:16', 'Made classified ''Deluxe Membership'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(309, 54, '-0.80', '2007-11-07 04:50:16', 'Made classified ''Deluxe Membership'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(310, 54, '-0.60', '2007-11-07 04:50:16', 'Made classified ''Deluxe Membership'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(311, 54, '-1.00', '2007-11-07 05:01:26', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(312, 54, '-0.50', '2007-11-07 05:01:26', 'Made classifed ''Testing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(313, 54, '-0.75', '2007-11-07 05:01:26', 'Made classified ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(314, 54, '-0.70', '2007-11-07 05:01:26', 'Made classified ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(315, 54, '-0.80', '2007-11-07 05:01:26', 'Made classified ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(316, 54, '-0.60', '2007-11-07 05:01:26', 'Made classified ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(317, 54, '-1.00', '2007-11-07 05:03:55', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(318, 54, '-0.50', '2007-11-07 05:03:55', 'Made classifed ''Testing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(319, 54, '-0.75', '2007-11-07 05:03:55', 'Made classified ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(320, 54, '-0.70', '2007-11-07 05:03:55', 'Made classified ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(321, 54, '-0.80', '2007-11-07 05:03:55', 'Made classified ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(322, 54, '-0.60', '2007-11-07 05:03:55', 'Made classified ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(323, 54, '10000.00', '2007-11-07 05:12:05', '100');
INSERT INTO `freetplclassified_transactions` VALUES(324, 54, '-1.00', '2007-11-07 05:12:41', 'Posted classified ''Software''');
INSERT INTO `freetplclassified_transactions` VALUES(325, 54, '-0.50', '2007-11-07 05:12:41', 'Made classifed ''Software'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(326, 54, '-0.75', '2007-11-07 05:12:41', 'Made classified ''Software'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(327, 54, '-0.70', '2007-11-07 05:12:41', 'Made classified ''Software'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(328, 54, '-0.80', '2007-11-07 05:12:41', 'Made classified ''Software'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(329, 54, '-0.60', '2007-11-07 05:12:41', 'Made classified ''Software'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(330, 54, '-1.00', '2007-11-07 05:26:21', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(331, 54, '-0.50', '2007-11-07 05:26:21', 'Made classifed ''Testing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(332, 54, '-0.75', '2007-11-07 05:26:21', 'Made classified ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(333, 54, '-0.70', '2007-11-07 05:26:21', 'Made classified ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(334, 54, '-0.80', '2007-11-07 05:26:21', 'Made classified ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(335, 54, '-0.60', '2007-11-07 05:26:21', 'Made classified ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(336, 54, '-1.00', '2007-11-07 05:33:49', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(337, 54, '-0.50', '2007-11-07 05:33:49', 'Made classifed ''Testing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(338, 54, '-0.75', '2007-11-07 05:33:49', 'Made classified ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(339, 54, '-0.70', '2007-11-07 05:33:49', 'Made classified ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(340, 54, '-0.80', '2007-11-07 05:33:49', 'Made classified ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(341, 54, '-0.60', '2007-11-07 05:33:49', 'Made classified ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(342, 54, '-1.00', '2007-11-07 06:00:17', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(343, 54, '-0.75', '2007-11-07 06:00:17', 'Made classified ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(344, 54, '-0.70', '2007-11-07 06:00:17', 'Made classified ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(345, 54, '-0.80', '2007-11-07 06:00:17', 'Made classified ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(346, 54, '-0.60', '2007-11-07 06:00:17', 'Made classified ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(347, 54, '-1.00', '2007-11-07 06:05:36', 'Posted classified ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(348, 54, '-0.75', '2007-11-07 06:05:36', 'Made classified ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(349, 54, '-0.70', '2007-11-07 06:05:36', 'Made classified ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(350, 54, '-0.80', '2007-11-07 06:05:36', 'Made classified ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(351, 54, '-0.60', '2007-11-07 06:05:36', 'Made classified ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(352, 54, '-1.00', '2007-11-07 06:06:34', 'Posted classified ''gggggg''');
INSERT INTO `freetplclassified_transactions` VALUES(353, 54, '-0.50', '2007-11-07 06:06:34', 'Made classifed ''gggggg'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(354, 54, '-0.75', '2007-11-07 06:06:34', 'Made classified ''gggggg'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(355, 54, '-0.70', '2007-11-07 06:06:34', 'Made classified ''gggggg'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(356, 54, '-0.80', '2007-11-07 06:06:34', 'Made classified ''gggggg'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(357, 54, '-0.60', '2007-11-07 06:06:34', 'Made classified ''gggggg'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(358, 54, '-1.00', '2007-11-07 06:09:02', 'Posted classified ''hhhhhhhhhh''');
INSERT INTO `freetplclassified_transactions` VALUES(359, 54, '-0.50', '2007-11-07 06:09:02', 'Made classifed ''hhhhhhhhhh'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(360, 54, '-0.75', '2007-11-07 06:09:02', 'Made classified ''hhhhhhhhhh'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(361, 54, '-0.70', '2007-11-07 06:09:02', 'Made classified ''hhhhhhhhhh'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(362, 54, '-0.80', '2007-11-07 06:09:02', 'Made classified ''hhhhhhhhhh'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(363, 54, '-0.60', '2007-11-07 06:09:02', 'Made classified ''hhhhhhhhhh'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(364, 54, '-1.00', '2007-11-07 06:10:01', 'Posted classified ''iiiiiiiiiii''');
INSERT INTO `freetplclassified_transactions` VALUES(365, 54, '-0.75', '2007-11-07 06:10:01', 'Made classified ''iiiiiiiiiii'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(366, 54, '-0.70', '2007-11-07 06:10:01', 'Made classified ''iiiiiiiiiii'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(367, 54, '-0.80', '2007-11-07 06:10:01', 'Made classified ''iiiiiiiiiii'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(368, 54, '-0.60', '2007-11-07 06:10:01', 'Made classified ''iiiiiiiiiii'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(369, 54, '-1.00', '2007-11-07 06:10:47', 'Posted classified ''yyyyyyyyyyy''');
INSERT INTO `freetplclassified_transactions` VALUES(370, 54, '-0.75', '2007-11-07 06:10:47', 'Made classified ''yyyyyyyyyyy'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(371, 54, '-0.70', '2007-11-07 06:10:47', 'Made classified ''yyyyyyyyyyy'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(372, 54, '-0.80', '2007-11-07 06:10:47', 'Made classified ''yyyyyyyyyyy'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(373, 54, '-0.60', '2007-11-07 06:10:47', 'Made classified ''yyyyyyyyyyy'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(374, 54, '-1.00', '2007-11-07 06:11:32', 'Posted classified ''jjj''');
INSERT INTO `freetplclassified_transactions` VALUES(375, 54, '-0.50', '2007-11-07 06:11:32', 'Made classifed ''jjj'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(376, 54, '-0.75', '2007-11-07 06:11:32', 'Made classified ''jjj'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(377, 54, '-0.70', '2007-11-07 06:11:32', 'Made classified ''jjj'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(378, 54, '-0.80', '2007-11-07 06:11:32', 'Made classified ''jjj'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(379, 54, '-0.60', '2007-11-07 06:11:32', 'Made classified ''jjj'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(380, 54, '-1.00', '2007-11-07 06:12:23', 'Posted classified ''mmmmmmmm''');
INSERT INTO `freetplclassified_transactions` VALUES(381, 54, '-0.75', '2007-11-07 06:12:23', 'Made classified ''mmmmmmmm'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(382, 54, '-0.70', '2007-11-07 06:12:23', 'Made classified ''mmmmmmmm'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(383, 54, '-0.80', '2007-11-07 06:12:23', 'Made classified ''mmmmmmmm'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(384, 54, '-0.60', '2007-11-07 06:12:23', 'Made classified ''mmmmmmmm'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(385, 54, '-1.00', '2007-11-07 12:29:47', 'Posted classified ''aaaaa''');
INSERT INTO `freetplclassified_transactions` VALUES(386, 54, '-0.50', '2007-11-07 12:29:47', 'Made classifed ''aaaaa'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(387, 54, '-0.75', '2007-11-07 12:29:47', 'Made classified ''aaaaa'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(388, 54, '-0.70', '2007-11-07 12:29:47', 'Made classified ''aaaaa'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(389, 54, '-0.80', '2007-11-07 12:29:47', 'Made classified ''aaaaa'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(390, 54, '-0.60', '2007-11-07 12:29:47', 'Made classified ''aaaaa'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(391, 54, '-1.00', '2007-11-07 17:40:05', 'Posted classified ''Software System''');
INSERT INTO `freetplclassified_transactions` VALUES(392, 54, '-0.50', '2007-11-07 17:40:05', 'Made classifed ''Software System'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(393, 54, '-0.75', '2007-11-07 17:40:05', 'Made classified ''Software System'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(394, 54, '-0.70', '2007-11-07 17:40:05', 'Made classified ''Software System'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(395, 54, '-0.80', '2007-11-07 17:40:05', 'Made classified ''Software System'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(396, 54, '-0.60', '2007-11-07 17:40:05', 'Made classified ''Software System'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(397, 54, '-1.00', '2007-11-07 17:56:57', 'Posted Listing ''Deluxe Membership''');
INSERT INTO `freetplclassified_transactions` VALUES(398, 54, '-0.50', '2007-11-07 17:56:57', 'Made Listing ''Deluxe Membership'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(399, 54, '-0.75', '2007-11-07 17:56:57', 'Made Listing ''Deluxe Membership'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(400, 54, '-0.70', '2007-11-07 17:56:57', 'Made Listing ''Deluxe Membership'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(401, 54, '-0.80', '2007-11-07 17:56:57', 'Made Listing ''Deluxe Membership'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(402, 54, '-0.60', '2007-11-07 17:56:57', 'Made Listing ''Deluxe Membership'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(403, 54, '-1.00', '2007-11-07 19:35:51', 'Posted Listing ''Donation Questions''');
INSERT INTO `freetplclassified_transactions` VALUES(404, 54, '-0.50', '2007-11-07 19:35:51', 'Made Listing ''Donation Questions'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(405, 54, '-0.75', '2007-11-07 19:35:51', 'Made Listing ''Donation Questions'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(406, 54, '-0.70', '2007-11-07 19:35:51', 'Made Listing ''Donation Questions'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(407, 54, '-0.80', '2007-11-07 19:35:51', 'Made Listing ''Donation Questions'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(408, 54, '-0.60', '2007-11-07 19:35:51', 'Made Listing ''Donation Questions'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(409, 55, '20.00', '2007-11-07 20:21:44', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(410, 56, '20.00', '2007-11-07 20:22:40', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(411, 56, '-1.00', '2007-11-07 20:23:20', 'Posted Listing ''Home Design ''');
INSERT INTO `freetplclassified_transactions` VALUES(412, 56, '-0.50', '2007-11-07 20:23:20', 'Made Listing ''Home Design '' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(413, 56, '-0.75', '2007-11-07 20:23:20', 'Made Listing ''Home Design '' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(414, 56, '-0.70', '2007-11-07 20:23:20', 'Made Listing ''Home Design '' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(415, 56, '-0.80', '2007-11-07 20:23:20', 'Made Listing ''Home Design '' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(416, 56, '-0.60', '2007-11-07 20:23:20', 'Made Listing ''Home Design '' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(417, 57, '20.00', '2007-11-08 00:45:58', 'Sign Up Bonus');
INSERT INTO `freetplclassified_transactions` VALUES(418, 57, '10000.00', '2007-11-08 00:46:16', '1000');
INSERT INTO `freetplclassified_transactions` VALUES(419, 54, '-1.00', '2007-11-08 00:57:13', 'Posted Listing ''Tampa CPA Firm''');
INSERT INTO `freetplclassified_transactions` VALUES(420, 54, '-0.50', '2007-11-08 00:57:13', 'Made Listing ''Tampa CPA Firm'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(421, 54, '-0.75', '2007-11-08 00:57:13', 'Made Listing ''Tampa CPA Firm'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(422, 54, '-0.70', '2007-11-08 00:57:13', 'Made Listing ''Tampa CPA Firm'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(423, 54, '-0.80', '2007-11-08 00:57:13', 'Made Listing ''Tampa CPA Firm'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(424, 54, '-0.60', '2007-11-08 00:57:13', 'Made Listing ''Tampa CPA Firm'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(425, 58, '20.00', '2007-11-08 01:05:56', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(426, 58, '-1.00', '2007-11-08 01:07:52', 'Posted Listing ''Tampa CPA Firm''');
INSERT INTO `freetplclassified_transactions` VALUES(427, 58, '-0.50', '2007-11-08 01:07:52', 'Made Listing ''Tampa CPA Firm'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(428, 58, '-0.75', '2007-11-08 01:07:52', 'Made Listing ''Tampa CPA Firm'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(429, 58, '-0.70', '2007-11-08 01:07:52', 'Made Listing ''Tampa CPA Firm'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(430, 58, '-0.80', '2007-11-08 01:07:52', 'Made Listing ''Tampa CPA Firm'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(431, 58, '-0.60', '2007-11-08 01:07:52', 'Made Listing ''Tampa CPA Firm'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(432, 58, '-1.00', '2007-11-08 01:12:03', 'Posted Listing ''Elite Creative ''');
INSERT INTO `freetplclassified_transactions` VALUES(433, 58, '-0.50', '2007-11-08 01:12:03', 'Made Listing ''Elite Creative '' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(434, 58, '-0.75', '2007-11-08 01:12:03', 'Made Listing ''Elite Creative '' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(435, 58, '-0.70', '2007-11-08 01:12:03', 'Made Listing ''Elite Creative '' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(436, 58, '-0.80', '2007-11-08 01:12:03', 'Made Listing ''Elite Creative '' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(437, 58, '-0.60', '2007-11-08 01:12:03', 'Made Listing ''Elite Creative '' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(438, 58, '-1.00', '2007-11-08 01:16:27', 'Posted Listing ''TeliWave Graphic Design''');
INSERT INTO `freetplclassified_transactions` VALUES(439, 58, '-0.50', '2007-11-08 01:16:27', 'Made Listing ''TeliWave Graphic Design'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(440, 58, '-0.75', '2007-11-08 01:16:27', 'Made Listing ''TeliWave Graphic Design'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(441, 58, '-0.70', '2007-11-08 01:16:27', 'Made Listing ''TeliWave Graphic Design'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(442, 58, '-0.80', '2007-11-08 01:16:27', 'Made Listing ''TeliWave Graphic Design'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(443, 58, '-0.60', '2007-11-08 01:16:27', 'Made Listing ''TeliWave Graphic Design'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(444, 58, '10000.00', '2007-11-08 01:16:55', '1000');
INSERT INTO `freetplclassified_transactions` VALUES(445, 58, '-1.00', '2007-11-08 01:21:30', 'Posted Listing ''Iventure Management''');
INSERT INTO `freetplclassified_transactions` VALUES(446, 58, '-0.50', '2007-11-08 01:21:30', 'Made Listing ''Iventure Management'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(447, 58, '-0.75', '2007-11-08 01:21:30', 'Made Listing ''Iventure Management'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(448, 58, '-0.70', '2007-11-08 01:21:30', 'Made Listing ''Iventure Management'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(449, 58, '-0.80', '2007-11-08 01:21:30', 'Made Listing ''Iventure Management'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(450, 58, '-0.60', '2007-11-08 01:21:30', 'Made Listing ''Iventure Management'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(451, 58, '-1.00', '2007-11-08 01:23:15', 'Posted Listing '' ABle viSual ''');
INSERT INTO `freetplclassified_transactions` VALUES(452, 58, '-0.50', '2007-11-08 01:23:15', 'Made Listing '' ABle viSual '' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(453, 58, '-0.75', '2007-11-08 01:23:15', 'Made Listing '' ABle viSual '' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(454, 58, '-0.70', '2007-11-08 01:23:15', 'Made Listing '' ABle viSual '' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(455, 58, '-0.80', '2007-11-08 01:23:15', 'Made Listing '' ABle viSual '' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(456, 58, '-0.60', '2007-11-08 01:23:15', 'Made Listing '' ABle viSual '' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(457, 58, '-1.00', '2007-11-08 01:25:43', 'Posted Listing ''Smartpages''');
INSERT INTO `freetplclassified_transactions` VALUES(458, 58, '-0.50', '2007-11-08 01:25:43', 'Made Listing ''Smartpages'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(459, 58, '-0.75', '2007-11-08 01:25:43', 'Made Listing ''Smartpages'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(460, 58, '-0.70', '2007-11-08 01:25:43', 'Made Listing ''Smartpages'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(461, 58, '-0.80', '2007-11-08 01:25:43', 'Made Listing ''Smartpages'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(462, 58, '-0.60', '2007-11-08 01:25:43', 'Made Listing ''Smartpages'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(463, 58, '-1.00', '2007-11-08 01:39:05', 'Posted Listing ''Slimming and Wellness''');
INSERT INTO `freetplclassified_transactions` VALUES(464, 58, '-0.50', '2007-11-08 01:39:05', 'Made Listing ''Slimming and Wellness'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(465, 58, '-0.75', '2007-11-08 01:39:05', 'Made Listing ''Slimming and Wellness'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(466, 58, '-0.70', '2007-11-08 01:39:05', 'Made Listing ''Slimming and Wellness'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(467, 58, '-0.80', '2007-11-08 01:39:05', 'Made Listing ''Slimming and Wellness'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(468, 58, '-0.60', '2007-11-08 01:39:05', 'Made Listing ''Slimming and Wellness'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(469, 58, '-1.00', '2007-11-08 01:41:34', 'Posted Listing '' Manpower Sourcing''');
INSERT INTO `freetplclassified_transactions` VALUES(470, 58, '-0.50', '2007-11-08 01:41:34', 'Made Listing '' Manpower Sourcing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(471, 58, '-0.75', '2007-11-08 01:41:34', 'Made Listing '' Manpower Sourcing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(472, 58, '-0.70', '2007-11-08 01:41:34', 'Made Listing '' Manpower Sourcing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(473, 58, '-0.80', '2007-11-08 01:41:34', 'Made Listing '' Manpower Sourcing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(474, 58, '-0.60', '2007-11-08 01:41:34', 'Made Listing '' Manpower Sourcing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(475, 58, '-1.00', '2007-11-08 01:43:55', 'Posted Listing ''Ads Space''');
INSERT INTO `freetplclassified_transactions` VALUES(476, 58, '-0.50', '2007-11-08 01:43:55', 'Made Listing ''Ads Space'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(477, 58, '-0.75', '2007-11-08 01:43:55', 'Made Listing ''Ads Space'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(478, 58, '-0.70', '2007-11-08 01:43:55', 'Made Listing ''Ads Space'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(479, 58, '-0.80', '2007-11-08 01:43:55', 'Made Listing ''Ads Space'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(480, 58, '-0.60', '2007-11-08 01:43:55', 'Made Listing ''Ads Space'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(481, 58, '-1.00', '2007-11-08 01:46:56', 'Posted Listing ''Lock+Store''');
INSERT INTO `freetplclassified_transactions` VALUES(482, 58, '-0.50', '2007-11-08 01:46:56', 'Made Listing ''Lock+Store'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(483, 58, '-0.75', '2007-11-08 01:46:56', 'Made Listing ''Lock+Store'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(484, 58, '-0.70', '2007-11-08 01:46:56', 'Made Listing ''Lock+Store'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(485, 58, '-0.80', '2007-11-08 01:46:56', 'Made Listing ''Lock+Store'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(486, 58, '-0.60', '2007-11-08 01:46:56', 'Made Listing ''Lock+Store'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(487, 59, '20.00', '2007-11-08 02:30:24', 'Sign Up Bonus');
INSERT INTO `freetplclassified_transactions` VALUES(488, 58, '-1.00', '2007-11-09 03:12:37', 'Posted Listing ''Ads Space''');
INSERT INTO `freetplclassified_transactions` VALUES(489, 58, '-0.50', '2007-11-09 03:12:37', 'Made Listing ''Ads Space'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(490, 58, '-0.75', '2007-11-09 03:12:37', 'Made Listing ''Ads Space'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(491, 58, '-0.70', '2007-11-09 03:12:37', 'Made Listing ''Ads Space'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(492, 58, '-0.80', '2007-11-09 03:12:37', 'Made Listing ''Ads Space'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(493, 58, '-0.60', '2007-11-09 03:12:37', 'Made Listing ''Ads Space'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(494, 59, '-1.00', '2007-11-09 16:23:27', 'Posted Listing ''Air Craft ''');
INSERT INTO `freetplclassified_transactions` VALUES(495, 59, '-0.50', '2007-11-09 16:23:27', 'Made Listing ''Air Craft '' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(496, 59, '-0.75', '2007-11-09 16:23:27', 'Made Listing ''Air Craft '' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(497, 59, '-0.70', '2007-11-09 16:23:27', 'Made Listing ''Air Craft '' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(498, 59, '-0.80', '2007-11-09 16:23:27', 'Made Listing ''Air Craft '' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(499, 59, '-0.60', '2007-11-09 16:23:27', 'Made Listing ''Air Craft '' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(500, 60, '20.00', '2007-11-09 18:43:11', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(501, 61, '20.00', '2007-11-09 19:02:12', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(502, 61, '-1.00', '2007-11-09 19:03:09', 'Posted Listing ''Testing''');
INSERT INTO `freetplclassified_transactions` VALUES(503, 61, '-0.50', '2007-11-09 19:03:09', 'Made Listing ''Testing'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(504, 61, '-0.75', '2007-11-09 19:03:09', 'Made Listing ''Testing'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(505, 61, '-0.70', '2007-11-09 19:03:09', 'Made Listing ''Testing'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(506, 61, '-0.80', '2007-11-09 19:03:09', 'Made Listing ''Testing'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(507, 61, '-0.60', '2007-11-09 19:03:09', 'Made Listing ''Testing'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(508, 62, '20.00', '2007-11-13 17:57:04', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(509, 63, '20.00', '2007-11-29 06:49:59', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(510, 63, '-1.00', '2007-11-29 06:52:53', 'Posted Listing ''hoistudio''');
INSERT INTO `freetplclassified_transactions` VALUES(511, 63, '-0.50', '2007-11-29 06:52:53', 'Made Listing ''hoistudio'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(512, 63, '-0.75', '2007-11-29 06:52:53', 'Made Listing ''hoistudio'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(513, 63, '-0.70', '2007-11-29 06:52:53', 'Made Listing ''hoistudio'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(514, 63, '-0.80', '2007-11-29 06:52:53', 'Made Listing ''hoistudio'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(515, 63, '-0.60', '2007-11-29 06:52:53', 'Made Listing ''hoistudio'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(516, 63, '-1.00', '2007-11-29 06:52:53', 'Enabled buynow option for Listing ''hoistudio''');
INSERT INTO `freetplclassified_transactions` VALUES(517, 64, '20.00', '2007-12-01 07:28:40', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(518, 65, '20.00', '2007-12-14 10:26:03', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(519, 65, '-1.00', '2007-12-14 10:34:37', 'Posted Listing ''Sandman 2 Bed Sleep Diagnostic System''');
INSERT INTO `freetplclassified_transactions` VALUES(520, 65, '-0.50', '2007-12-14 10:34:37', 'Made Listing ''Sandman 2 Bed Sleep Diagnostic System'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(521, 65, '-0.70', '2007-12-14 10:34:37', 'Made Listing ''Sandman 2 Bed Sleep Diagnostic System'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(522, 65, '-0.80', '2007-12-14 10:34:37', 'Made Listing ''Sandman 2 Bed Sleep Diagnostic System'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(523, 65, '-0.60', '2007-12-14 10:34:37', 'Made Listing ''Sandman 2 Bed Sleep Diagnostic System'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(524, 65, '-1.00', '2007-12-14 10:34:37', 'Enabled buynow option for Listing ''Sandman 2 Bed Sleep Diagnostic System''');
INSERT INTO `freetplclassified_transactions` VALUES(525, 66, '20.00', '2007-12-14 14:14:35', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(526, 67, '20.00', '2007-12-14 21:58:25', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(527, 67, '-1.00', '2007-12-14 22:04:21', 'Posted Listing ''Mais''');
INSERT INTO `freetplclassified_transactions` VALUES(528, 67, '-0.50', '2007-12-14 22:04:21', 'Made Listing ''Mais'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(529, 67, '-0.75', '2007-12-14 22:04:21', 'Made Listing ''Mais'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(530, 67, '-0.70', '2007-12-14 22:04:21', 'Made Listing ''Mais'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(531, 67, '-0.80', '2007-12-14 22:04:21', 'Made Listing ''Mais'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(532, 67, '-0.60', '2007-12-14 22:04:21', 'Made Listing ''Mais'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(533, 67, '-1.00', '2007-12-14 22:04:21', 'Enabled buynow option for Listing ''Mais''');
INSERT INTO `freetplclassified_transactions` VALUES(534, 68, '20.00', '2007-12-19 21:12:18', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(535, 69, '20.00', '2007-12-29 19:21:49', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(536, 69, '-1.00', '2007-12-29 19:50:32', 'Posted Listing ''mango''');
INSERT INTO `freetplclassified_transactions` VALUES(537, 69, '-0.75', '2007-12-29 19:50:32', 'Made Listing ''mango'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(538, 69, '-0.70', '2007-12-29 19:50:32', 'Made Listing ''mango'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(539, 69, '-0.80', '2007-12-29 19:50:32', 'Made Listing ''mango'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(540, 69, '-1.00', '2007-12-29 19:50:32', 'Enabled buynow option for Listing ''mango''');
INSERT INTO `freetplclassified_transactions` VALUES(541, 70, '20.00', '2008-01-05 17:39:19', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(542, 70, '-1.00', '2008-01-05 17:45:54', 'Posted Listing '',,nm,nm,''');
INSERT INTO `freetplclassified_transactions` VALUES(543, 70, '-0.50', '2008-01-05 17:45:54', 'Made Listing '',,nm,nm,'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(544, 70, '-0.75', '2008-01-05 17:45:54', 'Made Listing '',,nm,nm,'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(545, 70, '-0.70', '2008-01-05 17:45:54', 'Made Listing '',,nm,nm,'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(546, 70, '-0.80', '2008-01-05 17:45:54', 'Made Listing '',,nm,nm,'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(547, 70, '-0.60', '2008-01-05 17:45:54', 'Made Listing '',,nm,nm,'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(548, 70, '-1.50', '2008-01-05 18:05:52', 'Extended duration of classified '',,nm,nm,''');
INSERT INTO `freetplclassified_transactions` VALUES(549, 71, '20.00', '2008-01-08 06:12:49', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(550, 72, '20.00', '2008-01-09 20:21:46', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(551, 73, '20.00', '2008-01-10 15:50:19', 'Signup bonus');
INSERT INTO `freetplclassified_transactions` VALUES(552, 73, '-1.00', '2008-01-10 15:54:14', 'Posted Listing ''Bag''');
INSERT INTO `freetplclassified_transactions` VALUES(553, 73, '-0.50', '2008-01-10 15:54:14', 'Made Listing ''Bag'' to appear as Bold');
INSERT INTO `freetplclassified_transactions` VALUES(554, 73, '-0.75', '2008-01-10 15:54:14', 'Made Listing ''Bag'' to appear as Featured');
INSERT INTO `freetplclassified_transactions` VALUES(555, 73, '-0.70', '2008-01-10 15:54:14', 'Made Listing ''Bag'' to appear as Featured on Front Page');
INSERT INTO `freetplclassified_transactions` VALUES(556, 73, '-0.80', '2008-01-10 15:54:14', 'Made Listing ''Bag'' to appear as Featured in Gallery ');
INSERT INTO `freetplclassified_transactions` VALUES(557, 73, '-0.60', '2008-01-10 15:54:14', 'Made Listing ''Bag'' to appear as Highlighted');
INSERT INTO `freetplclassified_transactions` VALUES(558, 73, '-1.00', '2008-01-10 15:54:14', 'Enabled buynow option for Listing ''Bag''');

-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_types_fields`
--

CREATE TABLE `freetplclassified_types_fields` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_type_id` bigint(20) NOT NULL default '-1',
  `freetpl_field_id` bigint(20) NOT NULL default '-1',
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `freetplclassified_types_fields`
--


-- --------------------------------------------------------

--
-- Table structure for table `freetplclassified_us_states`
--

CREATE TABLE `freetplclassified_us_states` (
  `freetpl_id` bigint(20) NOT NULL auto_increment,
  `freetpl_state` varchar(255) default NULL,
  PRIMARY KEY  (`freetpl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `freetplclassified_us_states`
--

INSERT INTO `freetplclassified_us_states` VALUES(1, 'Alabama');
INSERT INTO `freetplclassified_us_states` VALUES(2, 'Alaska');
INSERT INTO `freetplclassified_us_states` VALUES(3, 'Arizona');
INSERT INTO `freetplclassified_us_states` VALUES(4, 'Arkansas');
INSERT INTO `freetplclassified_us_states` VALUES(5, 'California');
INSERT INTO `freetplclassified_us_states` VALUES(6, 'Colorado');
INSERT INTO `freetplclassified_us_states` VALUES(7, 'Connecticut');
INSERT INTO `freetplclassified_us_states` VALUES(8, 'Delaware');
INSERT INTO `freetplclassified_us_states` VALUES(9, 'Florida');
INSERT INTO `freetplclassified_us_states` VALUES(10, 'Georgia');
INSERT INTO `freetplclassified_us_states` VALUES(11, 'Hawaii');
INSERT INTO `freetplclassified_us_states` VALUES(12, 'Idaho');
INSERT INTO `freetplclassified_us_states` VALUES(13, 'Illinois');
INSERT INTO `freetplclassified_us_states` VALUES(14, 'Indiana');
INSERT INTO `freetplclassified_us_states` VALUES(15, 'Iowa');
INSERT INTO `freetplclassified_us_states` VALUES(16, 'Kansas');
INSERT INTO `freetplclassified_us_states` VALUES(17, 'Kentucky');
INSERT INTO `freetplclassified_us_states` VALUES(18, 'Louisiana');
INSERT INTO `freetplclassified_us_states` VALUES(19, 'Maine');
INSERT INTO `freetplclassified_us_states` VALUES(20, 'Maryland');
INSERT INTO `freetplclassified_us_states` VALUES(21, 'Massachusetts');
INSERT INTO `freetplclassified_us_states` VALUES(22, 'Michigan');
INSERT INTO `freetplclassified_us_states` VALUES(23, 'Minnesota');
INSERT INTO `freetplclassified_us_states` VALUES(24, 'Mississippi');
INSERT INTO `freetplclassified_us_states` VALUES(25, 'Missouri');
INSERT INTO `freetplclassified_us_states` VALUES(26, 'Montana');
INSERT INTO `freetplclassified_us_states` VALUES(27, 'Nebraska');
INSERT INTO `freetplclassified_us_states` VALUES(28, 'Nevada');
INSERT INTO `freetplclassified_us_states` VALUES(29, 'New Hampshire');
INSERT INTO `freetplclassified_us_states` VALUES(30, 'New Jersey');
INSERT INTO `freetplclassified_us_states` VALUES(31, 'New Mexico');
INSERT INTO `freetplclassified_us_states` VALUES(32, 'New York');
INSERT INTO `freetplclassified_us_states` VALUES(33, 'North Carolina');
INSERT INTO `freetplclassified_us_states` VALUES(34, 'North Dakota');
INSERT INTO `freetplclassified_us_states` VALUES(35, 'Ohio');
INSERT INTO `freetplclassified_us_states` VALUES(36, 'Oklahoma');
INSERT INTO `freetplclassified_us_states` VALUES(37, 'Oregon');
INSERT INTO `freetplclassified_us_states` VALUES(38, 'Pennsylvania');
INSERT INTO `freetplclassified_us_states` VALUES(39, 'Rhode Island');
INSERT INTO `freetplclassified_us_states` VALUES(40, 'South Carolina');
INSERT INTO `freetplclassified_us_states` VALUES(41, 'South Dakota');
INSERT INTO `freetplclassified_us_states` VALUES(42, 'Tennessee');
INSERT INTO `freetplclassified_us_states` VALUES(43, 'Texas');
INSERT INTO `freetplclassified_us_states` VALUES(44, 'Utah');
INSERT INTO `freetplclassified_us_states` VALUES(45, 'Vermont');
INSERT INTO `freetplclassified_us_states` VALUES(46, 'Virginia');
INSERT INTO `freetplclassified_us_states` VALUES(47, 'Washington');
INSERT INTO `freetplclassified_us_states` VALUES(48, 'Washington DC');
INSERT INTO `freetplclassified_us_states` VALUES(49, 'West Virginia');
INSERT INTO `freetplclassified_us_states` VALUES(50, 'Wisconsin');
INSERT INTO `freetplclassified_us_states` VALUES(51, 'Wyoming');

-- --------------------------------------------------------

--
-- Table structure for table `freetplprj_ads`
--

CREATE TABLE `freetplprj_ads` (
  `freetpltitle` varchar(255) default NULL,
  `id` bigint(20) NOT NULL auto_increment,
  `url` varchar(255) NOT NULL default '',
  `bannerurl` varchar(255) NOT NULL default '',
  `credits` bigint(20) NOT NULL default '0',
  `displays` bigint(20) NOT NULL default '0',
  `approved` varchar(255) NOT NULL default '',
  `size_id` bigint(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `freetplprj_ads`
--

INSERT INTO `freetplprj_ads` VALUES('dhgate', 8, 'http://www.dhgate.com/', 'http://www.biglobangs.com/images/imgadw.gif', 1000000000, 124, 'yes', 5);
INSERT INTO `freetplprj_ads` VALUES('051292_0532549_banner12', 9, 'http://www.lelong.com.my/', 'http://www.biglobangs.com/images/051292_0532549_banner12.gif', 1000000000, 144, 'yes', 5);
INSERT INTO `freetplprj_ads` VALUES('netbuilder.com.my', 10, 'http://www.netbuilder.com.my/html/', 'http://www.biglobangs.com/images/07836_468_60.gif', 1000000000, 24, 'yes', 5);
INSERT INTO `freetplprj_ads` VALUES('Scripts.com', 7, 'http://www.emall.sg', 'http://www.biglobangs.com/images/linkbanner468x50.gif', 1000000000, 415, 'yes', 5);
INSERT INTO `freetplprj_ads` VALUES('justwfh.com', 11, 'http://www.justwfh.com/', 'http://www.biglobangs.com/images/0782351_t4.gif', 1000000000, 25, 'yes', 5);

-- --------------------------------------------------------

--
-- Table structure for table `freetplprj_affiliate_banner`
--

CREATE TABLE `freetplprj_affiliate_banner` (
  `freetplaff_id` bigint(20) NOT NULL auto_increment,
  `freetplaff_title` varchar(255) default NULL,
  `freetplaff_text` longtext,
  `freetplaff_active` varchar(255) NOT NULL default 'no',
  `size_id` bigint(20) default NULL,
  PRIMARY KEY  (`freetplaff_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `freetplprj_affiliate_banner`
--

