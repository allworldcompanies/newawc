<?php

include_once "logincheck.php";

session_unregister("freetpl_clsplus_userid");
session_unregister("freetpl_clsplus_username");

header("Location: gen_confirm.php?errmsg=".urlencode("You have successfully logged out."));
die();
?>