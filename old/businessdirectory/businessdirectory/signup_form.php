<?
include_once "myconnect.php";
include_once "session.php";

if(isset($_SESSION["freetpl_clsplus_userid"])&&($_SESSION["freetpl_clsplus_userid"]<>""))
{
header("Location:"."gen_confirm_mem.php?errmsg=".urlencode('You are already logged in as '.$_SESSION["freetpl_clsplus_username"]));
die();
}

$len=mysql_fetch_array(mysql_query("select * from freetplclassified_config"));

$invalidaccess="No";
if( !isset($_REQUEST["rnum"]) || !isset($_REQUEST["email"] ) )
{
	$invalidaccess="Yes";
}
else
{
	$email=$_REQUEST["email"];
	$rnum=$_REQUEST["rnum"];
	if(!get_magic_quotes_gpc())
	{
		$email_s=str_replace("$","\$",addslashes($_REQUEST["email"]));
		$rnum_s=str_replace("$","\$",addslashes($_REQUEST["rnum"]));
	}
	else
	{
		$email_s=str_replace("$","\$",$_REQUEST["email"]);
		$rnum_s=str_replace("$","\$",$_REQUEST["rnum"]);
	}
	$rs0_query=mysql_query ("select * from freetplclassified_signups where freetpl_email='$email_s' and freetpl_rnum='$rnum_s'");
	if (!($rs0=mysql_fetch_array($rs0_query)))
	{
		$invalidaccess="Yes";
	}
}

if (($invalidaccess=="Yes")&&($len["freetpl_signup_verification"]=="yes"))
{
	header("Location: gen_confirm.php?err=signup&errmsg=".urlencode("Email verification failed, unable to continue"));
	die();
}

function main()
{
	global $len, $email, $rnum;
/////////////getting length of user name and password
$username_len=$len["username_len"];
$pwd_len=$len["pwd_len"];
if(count($_POST)>11)
{
$username=$_REQUEST["username"];
$fname=$_REQUEST["fname"];
$lname=$_REQUEST["lname"];
$c_name=$_REQUEST["company_name"];
$add1=$_REQUEST["add1"];
$add2=$_REQUEST["add2"];
$city=$_REQUEST["city"];
$state=$_REQUEST["state"];
$zip=$_REQUEST["zip_code"];
$country=$_REQUEST["country"];
$home_phone=$_REQUEST["home_phone"];
$work_phone=$_REQUEST["work_phone"];
$email=$_REQUEST["email"];
}
else
{
$username='';
$fname="";
$lname="";
$c_name="";
$add1="";
$add2="";
$city="";
$state="";
$zip="";
$country=0;
$home_phone="";
$work_phone="";
//$email="";
}
                        
?><SCRIPT language=javascript> 
<!--
function emailCheck (emailStr) {
var emailPat=/^(.+)@(.+)$/
var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
var validChars="\[^\\s" + specialChars + "\]"
var quotedUser="(\"[^\"]*\")"
var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
var atom=validChars + '+'
var word="(" + atom + "|" + quotedUser + ")"
var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
var matchArray=emailStr.match(emailPat)
if (matchArray==null) {
	alert("Email address seems incorrect (check @ and .'s)")
	return false
}
var user=matchArray[1]
var domain=matchArray[2]
if (user.match(userPat)==null) {
    alert("The username doesn't seem to be valid.")
    return false
}
var IPArray=domain.match(ipDomainPat)
if (IPArray!=null) {
    // this is an IP address
	  for (var i=1;i<=4;i++) {
	    if (IPArray[i]>255) {
	        alert("Destination IP address is invalid!")
		return false
	    }
    }
    return true
}
var domainArray=domain.match(domainPat)
if (domainArray==null) {
	alert("The domain name doesn't seem to be valid.")
    return false
}
var atomPat=new RegExp(atom,"g")
var domArr=domain.match(atomPat)
var len=domArr.length
if (domArr[domArr.length-1].length<2 || 
			domArr[domArr.length-1].length>4) {
		   alert("The address must end in a valid domain, or two letter country.")
   return false
}
if (len<2) {
   var errStr="This address is missing a hostname!"
   alert(errStr)
   return false
}
return true;
}



  function formValidate(form) {
	
	    if (form.username.value == "") {
	   alert('Please specify Username');
	   form.username.focus();
	   return false;
	   }
	   if (form.username.value.length< <? echo $username_len?>) {
	   alert('Username must be atleast <? echo $username_len;?> character(s) long');
	   form.username.focus();
	   return false;
	   }
		if((form.username.value.match(/[^a-zA-Z0-9_]/)))
		{
			alert("Username can contain only alphanumeric and underscore characters");
			form.username.focus();
			return(false);
		}
	   
		if(!form.email.value.match(/[a-zA-Z\.\@\d\_]/)) {
           alert('Please specify valid Email Address');
 			form.email.focus();
          return false;
           }
		if (!emailCheck (form.email.value) )
		{
			form.email.focus();
			return (false);
		}
		if(form.email.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from Email Address i.e. & < >");
			form.email.focus();
			return(false);
		}

       if(form.fname.value == "" ){
	   alert('Please specify First Name');
	    form.fname.focus();
          return false; 
           }
		if(form.fname.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from First Name i.e. & < >");
			form.fname.focus();
			return(false);
		}

	    if(form.lname.value == "" )
		{
  	    alert('Please specify Last Name');
			form.lname.focus();
        return false; 
        }
		if(form.lname.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from Last Name i.e. & < >");
			form.lname.focus();
			return(false);
		}
		if(form.company_name.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from Company Name i.e. & < >");
			form.company_name.focus();
			return(false);
		}
		
        if(form.add1.value == "" && form.add2.value == "")
		{
	    alert('Please specify Address');
		form.add1.focus();
        return false; 
        }
		if(form.add1.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from Address i.e. & < >");
			form.add1.focus();
			return(false);
		}
		if(form.add2.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from Address i.e. & < >");
			form.add2.focus();
			return(false);
		}
        if(form.city.value == "")
		{
	    alert('Please specify City');
			form.city.focus();
        return false; 
        }
		if(form.city.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from City i.e. & < >");
			form.city.focus();
			return(false);
		}

		 if(form.state.value== "" )
		 {
           alert('Please specify State');
			form.state.focus();
           return false;
           }
		if(form.state.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from State i.e. & < >");
			form.state.focus();
			return(false);
		}
        
		if(form.zip_code.value == "")
		{
	       alert('Please specify Zip/postal code');
			form.zip_code.focus();
           return false; 
        }
		if(form.zip_code.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from Zip Code i.e. & < >");
			form.zip_code.focus();
			return(false);
		}

        if(form.country.selectedIndex == 0)
		{
  	    alert('Please choose a Country');
			form.country.focus();
        return false; 
        }
        
        if(form.home_phone.value == "")
		{
	       alert('Please specify Business Url');
			form.home_phone.focus();
           return false; 
        }
		if(form.home_phone.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from Business Url i.e. & < >");
			form.home_phone.focus();
			return(false);
		}
		if(form.work_phone.value.match(/[&<>]+/))
		{
			alert("Please remove invalid characters from Phone i.e. & < >");
			form.work_phone.focus();
			return(false);
		}
		
    if (form.pwd.value == "") {
	   alert('Please specify Password');
			form.pwd.focus();
	   return false;
	   }
    if (form.pwd.value.length<<? echo $pwd_len;?>) {
	   alert('Password must be atleast <? echo $pwd_len;?> character(s) long');
			form.pwd.focus();
	   return false;
	   }		
	if (form.pwd.value != form.pwd2.value)
	{
		alert('Passwords do not match');
		form.pwd2.value="";
		form.pwd.focus();
		form.pwd.select();
		return false;
	}

	   
	return true;
  }
// -->
</SCRIPT>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td valign="top"> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="onepxtable">
        <tr class="titlestyle"> 
          <td >&nbsp;<img src="images/sm_333_oo.gif" width="9" height="9"> Get Registered With Us</td>
        </tr>
        <tr> 
          <td > <TABLE width=100% border=0 align="center" cellPadding=2  cellSpacing=5 >
              <FORM name=register onsubmit=return(formValidate(this)); 
                  method=post action="insertmember.php">
                <TR> 
                  <TD width="35%" height="25" align=left valign="top" class="innertablestyle"> 
                    <font  class="normal"><B>Username </B></font></TD>
                  <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                  <TD> <input maxlength=20 size=15 name=username value="<?php echo $username; ?>"> 
                    <font  class="normal">&nbsp;</font><font class="smalltext"  ><br>
                    must be at least <? echo $username_len;?> character(s) long</font></TD>
                </TR>
                <?PHP
                        
                if($len["freetpl_signup_verification"]<>"yes")
				{?>
                <TR> 
                  <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B>E-mail 
                    address</B></font></TD>
                  <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                  <TD><INPUT maxLength=99 size=25 name=email value="<? echo $email;?>"> 
                    <font class="smalltext"><br>
                    </font></TD>
                </TR>
                <?php	
				}	//end if
				else
				{
					echo '<INPUT type="hidden" name="email" value="'.$email.'">';
					echo '<INPUT type="hidden" name="rnum" value="'.$rnum.'">';
				}
				?>
                <TBODY>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B>Full 
                      Name</B></font></TD>
                    <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                    <TD><p> 
                        <INPUT name=fname value="<? echo $fname;?>" size=18 
                        maxLength=120>
                        , 
                        <input 
                        name=lname size="18" value="<? echo $lname;?>">
                        <br>
                        <font class="smalltext">(e.g. Ellen Grant)</font></p></TD>
                  </TR>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B>Company<br>
                      <font color="#003366" size="1"> </font> </B></font></TD>
                    <TD width="2%" align=left valign="top">&nbsp;</TD>
                    <TD><INPUT maxLength=120 size=25 name=company_name value="<? echo $c_name;?>"></TD>
                  </TR>
                  <TR> 
                    <TD width="35%" align=left valign="top" class="innertablestyle"><font  class="normal"><B>Address<br>
                      </B></font></TD>
                    <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                    <TD><INPUT maxLength=120 size=35 name=add1 value="<? echo $add1;?>"> 
                      <br> <INPUT maxLength=120 size=35 name=add2 value="<? echo $add2;?>"></TD>
                  </TR>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B>City, 
                      State(or Region)<br>
                      </B></font></TD>
                    <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                    <TD><INPUT maxLength=120 size=18 name=city value="<? echo $city;?>">
                      , 
                      <input 
                        name=state size="18" value="<? echo $state;?>"></TD>
                  </TR>
                  <?PHP
                        
                  ?>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B>Zip 
                      Code (or Postal code)<br>
                      </B></font></TD>
                    <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                    <TD><INPUT maxLength=11 size=15 name=zip_code value="<? echo $zip;?>"></TD>
                  </TR>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B>Country<br>
                      </B></font></TD>
                    <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                    <TD> <SELECT name=country>
                        <OPTION value=0
                          <? if($country==0) {echo " selected ";}?>>Select a Value 
                        <? 
						  $country1=mysql_query("select * from freetplclassified_country order by country");
						  while($rst= mysql_fetch_array($country1))
						  {
						  ?>
                        <OPTION value=<? echo $rst["id"];?> <? if($rst["id"]==$country) {echo " selected ";}?>><? echo $rst["country"];?></OPTION>
                        <?
							   } // wend
							   ?>
                      </SELECT> </TD>
                  </TR>
                  <?PHP
                        
                  ?>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B>Business Url <br>
                    </B></font></TD>
                    <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                    <TD><input name="home_phone" type="text" value="<? echo $home_phone;?>" size="50"></TD>
                  </TR>
                  <?
                        
                  ?>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B> 
                      Phone<br>
                      </B></font></TD>
                    <TD align=left valign="top">&nbsp;</TD>
                    <TD><INPUT maxLength=20 size=15 name=work_phone value="<? echo $work_phone;?>"> 
                      <font class="smalltext"><br>
                      (e.g., 123-456-7890) </font></TD>
                  </TR>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><b>Password<br>
                      </b></font></TD>
                    <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                    <TD><INPUT type=password maxLength=20 size=15 name=pwd> <font class="smalltext"  ><br>
                      must be atleast <? echo $pwd_len;?> character(s) long.</font></TD>
                  </TR>
                  <?PHP
                        
                  ?>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><B>Confirm 
                      Password<br>
                      </B></font></TD>
                    <TD align=left valign="top"><FONT class=red>*</FONT></TD>
                    <TD><INPUT type=password maxLength=20 size=15 name=pwd2> <font class="smalltext"  ><br>
                      must be atleast <? echo $pwd_len;?> character(s) long.</font></TD>
                  </TR>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal"><b>Terms 
                      And Conditions</b></font></TD>
                    <TD align=left valign="top">&nbsp;</TD>
                    <TD><font class="smalltext" >By submitting this form you agree 
                      to our Terms and conditions found <a onClick="return openpopup('termsandconditions.php');"  class="sidelink"  href="">here</a></font></TD>
                  </TR>
                  <TR> 
                    <TD width="35%" height="25" align=left valign="top" class="innertablestyle"><font  class="normal">&nbsp;</font></TD>
                    <TD align=left valign="top">&nbsp;</TD>
                    <TD><INPUT type=submit value="Register" name=submit> &nbsp;</TD>
                  </TR>
                </TBODY>
              </form>
            </TABLE></td>
        </tr>
      </table>
      <p>&nbsp;</p></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<?
                        
}// end main
include "template.php";
?>