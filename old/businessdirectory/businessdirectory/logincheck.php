<?php

include_once "session.php";  

if (!isset($_SESSION["freetpl_clsplus_userid"]) )
{
	$errmsg="You must be logged to access this page.";
	$id=0;
	
	if(isset($_REQUEST["id"]))
		$id=$_REQUEST["id"];
	
	if(preg_match("/\/add_fav.php/",$_SERVER['SCRIPT_NAME']))
	 	$errmsg="You must be logged-in to add Listings to your favorite list.";
	elseif(preg_match("/\/contactuser.php/",$_SERVER['SCRIPT_NAME']))
	 	$errmsg="You must be logged-in to contact seller.";
	
	$return_add=$_SERVER['PHP_SELF'];
	
	header("Location: signinform.php?id=$id&return_add=$return_add&errmsg=".urlencode($errmsg));
	
}
?>