<?
include "logincheck.php";
include_once "myconnect.php";
function main()
{
$rs0=mysql_fetch_array(mysql_query("select * from freetplclassified_config"));
$cur=mysql_fetch_array(mysql_query("select * from freetplclassified_currency where id=".$rs0["cur_id"]));

?>
<script language="JavaScript">
function Validator(form)
{
if(isNaN(form.bonus.value) || (form.bonus.value<0) || (form.bonus.value==""))
{
alert ('Enter a non negative numeric value for Bonus Amount!');
form.bonus.focus();
return false;
}
if(form.freetpl_same_fee.checked==true)
{
	if(isNaN(form.item_fees.value) || (form.item_fees.value<0) || (form.item_fees.value==""))
	{
	alert ('Enter a non negative numeric value for Fees for Posting Listings!');
	form.item_fees.focus();
	return false;
	}
}
if(isNaN(form.ext_cost.value) || (form.ext_cost.value<0) || (form.ext_cost.value==""))
{
alert ('Enter a non negative numeric value for Fees for Extending Durations!');
form.ext_cost.focus();
return false;
}
if(isNaN(form.buy_now.value) || (form.buy_now.value<0) || (form.buy_now.value==""))
{
alert ('Enter a non negative numeric value for Fees for Buy Now Option!');
form.buy_now.focus();
return false;
}

if(isNaN(form.featured_rate.value) || (form.featured_rate.value<0) || (form.featured_rate.value==""))
{
alert ('Enter a non negative numeric value for Fees for Featured Listing!');
form.featured_rate.focus();
return false;
}
if(isNaN(form.bold_rate.value) || (form.bold_rate.value<0) || (form.bold_rate.value==""))
{
alert ('Enter a non negative numeric value for Fees for Bold Listing!');
form.bold_rate.focus();
return false;
}
if(isNaN(form.highlight_rate.value) || (form.highlight_rate.value<0) || (form.highlight_rate.value==""))
{
alert ('Enter a non negative numeric value for Fees for Highlighted Listing!');
form.highlight_rate.focus();
return false;
}
if(isNaN(form.fp_featured_rate.value) || (form.fp_featured_rate.value<0)|| (form.fp_featured_rate.value==""))
{
alert ('Enter a non negative numeric value for Fees for Front Page Featured Listing!');
form.fp_featured_rate.focus();
return false;
}
if(isNaN(form.gallery_featured_rate.value) || (form.gallery_featured_rate.value<0) || (form.gallery_featured_rate.value==""))
{
alert ('Enter a non negative numeric value for Fees for Gallery Featured Listing!');
form.gallery_featured_rate.focus();
return false;
}
if(isNaN(form.buy_images.value) || (form.buy_images.value<=0) || (form.buy_images.value==""))
{
alert ('Enter a non negative numeric value for no of Paid Images!');
form.buy_images.focus();
return false;
}
if( isNaN(form.image_rate.value) || (form.image_rate.value<0 ) || (form.image_rate.value=="" ))
{
alert ('Enter a non negative numeric value for Fees for Paid Images!');
form.image_rate.focus();
return false;
}
return true;
}

function change_fee_box(frm)
{
	if(frm.freetpl_same_fee.checked==true)
		frm.item_fees.disabled=false;
	else
		frm.item_fees.disabled=true;
}
</script>
<form action="updatefees.php" method="post" name="frm1" id="frm1"  onSubmit="return Validator(this);" >
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="onepxtable">
          <tr> 
            <td height="25" class="titlestyle">&nbsp;Configure 
              Billing Fees</td>
          </tr>
    <tr>
      <td valign="top">
<table width="100%" align="center" cellpadding="2" cellspacing="5">
          <?PHP
                        
          ?>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Bonus 
              Amount:</font></strong></td>
            <td width="60%" ><font class="normal"><strong><? echo $cur["cur_name"];?></strong> 
              <input name="bonus" type="text" class="box1" id="bonus" value="<? echo $rs0["bonus"];?>" size="7">
              </font><font class="smalltext"><br>
              The amount shown above will be added in to users account when they 
              will register on the site. (Enter 0 to Disable)</font> </td>
          </tr>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Posting </font><font class="smalltext">Listings</font><font class="normal">:</font></strong></td>
            <td ><table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr>
                  <td><input name="freetpl_same_fee" type="checkbox" id="freetpl_same_fee" value="yes" onClick="javascript:change_fee_box(this.form);" <?php echo($rs0["freetpl_same_fee"]=='yes')?'checked':'';?>><font class="normal"><strong>Apply same fee for all categories</strong></font></td>
                </tr>
              </table>
              <strong><font class="normal"><? echo $cur["cur_name"];?></font></strong> 
              <input name="item_fees" type="text" class="box1" id="item_fees" value="<? echo $rs0["item_fees"];?>" size="7"> </font>
              <font class="smalltext"><br>
              The amount shown above will be deducted if a member posts a Listing 
              under any category. Fees can be set category wise from within Categories 
              tab.(Enter 0 to Disable)</font></td>
          </tr>
          <tr> 
            <td align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Extending Duration:</font></strong></td>
            <td ><font class="normal"><strong><? echo $cur["cur_name"];?> </strong> 
              <input name="ext_cost" type="text" class="box1" id="ext_cost" value="<? echo $rs0["ext_cost"];?>" size="7">
              <strong>&nbsp;per day</strong></font><font class="smalltext"><br>
              The amount shown above will be deducted if somebody extends a Listing 
              (Enter 0 to Disable)</font> </td>
          </tr>
          <?PHP
                        
          ?>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Buy Now Option:</font></strong></td>
            <td ><font class="normal"><strong><? echo $cur["cur_name"];?></strong> 
              <input name="buy_now" type="text" class="box1" id="bonus3" value="<? echo $rs0["buy_now"];?>" size="7">
              </font><font class="smalltext"><br>
              The amount shown above will be deducted if somebody chooses to activate 
              Buy Now option. (Enter 0 to Disable)</font></td>
          </tr>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Featured </font><font class="smalltext">Listing</font><font class="normal">:</font></strong></td>
            <td align="left" ><font class="normal"><strong><? echo $cur["cur_name"];?></strong> 
              <input name="featured_rate" type="text" class="box1" id="featured_rate" value="<? echo $rs0["featured_rate"];?>" size="7">
              </font><font class="smalltext"><br>
              The amount shown above will be deducted if somebody chooses to make 
              a Listing as a Featured item. (Enter 0 to Disable)</font></td>
          </tr>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Bold </font><font class="smalltext">Listing</font><font class="normal">:</font></strong></td>
            <td align="left" ><font class="normal"><strong><? echo $cur["cur_name"];?></strong> 
              <input name="bold_rate" type="text" class="box1" id="bold_rate" value="<? echo $rs0["bold_rate"];?>" size="7">
              </font><font class="smalltext"><br>
              The amount shown above will be deducted if somebody chooses to make 
              a Listing to appear as Bold. (Enter 0 to Disable)</font> </td>
          </tr>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Highlighted </font><font class="smalltext">Listing</font><font class="normal">:</font></strong></td>
            <td align="left" ><font class="normal"><strong><? echo $cur["cur_name"];?></strong> 
              <input name="highlight_rate" type="text" class="box1" id="highlight_rate2" value="<? echo $rs0["highlight_rate"];?>" size="7">
              </font><font class="smalltext"><br>
              The amount shown above will be deducted if somebody chooses to make 
              a Listing to appear as Highlighted. (Enter 0 to Disable)</font> 
            </td>
          </tr>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Front page Featured </font><font class="smalltext">Listing</font><font class="normal">:</font></strong></td>
            <td align="left" ><font class="normal"><strong><? echo $cur["cur_name"];?></strong> 
              <input name="fp_featured_rate" type="text" class="box1" id="fp_featured_rate" value="<? echo $rs0["fp_featured_rate"];?>" size="7">
              </font><font class="smalltext"><br>
              The amount shown above will be deducted if somebody chooses to make 
              a Listing to appear in the Front Page Featured Category. (Enter 
              0 to Disable)</font> </td>
          </tr>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Gallery Featured </font><font class="smalltext">Listing</font><font class="normal">:</font></strong></td>
            <td ><font class="normal"><strong><? echo $cur["cur_name"];?></strong></font> 
              <input name="gallery_featured_rate" type="text" class="box1" id="gallery_featured_rate2" value="<? echo $rs0["gallery_featured_rate"];?>" size="7"> 
              <font class="smalltext"><br>
              The amount shown above will be deducted if somebody chooses to make 
              a Listing to appear in Gallery section. (Enter 0 to Disable)</font>            </td>
          </tr>
          <?PHP
                        
          ?>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" ><strong><font class="normal">Fees 
              for Paid Images:</font></strong></td>
            <td ><font class="normal"><strong>&nbsp;&nbsp; 
              <input name="buy_images" type="text" class="box1" id="buy_images2" value="<? echo $rs0["buy_images"];?>" size="7">
              &nbsp;image(s) for <? echo $cur["cur_name"];?></strong></font> <input name="image_rate" type="text" class="box1" id="image_rate2" value="<? echo $rs0["image_rate"];?>" size="7"> 
              <font class="smalltext"><br>
              The amount shown above will be deducted if somebody chooses to post 
              images more than free images allowed for a Listing. (Enter price=0 
              to Disable)</font></td>
          </tr>
          <tr> 
            <td width="40%" align="right" valign="top" class="innertablestyle" >&nbsp;</td>
            <td ><input name="Submit" type="submit" class="submit" value="Update Billing Fees"></td>
          </tr>
        </table></td>
    </tr>
  </table>
  </form>
<script language="JavaScript" type="text/javascript">
change_fee_box(frm1);
</script><?
}// end main
include "template.php";?>
