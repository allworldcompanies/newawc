<?
include_once "logincheck.php"; 
include_once "myconnect.php";

if(!isset($_REQUEST["freetpl_id"]) || !is_numeric($_REQUEST["freetpl_id"]) || ($_REQUEST["freetpl_id"]<1) )
{
	header("Location: manage_types.php?msg=".urlencode("Invalid Id, unable to continue"));
	
}
$freetpl_id=$_REQUEST["freetpl_id"];
$freetplq_type="select * from freetplclassified_categories where id=$freetpl_id";
$freetplrow_type=mysql_fetch_array(mysql_query($freetplq_type));
if(!$freetplrow_type)
{
	header("Location: manage_types.php?msg=".urlencode("No such type exists, please try again"));
	
}
$freetpl_typename=$freetplrow_type["cat_name"];

if(count($_POST)>0)
{
	$freetpl_field_ids='-1';
	foreach($_POST as $freetplkey=>$freetplvalue)
	{
		if(substr($freetplkey,0,8)=='freetplfield_')
			$freetpl_field_ids.=','.$freetplvalue;
	}
	$freetpl_field_ids.=',-1';
	$freetplqd_t_f="delete from freetplclassified_types_fields where freetpl_type_id=$freetpl_id";
	mysql_query($freetplqd_t_f);
	$freetpl_ids_array=explode(",",$freetpl_field_ids);
//	echo $freetpl_field_ids;
	foreach($freetpl_ids_array as $freetplvalue)
		if($freetplvalue!=-1)
		{
			$freetplqi_t_f="insert into freetplclassified_types_fields (freetpl_type_id, freetpl_field_id) values ($freetpl_id, $freetplvalue)";
			mysql_query($freetplqi_t_f);
		}
	header("Location: associate_fields.php?freetpl_id=$freetpl_id&msg=".urlencode("Additional field list has been updated"));
	
}
else
{
	$freetplq_type_field="select * from freetplclassified_types_fields where freetpl_type_id=$freetpl_id";
	$freetplrs_type_field=mysql_query($freetplq_type_field);
	$freetpl_field_ids='-1';
	while($freetplrow_type_field=mysql_fetch_array($freetplrs_type_field))
		$freetpl_field_ids.=','.$freetplrow_type_field['freetpl_field_id'];
	$freetpl_field_ids.=',-1';
}	

function main()
{
	global $freetpl_field_ids, $freetpl_id, $freetpl_typename;
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="onepxtable">
          <tr> 
            <td height="25" class="titlestyle">&nbsp;Additional Fields for <font class="red"><?php echo $freetpl_typename; ?></font></td>
          </tr>
          <?
					  $field_query=mysql_query("select * from freetplclassified_additional_fields where freetpl_common='no' order by freetpl_name");
					  $cnt=0;
					  while($field=mysql_fetch_array($field_query))
					  {
					  $cnt++;
					  $rec_class="innertablestyle";
					  if($cnt%2==0)
					  $rec_class="alternatecolor";
                      ?>
          <tr class="<?php echo $rec_class;?>"> 
            <td valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr> 
                  <td width="5%"><input type="checkbox" name="freetplfield_<?php echo $field['freetpl_id'];?>" id="freetplfield_<?php echo $field['freetpl_id'];?>" value="<?php echo $field['freetpl_id'];?>" <?php echo(strstr($freetpl_field_ids,','.$field['freetpl_id'].','))?'checked':''; ?>></td>
                  <td width="95%"><font class="smalltext"> 
                    <?php 
						echo "<b>".$field["freetpl_label"]."</b>&nbsp;&nbsp;";
						echo "<font class='red'><b>".$field["freetpl_name"]."</b></font>&nbsp;&nbsp;";
						switch($field["freetpl_type"])
						{
						case "text": echo "<em>Textbox</em>";	break;
						case "textarea": echo "<em>Multiline Textbox</em>";	break;
						case "radio": echo "<em>Radio Buttons</em>";	break;
						case "select": echo "<em>Select List</em>";	break;
						case "checkbox": echo "<em>Check Box</em>";	break;
						}
						?>
                    </font></td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td><font class="smalltext"> 
                    <?php 
						if($field["freetpl_req"]=="yes")
						echo "Essential";
						else
						echo "Optional";
						echo "&nbsp;&nbsp;&nbsp;";
						if($field["freetpl_is_num"]=="yes")
						echo "Only Numeric";
						else
						echo "Any Value";
						//echo "&nbsp;&nbsp;&nbsp;".$field["freetpl_int_value"];
						?>
                    </font></td>
                </tr>
              </table></td>
          </tr>
          <?
		}	//end while
	  $cnt++;
	  $rec_class="innertablestyle";
	  if($cnt%2==0)
	  $rec_class="alternatecolor";                   ?>
          <tr class="<?php echo $rec_class;?>">
            <td valign="top"><input type="submit" name="Submit" value="Update">
              <input name="freetpl_id" type="hidden" id="freetpl_id" value="<?php echo $freetpl_id; ?>"></td>
          </tr>
        </table>      
	  </form>
</td>
  </tr>
  <tr> 
    <td> </td>
  </tr>
</table>
<?
}//main()
include "template.php";
?>
