<?php

include_once "session.php";
include_once "../styles.php";

if(!isset($_SESSION["freetpl_clsplus_adminid"]))
	die();

?><html>
<head>
<Title>Image Uploader</Title>

<script language="JavaScript">
function checkFile()
{
	if (form1.userfile.value == "")
	{
		alert("Please choose a file to upload");
		return (false);
	}
	if ( !form1.userfile.value.match(/(.jpg|.png|.gif|.bmp|.jpeg)$/i) )
	{
		alert("Please upload .gif/.jpg/.jpeg/.bmp/.png files only");
		form1.userfile.value="";
		form1.userfile.focus();
		return (false);
	}
	return(true);
}

</script>


</head>

<body bgcolor="<?php echo $freetpl_page_bg; ?>">
<FORM ENCTYPE="multipart/form-data" ACTION="doupload.php?box=<?php echo $_REQUEST["box"]?>&func=2" METHOD=post ID=form1 NAME=form1 onSubmit="javscript:return checkFile(form1);">
  <table border="0" cellpadding="0" cellspacing="0" class="onepxtable">
    <tr class="titlestyle"> 
      <td>&nbsp;Upload Image </TD>
    </TR>
    <tr class="innertablestyle"> 
      <td> <table width="100%">
          <tr> 
            <td valign="top" width="15"><font class="normal">1.</font></td>
            <td width="470"><font class="normal">To upload an file, click the 
              'Browse' button to select the file, or type the path to the file 
              in the Text-box below.</font></td>
          </tr>
          <tr> 
            <td valign="top" width="15"><font class="normal">2.</font></td>
            <td width="470"><font class="normal">Then click 'Upload' button to 
              complete the upload.</font></td>
          </tr>
          <tr> 
            <td valign="top" width="15"><font class="normal">3.</font></td>
            <td width="470"><font class="normal"><font class="red">NOTE:</font> 
              The file transfer can take from a few seconds to a few minutes depending 
              upon the size of the file. Please have patience while the file is 
              being uploaded.</font></td>
          </tr>
          <tr> 
            <td valign="top" width="15"><font class="normal">4.</font></td>
            <td width="470"><font class="normal"><font class="red">NOTE:</font> 
              The file will be renamed if the file with the same name is already 
              present.</font></td>
          </tr>
          <tr> 
            <td colspan="2" valign="top"><font class="normal"><strong>&nbsp;Hit 
              the [Browse] button to find the file on your computer.</strong></font></td>
          </tr>
        </table></TD>
    </TR>
    <TR class="innertablestyle"> 
      <TD><font class="normal"><strong>&nbsp;&nbsp;Image</strong></font> 
        <INPUT NAME=userfile SIZE=30 TYPE=file   MaxFileSize="1000000"> <input type="hidden" name="MAX_FILE_SIZE" value="1000000"> 
      </TD>
    </TR>
    <TR class="innertablestyle"> 
      <TD>&nbsp;</TD>
    </TR>
    <TR class="innertablestyle"> 
      <TD>&nbsp; 
        <input type="submit" value="Upload" name="uploadfile"></TD>
    </TR>
    <TR class="innertablestyle"> 
      <TD><table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr> 
            <td><font  class='normal'><font  class='red'>NOTE</font>: Please have 
              patience, you will not receive any notification until the file is 
              completely transferred.</font></td>
          </tr>
        </table></TD>
    </TR>
  </table>

</FORM>

   
<!--
<Script Language="JavaScript">
function listattach(filename)
{
window.opener.document.form123.<?php //request.QueryString("box") ?>.value=filename
window.close()
}
</script>
<Input type=button value=Done onClick="listattach('<?php //echo filename ?>')">
-->

</body>

</html>