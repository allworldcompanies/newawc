<?
include "logincheck.php";
include "myconnect.php";

/////////add new code
$start_char="freetpl_";

if(count($_POST)>0)
{
	$freetpl_common='no';
	if(isset($_REQUEST["freetpl_common"]))
		$freetpl_common='yes';

			if (!get_magic_quotes_gpc()) {
			$freetpl_name=str_replace('$', '\$',addslashes($_REQUEST["freetpl_name"]));
			$freetpl_type=str_replace('$', '\$',addslashes($_REQUEST["freetpl_type"]));
			$freetpl_label=str_replace('$', '\$',addslashes($_REQUEST["freetpl_label"]));
			$freetpl_int_values=str_replace('$', '\$',addslashes($_REQUEST["freetpl_int_values"]));
			$freetpl_width=str_replace('$', '\$',addslashes($_REQUEST["freetpl_width"]));
			$freetpl_num_lines=str_replace('$', '\$',addslashes($_REQUEST["freetpl_num_lines"]));
			$freetpl_req=str_replace('$', '\$',addslashes($_REQUEST["freetpl_req"]));
			$freetpl_disp=str_replace('$', '\$',addslashes($_REQUEST["freetpl_disp"]));
			$freetpl_num=str_replace('$', '\$',addslashes($_REQUEST["freetpl_num"]));
			}
			else
			{
			$freetpl_name=str_replace('$', '\$',$_REQUEST["freetpl_name"]);
			$freetpl_type=str_replace('$', '\$',$_REQUEST["freetpl_type"]);
			$freetpl_label=str_replace('$', '\$',$_REQUEST["freetpl_label"]);
			$freetpl_int_values=str_replace('$', '\$',$_REQUEST["freetpl_int_values"]);
			$freetpl_width=str_replace('$', '\$',$_REQUEST["freetpl_width"]);
			$freetpl_num_lines=str_replace('$', '\$',$_REQUEST["freetpl_num_lines"]);
			$freetpl_req=str_replace('$', '\$',$_REQUEST["freetpl_req"]);
			$freetpl_disp=str_replace('$', '\$',$_REQUEST["freetpl_disp"]);
			$freetpl_num=str_replace('$', '\$',$_REQUEST["freetpl_num"]);
			}
			$freetpl_width=(int)$freetpl_width;
			$freetpl_num_lines=(int)$freetpl_num_lines;
			$errcnt=0;
                        
		if(strlen(trim($_POST["freetpl_name"]))==0)
		{
			$errmsg[$errcnt]="Please specify Name.";
			$errcnt++;
		}
		elseif(!(preg_match("/^$start_char/",$freetpl_name)))
		{
			$errmsg[$errcnt]="Name must be start with $start_char.";
			$errcnt++;
		}
		elseif(preg_match("/[|;]+/",$freetpl_name))
		{
			$errmsg[$errcnt]="Name must not contain | or ; characters.";
			$errcnt++;

		}
		else
		{
			$rst=mysql_fetch_array(mysql_query("select * from freetplclassified_additional_fields where
			freetpl_name='$freetpl_name'"));
			if($rst)
			{
				$errmsg[$errcnt]="Additional Field with this name already exists.";
				$errcnt++;
			}
		}
		if ( strlen(trim($_POST["freetpl_label"]))==0)
		{
				$errmsg[$errcnt]="Please specify Label.";
				$errcnt++;
		}
		elseif(preg_match("/[|;]+/",$freetpl_label))
		{
			$errmsg[$errcnt]="Label must not contain | or ; characters.";
			$errcnt++;

		}
		if((strlen(trim($_POST["freetpl_width"]))==0)&&(($freetpl_type=="text")||($freetpl_type=="textarea")))
		{
				$errmsg[$errcnt]="Please specify Width.";
				$errcnt++;
		}
		elseif(preg_match("/[|;]+/",$freetpl_width))
		{
			$errmsg[$errcnt]="Width must not contain | or ; characters.";
			$errcnt++;

		}
		if((strlen(trim($_POST["freetpl_num_lines"]))==0)&&($freetpl_type=="textarea"))
		{
				$errmsg[$errcnt]="Please specify Number of Lines.";
				$errcnt++;
		}
		elseif(preg_match("/[|;]+/",$freetpl_num_lines))
		{
			$errmsg[$errcnt]="Number of Lines must not contain | or ; characters.";
			$errcnt++;

		}
                        
		if((strlen(trim($_POST["freetpl_int_values"]))==0)&&(($freetpl_type=="radio")||($freetpl_type=="select")))
		{
				$errmsg[$errcnt]="Please specify Initial Values.";
				$errcnt++;
		}
		elseif(preg_match("/[;]+/",$freetpl_int_values))
		{
			$errmsg[$errcnt]="Initial Values must not contain ; .";
			$errcnt++;

		}


if($errcnt==0)
{
                        
    mysql_query("insert into freetplclassified_additional_fields 
	(freetpl_type,freetpl_name,freetpl_label,freetpl_int_value,freetpl_req,freetpl_display,freetpl_width,freetpl_num_lines,freetpl_is_num, freetpl_common) values ('$freetpl_type','$freetpl_name','$freetpl_label','$freetpl_int_values','$freetpl_req','$freetpl_disp',$freetpl_width,$freetpl_num_lines,'$freetpl_num', '$freetpl_common')");
	
	if(mysql_affected_rows()>0)
	{
	header("Location: "."manage_additional_fields.php?msg=".urlencode("Additional Field has been added."));
	die();
	}
	else
	{
	header("Location: "."manage_additional_fields.php?msg=".urlencode("Some error occurred, please try again."));
	die();
	}
}
}
/////////////////////

function main()
{
global $errcnt,$errmsg,$start_char;

$freetpl_type="";
$freetpl_name="";
$freetpl_label="";
$freetpl_int_values="";
$freetpl_width="";
$freetpl_num_lines="";
$freetpl_req="yes";
$freetpl_disp="yes";
$freetpl_num="yes";
$freetpl_common='no';

////////////////////////////////
?>
<script language="JavaScript1.1">
function Validator()
{

if ( frm1.freetpl_name.value=='' )
{
	alert("Please specify Name");
	document.frm1.freetpl_name.focus();
	return (false);
}
if (!(frm1.freetpl_name.value.match(/^<?php echo $start_char?>/)))
{
	alert("Name must start with <?php echo $start_char?>");
	document.frm1.freetpl_name.focus();
	return (false);
}
if ((frm1.freetpl_name.value.match(/[|;]+/)))
{
	alert("Name must not contain '|' or ';'");
	document.frm1.freetpl_name.focus();
	return (false);
}

if ( frm1.freetpl_label.value=='' )
{
	alert("Please specify Label");
	document.frm1.freetpl_label.focus();
	return (false);
}
if ((frm1.freetpl_label.value.match(/[|;]+/)))
{
	alert("Label must not contain '|' or ';'");
	document.frm1.freetpl_label.focus();
	return (false);
}
if((frm1.freetpl_width.value=='')&&((frm1.freetpl_type.selectedIndex==0)||(frm1.freetpl_type.selectedIndex==1)))
{
	alert("Please specify Width");
	document.frm1.freetpl_width.focus();
	return (false);
}
if ((isNaN(frm1.freetpl_width.value))||(parseInt(frm1.freetpl_width.value)<=0))
{
	alert("Please specify a non negative numeric value for Width");
	document.frm1.freetpl_width.focus();
	return (false);
}
if((frm1.freetpl_num_lines.value=='')&&(frm1.freetpl_type.selectedIndex==1))
{
	alert("Please specify Number of Lines");
	document.frm1.freetpl_num_lines.focus();
	return (false);
}
if ((isNaN(frm1.freetpl_num_lines.value))||(parseInt(frm1.freetpl_num_lines.value)<=0))
{
	alert("Please specify a non negative numeric value for Number of Lines");
	document.frm1.freetpl_num_lines.focus();
	return (false);
}
if ((frm1.freetpl_num_lines.value.match(/[|;]+/)))
{
	alert("Number of Lines must not contain '|' or ';'");
	document.frm1.freetpl_num_lines.focus();
	return (false);
}
if((frm1.freetpl_int_values.value=='')&&((frm1.freetpl_type.selectedIndex==2)||(frm1.freetpl_type.selectedIndex==3)||(frm1.freetpl_type.selectedIndex==4)))
{
	alert("Please specify Initial Values");
	document.frm1.freetpl_int_values.focus();
	return (false);
}
if((frm1.freetpl_int_values.value!="")&&((frm1.freetpl_type.selectedIndex==0)||(frm1.freetpl_type.selectedIndex==1)||(frm1.freetpl_type.selectedIndex==4))&&(frm1.freetpl_int_values.value.match(/[|]+/)))
{
	alert("Initial Values must not contain '|'");
	document.frm1.freetpl_int_values.focus();
	return (false);
}
if ((frm1.freetpl_int_values.value.match(/[;]+/)))
{
	alert("Initial Values must not contain ';'");
	document.frm1.freetpl_int_values.focus();
	return (false);
}

return (true);
}

</script>
<?
                        
if(count($_POST)>0)
{
$freetpl_type=$_POST["freetpl_type"];
$freetpl_name=$_POST["freetpl_name"];
$freetpl_label=$_POST["freetpl_label"];
$freetpl_int_values=$_POST["freetpl_int_values"];
$freetpl_width=$_POST["freetpl_width"];
$freetpl_num_lines=$_POST["freetpl_num_lines"];
$freetpl_req=$_POST["freetpl_req"];
$freetpl_disp=$_POST["freetpl_disp"];
$freetpl_num=$_POST["freetpl_num"];
$freetpl_common=$_POST["freetpl_common"];

if($errcnt<>0)
	{
	?>
<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="errorstyle">
  <tr> 
    <td colspan="2"><strong>&nbsp;Your request cannot be processed due to following 
      reasons</strong></td>
  </tr>
  <tr height="10"> 
    <td colspan="2"></td>
  </tr>
  <?

for ($i=0;$i<$errcnt;$i++)
{
?>
  <tr valign="top"> 
    <td width="6%">&nbsp;<?php echo $i+1;?></td>
    <td width="94%"><?php echo  $errmsg[$i]; ?></td>
  </tr>
  <?
}
?>
</table><br>
<?
	}
	}		

?>
<table width="100%" height="20" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td colspan="2" valign="top"> <div align="center"> 
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="onepxtable">
          <tr align="left" class="titlestyle"> 
       <td height="25">&nbsp;Add Additional Field</td>
      </tr>
	  <tr>
		    <td valign="top">
<table width="100%" border="0" cellspacing="5" cellpadding="2">
                <form action="<? echo $_SERVER['PHP_SELF'];?>" method="post" name="frm1" id="frm1"  
		  onSubmit="return Validator();" >
                  <tr valign="top" class="row1"> 
                    <td width="40%" align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Type:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td width="60%"><select name="freetpl_type">
                        <option value="text" <?php if($freetpl_type=="text") echo "selected";?>>Text 
                        Box</option>
                        <option value="textarea" <?php if($freetpl_type=="textarea") echo "selected";?>> 
                        Mutiline Text Box</option>
                        <option value="select" <?php if($freetpl_type=="select") echo "selected";?>> 
                        Select List</option>
                        <option value="radio" <?php if($freetpl_type=="radio") echo "selected";?>>Radio 
                        Button</option>
                        <option value="checkbox" <?php if($freetpl_type=="checkbox") echo "selected";?>>Check 
                        Box</option>
                      </select> </td>
                  </tr>
                  <tr valign="top" class="row1"> 
                    <td align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Name:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td> <input name="freetpl_name" type="text"  id="freetpl_name" value="<? echo $freetpl_name;?>"> 
                      <font class="smalltext"><br>
                      Field Name must be unique and start with <?php echo $start_char;?></font> 
                    </td>
                  </tr>
                  <tr valign="top" class="row1"> 
                    <td align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Label:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td width="60%"> <input name="freetpl_label" type="text" id="freetpl_label" value="<? echo $freetpl_label?>"> 
                      <font class="smalltext">&nbsp;<br>
                      This will control the display text for the field</font> 
                    </td>
                  </tr>
                  <?PHP
                        
                  ?>
                  <tr valign="top" class="row1"> 
                    <td align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Width:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td> <input name="freetpl_width" type="text"  id="freetpl_width" value="<? echo $freetpl_width;?>" size="5"> 
                      <font class="smalltext">&nbsp;Characters<br>
                      Relevant only if Field Type is textbox or multiline textbox 
                      </font> </td>
                  </tr>
                  <tr valign="top" class="row1"> 
                    <td align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Number 
                        of Lines:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td width="60%"> <input name="freetpl_num_lines" type="text" id="freetpl_num_lines" value="<? echo $freetpl_num_lines?>" size="5"> 
                      <font class="smalltext">&nbsp;<br>
                      Relevant only if Field Type is multiline textbox </font></td>
                  </tr>
                  <tr valign="top" class="row1"> 
                    <td align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Initial 
                        Values:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td> <textarea name="freetpl_int_values" rows="4"  id="freetpl_int_values"><? echo $freetpl_int_values;?></textarea> 
                      <font class="smalltext">&nbsp;<br>
                      Provide the initial value(s) if any. <br>
                      In case of select list provide the different options seperated 
                      by | <br>
                      Preselected option must be preceeded by +<br>
                      In case of radio buttons provide the different radio buttons 
                      seperated by | <br>
                      Prechecked radio button must be preceeded by +</font></td>
                  </tr>
                  <tr valign="top" class="row1"> 
                    <td align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Required:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td><font class="normal"> 
                      <input name="freetpl_req" type="radio" value="yes" <?php if($freetpl_req=="yes") echo "checked"?>>
                      Required 
                      <input type="radio" name="freetpl_req" value="no" <?php if($freetpl_req=="no") echo "checked"?>>
                      Not Required</font><font class="smalltext">&nbsp;<br>
                      This will make field essential if you choose required</font></td>
                  </tr>
                  <tr valign="top" class="row1"> 
                    <td align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Numeric 
                        Check:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td><font class="normal"> 
                      <input name="freetpl_num" type="radio" value="yes" <?php if($freetpl_num=="yes") echo "checked"?>>
                      Required 
                      <input type="radio" name="freetpl_num" value="no" <?php if($freetpl_num=="no") echo "checked"?>>
                      Not Required</font><font class="smalltext">&nbsp;<br>
                      This will check field value for numeric if you choose required</font></td>
                  </tr>
                  <?PHP
                        
                  ?>
                  <tr valign="top" class="row1"> 
                    <td align="right" class="innertablestyle"> <div align="right"><font class="normal"><strong>Display 
                        Settings:</strong></font></div></td>
                    <TD align=left><font class="red">*</font></TD>
                    <td><font class="normal"> 
                      <input type="radio" name="freetpl_disp" value="yes" <?php if($freetpl_disp=="yes") echo "checked"?>>
                      Display 
                      <input type="radio" name="freetpl_disp" value="no" <?php if($freetpl_disp=="no") echo "checked"?>>
                      Don't Display</font><font class="smalltext"><br>
                      This will control whether to display information on the 
                      description page or not</font></td>
                  </tr>
                  <tr class="row1">
                    <td align="right" class="innertablestyle"><font class="normal"><strong>Common 
                      Field:</strong></font></td>
                    <td>&nbsp;</td>
                    <td><input name="freetpl_common" type="checkbox" id="freetpl_common" value="yes" <?php echo($freetpl_common=='yes')?'checked':'';?>>
                      <font class="smalltext"> Make field common for all categories</font></td>
                  </tr>
                  <tr class="row1"> 
                    <td width="40%" align="right" class="innertablestyle"><font class="normal">&nbsp;</font></td>
                    <td width="6">&nbsp;</td>
                    <td width="60%"> <input name="Submit" type="submit" class="submit" value="Add New"></td>
                  </tr>
                </form>
              </table></td>
	  </tr>
</table>

        
      </div></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td align="left" valign="top">
	<table width="100%" border="0" cellspacing="1" cellpadding="0" class="onepxtable">
        <tr> 
          <td height="25" class="titlestyle">&nbsp;Additional Fields</td>
        </tr>
        <?
					  $field_query=mysql_query("select * from freetplclassified_additional_fields order by freetpl_name");
					  $cnt=0;
					  while($field=mysql_fetch_array($field_query))
					  {
					  $cnt++;
					  $rec_class="innertablestyle";
					  if($cnt%2==0)
					  $rec_class="alternatecolor";
                      ?>
					<tr class="<?php echo $rec_class;?>"> 
					  <td valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="1">
              <tr> 
                <td><font class="smalltext"> 
                  <?php 
						echo "<b>".$field["freetpl_label"]."</b>&nbsp;&nbsp;";
						echo "<font class='red'><b>".$field["freetpl_name"]."</b></font>&nbsp;&nbsp;";
						switch($field["freetpl_type"])
						{
						case "text": echo "<em>Textbox</em>";	break;
						case "textarea": echo "<em>Multiline Textbox</em>";	break;
						case "radio": echo "<em>Radio Buttons</em>";	break;
						case "select": echo "<em>Select List</em>";	break;
						case "checkbox": echo "<em>Check Box</em>";	break;
						}
						?>
                  </font></td>
                <td width="150" align="left"><font class="normal"><?php if($field["freetpl_common"]=='no')	{?><a href="associate_cats.php?freetpl_id=<?php echo $field["freetpl_id"];?>" class="smalltext">Associate 
                  Categories </a><?php 	}	//end if
				  						else
										{
				echo 'Common field';
										}?></font></td>
                <td width="40" align="left">&nbsp;<a href="edit_additional_fields.php?id=<?php echo $field["freetpl_id"];?>" class="smalltext">Edit</a></td>
              </tr>
              <?PHP
                        
              ?>
              <tr> 
                <td><font class="smalltext"> 
                  <?php 
						if($field["freetpl_req"]=="yes")
						echo "Essential";
						else
						echo "Optional";
						echo "&nbsp;&nbsp;&nbsp;";
						if($field["freetpl_is_num"]=="yes")
						echo "Only Numeric";
						else
						echo "Any Value";
						//echo "&nbsp;&nbsp;&nbsp;".$field["freetpl_int_value"];
						?>
                  </font></td>
                <td width="40" align="left">&nbsp;</td>
                <td width="40" align="left">&nbsp;<a href="delete_additional_fields.php?id=<?php echo $field["freetpl_id"];?>" class="smalltext" onClick="return confirm('Do you really want to delete the additional field.');">Delete</a></td>
              </tr>
            </table>
					</td>
					</tr>
					<?
					  }
                    ?>
      </table>
		</td>
  </tr>
  <tr> 
    <td colspan="2" align="left" valign="top"> <div align="left"><font class="normal"><strong>&nbsp;</strong></font></div></td>
  </tr>
</table><?

}//end main
include "template.php";


?>