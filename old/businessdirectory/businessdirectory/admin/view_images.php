<?php
include_once "logincheck.php";
include_once "myconnect.php";
include_once "../styles.php";

if(isset($_REQUEST["id"]))
	$pid=(int)$_REQUEST["id"];
else
	$pid=0;

$product=mysql_fetch_array(mysql_query("select * from freetplclassified_products where id=$pid"));
if(!$product)
{
	echo "<font class='normal'><font class='red'><b>Classified not found. Click <a href='#' onclick='javascript:window.close();'>here</a> to close this window</b></font></font>";
	die();
}

$sql=mysql_query("select * from freetplclassified_images where pid=$pid");
$num=mysql_num_rows($sql);
                        
?>
<div align="center">

<script language="JavaScript">
function checkFile(form1)
{
	if (form1.userfile.value == "")
	{
		alert("Please choose a file to upload");
		form1.userfile.focus();
		return (false);
	}
	if ( !form1.userfile.value.match(/(\.jpg|\.png|\.gif|\.bmp|\.jpeg)$/i) )
	{
		alert("Please upload .gif/.jpg/.jpeg/.bmp/.png files only");
		form1.userfile.focus();
		return (false);
	}
	return(true);
}

function submit_frm(frm)
{
	if(checkFile(frm))
	{
		frm.action="doupload.php";
		frm.submit();
	}
}

</script>
<title>Upload Images</title>
<body bgcolor="<?php echo $freetpl_page_bg; ?>">
<table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="onepxtable">
        <tr class="titlestyle"> 
          <td>&nbsp;Images for classified <? echo $product["product_name"]; ?></td>
        </tr>
        <tr> 
          <td class="innertablestyle"><br>
            <font class="normal"> 
            <? 
		if (isset($_REQUEST["msg"]))
		{
		echo "<font class='red'>&nbsp;".$_REQUEST["msg"]."</font><br>";
		}
		?>
            &nbsp;Click <a class="insidelink" href=" " onClick="window.close();">here</a> 
            to close this window.</font><br> 
            <br></td>
        </tr>
        <tr> 
          <td><table width="100%" border="0" cellpadding="0" cellspacing="1"  >
              <?
	 if($rst=mysql_fetch_array($sql))
	 {
	$cnt=0;
	$row=0;
	while($rst)
	{
	$cnt++;
	if($cnt%3==1)
	{
	$row++;
	?>
              <tr> 
                <?
	}
	?>
                <td height="100%"> <table width="100%" height=100% border="0" cellpadding="1" cellspacing="0" class="innertablestyle">
                    <tr> 
                      <td align="left" valign="top"><div align="center"><img width="100" height="100" src="..\uploadedimages\<? echo $rst["url"];?>" ><br>
                          <a href="delete_image.php?id=<? echo $rst["id"];?>&pid=<? echo $rst["pid"];?>" class="small_link" onClick="return confirm('Do you really want to remove the image ?');">Remove</a></div></td>
                    </tr>
                  </table></td>
                <? 
                        
				$rst=mysql_fetch_array($sql);
				if(!($rst))
				{
				$blankpad=(($row*3)-$cnt)%3;
				
				while($blankpad>0)
				 {
				 
				 ?>
                <td><table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" class="innertablestyle">
                    <tr align="center" valign="middle"> 
                      <td><img src="../images/spacer.gif" width="100" height="100" border=0><br> 
                        <font class="smalltext">&nbsp;</font></td>
                    </tr>
                  </table></td>
                <?
				 $blankpad--;}
				 }
          
 if($cnt%3==0)
	{
	?>
              </tr>
              <?
	}
	
	}// end while
	?>
              <?
	}// end if
	else
	{	?>
<tr><td><font class='normal'><font class='red'>&nbsp;No image has been uploaded.</font></font></td></tr>
<?php
	}
	?>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td colspan="2" align="left"> </td>
  </tr>
  <tr> 
    <td colspan="2" align="left">&nbsp;</td>
  </tr>
</td>
  </tr>
  <tr> 
    <td height="25" colspan="2" align="left" valign="middle">&nbsp;</td>
  </tr>
  <tr> 
    <td height="25" colspan="2" align="left" valign="middle"> <table width="100%" border="0" cellspacing="0" cellpadding="0" class="onepxtable">
        <tr class="titlestyle"> 
          <td>&nbsp;Add Image</td>
        </tr>
        <tr> 
          <td><font class=" normal "> 
            <div align="center"> 
              <TABLE width="100%" border=0 align=center cellPadding=5 cellSpacing=1 class="innertablestyle">
        <FORM method=post enctype="multipart/form-data" name=form123 onSubmit="return checkFile(this);" >
                  <TBODY>
                    <TR vAlign=center> 
                      <TD colspan="3"><table>
                          <tr> 
                            <td valign="top" width="15"><font color="#000000" class="normal">1.</font></td>
                            <td width="470"><font color="#000000" class="normal">To 
                              add an image, click the 'Browse' button &amp; select 
                              the file, or type the path to the file in the Text-box 
                              below.</font></td>
                          </tr>
                          <tr> 
                            <td valign="top" width="15"><font color="#000000" class="normal">2.</font></td>
                            <td width="470"><font color="#000000" class="normal">Then 
                              click 'Upload' button to complete the process.</font></td>
                          </tr>
                          <tr> 
                            <td valign="top" width="15"><font color="#000000" class="normal">3.</font></td>
                            <td width="470"><font color="#000000" class="normal"><font class="red">NOTE</font>: 
                              The file transfer can take from a few seconds to 
                              a few minutes depending upon the size of the file. 
                              Please have patience while the file is being uploaded.</font></td>
                          </tr>
                          <tr> 
                            <td valign="top" width="15"><font color="#000000" class="normal">4.</font></td>
                            <td width="470"><font color="#000000" class="normal"><font class="red">NOTE</font>: 
                              The file will be renamed if the file with the same 
                              name is already present.</font></td>
                          </tr>
                        </table></TD>
                    </TR><?php
                       
                    ?><TR vAlign=center> 
                      <TD colspan="3"><font class="normal"><strong>Hit the [Browse] 
                        button to find the file on your computer. 
                        </strong></font></TD>
                    </TR>
                    <TR vAlign=center> 
                      <TD width="36%" class="subtitle"> <div align="left"><strong><font color="#000000" class="normal">Image 
                          <input type="hidden" name="pid" value="<? echo $pid;?>">
                          <input type="hidden" name="num" value="<? echo $num;?>">
                          </font></strong></div></TD>
                      <TD width="6" align="left" valign="top"><font class="red">*</font></TD>
                      <TD width="60%"><font class="normal" color="#666666"> 
                        <INPUT NAME=userfile SIZE=30 TYPE=file   MaxFileSize="1000000" >
                        <input type="hidden" name="MAX_FILE_SIZE" value="1000000">
                        </font></TD>
                    </TR>
                    <TR align=right> 
                      <TD> <div align="center"> </div></TD>
                      <TD width="6"><font class="normal">&nbsp;</font></TD>
                      <TD width="60%" align="left"> <input type="button" name="Submit2" value="Upload"onClick="javascript:submit_frm(this.form);"></TD>
                    </TR>
                    <TR align=left> 
                      <TD colspan="3"><font class="normal"><font class="red">NOTE</font>: 
                        Please have patience, you will not receive any notification 
                        until the file is completely transferred.</font></TD>
                    </TR>
                  </TBODY>
                </FORM>
              </TABLE>
            </div>
            </font></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>