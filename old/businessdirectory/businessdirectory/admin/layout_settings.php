<?php
include_once "logincheck.php";
include_once "myconnect.php";
function main()
{
$rs0=mysql_fetch_array(mysql_query("select * from freetplclassified_config"));
?>
<script language="JavaScript">
function Validator(form)
{
	if( form.list1.value== "" )
	{
	alert ('Please choose an image for site logo!');
	return false;
	}
	if( isNaN(form.th_width.value) || (form.th_width.value=="")||(form.th_width.value<=0) )
	{
	alert ('Please specify a non negative numeric value for Gallery Thumbnail Width!');
	form.th_width.focus();
	return false;
	}
	if( isNaN(form.th_width2.value) || (form.th_width2.value=="")||(form.th_width2.value<=0) )
	{
	alert ('Please specify a non negative numeric value for Listing Thumbnail Width!');
	form.th_width2.focus();
	return false;
	}
return true;
}
function attachment(box)
{
str="fileupload1.php?box="  + box;
freetpllogo_win=window.open(str,"Attachment","top=5,left=30,toolbars=no,maximize=yes,resize=yes,width=400,height=450,location=no,directories=no,scrollbars=yes");
freetpllogo_win.document.focus();
}

function removeattachment(box)
{
	box.value=""
}

</script>

<form action="updatelayout.php" method="post" name="form123" id="form123"  onSubmit="return Validator(this);" >
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="onepxtable">
    <tr> 
      <td height="25" class="titlestyle">&nbsp;Layout Parameters</td>
    </tr><?php
                        
    ?><tr> 
      <td valign="top">
<table width="100%" border="0" cellspacing="5" cellpadding="2">
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle" ><strong><font class="normal">Site 
              Logo:</font></strong></td>
            <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666"> 
              <input name = "list1" type = "text" id="list1" value="<?php echo $rs0["freetpl_logo"]; ?>" size="20" readOnly >
              <input type=BUTTON name=btn_name2 value="Upload" onClick=attachment('list1')>
              <input type=BUTTON name=buttonname2 value="Remove" onClick=removeattachment(list1)>
              </font><font class="smalltext"><br>
              This will control the logo for the site.</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle" ><strong><font class="normal">Default 
              Image:</font></strong></td>
            <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666"> 
              <input name = "list2" type = "text" id="list2" value="<?php echo $rs0["freetpl_default_image"]; ?>" size="20" readOnly >
              <input type=BUTTON name=btn_name2 value="Upload" onClick=attachment('list2')>
              <input type=BUTTON name=buttonname2 value="Remove" onClick=removeattachment(list2)>
              </font><font class="smalltext"><br>
              This will control the image to be displayed with Listings having 
              no picture on search result/ listing pages.</font></td>
          </tr>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle" ><strong><font class="normal">Show 
              Stats:</font></strong></td>
            <td width="52%"><select name="show_stat" id="show_stat">
                <option value="yes" <? if($rs0["show_stat"]=="yes") { echo " selected";}?>>Yes</option>
                <option value="no" <? if($rs0["show_stat"]=="no") { echo " selected";}?>>No</option>
              </select> <font class="smalltext"><br>
              This will control whether to display site stats on the front page 
              or not.</font></td>
          </tr>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle"><strong><font class="normal">Gallery 
              Thumbnail width:</font></strong></td>
            <td><font class="smalltext"> 
              <input name="th_width" type="text" class="box1" id="th_width" value="<? echo $rs0["th_width"];?>" size=" 7">
              pixels<br>
              This will control the width of thumbnails for gallery. </font></td>
          </tr>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle"><strong><font class="normal">Listing 
              Thumbnail width:</font></strong></td>
            <td><font class="smalltext"> 
              <input name="th_width2" type="text" class="box1" id="th_width2" value="<? echo $rs0["th_width2"];?>" size=" 7">
              pixels<br>
              This will control the width of thumbnails for the list of Listing 
              like in search results. </font></td>
          </tr>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle"><strong><font class="normal">Image 
              Magick:</font></strong></td>
            <td><font class="normal"> 
              <input type="radio" name="im_status" value="enable" <?
			  if($rs0["image_magik"]=="enable")
			  {
			  echo " checked";
			  }
			  ?>>
              Enabled 
              <input type="radio" name="im_status" value="disable" <?
			  if($rs0["image_magik"]<>"enable")
			  {
			  echo " checked";
			  }
			  ?>>
              Disabled </font><font class="smalltext"> <br>
              Some functions of the site depend on these settings, contact your 
              web host regarding this.</font></td>
          </tr>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle"><strong><font class="normal">Water 
              Marking:</font></strong></td>
            <td><font class="normal"> 
              <input type="radio" name="water_mark" value="enable" <?
			  if($rs0["water_marking"]=="enable")
			  {
			  echo " checked";
			  }
			  ?>>
              Enabled 
              <input type="radio" name="water_mark" value="disable" <?
			  if($rs0["water_marking"]<>"enable")
			  {
			  echo " checked";
			  }
			  ?>>
              Disabled </font><font class="smalltext"> <br>
              Relevant if Image Magick is enabled.</font></td>
          </tr>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle"><strong><font class="normal">Choose 
              Icons:</font></strong></td>
            <td width="52%"><select name="icon_list" id="icon_list">
                <?
                
								$rs_query=mysql_query("select * from freetplclassified_icons");
								while($rst=mysql_fetch_array($rs_query))
								{ 
								?>
                <option value="<? echo $rst["freetpl_id"];?>" <? if($rs0["freetpl_icon_list"]==$rst["freetpl_id"])
								{ echo "selected";}
								?>><? echo $rst["freetpl_title"];?></option>
                <?
                                }
								  ?>
              </select> <font class="smalltext"><br>
              This will control the icons for the Front end. You can Add more 
              and edit existing icons in Site Icons Section in Public View Tab.</font></font></td>
          </tr>
          <?PHP
                        
          ?>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle"><strong><font class="normal">Date 
              Format:</font></strong></td>
            <td><select name="date_format" id="date_format">
                <?
								$rs_query=mysql_query("select * from freetplclassified_dateformats order by freetpl_id desc");
								while($rst=mysql_fetch_array($rs_query))
								{ 
								?>
                <option value="<? echo $rst["freetpl_id"];?>" <? if($rs0["freetpl_date_format"]==$rst["freetpl_id"])
								{ echo "selected";}
								?>><? echo $rst["freetpl_format"];?></option>
                <?
                                }
								  ?>
              </select> <font class="smalltext" ><br>
              This will control the date string format for displaying date on 
              the site.</font></td>
          </tr>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle"><strong><font class="normal">Time 
              Format:</font></strong></td>
            <td><select name="time_format" id="time_format">
                <?
								$rs_query=mysql_query("select * from freetplclassified_timeformats order by freetpl_id desc");
								while($rst=mysql_fetch_array($rs_query))
								{ 
								?>
                <option value="<? echo $rst["freetpl_id"];?>" <? if($rs0["freetpl_time_format"]==$rst["freetpl_id"])
								{ echo "selected";}
								?>><? echo $rst["freetpl_format"];?></option>
                <?
                                }
								  ?>
              </select> <font class="smalltext"><br>
              This will control the time string format for displaying time on 
              the site.</font></td>
          </tr>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle" ><strong><font class="normal">Elapsed 
              Date:</font></strong></td>
            <td><select name="elapsed_date" id="elapsed_date">
                <option value="yes" <? if($rs0["elapsed_date"]=="yes") { echo " selected";}?>>Yes</option>
                <option value="no" <? if($rs0["elapsed_date"]=="no") { echo " selected";}?>>No</option>
              </select> <font class="smalltext"><br>
              This will control whether to display ending time in remaining time 
              format.</font></td>
          </tr>
          <?PHP
                        
          ?>
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle">&nbsp;</td>
            <td> <font class="normal"> 
              <input name="Submit" type="submit" class="submit" value="Update Site Configuration">
              </font></td>
          </tr>
        </table></td>
    </tr>
  </table>
  </form><?


}// end main
include "template.php";?>
