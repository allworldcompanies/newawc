<?php
include_once "logincheck.php";
include_once "myconnect.php";
function main()
{
$rs0=mysql_fetch_array(mysql_query("select * from freetplclassified_config"));
                        
?>
<script language="JavaScript">
function Validator(form)
{
if(isNaN(form.recperpage.value) || (form.recperpage.value<=0)||(form.recperpage.value==""))
{
alert ('Please specify a numeric value(greater than 0) for Records Per Page!');
form.recperpage.focus();
return false;
}
if(isNaN(form.recinpanel.value) || (form.recinpanel.value<=0)|| (form.recinpanel.value==""))
{
alert ('Please specify a numeric value(greater than 0) for No. of Latest Listings!');
form.recinpanel.focus();
return false;
}
if(isNaN(form.no_of_images.value) || (form.no_of_images.value<=0) ||  (form.no_of_images.value=="") )
{
alert ('Please specify a numeric value(greater than 0) for No. of Images!');
form.no_of_images.focus();
return false;
}
if(isNaN(form.premium_cnt.value) || (form.premium_cnt.value=="") || (form.premium_cnt.value<=0))
{
alert ('Please specify a numeric value(greater than 0) for No. of Premium Stores in Side Panel!');
form.premium_cnt.focus();
return false;
}
if(isNaN(form.featured_items.value) || (form.featured_items.value=="") || (form.featured_items.value<=0))
{
alert ('Please specify a numeric value(greater than 0) for No. of Featured Listings on Front Page!');
form.featured_items.focus();
return false;
}
if(isNaN(form.fp_images.value) || (form.fp_images.value<=0)|| (form.fp_images.value==""))
{
alert ('Please specify a numeric value(greater than 0) for No. of Images on Front Page!');
form.fp_images.focus();
return false;
}
if(isNaN(form.featured_cnt.value) || (form.featured_cnt.value<=0)|| (form.featured_cnt.value==""))
{
alert ('Please specify a numeric value(greater than 0) for No. of Featured Listings on Browse Category Page!');
form.featured_cnt.focus();
return false;
}
if(isNaN(form.username_len.value) || (form.username_len.value<=0)|| (form.username_len.value==""))
{
alert ('Please specify a non negative numeric value for Minimum Username Length!');
form.username_len.focus();
return false;
}
if(isNaN(form.pwd_len.value) || (form.pwd_len.value<=0)|| (form.pwd_len.value==""))
{
alert ('Please specify a non negative numeric value for Minimum Password Length!');
form.pwd_len.focus();
return false;
}
if(isNaN(form.free_images.value) || (form.free_images.value<0)|| (form.free_images.value=="" ))
{
alert ('Please specify a non negative numeric value for No. of Free Images!');
form.free_images.focus();
return false;
}
/*if(form.free_images.value > form.no_of_images.value)
{
alert ('Free Images must be less than or equals to no of images allowed!');
return false;
}*/
if( isNaN(form.max_period.value) || (form.max_period.value<=0 )|| (form.max_period.value=="" ))
{
alert ('Please specify a non negative numeric value for Maximum Duration!');
form.max_period.focus();
return false;
}<?PHP
                        
?>if( isNaN(form.max_ext_period.value) || (form.max_ext_period.value<-1 )|| (form.max_ext_period.value=="" ))
{
alert ('Please specify a valid numeric value for Maximum Extension Duration!');
form.max_ext_period.focus();
return false;
}

if( isNaN(form.image_size.value) || (form.image_size.value=="")||(form.image_size.value<=0) )
{
alert ('Please specify a non negative numeric value for Maximum Image Size!');
form.image_size.focus();
return false;
}

if( form.null_char.value== "" )
{
alert ('Please specify a value for Null Character!');
form.null_char.focus();
return false;
}
if( form.adminemail.value== "" )
{
alert ('Please specify a value for Admin Email!');
form.adminemail.focus();
return false;
}
if( form.sitename.value== "" )
{
alert ('Please specify a value for Website Name!');
form.sitename.focus();
return false;
}
if( form.site_keywords.value== "" )
{
alert ('Please specify a value for Website Keywords!');
form.site_keywords.focus();
return false;
}
if( form.siteaddrs.value== "" )
{
alert ('Please specify a value for Website Address!');
form.siteaddrs.focus();
return false;
}

if( form.welcome_msg.value== "" )
{
alert ('Please specify Welcome Message!');
form.welcome_msg.focus();
return false;
}
return true;
}

</script>

<form action="updateconfig.php" method="post" name="frm1" id="frm1"  onSubmit="return Validator(this);" >
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="onepxtable">
    <tr> 
      <td height="25" class="titlestyle">&nbsp;Configure Site Parameters</td>
    </tr>
    <tr> 
      <td><table width="100%" border="0" cellspacing="5" cellpadding="2">
          <tr valign="top" > 
            <td width="48%" class="innertablestyle"> <div align="right"><font class="normal"><strong>Records 
                Per Page:</strong></font></div></td>
            <td width="52%"> <input name="recperpage" type="text" class="box1" id="recperpage" value="<? echo $rs0["recperpage"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the default number of records to be shown on each 
              page.</font></td>
          </tr>
          <?PHP
                        
          ?>
          <tr valign="top" > 
            <td align="right" class="innertablestyle"><strong><font class="normal">No. 
              of Latest Listings:</font></strong></td>
            <td align="left"> <input name="recinpanel" type="text" class="box1" id="recinpanel" value="<? echo $rs0["recinpanel"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the number of latest Listings to be shown on front 
              page </font></td>
          </tr>
          <tr valign="top" > 
            <td class="innertablestyle"> <div align="right"><font class="normal"><strong>No. 
                of Images:</strong></font></div></td>
            <td> <input name="no_of_images" type="text" class="box1" id="no_of_images" value="<? echo $rs0["no_of_images"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the maximum number of images to be uploaded for 
              a Listing</font></td>
          </tr>
          <tr valign="top" > 
            <td align="right" class="innertablestyle"><strong><font class="normal">No. 
              of Featured Listings on Front Page:</font></strong></td>
            <td> <input name="featured_items" type="text" class="box1" id="featured_items" value="<? echo $rs0["featured_items"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the number of Featured Listings to be shown on front 
              page</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle"><strong><font class="normal">No. 
              of Images on Front Page:</font></strong></td>
            <td> <input name="fp_images" type="text" class="box1" id="fp_images" value="<? echo $rs0["fp_images"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the number of rows of images to be shown in gallery 
              featured section on the front page.</font></td>
          </tr>
          <?PHP
                        
          ?>
          <tr valign="top"> 
            <td align="right" class="innertablestyle"><strong><font class="normal">No. 
              of Featured Listings on Browse Category Page:</font></strong></td>
            <td> <input name="featured_cnt" type="text" class="box1" id="featured_cnt" value="<? echo $rs0["featured_cnt"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the number of featured Listings on the category 
              browsing page.</font></td>
          </tr>
          <tr valign="top" > 
            <td class="innertablestyle"> <div align="right"><font class="normal"><strong>Minimum 
                Username Length:</strong></font></div></td>
            <td> <input name="username_len" type="text" class="box1" id="username_len" value="<? echo $rs0["username_len"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the minimum number of characters in Username</font>            </td>
          </tr>
          <tr valign="top" > 
            <td align="right" class="innertablestyle"><font class="normal"><strong>Minimum 
              Password Length:</strong></font></td>
            <td> <input name="pwd_len" type="text" class="box1" id="pwd_len" value="<? echo $rs0["pwd_len"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the minimum number of characters in Password</font>            </td>
          </tr>
          <tr valign="top" > 
            <td height="24" align="right" class="innertablestyle"><strong><font class="normal">No. 
              of free Images:</font></strong></td>
            <td> <input name="free_images" type="text" class="box1" id="free_images" value="<? echo $rs0["free_images"];?>" size="7"> 
              <font class="smalltext"><br>
              This will control the number of free images to be uploaded for a 
              Listing</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle"><strong><font class="normal">Maximum 
              Duration:</font></strong></td>
            <td><strong><font class="normal"> 
              <input name="max_period" type="text" class="box1" id="max_period2" value="<? echo $rs0["max_period"];?>" size="7">
              <strong> Days</strong></font></strong><font class="smalltext"><br>
              This will control the maximum number of days for which a Listing 
              can be posted</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle"><strong><font class="normal">Maximum 
              Extension Duration:</font></strong></td>
            <td><strong><font class="normal"> 
              <input name="max_ext_period" type="text" class="box1" id="max_period2" value="<? echo $rs0["max_ext_period"];?>" size="7">
              <strong> Days</strong></font></strong><font class="smalltext"><br>
              This will control the maximum number of days for which a Listing 
              can be extended i.e. a Listing can be posted maximum for (maximum 
              duration) + ( maximum extension duration) (Enter 0 to Disable Extension 
              Feature, -1 to allow user to extend any number of days.)</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle"><strong><font class="normal">Maximum 
              Image Size:</font></strong> </td>
            <td><font class="normal"><strong> 
              <input name="image_size" type="text" class="box1" id="image_size2" value="<? echo $rs0["image_size"];?>" size="35">
              bytes<br>
              </strong></font><font class="smalltext">This will control the maximum 
              size of an image uploaded for a Listing</font></td>
          </tr>
          <tr valign="top"> 
            <td class="innertablestyle" > <div align="right"><font class="normal"><strong>Member 
                Approval :</strong></font></div></td>
            <td> <select name="freetpl_mem_approval" id="freetpl_mem_approval">
                <option value="admin" <?php echo ($rs0["freetpl_mem_approval"]=="admin")?'selected':''?>>Admin                </option>
                <option value="auto" <?php echo ($rs0["freetpl_mem_approval"]=="auto")?'selected':''?>>Auto</option>
              </select> <br> <font class="smalltext">Sets whether admin will approve 
              every new signup or it will be approved automatically.</font></td>
          </tr>
          <tr valign="top"> 
            <td class="innertablestyle" > <div align="right"><font class="normal"><strong>Email 
                Verification :</strong></font></div></td>
            <td> <select name="freetpl_signup_verification" id="freetpl_signup_verification">
                <option value="yes" <?php echo ($rs0["freetpl_signup_verification"]=="yes")?'selected':''?>>Enabled</option>
                <option value="no" <?php echo ($rs0["freetpl_signup_verification"]=="no")?'selected':''?>>Disabled</option>
              </select> <br> <font class="smalltext">Sets whether email gets verified 
              before signup or not.</font></td>
          </tr>
          <tr valign="top"> 
            <td class="innertablestyle" > <div align="right"><font class="normal"><strong><font class="normal">Listing</font> 
                Approval :</strong></font></div></td>
            <td> <select name="freetpl_prod_approval" id="select">
                <option value="admin" <?php echo ($rs0["freetpl_prod_approval"]=="admin")?'selected':''?>>Admin                </option>
                <option value="auto" <?php echo ($rs0["freetpl_prod_approval"]=="auto")?'selected':''?>>Auto</option>
              </select> <br> 
              <font class="smalltext">Sets whether admin will approve 
              every new Listing or it will be approved automatically upon posting.</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle" ><strong><font class="normal">Choose 
              Category Style:</font></strong></td>
            <td><select name="cat_choosen" id="cat_choosen">
                <option value="drop" <? if($rs0["cat_choosen"]=="drop") { echo " selected";}?>>Dropdown 
                list</option>
                <option value="pop" <? if($rs0["cat_choosen"]=="pop") { echo " selected";}?>>Popup 
                Window</option>
              </select> <font class="smalltext"><br>
              This will control the category selection process during posting 
              or editing a Listing.</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle"><strong><font class="normal">Null 
              Character:</font></strong></font></td>
            <td> <input cols=35 name="null_char" type="text" class="box1" id="null_char2" value="<? echo $rs0["null_char"];?>" size="35" > 
              <font class="smalltext"><br>
              This character will be displayed instead of NULL values.</font></td>
          </tr>
          <tr valign="top"> 
            <td class="innertablestyle" > <div align="right"><font class="normal"><strong>Admin 
                Email:</strong></font></div></td>
            <td> <input name="adminemail" type="text" class="box1" id="adminemail2" value="<? echo $rs0["admin_email"];?>" size="35" > 
              <font class="smalltext"><br>
              Any information regarding the site will be sent on this Email ID</font></td>
          </tr>
          <tr valign="top"> 
            <td class="innertablestyle" > <div align="right"><font class="normal"><strong>Website 
                Name:</strong></font></div></td>
            <td> <input name="sitename" type="text" class="box1" id="sitename2" value="<? echo $rs0["freetpl_site_name"];?>" size="35"> 
              <font class="smalltext"><br>
              This text will appear in the title bar of the browsers.</font></td>
          </tr>
          <tr valign="top"> 
            <td class="innertablestyle" > <div align="right"><font class="normal"><strong>Website 
                Keywords:</strong></font></div></td>
            <td> <input name="site_keywords" type="text" class="box1" id="sitename2" value="<? echo $rs0["freetpl_site_keywords"];?>" size="35"> 
              <font class="smalltext"><br>
              This will control the keywords for search engine optimisation.</font></td>
          </tr>
          <tr valign="top"> 
            <td class="innertablestyle" > <div align="right"><font class="normal"><strong>Website 
                Address:</strong></font></div></td>
            <td> <input name="siteaddrs" type="text" class="box1" id="siteaddrs2" value="<? echo $rs0["site_root"];?>" size="35"> 
              <font class="smalltext"> <br>
              This will control the website address for referring purposes. Please 
              enter the value including http://www in the website address e.g. 
              http://www.yoursite.com</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle" ><strong><font class="normal">Welcome 
              Message:</font></strong></td>
            <td> <textarea name="welcome_msg" cols="35" rows="5" class="box1" id="textarea"><? echo $rs0["welcome_msg"];?></textarea> 
              <font class="smalltext"><br>
              This will control the welcome message to be shown on the front page 
              of the site</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle" ><strong><font class="normal">Show 
              Contact Information:</font></strong></td>
            <td><select name="contact_info" id="contact_info">
                <option value="yes" <? if($rs0["contact_info"]=="yes") { echo " selected";}?>>Yes</option>
                <option value="no" <? if($rs0["contact_info"]=="no") { echo " selected";}?>>No</option>
              </select> <font class="smalltext"><br>
              This will control whether to display seller's contact information 
              on the description page or not.</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle" ><strong><font class="normal">Show 
              Extra Shipping Option:</font></strong></td>
            <td><select name="freetplshow_extra_shipping" id="freetplshow_extra_shipping">
		<option value="yes" <? if($rs0["freetplshow_extra_shipping"]=="yes") { echo " selected";}?>>Yes</option>
		<option value="no" <? if($rs0["freetplshow_extra_shipping"]=="no") { echo " selected";}?>>No</option>
              </select> <font class="smalltext"><br>
              This will control whether to display extra shipping cost option 
              while posting as well as at product description page.</font></td>
          </tr>
          <?php
                        
          ?>
          <tr valign="top"> 
            <td align="right" class="innertablestyle" ><strong><font class="normal">Site 
              Currancy:</font></strong></td>
            <td> <select name="cur_id">
                <?
			  $cats=mysql_query("select * from freetplclassified_currency ");
			  while($rst=mysql_fetch_array($cats))
			  {
			  		  ?>
                <option value="<? echo $rst["id"]; ?>" <? if($rst["id"]==$rs0["cur_id"]) echo " selected ";?>><? echo $rst["cur_name"]; ?></option>
                <?
					}//end while
					 ?>
              </select> <font class="smalltext"><br>
              <font class="red">This will control the currency setting for the 
              entire site. So, please choose it carefully once. If it will be 
              changed again then we will not be responsible for the consequences.              </font></font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle">&nbsp;</td>
            <td> <font class="normal"> 
              <input name="Submit" type="submit" class="submit" value="Update Site Configuration">
              </font></td>
          </tr>
        </table></td>
    </tr>
  </table>
  </form>

<?
}// end main
include "template.php";?>