<?
include_once "logincheck.php"; 
include_once "myconnect.php";

if(!isset($_REQUEST["freetpl_id"]) || !is_numeric($_REQUEST["freetpl_id"]) || ($_REQUEST["freetpl_id"]<1) )
{
	header("Location: manage_additional_fields.php?msg=".urlencode("Invalid Id, unable to continue"));
	
}
$freetpl_id=$_REQUEST["freetpl_id"];
$freetplq_type="select * from freetplclassified_additional_fields where freetpl_id=$freetpl_id";
$freetplrow_type=mysql_fetch_array(mysql_query($freetplq_type));
if(!$freetplrow_type)
{
	header("Location: manage_additional_fields.php?msg=".urlencode("No such field exists, please try again"));
	
}
$freetpl_typename=$freetplrow_type["freetpl_name"];

if(count($_POST)>0)
{
	$freetpl_field_ids='-1';		//actually cat ids
	foreach($_POST as $freetplkey=>$freetplvalue)
	{
		if(substr($freetplkey,0,8)=='freetplfield_')
			$freetpl_field_ids.=','.$freetplvalue;
	}
	$freetpl_field_ids.=',-1';
	$freetplqd_t_f="delete from freetplclassified_types_fields where freetpl_field_id=$freetpl_id";
	mysql_query($freetplqd_t_f);
	$freetpl_ids_array=explode(",",$freetpl_field_ids);
//	echo $freetpl_field_ids;
	foreach($freetpl_ids_array as $freetplvalue)
	if($freetplvalue!=-1)
	{
		$freetplqi_t_f="insert into freetplclassified_types_fields (freetpl_type_id, freetpl_field_id) values ($freetplvalue, $freetpl_id)";
	//		echo $freetplqi_t_f;
		mysql_query($freetplqi_t_f);
	}
	header("Location: manage_additional_fields.php?msg=".urlencode("Category list has been updated"));
	
}
else
{
	$freetplq_type_field="select * from freetplclassified_types_fields where freetpl_field_id=$freetpl_id";
	$freetplrs_type_field=mysql_query($freetplq_type_field);
	$freetpl_field_ids='-1';
	while($freetplrow_type_field=mysql_fetch_array($freetplrs_type_field))
		$freetpl_field_ids.=','.$freetplrow_type_field['freetpl_type_id'];		//'coz type_id is the varing field 
	$freetpl_field_ids.=',-1';
}	

function main()
{
	global $freetpl_field_ids, $freetpl_id, $freetpl_typename;
?>
<script language="JavaScript" type="text/javascript">
function select_all(frm)
{
  for (var i=0;i<frm.elements.length;i++)
  {
    var e =frm.elements[i];
    if ( (e.type=='checkbox') && ( e.name.indexOf('freetplfield_')==0) )
    {
       e.checked = frm.special0.checked;
    }
  }
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="onepxtable">
          <tr> 
            <td height="25" class="titlestyle">&nbsp;Categories for <font class="red"><?php echo $freetpl_typename; ?></font></td>
          </tr>
          <tr>
            <td height="25"><font class="normal"> 
              <input type="checkbox" name="special0" value="0" id="special0" onClick="javascript:select_all(this.form)" >
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All Categories</font></td>
          </tr>
          <?
	$freetplcat_arr=array();
	$rs_query=mysql_query("select * from freetplclassified_categories");
	$cnt=0;				  
	while($rst=mysql_fetch_array($rs_query))
	{
		$cat_path="";
		$child=mysql_fetch_array(mysql_query("select * from freetplclassified_categories where pid=".$rst["id"]));
		if($child)
			continue;
		$cat_path.=$rst["cat_name"];
		$par=mysql_query("select * from freetplclassified_categories where id=".$rst["pid"]);
		while($parent=mysql_fetch_array($par))
		{
			$cat_path=$parent["cat_name"]." > ".$cat_path;
			$par=mysql_query("select * from freetplclassified_categories where id=".$parent["pid"]);
		}
		$freetplcat_arr["{$rst['id']}"]=$cat_path;
	}	//end while
	
	asort($freetplcat_arr);
	foreach($freetplcat_arr as $freetplk => $freetplv)
	{
		$cnt++;
		$rec_class="innertablestyle";
		if($cnt%2==0)
			$rec_class="alternatecolor";
                      ?>
          <tr class="<?php echo $rec_class;?>"> 
            <td valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr> 
                  <td width="5%"><input type="checkbox" name="freetplfield_<?php echo $freetplk;?>" id="freetplfield_<?php echo $freetplk;?>" value="<?php echo $freetplk;?>" <?php echo(strstr($freetpl_field_ids,','.$freetplk.','))?'checked':''; ?>></td>
                  <td width="95%"><font class="normal"><? echo $freetplv;?></font></td>
                </tr>
              </table></td>
          </tr>
          <?
	}	//end while
	$cnt++;
	$rec_class="innertablestyle";
	if($cnt%2==0)
	  	$rec_class="alternatecolor";                   ?>
          <tr class="<?php echo $rec_class;?>"> 
            <td valign="top"><input type="submit" name="Submit" value="Update"> 
              <input name="freetpl_id" type="hidden" id="freetpl_id" value="<?php echo $freetpl_id; ?>"></td>
          </tr>
        </table>      
	  </form>
</td>
  </tr>
  <tr> 
    <td> </td>
  </tr>
</table>
<?
}//main()
include "template.php";
?>