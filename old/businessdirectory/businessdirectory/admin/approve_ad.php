<?php 

	/*----------------------------------------------------------------------------
	------------------------------------------------------------------------------
	--      THE CODE OF THIS SCRIPT HAS BEEN DEVELOPED BY FreeTpl     --
	--      AND IS MEANT TO BE USED ON THIS SITE ONLY AND IS NOT FOR REUSE,     --
	--      RESALE OR REDISTRIBUTION.                                           --
	--      IF YOU NOTICE ANY VIOLATION OF ABOVE PLEASE REPORT AT:              --
	--      info@freetpl.com                                           --
	--      http://www.freetpl.com                                      --
	--                                          --
	------------------------------------------------------------------------------
	----------------------------------------------------------------------------*/

include_once"logincheck.php";
include_once"myconnect.php";

if( !isset($_REQUEST["id"]) || !is_numeric($_REQUEST["id"]) )
{
	header("Location: ads.php?msg=".urlencode("Invalid Id, unable to continue"));
	die();
}
$id=$_REQUEST["id"];
$approve='';
//echo "select * from freetplprj_ads where id=$id";
$ads=mysql_fetch_array(mysql_query("select * from freetplprj_ads where id=$id"));
if($ads["approved"]=="yes")
{
$approve='no';
$msg1='Disapproved';
}else
{
$approve='yes';
$msg1='Approved';
}
mysql_query("update freetplprj_ads set approved='$approve' where id=$id");
//die();
if(mysql_affected_rows() == 1 )
{
	header("Location: manage_special.php?freetplad_type=top&msg=" .urlencode("Banner has been $msg1") );
	die();
}
else
{
	header("Location: manage_special.php?freetplad_type=top&msg=" .urlencode("Some error occurred, please try again") );
	die();
}
?>