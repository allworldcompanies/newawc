<?php 
include_once "session.php";
include_once "myconnect.php";
include_once "../styles.php";

if(!isset($_SESSION["freetpl_clsplus_adminid"]))
	die();

$freetpldontshowfinish=0;

function handleupload() 
{
	global $freetpldontshowfinish;
	if (is_uploaded_file($_FILES['userfile']['tmp_name'])) 
	{
		$realname = $_FILES['userfile']['name'];
	///////--------chking extension	
		if(!preg_match("/(\.jpg|\.png|\.gif|\.bmp|\.jpeg)$/i",$realname))
			die();
	///////--------end chking extension	
		if ($_FILES['userfile']['size']>(60000))
		{
			echo "Uploaded file must be less than 60k. Please close this window and try again";
			echo "<script language=\"JavaScript\">";
			echo "fname = ''";
			echo "</script>";
		}
		else
		{
			echo $realname . ", size: ". $_FILES['userfile']['size'] . " [ ";
			switch($_FILES['userfile']['error'])
			{ case 0: $mess = "Ok";
			  break;
			  case 1:
			  case 2: $mess = "Error : File size more than 512000 bytes";
			  break;
			  case 3: $mess = "Error : File partially uploaded";
			  break;
			  case 4: $mess = "Error : No File Uploaded";
			  break;
			}
			echo $mess . " ]  ";
                        

			mt_srand((double)microtime()*1000000);
			$randvar =  mt_rand(1,10000000);
			settype($randvar,"string");
			$extension=explode(".",$realname);
			$newfilename="../freetplclassified_icons/".$randvar.".".$extension[count($extension)-1];
			$shortfname = $randvar.".".$extension[count($extension)-1];
			while ( file_exists($newfilename) != FALSE )
			{
				$randvar =  mt_rand(1,10000000);
				settype($randvar,"string");
				$newfilename = "../freetplclassified_icons/" . $randvar.".".$extension[count($extension)-1];
				$shortfname =  $randvar.".".$extension[count($extension)-1];
			}
			copy($_FILES['userfile']['tmp_name'], $newfilename);
			echo "<script language=\"JavaScript\">";
			echo "fname = '" . $shortfname . "'";
			echo "</script>";
		}// Else fr more than 60k
	} 
	else 
	{
		echo "<font class=\"red\"><div align=center>Error : File Not Uploaded. Check Size & Try Again.<br><a href=\" javascript: onclick=history.go(-1);\">Go Back</a></div></font>";
		$freetpldontshowfinish = 1;
	}
}
?>

<html>
<head><title>Uploaded File Status</title>
<Script Language="JavaScript">
function closewin(fname)
{
window.opener.document.form123.<?php echo $_REQUEST["box"];?>.value=fname
window.close()
}
</script>
</head>

<body bgcolor="<?php echo $freetpl_page_bg; ?>" >

<table width="100%" class='innertablestyle'>
  <tr><td >
<strong><font class='normal'>Image 
    Uploader</font></strong>
</td></tr>
<tr><td ><hr>
</td></tr>
<tr><td >
    <strong><font class='normal'><font class="red">
	<?php handleupload(); ?></font></font></strong>
</td></tr>
<tr><td >&nbsp;
</td></tr>
<tr><td ><hr>
</td></tr>
<tr><td  align=center>
<?php if($freetpldontshowfinish!=1)	{?>
	<a href="javascript:onclick=closewin(fname)">FINISH</a>
<?php }	//end if?>	
</td></tr>
</table>
</body>
</html>