<?php
include_once "logincheck.php";
include_once "myconnect.php";
             
	if (!get_magic_quotes_gpc()) 
	{
		$site_name=str_replace('$', '\$',addslashes($_REQUEST["sitename"]));
		$admin_email=str_replace('$', '\$',addslashes($_REQUEST["adminemail"]));
		$site_root=str_replace('$', '\$',addslashes($_REQUEST["siteaddrs"]));
		$recperpage=str_replace('$', '\$',addslashes($_REQUEST["recperpage"]));
		$no_of_images=str_replace('$', '\$',addslashes($_REQUEST["no_of_images"]));
		$featured_items=str_replace('$', '\$',addslashes($_REQUEST["featured_items"]));
		$fp_images=str_replace('$', '\$',addslashes($_REQUEST["fp_images"]));
		$recinpanel=str_replace('$', '\$',addslashes($_REQUEST["recinpanel"]));
		$null_char=str_replace('$', '\$',addslashes($_REQUEST["null_char"]));
		$welcome_msg=str_replace('$', '\$',addslashes($_REQUEST["welcome_msg"]));
		$username_len=str_replace('$', '\$',addslashes($_REQUEST["username_len"]));
		$pwd_len=str_replace('$', '\$',addslashes($_REQUEST["pwd_len"]));
		$max_period=str_replace('$', '\$',addslashes($_REQUEST["max_period"]));
		$free_images=str_replace('$', '\$',addslashes($_REQUEST["free_images"]));
		$image_size=str_replace('$', '\$',addslashes($_REQUEST["image_size"]));
		$max_ext_period=str_replace('$', '\$',addslashes($_REQUEST["max_ext_period"]));
		$cur_id=str_replace('$', '\$',addslashes($_REQUEST["cur_id"]));
		$featured_cnt=str_replace('$', '\$',addslashes($_REQUEST["featured_cnt"]));
		$site_keywords=str_replace('$', '\$',addslashes($_REQUEST["site_keywords"]));
		$freetpl_signup_verification=str_replace('$', '\$',addslashes($_REQUEST["freetpl_signup_verification"]));
		$freetplshow_extra_shipping=str_replace('$', '\$',addslashes($_REQUEST["freetplshow_extra_shipping"]));
	}
	else
	{
		$site_name=str_replace('$', '\$',$_REQUEST["sitename"]);
		$admin_email=str_replace('$', '\$',$_REQUEST["adminemail"]);
		$site_root=str_replace('$', '\$',$_REQUEST["siteaddrs"]);
		$recperpage=str_replace('$', '\$',$_REQUEST["recperpage"]);
		$no_of_images=str_replace('$', '\$',$_REQUEST["no_of_images"]);
		$featured_items=str_replace('$', '\$',$_REQUEST["featured_items"]);
		$fp_images=str_replace('$', '\$',$_REQUEST["fp_images"]);
		$recinpanel=str_replace('$', '\$',$_REQUEST["recinpanel"]);
		$null_char=str_replace('$', '\$',$_REQUEST["null_char"]);
		$welcome_msg=str_replace('$', '\$',$_REQUEST["welcome_msg"]);
		$username_len=str_replace('$', '\$',$_REQUEST["username_len"]);
		$pwd_len=str_replace('$', '\$',$_REQUEST["pwd_len"]);
		$max_period=str_replace('$', '\$',$_REQUEST["max_period"]);
		$free_images=str_replace('$', '\$',$_REQUEST["free_images"]);
		$image_size=str_replace('$', '\$',$_REQUEST["image_size"]);
		$max_ext_period=str_replace('$', '\$',$_REQUEST["max_ext_period"]);
		$cur_id=str_replace('$', '\$',$_REQUEST["cur_id"]);
		$featured_cnt=str_replace('$', '\$',$_REQUEST["featured_cnt"]);
		$site_keywords=str_replace('$', '\$',$_REQUEST["site_keywords"]);
		$freetpl_signup_verification=str_replace('$', '\$',$_REQUEST["freetpl_signup_verification"]);
		$freetplshow_extra_shipping=str_replace('$', '\$',$_REQUEST["freetplshow_extra_shipping"]);
	}
	$cat_choosen=$_REQUEST["cat_choosen"];
	$contact_info=$_REQUEST["contact_info"];
	$premium_cnt=(int)$_REQUEST["premium_cnt"];
	$freetpl_mem_approval=$_REQUEST["freetpl_mem_approval"];
	$freetpl_prod_approval=$_REQUEST["freetpl_prod_approval"];
			
$sql="update freetplclassified_config set
freetpl_site_name='$site_name',
admin_email='$admin_email',
site_root='$site_root',
recperpage=$recperpage,
no_of_images=$no_of_images,
recinpanel=$recinpanel,
null_char='$null_char',
username_len=$username_len,
pwd_len=$pwd_len,
featured_items=$featured_items,
max_period=$max_period,
fp_images=$fp_images,
free_images=$free_images,
image_size=$image_size,
max_ext_period=$max_ext_period,
cur_id=$cur_id,
featured_cnt=$featured_cnt,
freetpl_site_keywords='$site_keywords',
cat_choosen='$cat_choosen',
premium_cnt=$premium_cnt,
freetpl_mem_approval='$freetpl_mem_approval',
freetpl_prod_approval='$freetpl_prod_approval',
contact_info='$contact_info',
freetpl_signup_verification='$freetpl_signup_verification',
freetplshow_extra_shipping='$freetplshow_extra_shipping',
welcome_msg='$welcome_msg'";
mysql_query($sql);


if(mysql_affected_rows()==1)
	header("Location: config.php?msg=".urlencode('Site parameters have been updated.'));
else
	header("Location: config.php?msg=".urlencode('Unable to update site parameters, please try again.'));

die();
?>