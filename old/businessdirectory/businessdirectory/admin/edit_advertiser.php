<?
include_once "../myconnect.php";
$errcnt=0;
if(count($_POST)<>0)		//IF SOME FORM WAS POSTED DO VALIDATION
{
//		ob_start();
		if(!get_magic_quotes_gpc())
		{
			$uname=str_replace("$","\$",addslashes($_REQUEST["uname"]));
			$pwd=str_replace("$","\$",addslashes($_REQUEST["pwd"]));
			$email=str_replace("$","\$",addslashes($_REQUEST["email"]));
		}
		else
		{
			$uname=str_replace("$","\$",$_REQUEST["uname"]);
			$pwd=str_replace("$","\$",$_REQUEST["pwd"]);
			$email=str_replace("$","\$",$_REQUEST["email"]);
		}
		$id=(int)$_REQUEST["id"];
		if ( strlen(trim($uname)) == 0 )
		{
			$errs[$errcnt]="Name must be provided";
			$errcnt++;
		}
		elseif(preg_match ("/[;<>&]/", $_REQUEST["uname"]))
		{
			$errs[$errcnt]="Name can not have any special character (e.g. & ; < >)";
			$errcnt++;
		}
	
		if ( !isset( $_REQUEST["pwd"] ) || (strlen(trim($_REQUEST["pwd"])) == 0) )
		{
			$errs[$errcnt]="Password must be provided";
			$errcnt++;
		}
		elseif( strcmp($_REQUEST["pwd"],$_REQUEST["pwd2"]) != 0)
		{
			$errs[$errcnt]="Retyped Password does not match the Password";
			$errcnt++;
		}
		
		if ( !isset( $_REQUEST["email"] ) || (strlen(trim($_REQUEST["email"] )) == 0) )
		{
			$errs[$errcnt]="Email Address must be provided";
			$errcnt++;
		}
		elseif(preg_match ("/[;<>&]/", $_REQUEST["email"]))
		{
			$errs[$errcnt]="Email can not have any special character (e.g. & ; < >)";
			$errcnt++;
		}
		elseif(mysql_num_rows(mysql_query("select * from freetplclassified_advertisers 
		where email='$email' and id<>$id"))!= 0)
		{
				$errs[$errcnt]="Some Member with same Email Address already exists";
				$errcnt++;
		}
		
		if($errcnt==0)
		{
		mysql_query("UPDATE freetplclassified_advertisers set uname='$uname',email='$email',pwd='$pwd' where id=$id");
		if(mysql_affected_rows()>0)
		{
		header("Location: advertisers.php?msg=".urlencode("Profile has been updated"));
		die();
		}
		else
		{
		header("Location: edit_advertiser.php?id=$id&msg=".urlencode("Some error occurred, no updations carried out."));
		die();
		}
	}

}// end if posted
function main()
{
global $errs, $errcnt;
$id=0;
if(isset($_REQUEST["id"])&&($_REQUEST["id"]<>""))
{
$id=$_REQUEST["id"];
}

$adv=mysql_fetch_array(mysql_query("select * from freetplclassified_advertisers where id=$id"));
$uname=$adv["uname"];
$email=$adv["email"];
$pwd=$adv["pwd"];
if(count($_POST)>0)
{
$uname=$_POST["uname"];
$email=$_POST["email"];
$pwd=$_POST["pwd"];
}
                        
if  (count($_POST)>0)
{

if ( $errcnt<>0 )
{
?>
<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="errorstyle">
  <tr> 
    <td colspan="2"><strong>&nbsp;Your request cannot be processed due to following 
      reasons</strong></td>
  </tr>
  <tr height="10"> 
    <td colspan="2"></td>
  </tr>
  <?

for ($i=0;$i<$errcnt;$i++)
{
?>
  <tr valign="top"> 
    <td width="6%">&nbsp;<?php echo $i+1;?></td>
    <td width="94%"><?php echo  $errs[$i]; ?></td>
  </tr>
  <?
}
?>
</table>
<br>
<?

}

}

?>
<SCRIPT language=javascript> 
//<!--
function emailCheck (emailStr) {
var emailPat=/^(.+)@(.+)$/
var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
var validChars="\[^\\s" + specialChars + "\]"
var quotedUser="(\"[^\"]*\")"
var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
var atom=validChars + '+'
var word="(" + atom + "|" + quotedUser + ")"
var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
var matchArray=emailStr.match(emailPat)
if (matchArray==null) {
	alert("Email address seems incorrect (check @ and .'s)")
	return false
}
var user=matchArray[1]
var domain=matchArray[2]
if (user.match(userPat)==null) {
    alert("The username doesn't seem to be valid.")
    return false
}
var IPArray=domain.match(ipDomainPat)
if (IPArray!=null) {
    // this is an IP address
	  for (var i=1;i<=4;i++) {
	    if (IPArray[i]>255) {
	        alert("Destination IP address is invalid!")
		return false
	    }
    }
    return true
}
var domainArray=domain.match(domainPat)
if (domainArray==null) {
	alert("The domain name doesn't seem to be valid.")
    return false
}
var atomPat=new RegExp(atom,"g")
var domArr=domain.match(atomPat)
var len=domArr.length
if (domArr[domArr.length-1].length<2 || 
			domArr[domArr.length-1].length>4) {
		   alert("The address must end in a valid domain, or two letter country.")
   return false
}
if (len<2) {
   var errStr="This address is missing a hostname!"
   alert(errStr)
   return false
}
return true;
}


  function formValidate(form) {
	if ( form.uname.value == "" ) {
       	   alert('Please Enter Name!');
		   form.uname.focus();
	   return false;
	   }

        if(!form.email.value.match(/[a-zA-Z\.\@\d\_]/)) 
		{
           alert('Invalid e-mail address.');
           form.email.focus();
		   return false;
        }
		
		if (!emailCheck (form.email.value) )
		{
			form.email.focus();
			return (false);
		}
        if(form.pwd.value == "")
		{
	   	   alert('Please Enter Password.');
           form.pwd.focus();
		   return false; 
        }
		if (form.pwd.value != form.pwd2.value)
		{
			alert('Passwords do not match.');
			form.pwd2.value="";
			form.pwd.focus();
			form.pwd.select();
			return false;
		}
		
	return true;
  }
// -->
</SCRIPT>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td align="center" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" >
        <tr> 
          <td align="center" valign="top" > <FORM name=register onsubmit=return(formValidate(this)); method=post action="<? echo $_SERVER['PHP_SELF'];?>">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td height="25" class="titlestyle">&nbsp;Edit Advertiser</td>
                </tr>
                <tr> 
                  <td valign="top"> <TABLE class="onepxtable" cellSpacing=1 cellPadding=1 width=100% border=0>
                      <TBODY>
                        <TR> 
                          <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class='normal'>
                            <input name="id" type="hidden" id="id" value="<? echo $id;?>">
                            &nbsp;<B>Advertiser Name</B></font></TD>
                          <TD width="6" align=left valign="top"><font class='red'>*&nbsp;</FONT></TD>
                          <TD width="60%"> <input name=uname value="<? echo $uname;?>" size=25 
                        maxlength=120> </TD>
                        </TR>
                        <TR> 
                          <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class='normal'>&nbsp;<B>Advertiser 
                            Email </B></font></TD>
                          <TD width="6" align=left valign="top"><font class='red'>*&nbsp;</FONT></TD>
                          <TD width="60%"> <INPUT name=email value="<? echo $email;?>" size=25 
                        maxLength=120></TD>
                        </TR><?
                        
                        ?><TR> 
                          <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class='normal'>&nbsp;<B>Password</B></font></TD>
                          <TD width="6" align=left valign="top"><font class='red'>*&nbsp;</FONT></TD>
                          <TD width="60%"> <input name=pwd type="password" value="<? echo $pwd;?>" size=25 maxlength=120> 
                            <font size="1" face="Verdana, Arial, Helvetica, sans-serif"  >&nbsp; 
                            </font></TD>
                        </TR>
                        <TR> 
                          <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class='normal'>&nbsp;<B>Confirm 
                            Password</B></font></TD>
                          <TD width="6" align=left valign="top"><font class='red'>*&nbsp;</FONT></TD>
                          <TD width="60%"> <input name=pwd2 type="password" id="pwd2" value="<? echo $pwd;?>" size=25 
                        maxlength=120> <font size="1" face="Verdana, Arial, Helvetica, sans-serif"  >&nbsp;</font></TD>
                        </TR><?PHP
                        
                        ?><TR> 
                          <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class='normal'>&nbsp;</font></TD>
                          <TD width="6" align=right>&nbsp;</TD>
                          <TD width="60%"> <INPUT type=submit value=Update name=submit> 
                            &nbsp; </TD>
                        </TR>
                      </TBODY>
                    </TABLE></td>
                </tr>
              </table>
            </form></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<?
}// end main
include "template.php";
?>