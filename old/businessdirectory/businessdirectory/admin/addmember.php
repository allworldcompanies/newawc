<?
include_once "myconnect.php";

function main()
{
/////////////getting length of user name and password
$len=mysql_fetch_array(mysql_query("select username_len,pwd_len from freetplclassified_config"));
$username_len=$len["username_len"];
$pwd_len=$len["pwd_len"];
if(count($_POST)>0)
{
$fname=$_REQUEST["fname"];
$lname=$_REQUEST["lname"];
$c_name=$_REQUEST["company_name"];
$add1=$_REQUEST["add1"];
$add2=$_REQUEST["add2"];
$city=$_REQUEST["city"];
$state=$_REQUEST["state"];
$zip=$_REQUEST["zip_code"];
$country=$_REQUEST["country"];
$home_phone=$_REQUEST["home_phone"];
$work_phone=$_REQUEST["work_phone"];
$email=$_REQUEST["email_addr"];
}
else
{
$fname="";
$lname="";
$c_name="";
$add1="";
$add2="";
$city="";
$state="";
$zip="";
$country=0;
$home_phone="";
$work_phone="";
$email="";
}
                        
?>

<SCRIPT language=javascript> 
<!--
function emailCheck (emailStr) {
var emailPat=/^(.+)@(.+)$/
var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
var validChars="\[^\\s" + specialChars + "\]"
var quotedUser="(\"[^\"]*\")"
var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
var atom=validChars + '+'
var word="(" + atom + "|" + quotedUser + ")"
var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
var matchArray=emailStr.match(emailPat)
if (matchArray==null) {
	alert("Email address seems incorrect (check @ and .'s)")
	return false
}
var user=matchArray[1]
var domain=matchArray[2]
if (user.match(userPat)==null) {
    alert("The username doesn't seem to be valid.")
    return false
}
var IPArray=domain.match(ipDomainPat)
if (IPArray!=null) {
    // this is an IP address
	  for (var i=1;i<=4;i++) {
	    if (IPArray[i]>255) {
	        alert("Destination IP address is invalid!")
		return false
	    }
    }
    return true
}
var domainArray=domain.match(domainPat)
if (domainArray==null) {
	alert("The domain name doesn't seem to be valid.")
    return false
}
var atomPat=new RegExp(atom,"g")
var domArr=domain.match(atomPat)
var len=domArr.length
if (domArr[domArr.length-1].length<2 || 
			domArr[domArr.length-1].length>4) {
		   alert("The address must end in a valid domain, or two letter country.")
   return false
}
if (len<2) {
   var errStr="This address is missing a hostname!"
   alert(errStr)
   return false
}
return true;
}

  function formValidate(form) {
	
	    if (form.username.value == "") {
	   alert('Please specify Username.');
	   form.username.focus();
	   return false;
	   }
	   if (form.username.value.length< <? echo $username_len?>) {
	   alert('Username must be atleast <? echo $username_len;?> character(s) long.');
	   form.username.focus();
	   return false;
	   }
		if((form.username.value.match(/[^a-zA-Z0-9_]/)))
		{
			alert("Username can contain only alphanumeric and underscore character");
			form.username.focus();
			return(false);
		}
	   
		if(!form.email_addr.value.match(/[a-zA-Z\.\@\d\_]/)) {
           alert('Invalid e-mail address.');
 			form.email_addr.focus();
          return false;
           }
		if (!emailCheck (form.email_addr.value) )
		{
			form.email_addr.focus();
			return (false);
		}
		if(form.email_addr.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Email (e.g. &  < >)");
			form.email_addr.focus();
			return(false);
		}

       if(form.fname.value == "" ){
	   alert('Please specify First Name.');
	    form.fname.focus();
          return false; 
           }
		if(form.fname.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from First Name (e.g. &  < >)");
			form.fname.focus();
			return(false);
		}

	    if(form.lname.value == "" )
		{
  	    alert('Please specify Last Name.');
			form.lname.focus();
        return false; 
        }
		if(form.lname.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Last Name (e.g. &  < >)");
			form.lname.focus();
			return(false);
		}
		if(form.company_name.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Company Name(e.g. &  < >)");
			form.company_name.focus();
			return(false);
		}
		
        if(form.add1.value == "" && form.add2.value == "")
		{
	    alert('Please specify Address.');
		form.add1.focus();
        return false; 
        }
		if(form.add1.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Address (e.g. &  < >)");
			form.add1.focus();
			return(false);
		}
		if(form.add2.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Address (e.g. &  < >)");
			form.add2.focus();
			return(false);
		}
        if(form.city.value == "")
		{
	    alert('Please specify City.');
			form.city.focus();
        return false; 
        }
		if(form.city.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from City (e.g. &  < >)");
			form.city.focus();
			return(false);
		}

		 if(form.state.value== "" )
		 {
           alert('Please specify State.');
			form.state.focus();
           return false;
           }
		if(form.state.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from State (e.g. &  < >)");
			form.state.focus();
			return(false);
		}
        
		if(form.zip_code.value == "")
		{
	       alert('Please specify Zip/postal code.');
			form.zip_code.focus();
           return false; 
        }
		if(form.zip_code.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Zip Code (e.g. &  < >)");
			form.zip_code.focus();
			return(false);
		}

        if(form.country.selectedIndex == 0)
		{
  	    alert('Please choose a Country.');
			form.country.focus();
        return false; 
        }
        
        if(form.home_phone.value == "")
		{
	       alert('Please specify Home Phone.');
			form.home_phone.focus();
           return false; 
        }
		if(form.home_phone.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Home Phone (e.g. &  < >)");
			form.home_phone.focus();
			return(false);
		}
		if(form.work_phone.value.match(/[&<>]+/))
		{
			alert("Please remove Invalid characters from Work Phone (e.g. &  < >)");
			form.work_phone.focus();
			return(false);
		}
		
    if (form.pwd.value == "") {
	   alert('Please specify Password.');
			form.pwd.focus();
	   return false;
	   }
    if (form.pwd.value.length<<? echo $pwd_len;?>) {
	   alert('Password must be atleast <? echo $pwd_len;?> character(s) long.');
			form.pwd.focus();
	   return false;
	   }		
	if (form.pwd.value != form.pwd2.value)
	{
		alert('Passwords do not match.');
		form.pwd2.value="";
		form.pwd.focus();
		form.pwd.select();
		return false;
	}

	   
	return true;
  }
// -->
</SCRIPT>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="onepxtable">
  <TR align="left" class="titlestyle"> 
    <TD height="25">&nbsp;Add New Member</TD>
  </TR><?PHP
                        
  ?><tr> 
    <td width="100%" valign="top"> <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#0099FF">
        <tr> 
          <td > <TABLE width=100% border=0 align="center" cellPadding=1  cellSpacing=5>
              <FORM name=register onsubmit=return(formValidate(this)); 
                  method=post action="insertmember.php">
                <TR> 
                  <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>Username:&nbsp;<br>
                    </B></font></TD>
                  <td> <INPUT  
                        maxLength=20 size=15 name=username > <font class="normal">&nbsp;</font><font size="1" face="Verdana, Arial, Helvetica, sans-serif"  ><br>
                    <font face="Arial, Helvetica, sans-serif">must be atleast 
                    <? echo $username_len;?> </font><font size="1" face="Verdana, Arial, Helvetica, sans-serif" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"  ><font face="Arial, Helvetica, sans-serif"> 
                    character(s) long.</font></font></font> </font></TD>
                </TR>
                <?PHP
                        
                ?>
                <TR> 
                  <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>E-mail 
                    address:&nbsp; </B></font></TD>
                  <td><INPUT  
                        maxLength=99 size=25 name=email_addr value="<? echo $email;?>"> 
                    <font size="1" face="Verdana, Arial, Helvetica, sans-serif"><br>
                    </font></TD>
                </TR>
                <TBODY>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>Full 
                      Name:&nbsp;</B></font></TD>
                    <td><p> 
                        <INPUT  
                        maxLength=120 size=20 name=fname value="<? echo $fname;?>">
                        <input 
                        name=lname  size="20" value="<? echo $lname;?>">
                        <br>
                        <font size="1" face="Verdana, Arial, Helvetica, sans-serif">(e.g., 
                        Ellen Grant)</font></p></TD>
                  </TR>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>Company:&nbsp; 
                      </B></font></TD>
                    <td><INPUT  
                        maxLength=120 size=25 name=company_name value="<? echo $c_name;?>"></TD>
                  </TR>
                  <TR> 
                    <TD width="40%" align=right valign="top" class="innertablestyle"><font class="normal"><B>Address</B></font><font class="normal"><B>:&nbsp;</B></font></TD>
                    <td><INPUT  
                        maxLength=120 size=35 name=add1 value="<? echo $add1;?>"> 
                      <br> <INPUT  
                        maxLength=120 size=35 name=add2 value="<? echo $add2;?>"></TD>
                  </TR>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>City, 
                      State(or Region:&nbsp;</B></font></TD>
                    <td><INPUT  
                        maxLength=120 size=20 name=city value="<? echo $city;?>">
                      , 
                      <input 
                        name=state  size="20" value="<? echo $state;?>"></TD>
                  </TR>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>Zip 
                      Code (or Postal code):&nbsp;</B></font></TD>
                    <td><INPUT  
                        maxLength=11 size=15 name=zip_code value="<? echo $zip;?>"></TD>
                  </TR>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>Country:&nbsp;</B></font></TD>
                    <td> <SELECT name=country>
                        <OPTION value=0
                          <? if($country==0) {echo " selected ";}?>>Select a Value 
                        <? 
						  $country1=mysql_query("select * from freetplclassified_country order by country");
						  while($rst= mysql_fetch_array($country1))
						  {
						  ?>
                        <OPTION value=<? echo $rst["id"];?> <? if($rst["id"]==$country) {echo " selected ";}?>><? echo $rst["country"];?></OPTION>
                        <?
							   } // wend
							   ?>
                      </SELECT> </TD>
                  </TR>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>Home 
                      Phone:&nbsp; </B></font></TD>
                    <td><INPUT  
                        maxLength=20 size=15 name=home_phone value="<? echo $home_phone;?>"> 
                      <font size="1" face="Verdana, Arial, Helvetica, sans-serif"><br>
                      <font face="Arial, Helvetica, sans-serif">(e.g., 123-456-7890) 
                      </font></font></TD>
                  </TR>
                  <?PHP
                        
                  ?>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>Work 
                      Phone:&nbsp; </B></font></TD>
                    <td><INPUT  
                        maxLength=20 size=15 name=work_phone value="<? echo $work_phone;?>"> 
                      <font size="1" face="Verdana, Arial, Helvetica, sans-serif"><br>
                      <font face="Arial, Helvetica, sans-serif">(e.g., 123-456-7890) 
                      </font></font></TD>
                  </TR>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><b>Password:&nbsp;</b></font></TD>
                    <td><INPUT type=password  
                        maxLength=20 size=15 name=pwd> <font size="1" face="Verdana, Arial, Helvetica, sans-serif"  ><br>
                      <font face="Arial, Helvetica, sans-serif">must be atleast 
                      <? echo $pwd_len;?> character(s) long</font></font></TD>
                  </TR>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal"><B>Confirm 
                      Password:&nbsp; </B></font></TD>
                    <td><INPUT type=password  
                        maxLength=20 size=15 name=pwd2> <font size="1" face="Verdana, Arial, Helvetica, sans-serif"  ><br>
                      <font face="Arial, Helvetica, sans-serif">must be atleast 
                      <? echo $pwd_len;?> character(s) long</font></font></TD>
                  </TR>
                  <TR> 
                    <TD width="40%" height="25" align=right valign="top" class="innertablestyle"><font class="normal">&nbsp;</font></TD>
                    <TD valign="top"> <INPUT type=submit value=Submit name=submit> 
                    </TD>
                  </TR>
              </form>
            </TABLE></td>
        </tr>
      </table></td>
  </tr>
</table>
<?
}// end main
include "template.php";
?>