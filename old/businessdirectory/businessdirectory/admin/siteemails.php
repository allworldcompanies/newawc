<?php

include_once("myconnect.php");
include_once "logincheck.php";

function RTESafe($strText) {
	//returns safe code for preloading in the RTE
	$tmpString = trim($strText);
	
	//convert all types of single quotes
	$tmpString = str_replace(chr(145), chr(39), $tmpString);
	$tmpString = str_replace(chr(146), chr(39), $tmpString);
	$tmpString = str_replace("'", "&#39;", $tmpString);
	
	//convert all types of double quotes
	$tmpString = str_replace(chr(147), chr(34), $tmpString);
	$tmpString = str_replace(chr(148), chr(34), $tmpString);
//	$tmpString = str_replace("\"", "\"", $tmpString);
	
	//replace carriage returns & line feeds
	$tmpString = str_replace(chr(10), " ", $tmpString);
	$tmpString = str_replace(chr(13), " ", $tmpString);
	
	return $tmpString;
}

function main()
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td> 
      <?
		
		$errcnt=0;
		$showform="";
		
		$mailid="";
		$fromid="";
		$subject="";
		$mail="";
                        
		if ( count($_POST)!=0 )
		{
		$mailid=$_REQUEST["mailid"];
		
		if ($_REQUEST["mailid"]!="" )
		{
		$rs=mysql_query("select * from freetplclassified_mails where mailid=" . $_REQUEST["mailid"] );
		$rs=mysql_fetch_array($rs);
		$fromid=$rs["fromid"];
		$subject=$rs["subject"];
		$mail=$rs["mail"];
		}
		
		if (  isset( $_REQUEST["update"])  )
		{
		
		if ( !isset( $_REQUEST["fromid"] ) || $_REQUEST["fromid"]=="" )
		{
			$errs[$errcnt]="From Email id must be provided";
			$errcnt++;
		}
		if ( !isset( $_REQUEST["subject"] ) || $_REQUEST["subject"]=="" )
		{
			$errs[$errcnt]="Email Subject must be provided";
			$errcnt++;
		}
		if ( !isset( $_REQUEST["mail"] ) || $_REQUEST["mail"]=="" )
		{
			$errs[$errcnt]="Email Contents must be provided";
			$errcnt++;
		}
		
		}
		
		}
		if  (count($_POST)<>0)
		{
		if ( $errcnt==0 )
		{
		if (  isset( $_REQUEST["update"])  )
		{
			if (!get_magic_quotes_gpc()) {
			$insert_fromid=str_replace('$', '\$',addslashes($_REQUEST["fromid"]));
			$insert_subject=str_replace('$', '\$',addslashes($_REQUEST["subject"]));
			$insert_mail=str_replace('$', '\$',addslashes($_REQUEST["mail"]));
			}
			else
			{
			$insert_fromid=str_replace('$', '\$',$_REQUEST["fromid"]);
			$insert_subject=str_replace('$', '\$',$_REQUEST["subject"]);
			$insert_mail=str_replace('$', '\$',$_REQUEST["mail"]);
			}
			$html="no";
			if(isset($_REQUEST["html_format"]))
			{
			$html="yes";
			}
		
		$update_str="update freetplclassified_mails set fromid='$insert_fromid', subject='$insert_subject', mail='$insert_mail',freetpl_html_format='".$html."' where mailid =" . $_REQUEST["mailid"];
		//echo $update_str;
		mysql_query($update_str);
		
		?>
      <div align="center"> <font class="normal"><font class="red">Mail has been 
        updated</font></font></div>
      <?
		$showform="No";
		}
		
		}
		else
		{
		?>
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="errorstyle">
        <tr> 
          <td colspan="2"><font class="normal">Your Update Email Request cannot 
            be processed due to following Reasons</font></td>
        </tr>
        <?
                        
for ($i=0;$i<$errcnt;$i++)
{
?>
        <tr> 
          <td width="6%"><font color="#FF0000"><?php echo $i+1; ?></font></td>
          <td width="94%"><font color="#FF0000" class="normal"><?php echo  $errs[$i]; ?> 
            </font></td>
        </tr>
        <?
}//end for
?>
      </table>
      <br> 
      <?

}

}
if (isset($_REQUEST["mailid"])&& ($_REQUEST["mailid"]!="" ))
{
$rs=mysql_query("select * from freetplclassified_mails where mailid=" . $_REQUEST["mailid"] );
$rs=mysql_fetch_array($rs);
$fromid=$rs["fromid"];
$subject=$rs["subject"];
$mail=$rs["mail"];
}

?>
    </td>
  </tr>
  <tr> 
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="onepxtable">
        <tr> 
          <td height="25"class="titlestyle">&nbsp;Configure Site Emails</td>
        </tr>
        <tr> 
          <td><table width="100%" border="0" cellspacing="5" cellpadding="2">
              <form name="form2" method="post" action="siteemails.php">
                <tr> 
                  <td width="40%" align="right" valign="top" class="innertablestyle"><strong><font class="normal">Choose 
                    Email:&nbsp; </font></strong> </td>
                  <td valign="top"><font class="smalltext"> 
                    <select name="mailid" id="mailid">
                      <option  value="">Select a mail message</option>
                      <option  value="">--------------------------------</option>
                      <option value="25" 
				   <? 
				  if  (isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="25")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
				  >Signup Verification Email (Member)</option>
 			<option value="26" 
				   <? 
				  if  (isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="26")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
				  >New Signup Waiting Approval (Admin)</option>	  
					  <option value="1" 
				   <? 
				  if  (isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="1")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
				  >Welcome Email (Member)</option>
                      <option value="4"				   
				  <? 
				  if  ( isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="4")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
>Forgot Password (Member)</option>
                       <option  value="">--------------------------------</option>
                       <option value="21"				   
				  <? 
				  if  ( isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="21")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
>New/Updated Listing Waiting Approval (Admin)</option>
                     <option value="5"				   
				  <? 
				  if  ( isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="5")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
>Listing Posted Notification (Member)</option>
                      <option value="2"				   
				  <? 
				  if  (isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="2")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
>Listing Approval Notification (Member)</option>
                    <option value="3" 				   <? 
				  if  (isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="3")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
>Listing Disapproval Notification (Member)</option>
                       <option  value="">--------------------------------</option>
                      <option value="6"				   
				  <? 
				  if  ( isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="6")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
>Purchase Request Notification (Member)</option>
                      <option value="8"				   
				  <? 
				  if  ( isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="8")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
>Message Received Notification (Member)</option>
                       <option value="9"				   
				  <? 
				  if  ( isset($_REQUEST["mailid"]) && $_REQUEST["mailid"]=="9")
				  {
				  echo " Selected ";
				  }
				  
				  ?>
>Offline Payment Notification (Admin)</option>
                   </select>
                    <br>
                    Choose an email to configure.</font></td>
                </tr>
                <tr> 
                  <td width="40%" align="right" valign="top" class="innertablestyle">&nbsp;</td>
                  <td valign="top"><font class="smalltext"> 
                    <input type="submit" name="Submit" value="Select email message">
                    </font></td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table></td>
  </tr><?php
                        
  ?><tr> 
    <td>&nbsp;</td>
  </tr>
  <? 
  
  if(isset($_REQUEST["mailid"])&& ($_REQUEST["mailid"]!="" ))
	  {
	  ?>
	<script language="JavaScript" type="text/javascript" src="richtext.js"></script>
	<script language="JavaScript">
	function validate(form)
	{
		updateRTEs();
		if(form.fromid.value=="")
		{
		 alert('Please specify From/Reply address');
		 form.fromid.focus();
		 return false;
		}
		if(form.subject.value=="")
		{
		 alert('Please specify Subject');
		 form.subject.focus();
		 return false;
		}
		if(form.mail.value=="")
		{
		 alert('Please specify Email Message');
 		 return false;
		}
		return true;
	}	
	</script>
  <form name="form1" method="post" action="siteemails.php" onSubmit="return validate(this);">
    <tr> 
      <td><table width="100%" border="0" cellspacing="0" cellpadding="2" class="onepxtable">
          <tr class="titlestyle"> 
            <td>Custom Shortcuts</td>
          </tr>
          <tr> 
            <td><font class="normal"><strong>MEMBER SPECIFIC:</strong><br>
              <font class="smalltext"> 1. &nbsp;%fname% will display the user's 
              first name.<br>
              2. &nbsp;%lname% will display the user's last name.<br>
              3. &nbsp;%email% will display the user's email id.<br>
              4. &nbsp;%username% will display user's username.<br>
              5. &nbsp;%password% will display the user's password.<br>
              </font><font class="normal"><font class="smalltext">6. &nbsp;%loginurl% 
              will display user's login link.</font></font><br>
              7<font class="smalltext">. &nbsp;%signup_url% will display user's 
              signup link in email verification mail.<br>
              </font> <strong>LISTING SPECIFIC:</strong><br>
              <font class="smalltext"> 1. &nbsp;%productname% will display Listing 
              name.<br>
              2. &nbsp;%producturl% will display </font><font class="normal"><font class="smalltext">Listing</font></font><font class="smalltext"> url.<br>
              </font> <strong>MESSAGE SPECIFIC:</strong><br>
              <font class="smalltext"> 1. &nbsp;%message_text% will display message 
              text.<br>
              2. &nbsp;%message_title% will display message title.<br>
              3. &nbsp;%sender_username% will display sender</font><font class="normal"><font class="smalltext">'</font></font><font class="smalltext">s 
              username.<br>
              4. &nbsp;%message_time% will display time at which message received.<br>
              5. &nbsp;%message_date% will display date on which message received.</font></font></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="onepxtable">
    <tr class="titlestyle"> 
      <td height="25">&nbsp;Edit Mail Template</td>
    </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="5" cellpadding="2">
                <tr> 
                  <td width="40%" align="right" valign="top" class="innertablestyle"><strong><font class="normal">From 
                    / Reply address: 
                    <input name="update" type="hidden" value="Yes">
                    <input name="mailid" type="hidden" value="<?php echo $mailid; ?>">
                    </font></strong></td>
                  <td><font class="smalltext"> 
                    <input name="fromid" type="text" value="<?php echo $fromid; ?>" size="30">
                    <br>
                    The above Email Address will be sent as Sender's Email.</font></td>
                </tr>
                <tr> 
                  <td width="40%" align="right" valign="top" class="innertablestyle"><strong><font class="normal">Subject:</font></strong></td>
                  <td><font class="smalltext"> 
                    <input name="subject" type="text" value="<?php echo $subject; ?>" size="30">
                    <br>
                    The above text will be the custom subject for the Email</font></td>
                </tr>
                <tr> 
                  <td width="40%" align="right" valign="top" class="innertablestyle"><strong><font class="normal">Email 
                    Message:</font></strong></td>
                  <td><font class="smalltext"> 
                    <!--textarea name="mail" cols="40" rows="10"><?php echo $mail; ?></textarea-->
                                <? ?><script language="JavaScript" type="text/javascript">
//<!--

<?
                        
$content = $mail;
$content = RTESafe($content);
?>//Usage: initRTE(imagesPath, includesPath, cssFile)
initRTE("../images/", "", "");

//Usage: writeRichText(fieldname, html, width, height, buttons)
writeRichText('mail', '<?=$content?>', 450, 200, true, false);
//-->
</script>
              <noscript>
              <b>Javascript must be enabled to use this 
                form.</b></noscript><? ?><br>
                    The above text will be sent as automated mail upon certain 
                    actions e.g. Welcome message when a new user signs up.</font></td>
                </tr>
                <tr>
                  <td class="innertablestyle">&nbsp;</td>
                  <td><font class="normal"> 
                    <input name="html_format" type="checkbox" id="html_format" value="yes" <?php if($rs["freetpl_html_format"]=="yes") {echo "checked";}?>>
                    Send as HTML </font></td>
                </tr>
                <tr> 
                  <td width="40%" class="innertablestyle">&nbsp;</td>
                  <td> <input type="submit" name="Submit2" value="Save message"> 
                  </td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
  </form>
  <?php
  }// if mail
?>
</table>
<?
}// end of main()
include "template.php";
?>
