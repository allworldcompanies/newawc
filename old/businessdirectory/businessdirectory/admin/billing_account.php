<?
include "logincheck.php";
include_once "myconnect.php";
function main()
{
$rs0=mysql_fetch_array(mysql_query("select * from freetplclassified_config"));
?>
<script language="JavaScript">
//<!--
function emailCheck (emailStr) {
var emailPat=/^(.+)@(.+)$/
var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
var validChars="\[^\\s" + specialChars + "\]"
var quotedUser="(\"[^\"]*\")"
var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
var atom=validChars + '+'
var word="(" + atom + "|" + quotedUser + ")"
var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
var matchArray=emailStr.match(emailPat)
if (matchArray==null) {
	alert("PayPal ID seems incorrect (check @ and .'s)")
	return false
}
var user=matchArray[1]
var domain=matchArray[2]
if (user.match(userPat)==null) {
    alert("PayPal ID doesn't seem to be valid.")
    return false
}
var IPArray=domain.match(ipDomainPat)
if (IPArray!=null) {
    // this is an IP address
	  for (var i=1;i<=4;i++) {
	    if (IPArray[i]>255) {
	        alert("Destination IP address is invalid!")
		return false
	    }
    }
    return true
}
var domainArray=domain.match(domainPat)
if (domainArray==null) {
	alert("The domain name doesn't seem to be valid.")
    return false
}
var atomPat=new RegExp(atom,"g")
var domArr=domain.match(atomPat)
var len=domArr.length
if (domArr[domArr.length-1].length<2 || 
			domArr[domArr.length-1].length>4) {
		   alert("The address must end in a valid domain, or two letter country.")
   return false
}
if (len<2) {
   var errStr="This address is missing a hostname!"
   alert(errStr)
   return false
}
return true;
}


function Validator(form)
{
   if( form.freetpl_paypal_id.value!= "" )
    {
	   if (!emailCheck (form.freetpl_paypal_id.value) )
			{
				form.freetpl_paypal_id.focus();
				return (false);
			}
     }
	 else
	 {
		 if(form.freetpl_enable_paypal.checked==true)
		 {
		 alert('Please specify Paypal ID');
		 form.freetpl_paypal_id.focus();
		 return(false);
		 }
	 }
<?php
              
?>
    if(form.freetpl_seller_id.value!="")
    {
	   if (isNaN(form.freetpl_seller_id.value)||(form.freetpl_seller_id.value<=0) )
			{
				alert("Please specify a valid numeric value for 2Checkout Seller's ID");
				form.freetpl_seller_id.focus();
				return (false);
			}
     }
	 else
	 {
		 if(form.freetpl_enable_checkout.checked==true)
		 {
		 alert("Please specify 2Checkout Seller's ID");
		 form.freetpl_seller_id.focus();
		 return(false);
		 }
	 }
	
	 if((form.freetpl_enable_offline.checked==true)&&(form.freetpl_offline_details.value==""))
	 {
		 alert("Please specify Offline Payment Details");
		 form.freetpl_offline_details.focus();
		 return(false);
	 }
	 
	 if((form.freetpl_enable_offline.checked==false)&&(form.freetpl_enable_paypal.checked==false)
	&&(form.freetpl_enable_checkout.checked==false))
	 {
		 alert("Please choose atleast one payment option");
		 form.freetpl_paypal_id.focus();
		 return(false);
	 }
	 if(form.freetpl_billing_terms.value=="")
	 {
		 alert("Please specify Billing Terms");
		 form.freetpl_billing_terms.focus();
		 return(false);
	 }
return true;
}
//-->
</script>
<form action="update_account.php" method="post" name="frm1" id="frm1"  onSubmit="return Validator(this);" >
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="onepxtable">
    <tr class="titlestyle"> 
      <td height="25">&nbsp;Configure Billing Account</td>
     </tr><?PHP
                        
    ?><tr> 
      <td><table width="100%" border="0" align="center" cellpadding="2" cellspacing="5">
          <tr valign="top" class="row1"> 
            <td width="40%" class="innertablestyle" > <div align="right"><strong><font class="normal">Paypal 
                ID:</font></strong></div></td>
            <TD align=left><font  class='red'>*</font></TD>
            <td> <font class="smalltext"> 
              <input name="freetpl_paypal_id" type="text" class="box1" id="recperpage3" value="<? echo $rs0["freetpl_paypal_id"];?>" size="40">
              <br>
              <input name="freetpl_enable_paypal" type="checkbox" id="freetpl_enable_paypal" value="yes" <?
				if($rs0["freetpl_enable_paypal"]=="yes") echo "checked";	?>>
              Enable PayPal Payments.<br>
              Please specify your PayPal ID as required to run paid Listing 
              services through PayPal.</font></td>
          </tr>
          <tr valign="top" class="row1"> 
            <td width="40%" class="innertablestyle" > <div align="right"><strong><font class="normal">2Checkout 
                Seller's ID:</font></strong></div></td>
            <TD align=left><font  class='red'>*</font></TD>
            <td> <font class="smalltext"> 
              <input name="freetpl_seller_id" type="text" class="box1" id="recperpage3" value="<? echo $rs0["freetpl_seller_id"];?>" size="40">
              <br>
              <input name="freetpl_enable_checkout" type="checkbox" id="freetpl_enable_checkout2" 
				value="yes" <? if($rs0["freetpl_enable_checkout"]=="yes") echo "checked";	?>>
              Enable 2Checkout Payments.<br>
              Please specify your 2Checkout Seller's ID as required to run paid 
              Listing services through 2Checkout.</font></td>
          </tr>
          <tr valign="top" class="row1"> 
            <td width="40%" class="innertablestyle" > <div align="right"><strong><font class="normal">Pay 
                Offline Details:</font></strong></div></td>
            <TD align=left><font  class='red'>*</font></TD>
            <td><font class="smalltext"> 
              <textarea name="freetpl_offline_details" cols="35" rows="5" class="box1" id="freetpl_offline_details"><? echo $rs0["freetpl_offline_details"]; ?></textarea><br>
              <input name="freetpl_enable_offline" type="checkbox" id="freetpl_enable_offline2" 
				value="yes" <? if($rs0["freetpl_enable_offline"]=="yes") echo "checked";	?>>
              Enable Offline Payments<br>Please specify details for offline payments.</font></td>
          </tr>
          <tr valign="top" class="row1"> 
            <td width="40%" class="innertablestyle" > <div align="right"><strong><font class="normal">Billing 
                Terms:</font></strong></div></td>
            <TD align=left><font  class='red'>*</font></TD>
            <td> <font  class="smalltext"> 
              <textarea name="freetpl_billing_terms" cols="35" rows="5" class="box1" id="recperpage3"><? echo $rs0["freetpl_billing_terms"];?></textarea><br>
              The text provided above will be shown on add money page.</font></td>
          </tr>
          <tr> 
            <td width="40%" valign="top" class="innertablestyle">&nbsp;</td>
            <td>&nbsp;</td>
            <td><input name="Submit" type="submit" class="submit" value="Update Configuration"></td>
          </tr>
        </table></td>
    </tr>
  </table>
  </form><?
}// end main
include "template.php";?>