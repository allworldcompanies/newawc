<?php
include_once("myconnect.php");
include_once("logincheck.php");
$msg="";
$selected=0;

if(count($_POST)>0)
{
////////===================Delete Icons
if(isset($_REQUEST["deleteicons"]))
{
	$selected=1;
	$cnt=0;
	$dir = opendir("../freetplclassified_icons");
	while($item = readdir($dir))
	{
		$used=0;
		$icon_query=mysql_query("select * from freetplclassified_icons");
		while ( $icon=mysql_fetch_array($icon_query))
		{
			for($i=2;$i<=8;$i++)
			{
				if ($icon[$i]==$item)
				$used=1;
			}
		}
		//USED IN site logo 
		$sql1="select * from freetplclassified_config where 1";
		$rs_query=mysql_query($sql1);
		while ( $rs=mysql_fetch_array($rs_query))
		{
			if (($rs["freetpl_logo"]==$item)||($rs["freetpl_default_image"]==$item))
			$used=1;
		}
		if ( ($used==0) && ($item!=".") && ($item!="..") &&($item!="ipn_error.txt"))
		{
			unlink("../freetplclassified_icons/".$item);
			$cnt++;
		}
	}// end while
	closedir($dir);
	$msg .= ($cnt==1)?"$cnt Icon":"$cnt Icons"; 
	$msg .= " Removed <br>";
}
////////===================Delete Images
if(isset($_REQUEST["deleteimages"]))
{
	$selected=1;
	$cnt=0;
	$dir = opendir("../uploadedimages");
	while($item = readdir($dir))
	{
		$used=0;
		$icon_query=mysql_query("select * from freetplclassified_images");
		while ( $image=mysql_fetch_array($icon_query))
		{
				if ($image["url"]==$item)
				{
				$used=1;
				}
		}
		$mem_q=mysql_query("select * from freetplclassified_members");
		while($mem=mysql_fetch_array($mem_q))
		{
			if(($mem["logo_url"]==$item)||($mem["small_logo"]==$item))
			{
			$used=1;
			}
		}
		$ban_q=mysql_query("select * from freetplbanners_sizes");
		while($ban=mysql_fetch_array($ban_q))
		{
			if(($ban["default_banner"]==$item))
			{
			$used=1;
			}
		}
///////////---------------
		if ( ($used==0) && ($item!=".") && ($item!="..") &&($item!="ipn_error.txt"))
		{
		//echo "-$item-";
		unlink("../uploadedimages/".$item);
		$cnt++;
		}
	}// end while
	closedir($dir);
	$msg .= ($cnt==1)?"$cnt Image":"$cnt Images"; 
	$msg .= " Removed <br>";
}
//===========delete classifieds
if(isset($_REQUEST["deleteprod"]))
{
	if(isset($_REQUEST["max_days"])&&is_numeric($_REQUEST["max_days"])&&($_REQUEST["max_days"]>0))
	{
		header("Location:"."delete_expired.php?max_days=".$_REQUEST["max_days"]);
		
	}
	else
	{
	 	$msg.="Please choose a valid value for number of days";
	}
}
//=============================
//header("Location:"."cleanup.php?msg=".urlencode($msg));

}

function main()
{
global $msg,$selected;
$config=mysql_fetch_array(mysql_query("select * from freetplclassified_config "));
$showform="";
//$msg="";
//$selected=0;
if(count($_POST)>0)
{
?> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="errorstyle">
  <tr>
    <td>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
        <tr> 
    <td  valign="top"><? 
      
if  (count($_POST)<>0)
{

      ?><font class='normal red'> 
      <?php
	 if($selected<>0)
	  {
	  ?>
      Cleanup has completed<br>
      <?
	  }
	  else
	  { 
	  	echo "Please select an option.";
	  }
		if (isset($msg) )
		{
		echo $msg;
		}
		?>
      </font> 
    </td>
  </tr>
</table></td>
  </tr>
</table><?php
    
}
?><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<?
}	//end if post
?>
<table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="onepxtable">
  <tr class="titlestyle"> 
    <td height="25">&nbsp;Cleanup Routines</td>
  </tr>
  <script language="JavaScript">
  function validate_form(frm)
  {
  	if((frm.deleteicons.checked==false)&&(frm.deleteimages.checked==false)&&(frm.deleteprod.checked==false)  )
	{
		alert('Please select some option');
		frm.deleteicons.focus();
		return false;
	}
	if((frm.deleteprod.checked==true)&&((frm.max_days.value=="")||isNaN(frm.max_days.value)||(frm.max_days.value<=0)))
	{
		alert('Please specify a valid numeric value for number of days');
		frm.max_days.focus();
		return false;
	  
	}
	return true;
  }
  </script>
  <form name="form1" method="post" action="cleanup.php" onSubmit="return validate_form(this);">
    <tr> 
      <td valign="top" class='innertablestyle'> <font class='normal'> 
        <input name="deleteicons" type="checkbox" id="deleteicons" value="Yes">
        Remove all unused uploaded icons / Website logos/Default Images. </font> 
      </td>
    </tr>
    <!--tr> 
      <td valign="top" class='innertablestyle'><font class='normal'> 
        <input type="checkbox" name="deleteimages" value="Yes">
        Delete any images not being used in logo by members.</font></td>
    </tr-->
    <!--<tr> 
      <td align="left" valign="top" class='innertablestyle'><font class='normal'> 
        <input name="deleteicons" type="checkbox" id="deleteexp" value="Yes">
        Delete all icons not being used in the site. </font></td>
    </tr>-->
    <tr> 
      <td align="left" valign="top" class='innertablestyle'><font class='normal'> 
        <input name="deleteimages" type="checkbox" id="deleteimages" value="Yes">
        Remove all Images/logos not being used by listings/members. </font></td>
    </tr>
    <tr> 
      <td align="left" valign="top" class='innertablestyle'><font class='normal'> 
        <input name="deleteprod" type="checkbox" id="deleteprod" value="Yes">
        Remove all Listings expired 
        <input name="max_days" type="text" id="max_days" size="5">
        days before. </font></td>
    </tr>
    <tr> 
      <td align="right" valign="top" class='innertablestyle'><div align="left"> 
          <p><font class='normal'><font class='red'>This operation is non reversible. 
            Once removed, information can't be retrieved.<br>
            </font></font><font class='normal'><br>
            <input type="submit" name="Submit2" value="Start the Cleanup" >
            <!-- onClic k="javascript:alert('This feature is disabled in the demo.');"-->
            </font></p>
        </div></td>
    </tr>
  </form>
</table>
<?
}  //End of main
include_once("template.php");
?>