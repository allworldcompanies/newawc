<table width="151" height="100%" border="0" align="left" cellpadding="1" cellspacing="0">
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td valign="top" class="innertablestyle"><table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr> 
                <td><font class="normal"><strong>Admin 
                  Settings</strong></font></td>
              </tr>
              <tr> 
                <td><font class="normal"><a href="adminhome.php" class="sidelink">Home</a></font></td>
              </tr>
              <tr> 
                <td><font class="normal"><a href="config.php">Configure 
                  Site</a></font></td>
              </tr>
              <tr> 
                <td><font class="normal"><a href="changepassword.php">Change 
                  Password</a></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="siteemails.php">Config 
                    Emails</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="logout.php">Logout</a></font></div></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td valign="top" class="innertablestyle"><table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr> 
                <td><font class="normal"><strong> 
                  Categories</strong></font></td>
              </tr>
              <tr> 
                <td><font class="normal"><a href="browsecats.php">Manage 
                  Catergories</a></font></td>
              </tr>
              <tr> 
                <td><font class="normal"><a href="add_cat.php" class="sidelink">Add 
                  New</a></font></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td valign="top" class="innertablestyle"><table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr> 
                <td><font class="normal"><strong> 
                  Members</strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="members.php">Manage 
                    Members</a></font></div></td>
              </tr>
              <tr> 
                <td><font class="normal"><a href="addmember.php" class="sidelink">Add 
                  New Member</a></font></td>
              </tr>
			                <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="view_transactions.php">Billing 
                    Transactions</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="add_tc.php">Make 
                    a Transaction </a></font></div></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td valign="top" class="innertablestyle"><table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr> 
                <td><font class="normal"><strong> 
                  Classifieds</strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><font class="normal"><a href="product.php">Manage 
                  Listings </a></font></td>
              </tr>
              <tr> 
                <td><font class="normal"><a href="product.php?type=2" class="sidelink">All 
                  Closed Listings</a></font></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="100%" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td valign="top" class="innertablestyle"><table width="100%" border="0" cellspacing="1" cellpadding="1">
              <tr> 
                <td><font class="normal"><strong> 
                  Advertisers</strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="advertisers.php">Manage 
                    Advertisers</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><font class="normal"><a href="view_adv_transactions.php">Billing 
                  Transactions</a></font></td>
              </tr>
              <tr bgcolor="">
                <td><font class="normal"><a href="add_adv_transaction.php">Make 
                  a Transaction </a></font></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="152" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td bgcolor="F5F5F5" ></td>
        </tr>
        <tr> 
          <td align="left" valign="top" bgcolor="F5F5F5"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="90%"><font class="normal"><strong>Banner 
                  Options </strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="plans.php">Banner 
                    Plans </a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="ads.php">Banners</a></font></div></td>
              </tr>
              <tr bgcolor="">
                <td><font class="normal"><a href="addad.php">Add 
                  a banner Ad</a></font></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="152" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td bgcolor="F5F5F5" ></td>
        </tr>
        <tr> 
          <td align="left" valign="top" bgcolor="F5F5F5"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="90%"><font class="normal"><strong>Billing 
                  Options </strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="billing_account.php">Billing 
                    Account</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="billing_fee.php">Config 
                    Billing Fees</a></font></div></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="152" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td bgcolor="F5F5F5" ></td>
        </tr>
        <tr> 
          <td align="left" valign="top" bgcolor="F5F5F5"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="90%"><font class="normal"><strong>Feedbacks</strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="mem_feedback.php">By 
                    Members </a></font></div></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="152" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td bgcolor="F5F5F5" ></td>
        </tr>
        <tr> 
          <td align="left" valign="top" bgcolor="F5F5F5"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="90%"><font class="normal"><strong>Messages</strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="messages.php">View 
                    all Messages</a></font></div></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td valign="top" >&nbsp;</td>
  </tr>
  <tr> 
    <td valign="top" bgcolor="#75A7AC" > <table width="152" height="100%" border="0" cellpadding="1" cellspacing="0">
        <tr> 
          <td bgcolor="F5F5F5" ></td>
        </tr>
        <tr> 
          <td align="left" valign="top" bgcolor="F5F5F5"> <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr> 
                <td width="90%"><font class="normal"><strong>Mailing 
                  Options </strong></font></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="email.php">Email 
                    a Member</a></font></div></td>
              </tr>
              <tr bgcolor=""> 
                <td><div align="left"><font class="normal"><a href="emailall.php">Email 
                    All</a></font></div></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="100%" valign="top" >&nbsp;</td>
  </tr>
</table>
