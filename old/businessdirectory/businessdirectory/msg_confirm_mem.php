<?php
include_once "myconnect.php";
function main()
{
?>
<table width="80%" border="0" align="center" cellpadding="1" cellspacing="0">
  <tr> 
    <td></td>
  </tr>
  <tr height="20"> 
    <td ></td>
  </tr><?php
  if(isset($_REQUEST["errmsg"])&&($_REQUEST["errmsg"]<>""))
  {
  ?><tr>
    <td valign="top"> 
      <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="msgstyle">
        <tr align="left"> 
          <td colspan="2"><strong><?php echo $_REQUEST["errmsg"];?></strong></td>
        </tr>
        <?php
		if(isset($_REQUEST["err"])&&($_REQUEST["err"]<>""))
		{
		 $return_path=$_REQUEST["err"].".php";
		 if(isset($_REQUEST["id"])&&($_REQUEST["id"]<>""))
		 { $return_path.="?freetpl_id=".$_REQUEST["id"];} 
		
        ?>
        <tr> 
          <td align="center">&nbsp;</td>
          <td align="left"> Click <a href="<?php echo $return_path;?>">here</a> 
            to try again.</td>
        </tr>
        <?php
		}
	
        ?>
        <tr> 
          <td align="center">&nbsp;</td>
          <td align="left"> Click <a href="show_messages.php">here</a> to view 
            all messages sent/received by you. </td>
        </tr>
        <tr> 
          <td align="center">&nbsp;</td>
          <td align="left">Click <a href="show_contacts.php">here</a> to manage 
            contact list. </td>
        </tr>
        <tr> 
          <td align="center">&nbsp;</td>
          <td align="left">Click <a href="show_blocked.php">here</a> to manage 
            block list. </td>
        </tr>
        <tr> 
          <td align="center">&nbsp;</td>
          <td align="left">Click <a href="logout.php">here</a> to logout. </td>
        </tr>
      </table>
      
    </td>
  </tr><?php
  }
  ?><tr> 
    <td> <div align="center"> </div></td>
  </tr>
</table>
  
<? 
}// end of main()
include "template.php";
?>