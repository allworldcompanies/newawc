<?php
include_once("myconnect.php");

$config=mysql_fetch_array(mysql_query("select * from freetplclassified_config"));
	//bye passed to make verification necessary
if($config["freetpl_signup_verification"]=="no")
{
	header("Location: signup_form.php");
	die();
}

$errcnt=0;
if(count($_POST)<>0)		//IF SOME FORM WAS POSTED DO VALIDATION
{
		
		if(!get_magic_quotes_gpc())
		{
			$email=str_replace("$","\$",addslashes($_REQUEST["email"]));
		}
		else
		{
			$email=str_replace("$","\$",$_REQUEST["email"]);
		}
	
		if ( !isset( $_REQUEST["email"] ) || (strlen(trim($_REQUEST["email"] )) == 0) )
		{
			$errs[$errcnt]="Email Address must be provided";
			$errcnt++;
		}
		elseif(preg_match ("/[;<>&]/", $_REQUEST["email"]))
		{
			$errs[$errcnt]="Email can not have any special character (e.g. & ; < >)";
			$errcnt++;
		}
		else
		{
			$mem_query=mysql_query ("select * from freetplclassified_members where email='$email'");
			if ($mem=mysql_fetch_array($mem_query))
			{
				$errs[$errcnt]="Some member with same Email Id already exists.";
				$errcnt++;
			
			}
		}
		
		if($errcnt==0)
		{
			$config=mysql_fetch_array(mysql_query("select * from freetplclassified_config "));
			
			mysql_query ("delete from freetplclassified_signups where freetpl_email='$email'");
	
			$rnum =  mt_rand(1,1000000000);
			$insert_str="Insert into `freetplclassified_signups` ( freetpl_email ,freetpl_rnum, freetpl_onstamp) 
			VALUES ( '$email','$rnum', " . date("YmdHis",time()) . ")";
			mysql_query($insert_str);
			if(mysql_affected_rows()>0)
			{
			//==================send confirmation mail================
			$freetpl_null_char=$config["null_char"];
			$signup_url=$config["site_root"]."/signup_form.php?rnum=$rnum&email=".$_REQUEST["email"];
			$signup_url="<a href='$signup_url' target='_blank' title='Click to complete registration'>$signup_url</a>";
			
			$sql = "SELECT * FROM freetplclassified_mails where mailid=25" ;
			$rs_query=mysql_query($sql);
			
			if ( $rs=mysql_fetch_array($rs_query) and $rs["freetpl_status"]=='yes' )
			{
				 $from =$rs["fromid"];
				 $to = $_REQUEST["email"];
				 $subject =$rs["subject"];
				 $header="From:" . $from . "\r\n" ."Reply-To:". $from  ;

				 $body=str_replace("%email%", $_REQUEST["email"],str_replace("%password%",$freetpl_null_char,str_replace("%fname%", $freetpl_null_char,str_replace("%username%", $freetpl_null_char,str_replace("%lname%",$freetpl_null_char, $rs["mail"]) )))); 
				$body=str_replace("%signup_url%",$signup_url,str_replace("%loginurl%",$freetpl_null_char,$body));

				if( $rs["freetpl_html_format"]=="yes" )
				{
					$header .= "\r\nMIME-Version: 1.0";
					$header .= "\r\nContent-type: text/html; charset=iso-8859-1\r\n";
				}
						
// 	echo "--from:-$from----to:-$to---sub:-$subject----head:-$header----";
// 	echo "<pre>$body</pre>";
	
 				@mail($to,$subject,$body,$header);
			}		
		//========================================================
	//			echo "<br>Just comment this before packing<br>".$signup_url."<br>";

			header("Location: gen_confirm.php?errmsg=".urlencode("Please check your email and follow the link provided in it to complete registration process, Thank You."));
			die();
		}// if inserted
		else
		{
			header("Location: gen_confirm.php?err=signup&errmsg=".urlencode("Some error occurred, unable to complete the signup process."));
			die();
		}
		}// if no errors

}// if form posted
else
{
	$email='';
}

function main ()
{
global $errs, $errcnt, $email;

$showform="";
     	      	  
if  (count($_POST)>0)
{

if ( $errcnt<>0 )
{
?>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="errorstyle">
  <tr>
    <td colspan="2"><strong>&nbsp;Your signup request cannot be processed due 
      to following reasons</strong></td>
  </tr>
  <tr height="10"> 
    <td colspan="2"></td>
  </tr>
  <?

for ($i=0;$i<$errcnt;$i++)
{
?>
  <tr valign="top"> 
    <td width="6%">&nbsp;<?php echo $i+1;?></td>
    <td width="94%"><?php echo  $errs[$i]; ?></td>
  </tr>
  <?
}
?>
</table>

<?

}

}

if ($showform<>"No")
{
?>
<script language="JavaScript">
	function emailCheck (emailStr) {
	var emailPat=/^(.+)@(.+)$/
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
	var validChars="\[^\\s" + specialChars + "\]"
	var quotedUser="(\"[^\"]*\")"
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
	var atom=validChars + '+'
	var word="(" + atom + "|" + quotedUser + ")"
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
	var matchArray=emailStr.match(emailPat)
	if (matchArray==null) {
		alert("Email Address seems incorrect (check @ and .'s)")
		return false
	}
	var user=matchArray[1]
	var domain=matchArray[2]
	if (user.match(userPat)==null) {
		alert("Username doesn't seem to be valid in the Email Address")
		return false
	}
	var IPArray=domain.match(ipDomainPat)
	if (IPArray!=null) {
		// this is an IP address
		  for (var i=1;i<=4;i++) {
			if (IPArray[i]>255) {
				alert("Destination IP address doesn't seem to be valid")
			return false
			}
		}
		return true
	}
	var domainArray=domain.match(domainPat)
	if (domainArray==null) {
		alert("Domain name doesn't seem to be valid")
		return false
	}
	var atomPat=new RegExp(atom,"g")
	var domArr=domain.match(atomPat)
	var len=domArr.length
	if (domArr[domArr.length-1].length<2 || 
			domArr[domArr.length-1].length>4) {
		   alert("The address must end in a valid domain, or two letter country.")
	   return false
	}
	if (len<2) {
	   var errStr="Email Address is missing a hostname";
	   alert(errStr)
	   return false
	}
	return true;
	}

	function validate(form)
	{
    if(!form.email.value.match(/[a-zA-Z\.\@\d\_]/)) 
		{
           alert('Please specify valid Email Address');
           form.email.focus();
		   form.email.select();
		   return false;
        }
		
	if (!emailCheck (form.email.value) )
		{
			form.email.focus();
			form.email.select();
		   return (false);
		}

	return true;
	}
</script>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF'];	?>" onSubmit="return validate(this);">
  <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr class="titlestyle">
      <td>&nbsp;Signup Process</td>
  </tr>
  <tr>
    <td>
 <table width="100%" border="0" align="center" cellpadding="2" cellspacing="5" class="onepxtable">
          <tr valign="top"> 
            <td width="40%" align="right" class="innertablestyle"><font class="normal"><strong>Email 
              Address</strong></font></td>
            <td width="6"><font class="red">*</font></td>
            <td width="60%"><font face="Arial, Helvetica, sans-serif" size="2"> 
              <input name="email" type="text" class=select size="30" maxlength="40" value="<?php echo $email; ?>">
              <br>
              </font><font class='smalltext'>Your older verification code will 
              be disabled once you request for newer verification code.</font></td>
          </tr>
          <tr valign="top"> 
            <td align="right" class="innertablestyle">&nbsp;</td>
            <td>&nbsp;</td>
            <td><input name="submit"  type="submit" value="Continue"></td>
          </tr>
        </table>
</td>
  </tr>
</table>  </form>
<?
} //If showform = No? ends here

}
include_once("template.php");

 ?>