<?php require("session.php"); ?>
<?php require("functions.php"); ?>
<?php require("connection.php"); ?>
<?php require("header.php"); ?>
<?php require('func.php'); ?>

<section class="clear container">
 
	<?php
		require("includes/search_form.php"); 	//Search Engine for companies
		require("includes/search_table.php"); 	//Table for Searched Companies
	?>	
 
</section>
 

<?php require("footer.php"); ?>