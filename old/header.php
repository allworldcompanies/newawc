<?php

header_remove("Expires");
header_remove("Cache-Control");
header_remove("Pragma");
header_remove("Last-Modified");
?>
<!DOCTYPE HTML>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Companies Database</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="-1">


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8/jquery.validate.min.js"></script>
        <script src="js/popup.js" ></script>
        <script src="js/default.js"></script>
        <script src="js/docs.js"></script>
        <link rel="stylesheet" type="text/css" media="screen,projection" href="css/jquery-fallr-1.0.css" />
        <link rel="stylesheet" href="css/avgrund.css">
        <script src="js/jquery.avgrund.js"></script>
	      <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	      <script type="text/javascript" src="js/jquery-fallr-1.0.pack.js"></script>
        <script type="text/javascript" src="js/fallr-base.js"></script>
        <link rel="stylesheet" media="screen" href="css/style.css">

   </head>


    <body class="page">
            <div class="container first">
                <div class="section row">
                    <div class="col four lg">
			            <a class="logo" href="http://www.allworldcompanies.com"><img src="../img/logo.png" title="HOME"/></a>
                    </div>

                    <div class="row">
                        <div class="col five user">
                           <ul>
                            <?php if (isset($_SESSION['users_fname'])) { ?>
                           <li><font color="#666">Hello <?php echo $_SESSION['users_lname']; ?>!</font></li><li>|</li>
                           <li><a href="./user_profile.php"> My profile</a></li><li>|</li>
                           <li><a href="logout.php">Log Out</a></li>


                           <?php if ($_SESSION['users_role'] == 'admin') { echo "<li>|</li> <li><a href='admin.php'>CP</a></li> "; } ?>



                         <?php  } else {        ?>

                           <li>Welcome Guest!</li><li>|</li>
                           <li><a href="#fallr-forms" class="button">Login</a></li><li>|</li>
                           <li><a href="register.php">Register</a></li><li>|</li>
                           <li><a href="#">FAQ</a></li>

                         <?php } ?>
                            <li>|</li><li><?php require("send_opinion.php"); ?></li>
                            <li>

                    <?php if (isset($_SESSION['users_fname'])) { ?>
                    <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" style="position:relative;">

                    <input type="hidden" name="cmd" value="_cart">

                    <input type="hidden" name="business" value="sales@screw-paypal.com">

                    <input type="image" src="img/cart.png" style="position:absolute; top:-10px; border:none; width:100px;"  border="0" name="submit" alt="Make payments with PayPal - it'sfast, free and secure!">

                    <input type="hidden" name="display" value="1">

                    </form>
                      <?php } ?>

                            </li>
                         </ul>
                         <div class="clear"></div>
                        </div>

                        <div class="col three social">
                         <a href="#"><img src="../img/youtube.png" title="youtube" width="34px" height="auto" /></a> <a href="#"><img src="../img/vimeo.png" title="Vimeo" width="34px" height="auto"/></a>
                         <a href="#"><img src="../img/myspace.png" title="My Space" width="34px" height="auto"/></a> <a href="#"><img src="../img/googleplus.png" title="Google+" width="34px" height="auto"/></a>
                         <a href="#"><img src="../img/twitter.png" title="Twitter" width="34px" height="auto" /></a> <a href="#"><img src="../img/facebook.png" title="Facebook" width="34px" height="auto" /></a>
                         <div class="clear"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col eight">
                          <div class="navigation right">
                                <div class="main-menu">
                                <ul class="menu clear">
                                 <li><a href="#"> About Us</a></li>
                                 <li><a href="#"> News</a></li>
                                 <li><a href="#"> Special</a></li>
                                 <li><a href="#"> Discounts</a></li>
                                 <li><a href="#"> Trade</a></li>
                                 <li class="sub"><a href="services.php"> Services</a>
                                     <ul>
                                       <li><a href="#">Search Database</a></li>
                                       <li><a href="#">Campains</a></li>
                                       <li><a href="#">LookUp</a></li>
                                       <li><a href="#">Classmates</a></li>
                                       <li><a href="#">Ancestors</a></li>
                                       <li><a href="#">Jobs World</a></li>
                                     </ul>
                                 </li>
                                 <li><a href="index.php">Home</a></li>
                               <div class="clear"></div>
                               </ul>
                              </div>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
              <style type="text/css">




input[type="submit"], .button-search{
  width:auto;
  color:#000;
    position: relative;
    overflow: visible;
    display: inline-block;
    padding: 5px 15px;
    border: 1px solid #d4d4d4;
    margin-left: 10px;
    margin-top: 10px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #fff;
    white-space: nowrap;
    cursor: pointer;
    background-color: #ececec;
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f4f4f4), to(#ececec));
    background-image: -moz-linear-gradient(#f4f4f4, #ececec);
    background-image: -o-linear-gradient(#f4f4f4, #ececec);
    background-image: linear-gradient(#f4f4f4, #ececec);
    -webkit-background-clip: padding;
    -moz-background-clip: padding;
    -o-background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, .25);
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-transition:all 0.3s;
}

input[type="submit"]:hover, .button-search:hover, .button-search:focus
input[type="submit"]:focus {
    border-color: #3072b3;
    border-bottom-color: #2a65a0;
    text-decoration: none;
    text-shadow: -1px -1px 0 rgba(0,0,0,0.3);
    color: #fff;
    background-color: #3C8DDE;
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#599bdc), to(#3072b3));
    background-image: -moz-linear-gradient(#599bdc, #3072b3);
    background-image: -o-linear-gradient(#599bdc, #3072b3);
    background-image: linear-gradient(#599bdc, #3072b3);
}
.domain-button{
  width:211px;
}

#loading {
  position: absolute;
  left: 50%;
  bottom: 50%;
}

              </style>



<div id="loading">
  <img src="img/301.gif" alt="loading" width="32" />
</div>

<script type="text/javascript">

$(window).load(function(){

  $("#loading").hide();

});

</script>