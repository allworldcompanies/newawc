<div id="database" class="three-four col">

<style type="text/css">
strong {
	font-weight: bold;
	font-size: 110%;
}
</style>

<?php

	$tbl_name="companies";		//Table Name

	$adjacents = 3;
	

	$query = "SELECT COUNT(*) as num FROM $tbl_name WHERE LENGTH(address) > 1 AND LENGTH(phone) > 1 AND LENGTH(email) > 1 AND LENGTH(website) > 1 ";
	$total_pages = mysql_fetch_array(mysql_query($query));
	$total_pages = $total_pages['num'];
	

	$targetpage = $_SERVER['PHP_SELF']; 	
	$limit = 10; 								// Fields Per Page
	$page = isset($_GET['page']) ? $_GET['page'] : 1;
	if(isset($page)) 
		$start = ($page - 1) * $limit;
	else
		$start = 1;								


	$sql = "SELECT ID,company_name, address, phone, position, email, website FROM $tbl_name WHERE LENGTH(address) > 1 AND LENGTH(phone) > 1 AND LENGTH(email) > 1 AND LENGTH(website) > 1  ORDER BY RAND() LIMIT $start, $limit ";
	$result = mysql_query($sql);
	

	$prev = $page - 1;							
	$next = $page + 1;							
	$lastpage = ceil($total_pages/$limit);		
	$lpm1 = $lastpage - 1;						
	

	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		// Prev Button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage?page=$prev\">&larr; previous</a>";
		else
			$pagination.= "<span class=\"disabled\">&larr; previous</span>";	
		
			
		if ($lastpage < 7 + ($adjacents * 2))	
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\"><strong> $counter </strong></span>";
				else
					$pagination.= "<a href=\"$targetpage?page=$counter\"> $counter </a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	
		{
			
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\"><strong> $counter </strong></span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\"> $counter </a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\"> $lpm1 </a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\"> $lastpage </a>";		
			}
			
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage?page=1\"> 1 </a>";
				$pagination.= "<a href=\"$targetpage?page=2\"> 2 </a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\"><strong> $counter </strong></span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\"> $counter </a>";
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\"> $lpm1 </a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\"> $lastpage </a>";
			}
			
			else
			{
				$pagination.= "<a href=\"$targetpage?page=1\"> 1 </a>";
				$pagination.= "<a href=\"$targetpage?page=2\"> 2 </a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\"> <strong> $counter </strong></span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\"> $counter </a>";
				}
			}
		}

		// Next Button
		if ($page < $counter - 1) 
			$pagination.= "<a href=\"$targetpage?page=$next\">next &rarr;</a>";
		else
			$pagination.= "<span class=\"disabled\">next &rarr;</span>";
		$pagination.= "</div>\n";		
	}
?>

<table class="tb-responsive">
    <thead>
	<tr class="first-tr">
		<th><h2>Company Name</h2></th>
        <th><h2>Address</h2></th>
		<th><h2>Phone</h2></th>
        <th><h2>Position</h2></th>
        <th><h2>Email</h2></th>
        <th><h2>Website</h2></th>
    </thead>
	</tr>
	<tbody>
<?php while($row = @mysql_fetch_array($result)) { ?>

	<tr onclick="document.location='company_user.php<?php echo "?id=" . $row['ID']; ?> ';">
		<td><?php echo $row["company_name"]; ?></td>
		<td><?php echo $row["address"]; ?></td>
		<td><?php if($verify > 0){  echo $row["phone"]; } else { if ($row["phone"] <> '') { echo substr($row["phone"], 0, 2) . ' [...] ' . substr($row["phone"], 5); }}?></td>
		<td><?php echo $row["position"]; ?></td>
        <td><?php if($verify > 0){  echo $row["email"]; } else { if ($row["email"] <> '') { echo substr($row["email"], 0, 1) . ' [...] ' . strstr($row["email"], '@'); }}?></td>
		<td><?php if($verify > 0){  echo $row["website"]; } else { if ($row["website"] <> '') { echo strstr($row["website"], '.', true) . ' [...] ' . strstr($row["website"], '.'); }}?></td>

    </tr>
<?php
} // End While
?>
    </tbody>
</table>
<p style="padding-top: 5px; text-align:center;"><?php echo $pagination; ?></p>
				

</div>