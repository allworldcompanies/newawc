var myDropzone;

Dropzone.options.myDropzone = {
	init: function() {
	  this.on("addedfile", function(file) {

	    // Create the remove button
	    var removeButton = Dropzone.createElement("<button class='btn btn-dropzone'>Remove file</button>");


	    // Capture the Dropzone instance as closure.
	    var _this = this;

	    // Listen to the click event
	    removeButton.addEventListener("click", function(e) {
	      // Make sure the button click doesn't submit the form:
	      e.preventDefault();
	      e.stopPropagation();

	      // Remove the file preview.
	      _this.removeFile(file);
	      // If you want to the delete the file on the server as well,
	      // you can do the AJAX request here.
	    });

	    // Add the button to the file preview element.
	    file.previewElement.appendChild(removeButton);
	  });
	}
};

/**************************
*
*	EXT - for the quotes
*
**************************/
(function($){
	$.fn.extend({
        //plugin name - rotaterator
        rotaterator: function(options) {

        	var defaults = {
        		fadeSpeed: 600,
        		pauseSpeed: 100,
        		child:null
        	};

        	var options = $.extend(defaults, options);

        	return this.each(function() {
        		var o =options;
        		var obj = $(this);
        		var items = $(obj.children(), obj);
        		items.each(function() {$(this).hide();})
        		if(!o.child){var next = $(obj).children(':first');
        	}else{var next = o.child;
        	}
        	$(next).fadeIn(o.fadeSpeed, function() {
        		$(next).delay(o.pauseSpeed).fadeOut(o.fadeSpeed, function() {
        			var next = $(this).next();
        			if (next.length == 0){
        				next = $(obj).children(':first');
        			}
        			$(obj).rotaterator({child : next, fadeSpeed : o.fadeSpeed, pauseSpeed : o.pauseSpeed});
        		})
        	});
        });
        }
    });
})(jQuery);

$(function(){

	//home page quotes
	$('#quotes').rotaterator({fadeSpeed:1000, pauseSpeed:3000});

	//always activate first tab
	$('#myTab a:eq(1)').tab('show');


    //listings show more
    $( "#more_make" ).bind( "click", function() {
        $('#more_make_list').show();
        $('#more_make_link').hide();
        return false;
    });
    $( "#less_make" ).bind( "click", function() {
        $('#more_make_list').hide();
        $('#more_make_link').show();
        return false;
    });
	//the graph
    if($("#visualization").length > 0){


          var css_id = "#visualization";
        var data = [
            {label: 'BMW 525D SE TOURING SILVER', data: [[1,700], [2,600], [3,400], [4,390], [5,300], [6,300], [7,300]]},
            {label: 'SEAT Leon 1.6 TDI CR S 5dr (2010)', data: [[1,800], [2,600], [3,400], [4,400], [5,500], [6,400], [7,400]]},
            {label: '2008 Scuderia Spider 16M', data: [[1,800], [2,200], [3,200], [4,200], [5,100], [6,50], [7,0]]}
        ];
        var options = {
            series: {stack: 0,
                     lines: {show: true, steps: false },
                     bars: {show: false, barWidth: 0.9, align: 'center'}
            },
            xaxis: {ticks: [[1,'Sun'], [2,'Mon'], [3,'Tue'], [4,'Wed'], [5,'Thu'], [6,'Fri'], [7,'Sat']]}
        };

        $.plot($(css_id), data, options);
    }


	//make text area bigger
	var textarea_height = $('textarea.expand').height();
    $('textarea.expand').focus(function () {
        $(this).animate({ height: "400px" }, 500);
    });
    $('textarea.expand').focusout(function () {
        $(this).animate({ height: textarea_height }, 500);
    });

	//activate dropzone
	if($("#my-dropzone").length > 0) {
		//myDropzone = new Dropzone("#my-dropzone", {url: "test.php", autoProcessQueue:false});
	}

    $(document)
        .on('change', '.btn-file :file', function() {
            $('#file-select').attr('src', 'css/images/loading.png');
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label, input]);
    });

    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        readURL(event);
    });

    //fancybox
    $(".fancybox").fancybox({
        prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});

    $(".fancybox-button").fancybox({
		prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});

    $('.fancybox-media').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {}
		}
	});

    //for the dropdown regions box
    $( "#regionsBtn" ).bind( "click", function() {
        $('#myTab a:eq(0)').tab('show');
        $('#regionsModal').modal({
            keyboard: false
        });
        return false;
    });

    $(document).click(function(event) {
        if($(event.target).parents().index($('#regionsModal')) == -1) {
            if($('#regionsModal').is(":visible")) {
                $('#regionsModal').modal('hide');
            }
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    //the login modals
    $('#modalLogin').on('show.bs.modal', function () {
        $('#modalSignup').modal('hide');
    });

    $('#modalForgot').on('show.bs.modal', function () {
        $('#modalLogin').modal('hide');
    });

});

function readURL(event) {
    var input = event.target;
    console.log(input.files[0]);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#file-select')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
//var url = window.location.protocol+'//'+window.location.host;
var url = "http://allworldcompanies.com";
$('.search-dropdown li').click(function(){
    $('.search-dropdown-current').html($(this).text()+' <span class="caret"></span>').data('by', $(this).data('by'));
});

$('.search-btn').click(function(){
    var val = $('.search-input').val();
  //  if (val.length > 0) {
        window.location = url+'/listings/?filter=1&'+$('.search-dropdown-current').data('by')+'='+val;
   // } else {
     //   alert('Search input cannot be empty.');
    //}
})

$('.search-input').on('keyup', function(event){
    if (event.keyCode == 13) $('.search-btn').trigger('click');
});

$('.notification i').click(function(){
    $(this).parents('.notification').slideUp(200);
})

function resetForm($form) {

}

$('.reset-form').click(function(){
    var $form = $(this).parents('form');
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $form.find('.select2-search-choice').remove();
    return false;
})

$(".countries").select2({ maximumSelectionSize: 5 });
/*$(".domains").select2({ maximumSelectionSize: 10 });*/
$('.select2-container').click(function(){
    $(this).find('.select2-choices').trigger('click');
})

$('.confirm-js').click(function(){
    if (!confirm('Are you sure?')) return false;
    return true;
});

$('button[type="submit"]').click(function(){
    $(this).parents('form').submit();
});

function MultiAjaxAutoComplete(element, url) {
    $(element).select2({
        placeholder: "",
        minimumInputLength: 1,
        multiple: true,
        ajax: {
            url: url,
            data: function(term) {
                return {
                    q: term
                };
            },
            results: function(data) {
                return {
                    results: data
                };
            }
        },
        formatResult: formatResult,
        formatSelection: formatSelection,
        initSelection: function(element, callback) {
            var data = [];
            $(element.val().split(",")).each(function(i) {
                var item = this.split(':');
                data.push({
                    id: item[0]
                });
            });
            callback(data);
        }
    });
};

function formatResult(domain) {
    return '<div>' + domain.id + '</div>';
};

function formatSelection(data) {
    return data.id;
};

MultiAjaxAutoComplete('#company_domains', url+'/ajax/domains/');
 

var companyDetails = $('#company-details');
if (companyDetails.length > 0) {
    $.get(companyDetails.data('url'), function( data ) {
        data = JSON.parse(data);
        companyDetails.find('.companies-wphone-picked').html(data.companies_wphone_picked+' companies');
        companyDetails.find('.companies-wwebsite-picked').html(data.companies_wwebsite_picked+' companies');
        companyDetails.find('.companies-wfax-picked').html(data.companies_wfax_picked+' companies');
        companyDetails.find('.companies-wemail-picked').html(data.companies_wemail_picked+' companies');
        $('.loading-td').removeClass('loading-td');
    });
}
var companiesFound = $('#companies-found');
if (companiesFound.length > 0) {
    $.get(companiesFound.data('url'), function( data ) {
        companiesFound.find('span').html(data);
    });
}

$('.submit-form').click(function(){
	//$(this).hide();
	$($(this).attr('href')).submit();
   
	return false;
});

$('.buy-selected').click(function(){
    var ids = '';
    $('input[name="selected"]:checked').each(function(){
        ids += $(this).val()+',';
    });
    $('.selectedIds').val(ids.slice(0,-1));
});

$('.buy-checkboxes-toggle').click(function(){
    $(this).toggleClass('tg');
    if (!$(this).hasClass('tg')) {
        $('input[name="selected"]').prop('checked', true);
        $('.buy-checkboxes-toggle-label').text('Uncheck all');
    } else {
        $('input[name="selected"]').prop('checked', false);
        $('.buy-checkboxes-toggle-label').text('Check all');
    }
});
$('#form-recovery').submit(function(){
    if ($('input[name="password"]').val() != $('input[name="confirm_password"]').val()) {
        alert("The passwords are not the same. Please check again");
        return false;
    }
});
/*
$('.submit-form-onclick').click(function(){
    var th = $(this),
        f = $(this).attr('for'),
        t = $('#'+f);

    if (t.is(':checked')) {
        t.attr('checked', false);
    } else {
        t.attr('checked', true);
    }
    th.parents('form').submit();
});*/

var listings = $('#listings');
if (typeof (listings) != 'undefined' && listings.length > 0) {
    var last_listing = listings.find('.listing:last');

    $('.load-more').click(function(){
        var $this = $(this),
            page = $('.page-hidden:last').val(),
            pageUrl = page != 0 ? page : 0;

        $this.text('loading...');
        setTimeout(function(){
            $.ajax({
                type:'GET',
                url: '/listings/'+pageUrl+'/json',
                async: true,
                success:function (response) {
                    $('#listings').append(response);
                    $this.text('load more');

                }
            });
        }, 300);

    });
}