<?php if (!defined('BASEPATH')) exit('No direct script access allowed');									
ob_start(); session_start();									
class Common_model extends CI_Model {									
									
    /**									
     * Index Page for this controller.									
     *									
     * Maps to the following URL									
     * 		http://example.com/index.php/welcome							
     * 	- or -								
     * 		http://example.com/index.php/welcome/index							
     * 	- or -								
     * Since this controller is set as the default controller in									
     * config/routes.php, it's displayed at http://example.com/									
     *									
     * So any other public methods not prefixed with an underscore will									
     * map to /index.php/welcome/<method_name>									
     * @see http://codeigniter.com/user_guide/general/urls.html									
     */									
	 								
    public function __Construct() {									
        $this->load->helper('url');									
									
        //	error_reporting(0);								
    }									
									
    public function table_exists($tblname, $srv = 1) {									
        if ($srv == 1) {									
            if ($this->db->table_exists($tblname)) {									
                return true;									
            }									
            return false;									
        } else if ($srv == 2) {									
            $mariadbcon = $this->load->database('mariadbcon', TRUE);									
            if ($mariadbcon->table_exists($tblname)) {									
                return true;									
            }									
            return false;									
        }									
    }									
									
    public function prepare_upload($srv = 1) {									
        if ($srv == 1) {									
            $default_tbname = "companies";									
            global $User_Name;									
            $dataupload = '<input type="hidden" id="srv" name="srv" value="1"/><form method="post" name="frmupld" enctype="multipart/form-data">									
					<label for="v_upl_tbname"> Table Name </label> <input type="text" name="v_upl_tbname" id="v_upl_tbname" value="' . $default_tbname . '" /><br><br>				
					<input type="file" name="v_upl_file" ><br><br>				
									
					<!--select title="Production/Interface" id="prod_interf" name="prod_interf" >				
						<option values="Production">Production</option>			
						<option values="Interface">Interface</option>			
					</select-->				
					<br><br><br>				
					<input type="submit" name="send_file" width="50" height="10" id="send_file" VALUE="Upload File!">				
				</form>';					
            return $dataupload;									
        }									
        if ($srv == 2) {									
									
            $default_tbname = "tbl_comp_data_test";									
            global $User_Name;									
            $dataupload = '<input type="hidden" id="srv" name="srv" value="2"/><form method="post" name="frmupld" enctype="multipart/form-data">									
					<label for="v_upl_tbname"> Table Name </label> <input type="text" name="v_upl_tbname" id="v_upl_tbname" value="' . $default_tbname . '" /><br><br>				
					<input type="file" name="v_upl_file" ><br><br>				
									
					<!--select title="Production/Interface" id="prod_interf" name="prod_interf" >				
						<option values="Production">Production</option>			
						<option values="Interface">Interface</option>			
					</select-->				
					<br><br><br>				
					<input type="submit" name="send_file" width="50" height="10" id="send_file" VALUE="Upload File!">				
				</form>';					
            return $dataupload;									
        }									
    }									
									
    function get_colummns_db($v_table, $srv = 1) {									
        $coldb = array();									
									
        if ($srv == 1) {									
            $fields = $this->db->list_fields($v_table);									
            $x = 0;									
            if (count($fields) > 0) {									
                foreach ($fields as $field) {									
                    $coldb[$x] = $field;									
                    $x++;									
                }									
            } else {									
                return false;									
            }									
            return $coldb;									
        }									
									
        if ($srv == 2) {									
            $mariadbcon = $this->load->database('mariadbcon', TRUE);									
            $fields = $mariadbcon->list_fields($v_table);									
            $x = 0;									
            if (count($fields) > 0) {									
                foreach ($fields as $field) {									
                    $coldb[$x] = $field;									
                    $x++;									
                }									
            } else {									
                return false;									
            }									
            return $coldb;									
        }									
    }									
									
    function scrie_log($somecontent) {									
        $filename = 'test_log.txt';									
        if (!$handle = fopen($filename, 'a')) {									
            echo "Cannot open file ($filename)";									
            exit;									
        }									
        // Write $somecontent to our opened file.									
        if (fwrite($handle, $somecontent . "\n") === FALSE) {									
            echo "Cannot write to file ($filename)";									
            exit;									
        }									
        fclose($handle);									
    }									
									
    function get_table($v_line, $uploadfile, $columndb, $tblname, $srv = 1) {									
        global $columndb;									
        global $objPHPExcel;									
									
        /*									
          $query = $this->db->query("SHOW VARIABLES LIKE '%timeout%'");									
          if($query->num_rows() >0){									
          foreach ($query->result() as $row)	{								
          // echo  $row->Variable_name.':'.$row->Value."<br>\n";									
          }									
          }									
          $query = $this->db->query("SHOW VARIABLES LIKE '%max%'");									
          if($query->num_rows() >0){									
          foreach ($query->result() as $row)	{								
          //  echo  $row->Variable_name.':'.$row->Value."<br>\n";									
          }									
          }									
          $query = $this->db->query("SHOW VARIABLES LIKE '%memor%'");									
          if($query->num_rows() >0){									
          foreach ($query->result() as $row)	{								
          //  echo  $row->Variable_name.':'.$row->Value."<br>\n";									
          }									
          }									
         */									
        /*   echo "<br><br>";									
          $query = $this->db->query("SET SESSION wait_timeout=1800");									
          $query = $this->db->query("SET SESSION interactive_timeout=1800");									
          //$query = $this->db->query("SET GLOBAL max_allowed_packet=128000000");									
          $query = $this->db->query("SHOW VARIABLES LIKE '%timeout%'");									
          if($query->num_rows() >0){									
          foreach ($query->result() as $row)	{								
          //  echo  $row->Variable_name.':'.$row->Value."<br>\n";									
          }									
          }									
          $query = $this->db->query("SHOW VARIABLES LIKE '%max%'");									
          if($query->num_rows() >0){									
          foreach ($query->result() as $row)	{								
          //  echo  $row->Variable_name.':'.$row->Value."<br>\n";									
          }									
          }									
          $query = $this->db->query("SHOW VARIABLES LIKE '%memor%'");									
          if($query->num_rows() >0){									
          foreach ($query->result() as $row)	{								
          //  echo  $row->Variable_name.':'.$row->Value."<br>\n";									
          }									
          }									
         */									
        if (substr($uploadfile, -3) == "xls") {									
            $objReader = PHPExcel_IOFactory::createReader('Excel5');									
        } elseif (substr($uploadfile, -4) == "xlsx") {									
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');									
        } elseif (substr($uploadfile, -3) == "csv") {									
            $objReader = PHPExcel_IOFactory::createReader('CSV');									
        }									
        $objPHPExcel = $objReader->load("$uploadfile");									
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {									
            echo 'Worksheet - ', $worksheet->getTitle(), EOL;									
            echo "<input type='hidden' id='srv' name='srv' value='" . $srv . "'/><table  border=\"5\"  bordercolor=\"#E8B900\" >";									
            $v_row = 0;									
            foreach ($worksheet->getRowIterator() as $row) {									
                $v_row = $v_row + 1;									
                echo "<tr border=\"3\"  bordercolor=\"#E8B900\">";									
                $v_col = 0;									
                $cellIterator = $row->getCellIterator();									
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set									
                foreach ($cellIterator as $cell) {									
                    $v_col++;									
                    if (!is_null($cell)) {									
                        if ($v_row == 1) {									
                            echo "<th>";									
                            /* echo '        Cell - ' , $cell->getCoordinate() , ' - ' , $cell->getCalculatedValue() , EOL; */									
                            echo '<table><tr><td style="color:red">';									
                            echo "DB :: ";									
                            echo '</td><td>';									
                            $this->create_combo('Combo_' . $v_col, $columndb, $v_col);									
                            echo '</td></tr></table>';									
                            echo '<table><tr><td style="color:red">';									
                            echo 'Col Imp. File :: </td><td>';									
                            echo $cell->getCalculatedValue(), EOL;									
                            echo '</td></tr></table>';									
                            echo '<table><tr><td style="color:red">';									
                            echo 'Load Column :: </td><td>';									
                            echo '<select									
									 title="choose col for import" id="Loadcol_' . $v_col . '"
									 name="Loadcol_' . $v_col . '" >
									<option values="Yes">Yes</option>
									<option values="No" selected="selected">No</option>
									</select>';
                            echo '</td></tr></table>';									
                            echo '<table><tr><td style="color:red">';									
                            echo 'Ord. Concat :: </td><td style="color:teal">';									
                            echo '<select									
									 title="choose order to concat" id="Ord_conc_' . $v_col . '"
									 name="Ord_conc_' . $v_col . '" >
									<option>1</option>
									<option>2</option>
									<option>3</option>
									</select>';
                            echo '</td></tr></table>';									
                            echo '<div id="div_free_col_' . $v_col . '" style="display:none">';									
                            echo '<table><tr><td style="color:red">';									
                            echo 'Create New Col DB :: </td><td style="color:teal">';									
                            echo '<input type="text" name="txt_' . $v_col . '" id="txt_' . $v_col . '" value="" >';									
                            echo '</td></tr></table>';									
                            echo '</div>';									
                            echo "</th>";									
                        } else {									
                            echo "<td>";									
                            echo $cell->getCalculatedValue(), EOL;									
                            echo "</td>";									
                        }									
                    }									
                }									
                echo "</tr>";									
                if ($v_row == 1) {									
                    $_SESSION['v_col'] = $v_col;									
                    echo '<input name="v_nr_col" id="v_nr_col" type="hidden" value="' . $v_col . '">';									
                }									
                if ($v_row == $v_line) {									
                    break;									
                }									
            }									
            if ($srv == 1) {									
                $ajaxurl = site_url('import');									
            } else {									
                $ajaxurl = site_url('import_maria');									
            }									
            echo '<input name="ajaxurl" id="ajaxurl" type="hidden" value="' . $ajaxurl . '">';									
            echo '<input type="hidden" name="tblname" id="tblname" value="' . $tblname . '">';									
            echo "</table>";									
            break;									
        }									
    }									
									
    function create_combo($v_combo_name, $columndb, $v_col) {									
        $vnrcol = count($columndb);									
        echo '<select name="' . $v_combo_name . '" id="' . $v_combo_name . '" onchange="check_none(\'' . $v_combo_name . '\',\'txt_' . $v_col . '\',\'div_free_col_' . $v_col . '\')">';									
        for ($i = 0; $i < $vnrcol; $i++) {									
            echo '<option value="' . $columndb[$i] . '">' . $columndb[$i] . '</option>';									
        }									
        echo '<option>none</option>';									
        echo '</select>';									
    }									
									
    function extract_key($v_choose, $v_map0, $v_map1) {									
        $vl_map0 = explode(";;;", $v_map0);									
        $vl_map1 = explode(";;;", $v_map1);									
        $v_val_col = "";									
        switch ($v_choose) {									
            case 1 : $v_field = "reg_unique_code";									
                break;									
            case 2 : $v_field = "company";									
                break;									
            case 3 : $v_field = "country";									
                break;									
        }									
        $vlung = count($vl_map0);									
        //echo "length of vl_map0 = ".$vlung , EOL;									
        for ($i = 0; $i < $vlung; $i++) {									
            if ($vl_map0[$i] == $v_field) {									
                $v_val_col = $vl_map1[$i];									
            }									
        }									
        return $v_val_col;									
    }									
									
    function get_diff($a, $b) {									
        $map = $out = array();									
        foreach ($a as $val)									
            $map[$val] = 1;									
        foreach ($b as $val)									
            unset($map[$val]);									
        return array_keys($map);									
    }									
									
    function read_for_import($v_map0, $v_map1, $srv = 1) {									
        $mariadbcon = $this->load->database('mariadbcon', TRUE);									
        global $columndb;									
        global $go_prod;									
        GLOBAL $local_set;									
        global $connection;									
        global $tblname;									
        $uploadfile = $_SESSION['uploadfile'];									
        if (substr($uploadfile, -3) == "xls") {									
            $this->scrie_log("the file is xls");									
            $objReader = PHPExcel_IOFactory::createReader('Excel5');									
        } elseif (substr($uploadfile, -4) == "xlsx") {									
            $this->scrie_log("the file is xlsx");									
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');									
        } elseif (substr($uploadfile, -3) == "csv") {									
            $this->scrie_log("the file is csv");									
            $objReader = PHPExcel_IOFactory::createReader('CSV');									
        }									
        $this->scrie_log("inainte de objReader-->load(uploadfile)");									
        $objPHPExcel = $objReader->load("$uploadfile");									
        $this->scrie_log("dupa de objReader-->load(uploadfile)");									
        $v_reg_uniq = $this->extract_key(1, $v_map0, $v_map1); //extract reg unique_code (p=1) notice that still have ||||| inside									
        $v_customer = $this->extract_key(2, $v_map0, $v_map1); //extract customer (p=2), country(p=3)									
        $v_country = $this->extract_key(3, $v_map0, $v_map1); //extract country(p=3)									
        if (substr($v_map0, -3) == ";;;") {									
            $v_map0 = substr($v_map0, 0, -3);									
        }									
        if (substr($v_map1, -3) == ";;;") {									
            $v_map1 = substr($v_map1, 0, -3);									
        }									
        $v_remap_update0 = explode(";;;", $v_map0);									
        $v_remap_update1 = explode(";;;", $v_map1);									
        $v_new_cols = $this->get_diff($v_remap_update0, $columndb);									
        $v_new_cols_len = sizeof($v_new_cols);									
        $go_prod = 1;									
        //preparation for adding new collumns									
        for ($k = 0; $k < $v_new_cols_len; $k++) {									
            if ($v_new_cols[$k] <> "") {									
                if ($go_prod == 1) {									
                    $query = "ALTER TABLE " . $tblname . " ADD " . $v_new_cols[$k] . " VARCHAR( 150 ) NULL DEFAULT NULL";									
                } else {									
                    $query = "ALTER TABLE int_in_customer ADD " . $v_new_cols[$k] . " VARCHAR( 150 ) NULL DEFAULT NULL";									
                }									
                echo ' query = ' . $query, EOL;									
									
                if ($srv == 1) {									
                    $query = $this->db->query($query);									
                    $query = $this->db->query("COMMIT");									
                } else if ($srv == 2) {									
                    $query = $mariadbcon->query($query);									
                    $query = $mariadbcon->query("COMMIT");									
                }									
									
                echo 'the collumn ' . $v_new_cols[$k] . ' has been created', EOL;									
            }									
        }									
        if ($srv == 1) {									
            $query = $this->db->query("START TRANSACTION");									
        } else if ($srv == 2) {									
            $query = $mariadbcon->query("START TRANSACTION");									
        }									
									
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {									
            echo 'Worksheet - ', $worksheet->getTitle(), EOL;									
            $v_row = 0;									
            foreach ($worksheet->getRowIterator() as $row) {									
                $v_row = $v_row + 1;									
                $v_col = 0;									
                $cellIterator = $row->getCellIterator();									
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set									
                $v_row_read = Array($_SESSION['v_col']);									
                foreach ($cellIterator as $cell) { //read colls for a certain row									
                    $v_index_read_col = $v_col;									
                    $v_col++;									
                    $v_row_read[$v_index_read_col] = "";									
                    if (!is_null($cell)) {									
                        if ($v_row > 1) {									
                            $v_row_read[$v_index_read_col] = ($cell->getCalculatedValue());									
                            $v_row_read[$v_index_read_col] = mysql_real_escape_string($v_row_read[$v_index_read_col]);									
                        }									
                    }									
                    if ($v_col == $_SESSION['v_col']) {									
                        break;									
                    }									
                } // end foreach ($cellIterator as $cell)									
                //echo '    after alter table area- ' ,  EOL;									
                if ($v_row > 1) {									
                    if ($go_prod == 1) {									
                        //prepare for insert if no update									
                        $query = "INSERT INTO " . $tblname . " ( ";									
                        $query0 = "";									
                        $query1 = "";									
                        for ($i = 0; $i < sizeof($v_remap_update0); $i++) {									
                            $v_remap_update1[$i] = str_replace("||", "", $v_remap_update1[$i]);									
                            $v_val_comb = explode("|", $v_remap_update1[$i]);									
                            $v_ru1 = "";									
                            for ($j = 0; $j < sizeof($v_val_comb); $j++) {									
                                $buf = str_replace("COL_", "", $v_val_comb[$j]);									
                                if ($buf != '') {									
                                    $buf = 1 * $buf - 1;									
                                    $v_ru1 = $v_ru1 . " " . $v_row_read[$buf];									
                                }									
                            }									
                            $v_ru1 = substr($v_ru1, 1);									
                            $v_ru1 = trim($v_ru1);									
                            if (!empty($v_ru1) && !empty($v_remap_update0[$i])) {									
                                $query0 = $query0 . $v_remap_update0[$i] . ", "; //used for colls									
                                $query1 = $query1 . "'" . $v_ru1 . "', "; // used for values of cols									
                            }									
                        }									
                        $query0 = substr($query0, 0, -2);									
                        $query1 = substr($query1, 0, -2);									
                        if (!empty($query0) && !empty($query1)) {									
                            $query = $query . $query0 . ") VALUES ( " . $query1 . ")";									
                            $this->scrie_log($query);									
                            if ($srv == 1) {									
                                $query = $this->db->query($query);									
                            } else if ($srv == 2) {									
                                $query = $mariadbcon->query($query);									
                            }									
                            if ($v_row % 1000 == 0) {									
									
                                if ($srv == 1) {									
                                    $query = $this->db->query("COMMIT");									
                                    $query = $this->db->query("START TRANSACTION");									
                                } else if ($srv == 2) {									
									
                                    $query = $mariadbcon->query("COMMIT");									
                                    $query = $mariadbcon->query("START TRANSACTION");									
                                }									
                            }									
									
									
                            if ($srv == 1) {									
									
                                if ($this->db->_error_number() == 0) {									
                                    if ($this->db->affected_rows() <> 0 && 1 == 2) {									
                                        echo $this->db->affected_rows() . " row/rows has/have been INSERTED at step " . $v_row, EOL;									
                                    }									
                                } else {									
                                    echo $this->db->_error_number() . ": " . $this->db->_error_message() . " --lin282--has been encountered for while INSERT row " . $v_row, EOL;									
                                    $this->scrie_log(" --lin282--has been encountered for while INSERT row " . $v_row);									
                                }									
                            } else if ($srv == 2) {									
									
                                if ($mariadbcon->_error_number() == 0) {									
                                    if ($mariadbcon->affected_rows() <> 0 && 1 == 2) {									
                                        echo $mariadbcon->affected_rows() . " row/rows has/have been INSERTED at step " . $v_row, EOL;									
                                    }									
                                } else {									
                                    echo $mariadbcon->_error_number() . ": " . $mariadbcon->_error_message() . " --lin282--has been encountered for while INSERT row " . $v_row, EOL;									
                                    $this->scrie_log(" --lin282--has been encountered for while INSERT row " . $v_row);									
                                }									
                            }									
                        }									
                        $v_row = $v_row + 1;									
                        //END prepare for insert if no update									
                    } //end just insert in final table									
                    else { // start old logique with update and if not exist with insert. information is inserted in interface table									
                        if ($srv == 1) {									
                            $this->db = $this->db;									
                        } else if ($srv == 2) {									
                            $this->db = $mariadbcon;									
                        }									
									
									
                        $query = "UPDATE int_in_customer SET ";									
                        for ($i = 0; $i < sizeof($v_remap_update0); $i++) { //prepare for update									
                            if (array_key_exists($i, $v_remap_update0)) {									
                                $v_remap_update1[$i] = str_replace("||", "", $v_remap_update1[$i]);									
                                if (substr($v_remap_update1[$i], -1) == "|") {									
                                    $v_remap_update1[$i] = substr($v_remap_update1[$i], 0, -1);									
                                }									
                                $v_val_comb = explode("|", $v_remap_update1[$i]); //UNSPLIT COLLUMN CONCATENATED									
                                $v_ru1 = "";									
                                for ($j = 0; $j < sizeof($v_val_comb); $j++) { //try to make concatenation if is the case									
                                    if (array_key_exists($j, $v_val_comb)) {									
                                        $buf = str_replace("COL_", "", $v_val_comb[$j]);									
                                        $buf = 1 * $buf - 1;									
                                        $v_ru1 = $v_ru1 . " " . $v_row_read[$buf];									
                                        $v_ru1 = trim($v_ru1);									
                                    }									
                                }									
                                if ($v_ru1 == "") {									
                                    $v_ru1 = "ERROR";									
                                }									
                                $query = $query . $v_remap_update0[$i] . " = '" . $v_ru1 . "', ";									
                            } //IF array_key_exists($i, $v_remap_update0)									
                        } //END prepare for update									
                        $query = substr($query, 0, -2); //remove last caracter comma									
                        $query = $query . " WHERE ";									
                        $buf = str_replace("COL_", "", $v_reg_uniq);									
                        $buf = 1 * $buf - 1;									
                        $query = $query . "reg_unique_code = '" . $v_row_read[$buf] . "' AND ";									
                        $chk_null_ruc = 0;									
                        if (trim($v_row_read[$buf]) == "") {									
                            $chk_null_ruc = 1;									
                        }									
                        $buf = str_replace("COL_", "", $v_country);									
                        $buf = 1 * $buf - 1;									
                        $query = $query . "country = '" . $v_row_read[$buf] . "'";									
                        $chk_null_country = 0;									
                        if (trim($v_row_read[$buf]) == "") {									
                            $chk_null_country = 1;									
                        }									
                        if ($chk_null_ruc == 0 && $chk_null_country == 0) {									
                            $result = $this->db->query($query);									
                            if ($this->db->_error_number() == 0) { // TEST if error while UPDATE									
                                if ($this->db->affected_rows() == 0) { //prepare for insert if no update									
                                    $query = "INSERT INTO int_in_customer ( ";									
                                    $query0 = "";									
                                    $query1 = "";									
                                    for ($i = 0; $i < sizeof($v_remap_update0); $i++) {									
                                        $v_remap_update1[$i] = str_replace("||", "", $v_remap_update1[$i]);									
                                        $v_val_comb = explode("|", $v_remap_update1[$i]);									
                                        $v_ru1 = "";									
                                        for ($j = 0; $j < sizeof($v_val_comb); $j++) {									
                                            $buf = str_replace("COL_", "", $v_val_comb[$j]);									
                                            $buf = 1 * $buf - 1;									
                                            $v_ru1 = $v_ru1 . " " . $v_row_read[$buf];									
                                        }									
                                        $v_ru1 = substr($v_ru1, 1);									
                                        $v_ru1 = trim($v_ru1);									
                                        $query0 = $query0 . $v_remap_update0[$i] . ", "; //used for colls									
                                        $query1 = $query1 . "'" . $v_ru1 . "', "; // used for values of cols									
                                    }									
                                    $query0 = substr($query0, 0, -2);									
                                    $query1 = substr($query1, 0, -2);									
                                    $query = $query . $query0 . ") VALUES ( " . $query1 . ")";									
                                    echo '    query for 382 --insert- ', $query, EOL;									
                                    $result = $this->db->query($query);									
                                    if ($this->db->_error_number() == 0) {									
                                        if ($this->db->affected_rows() <> 0) {									
                                            echo $this->db->affected_rows() . " row/rows has/have been INSERTED at step " . $v_row, EOL;									
                                        }									
                                    } else {									
                                        echo $this->db->_error_number() . ": " . $this->db->_error_message() . " --393 -- has been encountered for while INSERT row " . $v_row, EOL;									
                                    }									
                                } //END prepare for insert if no update									
                                else {									
                                    echo $this->db->affected_rows() . " row/rows has/have been UPDATED at step " . $v_row, EOL;									
                                }									
                            } //END TEST if error while UPDATELS									
                            else {									
                                echo $this->db->_error_number() . ": " . $this->db->_error_message() . " has been encountered for while UPDATE row " . $v_row, EOL;									
                            }									
                        } else { //if ($chk_null_ruc == 0 && $chk_null_country == 0) //ERROR if									
                            $errmsg = "";									
                            if ($chk_null_country == 1) {									
                                $errmsg = ", Country is null";									
                            }									
                            if ($chk_null_ruc == 1) {									
                                $errmsg = $errmsg . ", Reg_unique_code is null";									
                            }									
                            $errmsg = "For row " . $v_row . " " . substr($errmsg, 2);									
                            echo $errmsg, EOL;									
                        }									
                    } // end old logic regard inserting in interface table									
                }									
            }// foreach ($worksheet->getRowIterator() as $row)									
            break;									
        } //foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)									
        $result = $this->db->query("COMMIT");									
        echo "Import Finish!";									
        echo "<br>";									
        echo "Please check if you have error";									
    }									
									
    public function logged_in() {									
        return isset($_SESSION['users_id']);									
    }									
									
    public function confirm_logged_in() {									
        if (!logged_in()) {									
            redirect_to("login.php");									
        }									
    }									
									
    public function mysql_prep($value) {									
        $magic_quotes_active = get_magic_quotes_gpc();									
        $new_enough_php = function_exists("mysql_real_escape_string");									
        if ($new_enough_php) {									
            if ($magic_quotes_active) {									
                $value = stripslashes($value);									
            }									
            $value = mysql_real_escape_string($value);									
        } else {									
            if (!$magic_quotes_active) {									
                $value = addslashes($value);									
            }									
        }									
        return $value;									
    }									
									
    public function confirm_query($result_set) {									
        if (!$result_set) {									
            die("Database query failed: " . mysql_error());									
        }									
    }									
									
    public function active() {									
        if (isset($_SESSION['users_active']))									
            if (($_SESSION['users_active']) == "active") {									
                return true;									
            } else {									
                return false;									
            }									
    }									
									
    public function admin() {									
        if (isset($_SESSION['users_role']))									
            if (($_SESSION['users_role']) == "admin") {									
                return true;									
            } else {									
                return false;									
            }									
    }									
									
    public function user() {									
        if (isset($_SESSION['users_role']))									
            if (($_SESSION['users_role']) == "user") {									
                return true;									
            } else {									
                return false;									
            }									
    }									
									
    public function getTierOne() {									
        $result = mysql_query("SELECT country FROM companies GROUP BY country ORDER BY country")									
                or die(mysql_error());									
        if (isset($_POST['drop_1'])) {									
            echo "<option value='" . $_POST['drop_1'] . "' selected>" . $_POST['drop_1'] . "</option>";									
        } else {									
            echo '<option value="">-Country-</option>';									
        }									
        while ($tier = mysql_fetch_array($result)) {									
            echo '<option value="' . $tier['country'] . '">' . $tier['country'] . '</option>';									
        }									
    }									
									
    public function drop_1($drop_var) {									
        include_once('connection.php');									
        $result = mysql_query("SELECT city FROM companies WHERE country='$drop_var' GROUP BY city ORDER BY city")									
                or die(mysql_error());									
        echo '<select name="tier_two" id="tier_two" class="inp" style="width: 211px;">';									
        if ($_POST['tier_two'] <> '') {									
            echo "<option value='" . $_POST['tier_two'] . "' selected>" . $_POST['tier_two'] . "</option>";									
        } else {									
            echo '<option value="" selected>-City-</option>';									
        }									
        while ($drop_2 = mysql_fetch_array($result)) {									
            echo '<option value="' . $drop_2['city'] . '">' . $drop_2['city'] . '</option>';									
        }									
        echo '</select> ';									
    }									
									
}									
									
/* End of file comman model.php */									
/* Location: ./application/models/common.php */	 								
