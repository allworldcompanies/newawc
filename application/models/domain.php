<?php

class Domain extends MY_Model {

    const DB_TABLE = 'new_domains';
    const DB_TABLE_PK = 'domain_id';

    public $domain_id;
    public $domain_name;
    public $domain_date_added;
}