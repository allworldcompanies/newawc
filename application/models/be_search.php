<?php

class Be_search extends MY_Model {

    const DB_TABLE = 'new_be_searches';
    const DB_TABLE_PK = 'search_id';

    public $search_id;
    public $search_user_id;
    public $search_array;
    public $search_date_added;
}
