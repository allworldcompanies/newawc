<?php

class Available_companies extends MY_Model {

    const DB_TABLE = 'new_available_companies';
    const DB_TABLE_PK = 'available_companies_id';

    public $available_companies_id;
    public $available_companies_paid;
    public $available_companies_user_id;
    public $available_companies_number;
    public $available_companies_type;
    public $available_companies_date_added;

    public function getByUserId($id) {
        $result = $this->db->query('SELECT * FROM '.$this::DB_TABLE.' WHERE `available_companies_user_id` = '.$id)->row();
        return $result;
    }
}
