<?php

class New_parser extends MY_Model {

    const DB_TABLE = 'new_parser';
    const DB_TABLE_PK = 'parser_id';

    public $parser_id;
    public $parser_link;
    public $parser_done;
    public $parser_type;
}
