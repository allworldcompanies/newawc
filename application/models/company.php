<?php

class Company extends MY_Model {

    const DB_TABLE = 'new_companies';
    const DB_TABLE_PK = 'company_id';

    public $company_id;
    public $company_url;
    public $company_name;
    public $company_cui;
    public $company_inchiriere;
    public $company_contact;
    public $company_position;
    public $company_domain;
    public $company_subdomain;
    public $company_setup_date;
    public $company_address;
    public $company_postal_code;
    public $company_country_id;
    public $company_district;
    public $company_city;
    public $company_phone;
    public $company_website;
    public $company_capital_invested;
    public $company_fax;
    public $company_email;
    public $company_employee;
    public $company_contacts;
    public $company_turnover;
    public $company_2009;
    public $company_2010;
    public $company_2011;
    public $company_2012;
    public $company_date_added;

    public function filter($obj = array(), $limit = null, $perpage = null, $type = 'object', $order = ' ') {
        $addToSql = '';
        $addToSql .=!empty($obj['company_name']) ? ' AND `company_name` LIKE "%' . $obj['company_name'] . '%"' : '';

        if (!empty($obj['company_country_id']) && is_array($obj['company_country_id'])) {
            $fic = '';
            foreach ($obj["company_country_id"] as $ic)
                if (!empty($ic))
                    $fic .= intval($ic) . ',';
            if (!empty($fic))
                $addToSql .= ' AND `company_country_id` IN (' . trim($fic, ',') . ')';
        }

        $obj['company_domain'] = !empty($obj['company_domain']) ? explode(',', $obj['company_domain']) : '';
        if (!empty($obj['company_domain']) && is_array($obj['company_domain'])) {
            $addToSql .= ' AND ( ';
            foreach ($obj["company_domain"] as $do) {
                if (!empty($do)) {
                    $addToSql .= ' `company_domain` LIKE "%' . trim($do) . '%" ';
                    if ($do != end($obj["company_domain"]))
                        $addToSql .= ' OR ';
                }
            }
            $addToSql .= ' ) ';
        }

        //  $addToSql   .= !empty($obj['company_name']) ? ' AND `company_name` LIKE "%'.$obj['company_name'].'%"' : '';
        //    $addToSql   .= !empty($obj['company_domain']) ? ' AND `company_domain` LIKE "%'.$obj['company_domain'].'%"' : '';
        //    $addToSql   .= !empty($obj['company_subdomain']) ? ' AND `company_name` LIKE "%'.$obj['company_name'].'%"' : '';
        $addToSql .=!empty($obj['company_setup_date']) ? ' AND `company_setup_date` LIKE "%' . $obj['company_setup_date'] . '%"' : '';
        $addToSql .=!empty($obj['company_contact']) ? ' AND `company_contact` LIKE "%' . $obj['company_contact'] . '%"' : '';
        $addToSql .=!empty($obj['company_address']) ? ' AND `company_address` LIKE "%' . $obj['company_address'] . '%"' : '';
        $addToSql .=!empty($obj['company_phone']) ? ' AND `company_phone` LIKE "%' . $obj['company_phone'] . '%"' : '';
        $addToSql .=!empty($obj['company_website']) ? ' AND `company_website` LIKE "%' . $obj['company_website'] . '%"' : '';
        $addToSql .=!empty($obj['company_fax']) ? ' AND `company_fax` LIKE "%' . $obj['company_fax'] . '%"' : '';
        $addToSql .=!empty($obj['company_email']) ? ' AND `company_email` LIKE "%' . $obj['company_email'] . '%"' : '';

        $addToSql .=!empty($obj['with_email']) ? ' AND `company_email` IS NOT NULL AND LENGTH(`company_email`) > 2 ' : '';
        $addToSql .=!empty($obj['with_website']) ? ' AND `company_website` IS NOT NULL AND LENGTH(`company_website`) > 2 ' : '';
        $addToSql .=!empty($obj['with_phone']) ? ' AND `company_phone` IS NOT NULL AND LENGTH(`company_phone`) > 2 ' : '';
        $addToSql .= $order;

        $limitPerPage = (isset($limit) && isset($perpage)) ? ' LIMIT ' . $limit . ',' . $perpage : '';
        //$limitPerPage   = $type != 'count' ? ' LIMIT 25 ' : $limitPerPage;



        $mariadbcon = $this->load->database('mariadbcon', TRUE);
        switch ($type) {
            case 'count':
                $result = $this->db->query('SELECT COUNT(*) as n FROM ' . $this::DB_TABLE . ' WHERE `company_id` > 0 ' . $addToSql . $limitPerPage)->result();
                $result1 = $mariadbcon->query('SELECT count(*) as m FROM tbl_comp_data WHERE `company_id` > 0 ' . $addToSql . $limitPerPage)->result();

                return current($result)->n + current($result1)->m;
                break;

            case 'ids':

                $result = $this->db->query('SELECT `company_id`, `company_name` FROM `' . $this::DB_TABLE . '` WHERE `company_id` > 0 ' . $addToSql . $limitPerPage);
                return $result->result();
                break;

            case 'details':

                $result = $this->db->query('SELECT `company_id` FROM ' . $this::DB_TABLE . ' WHERE `company_email` IS NOT NULL && LENGTH(`company_email`) > 1 ' . $addToSql . $limitPerPage);
                $return['companies_wemail_picked'] = nice_number($result->num_rows());

                $result = $this->db->query('SELECT `company_id` FROM ' . $this::DB_TABLE . ' WHERE `company_website` IS NOT NULL && LENGTH(`company_website`) > 1 ' . $addToSql . $limitPerPage);
                $return['companies_wwebsite_picked'] = nice_number($result->num_rows());

                $result = $this->db->query('SELECT `company_id` FROM ' . $this::DB_TABLE . ' WHERE `company_phone` IS NOT NULL && LENGTH(`company_phone`) > 1 ' . $addToSql . $limitPerPage);
                $return['companies_wphone_picked'] = nice_number($result->num_rows());

                $result = $this->db->query('SELECT `company_id` FROM ' . $this::DB_TABLE . ' WHERE `company_fax` IS NOT NULL && LENGTH(`company_fax`) > 1 ' . $addToSql . $limitPerPage);
                $return['companies_wfax_picked'] = nice_number($result->num_rows());

                return $return;
                break;

            case 'array':
                $result = $this->db->query('SELECT * FROM ' . $this::DB_TABLE . ' WHERE `company_id` > 0 ' . $addToSql . $limitPerPage);

                if (count($result->result()) > 0) {
                    return $result->result_array();
                } else {
                    $result1 = $mariadbcon->query('SELECT * FROM tbl_comp_data WHERE `company_id` > 0 ' . $addToSql . $limitPerPage);
                    return $result1->result_array();
                }

                break;

            default:
                $result = $this->db->query('SELECT * FROM ' . $this::DB_TABLE . ' WHERE `company_id` > 0 ' . $addToSql . $limitPerPage);

                if (count($result->result()) > 0) {
                    return $result->result();
                } else {

                    $result1 = $mariadbcon->query('SELECT * FROM tbl_comp_data WHERE `company_id` > 0 ' . $addToSql . $limitPerPage);
                    return $result1->result();
                }

                break;
        }
    }

    public function getRelatedCompaniesByCompanyId($id) {
        $company = $this->load($id);

        if (count($company) <= 0) {
            $mariadbcon = $this->load->database('mariadbcon', TRUE);
            $company = $mariadbcon->query('select * from tbl_comp_data where company_id="' . $id . '"')->result();
            $company = (object) $company[0];
        }


        $companyDomains = explode(',', $company->company_domain);
        $addSql = ' AND ( ';
        foreach ($companyDomains as $cd) {
            $addSql .= '`company_domain` LIKE "%' . trim($cd) . '%" ';
            if ($cd != end($companyDomains))
                $addSql .= ' OR ';
        }
        $addSql .= ' ) ';

        if (!empty($company->company_country_id)) {
            $result = $this->db->query('SELECT * FROM ' . $this::DB_TABLE . ' WHERE `company_country_id` = ' . $company->company_country_id . $addSql . ' AND `company_id` <> ' . $id . ' LIMIT 10')->result();
            return $result;
        } else {
if(empty($company->company_country_id)){
     return false;
     exit;
}
            $result = $mariadbcon->query('SELECT * FROM tbl_comp_data WHERE `company_country_id` = ' . $company->company_country_id . $addSql . ' AND `company_id` <> ' . $id . ' LIMIT 10')->result();

            if (count($result) > 0) {
                return $result;
            } else {
                return false;
            }
        }
    }

}
