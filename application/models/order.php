<?php

class Order extends MY_Model {

    const DB_TABLE = 'new_orders';
    const DB_TABLE_PK = 'order_id';

    public $order_id;
    public $order_type;
    public $order_price_company;
    public $order_price_total;
    public $order_user_id;
    public $order_company_ids;
    public $order_subject;
    public $order_content;
    public $order_email;
    public $order_date;

    public function getTodayIncome()
    {
        $return = $this->db->query('SELECT SUM(`order_price_total`) as `price` FROM '.$this::DB_TABLE.' WHERE `order_type` = 1 AND DATE(`order_date`) = DATE(NOW())');
        return $return->row();
    }

    public function getTotalIncome()
    {
        $return = $this->db->query('SELECT SUM(`order_price_total`) as `price` FROM '.$this::DB_TABLE.' WHERE `order_type` = 1');
        return $return->row();
    }

    public function getByUserId($id = null, $orderType = false)
    {
        $add = $orderType ? ' AND `order_type` = '.$orderType : '' ;
        $return = $this->db->query('SELECT * FROM '.$this::DB_TABLE.' WHERE `order_type` != 0 '.$add.' AND `order_user_id` = '.$id);
        return $return->result();
    }
}
