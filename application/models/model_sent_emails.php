<?php
Class Model_sent_emails extends MY_Model
{
    var $id                 = '';
    var $user_email         = '';
    var $subject            = '';
    var $content            = '';
    var $emails             = '';
    var $status             = '';

    function __construct()
    {
        parent::__construct();
    }
}