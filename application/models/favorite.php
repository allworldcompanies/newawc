<?php

class Favorite extends MY_Model {

    const DB_TABLE = 'new_favorites';
    const DB_TABLE_PK = 'favorite_id';

    public $favorite_id;
    public $favorite_user_id;
    public $favorite_company_id;
    public $favorite_date_added;
}
