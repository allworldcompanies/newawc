<?php

class Default_info extends MY_Model {

    const DB_TABLE = 'new_default_info';
    const DB_TABLE_PK = 'default_info_id';

    public $default_info_id;
    public $default_info_name;
    public $default_info_array;
    public $default_info_date_added;

    public function getCountriesWithCompanies(){
        $inCids = $countries =  array();
        $row = $this->db->query('SELECT `default_info_array` FROM '.$this::DB_TABLE.' WHERE `default_info_name` = "countries_with_companies"')->row();

        if (!empty($row)) {
            return json_decode($row->default_info_array);
        } else {
            return '';
        }
    }
	
	public  function Email($email,$message,$subject,$user_email)
	  { 
	     $config['mailtype'] = 'html';
		 $this->email->initialize($config);
         //$this->email->from('info@studentsofferingsupport.ca', '');
         $this->email->from($user_email);
		 $this->email->to($email); 
         

         $this->email->subject($subject);
		
		 $this->email->message($message);	

		 $this->email->send();

		
	 }    
	 
	 public  function NewsletterEmail($email,$message,$subject)
	  { 
	     $config['mailtype'] = 'html';
		 $this->email->initialize($config);
         //$this->email->from('info@studentsofferingsupport.ca', '');
         $this->email->from('paul_costas@yahoo.com');
		 $this->email->to($email); 
         

         $this->email->subject($subject);
		
		 $this->email->message($message);	

		 $this->email->send();

		
	 }    

	public  function updatedata($table='',$where ='',$data = '')
	   {   
	        // print_r($data);
			 //print_r($where);die(); 
		  	 if($table == '')
			 {
            	return 'no';
			 }
			 if( $where != '')
			 {
			 $this->db->where($where);
			 }else
			 {
				return 'no'; 
			}		     
			if($data != '' )
			{
				$update = $this->db->update($table,$data); //echo 'dheeraj';die;
			}else
			{
			 return  'no';
			}
        	return $update;	   
	}
	  
	
}
