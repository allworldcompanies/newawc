<div class="jumbotron home-search" style="">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <br/>
                <p class="main_description">Search thousands of companies all in one place</p>
                <br/><br/>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text-center">
                        <div class="row">
                            <div class="col-sm-12 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon input-group-addon-text">Find the company</span>
                                    <input type="text" class="form-control col-sm-3 search-input" placeholder="e.g. Company Name" >
                                    <div class="input-group-addon hidden-xs" style="display:none">
                                        <div class="btn-group">
                                            <button class="btn dropdown-toggle search-dropdown-current" data-toggle="dropdown" data-by="company_name">
                                                Company Name
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center">
                        <span class="btn btn-primary search-btn">Search</span>
                    </div>
                </div>
                <br/>
                <br/>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center">
                        <div id="quotes">
                            <div class="text-item" style="display: none;">
                                Worldwide, we conduct <strong>131 billion searches per month</strong> on the web
                            </div>
                            <div class="text-item" style="display: none;">
                                <strong>78% of Internet users</strong> conduct product <strong>research online</strong>
                            </div>
                            <div class="text-item" style="display: none;">
                                <strong>78% of business people</strong> use their <strong>mobile</strong> device to <strong>check email</strong>
                            </div>
                            <div class="text-item" style="display: none;">
                                <strong>Relevant emails</strong> drive <strong>18 times more <strong>revenue</strong> than broadcast emails
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/><br/><br/>
        <h2>Search companies by countries <a class="btn btn-primary btn-sm" style="margin-left:15px" href="<?php echo site_url('listings'); ?>">See all</a></h2><br/>
        <div class="row directory-counties hidden-xs">
            <div class="col-sm-12">
                <div class="counties-pane" style="border: 1px solid #e6e6e6;min-height: 100%;">
                    <?php if (!empty($countries)) { ?>
                        <ul class="countries-ul">
                            <?php foreach($countries as $kcountry => $country) { ?>
                                <li class="country">
                                    <a href="<?php echo site_url('listings/country/'.url_slug($country->country_short_name).'-'.$country->country_id); ?>"><?php echo $country->country_short_name; ?></a>
                                </li>
                            <?php } ?>
                            <li style="clear:both"></li>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>