<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Companies</small></h1>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-4">
            <a href="<?php echo site_url('backend/companies/new'); ?>" class="btn btn-info">Create New Company</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover" style="/*table-layout: fixed;word-break: break-word;*/">
                <thead>
                    <tr>
                        <td><b>Id</b></td>
                        <td><b>Name</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Position</b></td>
                        <td><b>Domain</b></td>
                        <!--<td><b>Subomain</b></td>-->
                        <td><b>Setup Date</b></td>
                        <td><b>Address</b></td>
                        <td><b>Country</b></td>
                        <td><b>City</b></td>
                        <!--<td><b>Capital Invested</b></td>-->
                        <td><b>Fax</b></td>
                        <td><b>Email</b></td>
                        <td><b>Employee</b></td>
                        <td><b>Contacts</b></td>
                        <!--<td><b>Turnover</b></td>-->
                        <td><b>Added</b></td>
                        <td><b>Actions</b></td>
                    </tr>
                </thead>
                <tbody>
                <?php if (!empty($companies)) { ?>
                    <?php foreach($companies as $company) { ?>
                        <tr>
                            <td><?php echo $company->company_id; ?></td>
                            <td><?php echo $company->company_name; ?></td>
                            <td><?php echo $company->company_contact; ?></td>
                            <td><?php echo $company->company_position; ?></td>
                            <td><?php echo $company->company_domain; ?></td>
                            <!--<td><?php /*echo $company->company_subdomain; */?></td>-->
                            <td><?php echo $company->company_setup_date; ?></td>
                            <td><?php echo $company->company_address; ?></td>
                            <td><?php echo $company->company_country_id; ?></td>
                            <td><?php echo $company->company_city; ?></td>
                            <!--<td><?php /*echo $company->company_capital_invested; */?></td>-->
                            <td><?php echo $company->company_fax; ?></td>
                            <td><?php echo $company->company_email; ?></td>
                            <td><?php echo $company->company_employee; ?></td>
                            <td><?php echo $company->company_contacts; ?></td>
                            <!--<td><?php /*echo $company->company_turnover; */?></td>-->
                            <td><?php echo time_elapsed_string($company->company_date_added); ?></td>
                            <td>
                                <a href="<?php echo site_url('backend/companies/delete/'.$company->company_id); ?>">Delete</a> |
                                <a href="<?php echo site_url('backend/companies/edit/'.$company->company_id); ?>">Edit</a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <ul class="pagination">
                <?php echo !empty($links) ? $links : ''; ?>
            </ul>
        </div>
    </div>

</div><!-- /#page-wrapper -->