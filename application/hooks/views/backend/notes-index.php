<style>
    @import 'http://fonts.googleapis.com/css?family=Noto+Sans';

    .chat-textarea {
        resize:none;background:none
    }
    .chat-thread::-webkit-scrollbar, .chat-textarea::-webkit-scrollbar {
        width: 10px;
    }

    .chat-thread::-webkit-scrollbar-track, .chat-textarea::-webkit-scrollbar-track  {
        border-radius: 10px;
        background-color: #e6e6e6;
    }

    .chat-thread::-webkit-scrollbar-thumb, .chat-textarea::-webkit-scrollbar-thumb  {
        border-radius: 10px;
        background-color: #2c3e50;
    }

    .chat-thread {
        margin: 24px auto 0 auto;
        padding: 0 20px 0 0;
        list-style: none;
        overflow-y: scroll;
        overflow-x: hidden;
    }

    .chat-thread li {
        position: relative;
        clear: both;
        display: inline-block;
        padding: 16px 40px 16px 20px;
        margin: 0 0 20px 0;
        font: 14px/20px 'Noto Sans', sans-serif;
        border-radius: 10px;
        background-color: #34495e;
    }

    /* Chat - Avatar */
    .chat-thread li .from-name {
        position: absolute;
        top: 7px;
        height: 50px;
        border-radius: 50px;
        color: #34495e;
        font-weight: bold;
        width: 190px;
        font-size: 13px;
    }

    /* Chat - Speech Bubble Arrow */
    .chat-thread li:after {
        position: absolute;
        top: 15px;
        content: '';
        width: 0;
        height: 0;
        border-top: 15px solid #34495e;
    }

    .chat-thread li:nth-child(odd) {
        animation: show-chat-odd 0.15s 1 ease-in;
        -moz-animation: show-chat-odd 0.15s 1 ease-in;
        -webkit-animation: show-chat-odd 0.15s 1 ease-in;
        float: right;
        margin-right: 80px;
        color: #fff;
    }

    .chat-thread li:nth-child(odd) .from-name {
        right: -215px;
    }

    .chat-thread li:nth-child(odd):after {
        border-right: 15px solid transparent;
        right: -14px;
    }

    .chat-thread li:nth-child(even) {
        animation: show-chat-even 0.15s 1 ease-in;
        -moz-animation: show-chat-even 0.15s 1 ease-in;
        -webkit-animation: show-chat-even 0.15s 1 ease-in;
        float: left;
        color: #fff;
    }

    .chat-thread li:nth-child(even) .from-name {
        left: -215px;
        text-align: right;
    }

    .chat-thread li:nth-child(even):after {
        border-left: 15px solid transparent;
        left: -14px;
    }

    .chat-window {
        position: fixed;
        bottom: 18px;
    }

    .chat-window-message {
        width: 100%;
        height: 48px;
        font: 32px/48px 'Noto Sans', sans-serif;
        background: none;
        color: #0AD5C1;
        border: 0;
        border-bottom: 1px solid rgba(25, 147, 147, 0.2);
        outline: none;
    }

    /* Small screens */
    @media all and (max-width: 767px) {
        .chat-thread {
            width: 90%;
            height: 260px;
        }

        .chat-window {
            left: 5%;
            width: 90%;
        }
    }
    /* Medium and large screens */
    @media all and (min-width: 768px) {
        .chat-thread {
            width: 70%;
            height: 700px;
            padding:0 140px;
        }

        .chat-window {
            left: 25%;
            width: 50%;
        }
    }

</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div id="convo" data-from="Sonu Joshi" data-to="Udita Pal">
                <ul class="chat-thread" id="chat-content">
                    <?php if (!empty($notes)) { ?>
                        <?php foreach($notes as $note) { ?>
                            <li>
                                <?php echo $note->note_content; ?>
                                <span class="from-name"><?php echo $note->user->user_name; ?></span>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" style="width:60%;margin:15px auto" enctype="multipart/form-data" action="<?php echo site_url('backend/notes/save/'); ?>" method="post" role="form">
                <div class="form-group">
                    <div class="col-sm-10">
                        <textarea name="note_content" class="form-control chat-textarea" rows="4"></textarea>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>