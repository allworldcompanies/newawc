<hr class="topbar"/>
<div class="container">
    <br />
    <div class="row">

        <div class="col-sm-12">
            <h1>Create an account</h1>

            <hr />
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <form class="form-vertical" method="post" action="<?php echo site_url('account/register'); ?>">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12" >
                                    <div class="well">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Full name</label>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control " id="exampleInputEmail1" required name="firstname" placeholder="First name">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control " id="exampleInputEmail1" required name="lastname" placeholder="Last name">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control " id="exampleInputEmail1" required name="email" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" required name="password" placeholder="Make sure your password is strong">
                                        </div>
                                        <br />
                                        <button type="submit" class="btn btn-primary">Create account</button>
                                    </div>
                                </div>
                            </div>
                </div>

                <div class="col-md-6 col-sm-12 account-sidebar hidden-sm">

                    <div class="row">
                        <div class="col-sm-3" style="text-align: center;">
                            <img src="<?php echo base_url(); ?>assets/css/images/icons/Crest.png" width="50"/>
                        </div>
                        <div class="col-sm-8">
                            <h3>Why us?</h3>
                            <p>We're one of the most recognisable brands, attracting thousands of buyers every month.<p>
                        </div>
                    </div>
                    <br />

                    <div class="row">
                        <div class="col-sm-3" style="text-align: center;">
                            <img src="<?php echo base_url(); ?>assets/css/images/icons/Pie-Chart.png" width="40"/>
                        </div>
                        <div class="col-sm-8">
                            <h3>Magnet for buyers</h3>
                            <p>We make sure your listings receive maximum exposure and is presented in an engaging way</p>
                        </div>
                    </div>
                    <br />

                    <div class="row">
                        <div class="col-sm-3" style="text-align: center;">
                            <img src="<?php echo base_url(); ?>assets/css/images/icons/Search.png" width="40"/>
                        </div>
                        <div class="col-sm-8">
                            <h3>Focused searches</h3>
                            <p>Our technology and algorithm matches potential buyers directly to your listings</p>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-3" style="text-align: center;">
                            <img src="<?php echo base_url(); ?>assets/css/images/icons/Telephone.png" width="40"/>
                        </div>
                        <div class="col-sm-8">
                            <h3>Mobile web</h3>
                            <p>Your listings will always be accessible to everyone, even when they are on the move, via our responsive mobile website</p>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            </fieldset>
            </form>
        </div>
    </div>
</div>