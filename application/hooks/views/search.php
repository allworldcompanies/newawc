<div class="jumbotron home-search" style="">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <br/>
                <p class="main_description">Search thousands of companies all in one place</p>
                <br/><br/>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text-center">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="input-group">
                                    <span class="input-group-addon input-group-addon-text">Find me a</span>
                                    <input type="text" class="form-control col-sm-3"
                                           placeholder="e.g.  Agriculture, France, Volkswagen ">

                                    <div class=" input-group-addon hidden-xs">
                                        <div class="btn-group">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                All categories <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Company Name</a></li>
                                                <li><a href="#">Country</a></li>
                                                <li><a href="#">City</a></li>
                                                <li><a href="#">Domain</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center">
                        <a href="listings.html" class="btn btn-primary search-btn">Search</a>
                    </div>
                </div>
                <br/>
                <br/>

                <div class="row">
                    <div class="col-sm-12" style="text-align: center">
                        <div id="quotes">
                            <div class="text-item" style="display: none;">
                                Worldwide, we conduct <strong>131 billion searches per month</strong> on the web
                            </div>
                            <div class="text-item" style="display: none;">
                                <strong>78% of Internet users</strong> conduct product <strong>research online</strong>
                            </div>
                            <div class="text-item" style="display: none;">
                                <strong>78% of business people</strong> use their <strong>mobile</strong> device to <strong>check email</strong>
                            </div>
                            <div class="text-item" style="display: none;">
                                <strong>Relevant emails</strong> drive <strong>18 times more <strong>revenue</strong> than broadcast emails
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-8">
    <div class="row directory">
        <div class="col-sm-12 ">
            <h2><span>Directory listings</span></h2>
        </div>
    </div>
    <div class="row directory">
        <div class="col-xs-12">
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Houses and flats</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-truck"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Car and vehicles</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Shopping</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Houses and flats</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-truck"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Car and vehicles</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Shopping</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Houses and flats</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-truck"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Car and vehicles</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="directory-block col-sm-4 col-xs-6">
                <div class="row">
                    <div class="col-sm-3">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="col-sm-9">
                        <h4>Shopping</h4>

                        <p>
                            <a href="listings.html">Cars</a>, <a href="listings.html">Car Parts</a>, <a
                                href="listings.html">Campervans</a>, <a href="listings.html">Motobikes</a>, <a
                                href="listings.html">Scooters</a>, <a href="listings.html">Vans</a>, <a
                                href="listings.html">Trucks</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row directory">
        <div class="col-sm-12 ">
            <h2><span>Country listings</span></h2>
        </div>
    </div>
    <div class="row directory-counties hidden-xs">
        <div class="col-sm-12">
            <div class="counties-pane" style="border: 1px solid #e6e6e6;-webkit-column-count: 3;-moz-column-count: 3;column-count: 3;min-height: 100%;">
                <?php if (!empty($countries)) { ?>
                    <div class="col-sm-12">
                        <?php foreach($countries as $country) { ?>
                            <div class="country">
                                <a href="<?php echo site_url('search/country/'.$country->short_name); ?>"><?php echo $country->short_name; ?></a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-md-4 ">
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-12 col-lg-11 pull-right">
            <br class="hidden-sm hidden-xs"/>
            <br class="hidden-sm hidden-xs"/>
            <br class="hidden-sm hidden-xs"/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Quick guide
                </div>
                <ul class="list-group">
                    <li class="list-group-item"><a href="typography.html">How we work</a></li>
                    <li class="list-group-item"><a href="typography.html">How to profit </a></li>
                    <li class="list-group-item"><a href="typography.html">Send emails to all companies</a></li>
                    <li class="list-group-item"><a href="typography.html">Help and contact us</a></li>
                    <li class="list-group-item"><a href="typography.html">Frequently asked questions</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-12 col-lg-11 pull-right">
            <div class="panel panel-default">
                <div class="panel-body" style="height: 102px; display: block;">
                    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="265"
                         data-layout="standard" data-action="like" data-show-faces="false" data-share="false"
                         style="display: block; height: 30px;">
                    </div>
                    <br/>
                    <!-- Place this tag where you want the +1 button to render. -->
                    <div class="g-plusone" data-annotation="inline" data-width="300"
                         style="display: block; height: 30px;">
                    </div>
                    <!-- Place this tag after the last +1 button tag. -->
                    <script type="text/javascript">
                        (function () {
                            var po = document.createElement('script');
                            po.type = 'text/javascript';
                            po.async = true;
                            po.src = 'https://apis.google.com/js/platform.js';
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(po, s);
                        })();
                    </script>
                </div>
                <div class="panel-footer">
                    <a href="https://twitter.com/twitterapi" class="twitter-follow-button" data-dnt="true">Follow
                        @twitterapi</a>
                    <script>!function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//platform.twitter.com/widgets.js";
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, "script", "twitter-wjs");</script>
                </div>
            </div>
            <p class="main_slogan" style="margin: 28px 0">
                Currently listing 355,785 classified ads in the United Kingdom.
            </p>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-12 col-lg-11 pull-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Premium listings
                </div>
                <div class="panel-body">
                    <div class="featured-gallery">
                        <div class="row">
                            <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"
                                 title="Programmer job availiable at Uber in London">
                                <a href="details.html" class="">
                                    <img alt="" src="css/images/logos/uberlogo_large_verge_medium_landscape.png"
                                         style="width: 100%">
                                </a>
                            </div>
                            <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"
                                 title="Porsche Boxster S, 2.9 2dr reg Apr 2007 ">
                                <a href="details.html" class="">
                                    <img alt="" src="css/images/logos/car-78738_150.jpg"/>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"
                                 title="Please find my lost cat">
                                <a href="details.html" class="">
                                    <img alt="" src="css/images/logos/cats-q-c-120-80-4.jpg"/>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"
                                 title="Mini copper looking for a quick sell !! - London - £2,485">
                                <a href="details.html" class="">
                                    <img alt="" src="css/images/logos/transport-q-c-120-80-8.jpg"/>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"
                                 title="Old MP3 player for sale">
                                <a href="details.html" class="">
                                    <img alt="" src="css/images/logos/technics-q-c-120-80-10.jpg"/>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"
                                 title="Designer job availiable at Uber in London">
                                <a href="details.html" class="">
                                    <img alt="" src="css/images/logos/uberlogo_large_verge_medium_landscape.png"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- /.container -->
<!-- Modal -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sign in to your account</h4>
            </div>
            <div class="modal-body">
                <p>
                    If you have an account with us, please enter your details below.
                </p>

                <form method="POST" action="account_dashboard.html" accept-charset="UTF-8" id="user-login-form"
                      class="form ajax" data-replace=".error-message p">
                    <div class="form-group">
                        <input placeholder="Your username/email" class="form-control" name="email" type="text">
                    </div>
                    <div class="form-group">
                        <input placeholder="Your password" class="form-control" name="password" type="password"
                               value="">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary pull-right">Login</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a data-toggle="modal" href="#modalForgot">Forgot your password?</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="text-align: center">
                <div class="error-message">
                    <p style="color: #000; font-weight: normal;">
                        Don't have an account? <a class="link-info" href="register.html">Register now</a>
                    </p>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Modal -->
<div class="modal fade" id="modalForgot" tabindex="-1" role="dialog" aria-labelledby="modalForgot" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Forgot your password?</h4>
            </div>
            <div class="modal-body">
                <p>
                    Enter your email to continue
                </p>

                <form role="form">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Your email address">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <a href="my_account.html" class="btn btn-primary pull-right">Continue</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->