<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Profile</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo site_url('account/save'); ?>" enctype="multipart/form-data">
                        <fieldset>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <div class="form-group"><!--
                                        <label for="inputEmail1" class="col-sm-12 control-label">You</label>-->
                                        <div class="col-sm-11">
                                            <img id="file-select" src="<?php echo !empty($account->user_avatar) ? site_url('uploads/'.$account->user_avatar) : site_url('assets/img/no-avatar.png'); ?>" width="100%" /><br />

                                            <span class="btn btn-default btn-file">
                                                Click to upload <input type="file" name="file">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <br />
                                    <div class="form-group">
                                        <label for="inputEmail1" class="col-sm-4 control-label">Display name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" required value="<?php echo $account->user_name; ?>" name="user_name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail1" class="col-sm-4 control-label">Company name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="<?php echo $account->user_company_name; ?>" name="user_company_name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail1" class="col-sm-4 control-label">Website</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control"  value="<?php echo $account->user_website; ?>" name="user_website">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="inputEmail1" style="width: 100%" class="control-label">About you</label><br /><br />
                                    <textarea class="form-control col-sm-8" rows="6" style="width: 99%" name="user_about"><?php echo $account->user_about; ?></textarea>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="inputEmail1" class="col-sm-2 control-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $account->user_address; ?>" name="user_address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail1" class="col-sm-2 control-label">Email address</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" required value="<?php echo $account->user_email; ?>" name="user_email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail1" class="col-sm-2 control-label">Mobile</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $account->user_mobile; ?>" name="user_mobile">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail1" class="col-sm-2 control-label">Home phone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $account->user_phone; ?>" name="user_phone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail1" class="col-sm-2 control-label">Fax</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $account->user_fax; ?>" name="user_fax">
                                </div>
                            </div>

                            <br />
                            <button type="submit" class="btn btn-primary">Save profile</button>
                </div>
            </div>
        </div>
        </fieldset>
        </form>
    </div>




</div>
