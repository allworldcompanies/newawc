<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Your Searches</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td>Search Id</td>
                                <td>Search Data</td>
                                <td>Date Searched</td>
                                <td>Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($searches)) { ?>
                            <?php foreach($searches as $search) { ?>
                                <?php
                                    if (trim(strlen($search->search_array)) < 5) continue;
                                    $link = site_url('listings').'/?filter=1';
                                    $jsonArray = (array)json_decode($search->search_array);
                                    foreach($jsonArray as $key => $val) $link .= '&'.$key.'='.$val;
                                ?>
                                <tr>
                                    <td style="width: 75px;text-align: center"><?php echo $search->search_id; ?></td>
                                    <td style="word-break: break-word">
                                        <?php foreach($jsonArray as $fieldName => $fieldValue) { ?>
                                            <?php echo ucwords(str_replace('_', ' ', $fieldName)).': '; ?>
                                            <?php echo is_array($fieldValue) ? implode(', ', $fieldValue) : $fieldValue; ?><br/>
                                        <?php } ?>
                                    </td>
                                    <td style="width: 110px"><?php echo time_elapsed_string($search->search_date_added); ?></td>
                                    <td><a href="<?php echo $link; ?>" style="color:#0061c2">Search</a></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Your Searches</div>
                <div class="panel-body">

                </div>
            </div>
        </div>
    </div>
</div>-->