<?php

class MY_Model extends CI_Model {
    const DB_TABLE = 'abstract';
    const DB_TABLE_PK = 'abstract';

    /**
     * Create record.
     */
    private function insert() {
        $this->db->insert($this::DB_TABLE, (object) array_filter((array) $this));
        $this->{$this::DB_TABLE_PK} = $this->db->insert_id();
    }

    /**
     * Update record.   
     */
    private function update() {
        $dtp = $this::DB_TABLE_PK;
        //$this->db->update($this::DB_TABLE, $this, array($dtp=>$this->$dtp));
        $this->db->update($this::DB_TABLE, (object) array_filter((array) $this), array($dtp=>$this->$dtp));
    }

    /**
     * Populate from an array or standard class.
     * @param mixed $row
     */
    public function populate($row) {
        foreach ($row as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Load from the database.
     * @param int $id
     */
    public function load($id) {
        $query = $this->db->get_where($this::DB_TABLE, array($this::DB_TABLE_PK => $id));
        return $query->row();
    }

    /**
     * Delete the current record.
     */
    public function delete() {
        $this->db->delete($this::DB_TABLE, array(
           $this::DB_TABLE_PK => $this->{$this::DB_TABLE_PK},
        ));
        unset($this->{$this::DB_TABLE_PK});
    }

    /**
     * Save the record.
     */
    public function save() {
        if (isset($this->{$this::DB_TABLE_PK})) {
            $this->update();
        }
        else {
            $this->insert();
        }
    }

    /**
     * Get an array of Models with an optional limit, offset.
     *
     * @param int $limit Optional.
     * @param int $offset Optional; if set, requires $limit.
     * @return array Models populated by database, keyed by PK.
     */
    public function get($limit = 0, $offset = 0) {
        if ($limit) {
            $query = $this->db->get($this::DB_TABLE, $limit, $offset);
        }
        else {
            $query = $this->db->get($this::DB_TABLE);
        }
        $ret_val = array();
        $class = get_class($this);
        foreach ($query->result() as $row) {
            $model = new $class;
            $model->populate($row);
            $ret_val[$row->{$this::DB_TABLE_PK}] = $model;
        }
        return $ret_val;
    }

    public function search($params = array(), $limit = null, $perpage = null, $like = true,$companysearch=0) {
        $this->db->select('*');
        $this->db->from($this::DB_TABLE);
        foreach ($params as $key => $param) {
            if (!empty($param) && !empty($key)) {
                if (is_array($param)) {
                    foreach($param as $pkey => $pval) {
                        $this->db->or_where($key, $pval, false);
						
                    }
                } else {
                    if ($like) {
                        $this->db->like($key, $param);
                    } else {
                        $this->db->where($key, $param);
                    }
                }

            }
        }
        if (isset($limit) && isset($perpage)) {
            $this->db->limit($perpage, $limit);
        }
          
        if($companysearch!='1')
	   $this->db->order_by($this::DB_TABLE_PK, "desc");
	   else
	   $this->db->order_by("company_email","desc");
		//$this->db->order_by('company_contacts', "desc");
		//$this->db->order_by('company_setup_date', "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function count($params = array(), $limit = null, $perpage = null) {
        $this->db->select('*');
        $this->db->from($this::DB_TABLE);
        foreach ($params as $key => $param) {
            if (!empty($param) && !empty($key)) {
                if (is_array($param)) {
                    foreach($param as $pkey => $pval) {
                        $this->db->or_where($key, $pval, false);
                    }
                } else {
                    $this->db->like($key, $param);
                }
            }
        }
        if (isset($limit) && isset($perpage)) {
            $this->db->limit($perpage, $limit);
        }
        return $this->db->count_all_results();
    }
}