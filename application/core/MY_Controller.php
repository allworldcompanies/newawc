<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Frontend_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
		ini_set('memory_limit', '-1');
        $production = $this->input->get('production');
        if (!empty($production)) $this->session->set_userdata('production', true);

        $session_production = $this->session->userdata('production');
        if (empty($session_production)) {
         //  die('Under development...');
        }


        /*$this->session->unset_userdata('products');
        $this->session->unset_userdata('paypal_products');*/
        $this->data                         = array();
        $notification                       = $this->session->userdata('notification');
        $this->data['account']              = $this->user->load($this->session->userdata('id'));
        $this->data['notification']         = $notification;
        $this->data['sessionProducts']      = $this->session->userdata('products');
        $countries = $this->country->get();
        $this->data['countriesDefault'] = array();
        foreach($countries as $country) $this->data['countriesDefault'][$country->country_id] = $country;

        $options = $this->option->get();
        $this->data['options'] = array();
        if (!empty($options)) foreach($options as $option) $this->data['options'][$option->option_name] = $option->option_value;
        $this->session->unset_userdata('notification');
        if (!empty($this->data['account'])) {
            $this->data['availableCompanies'] = $this->available_companies->getByUserId($this->data['account']->user_id);
        }


        $this->data['judete']['Alba'] = '258';
        $this->data['judete']['Arad'] = '257';
        $this->data['judete']['Arges'] = '248';
        $this->data['judete']['Bacau'] = '234';
        $this->data['judete']['Bihor'] = '259';
        $this->data['judete']['Bistrita-Nasaud'] = '263';
        $this->data['judete']['Botosani'] = '231';
        $this->data['judete']['Brasov'] = '268';
        $this->data['judete']['Brailia'] = '239';
        $this->data['judete']['Buzau'] = '238';
        $this->data['judete']['Caras-Severin'] = '255';
        $this->data['judete']['Calarasi'] = '242';
        $this->data['judete']['Cluj'] = '264';
        $this->data['judete']['Constanta'] = '241';
        $this->data['judete']['Covasna'] = '267';
        $this->data['judete']['Dambovita'] = '245';
        $this->data['judete']['Doij'] = '251';
        $this->data['judete']['Galati'] = '236';
        $this->data['judete']['Giurgiu'] = '246';
        $this->data['judete']['Gorj'] = '253';
        $this->data['judete']['Harghita'] = '266';
        $this->data['judete']['Hunedoara'] = '254';
        $this->data['judete']['Ialomita'] = '243';
        $this->data['judete']['Iasi'] = '232';
        $this->data['judete']['Ilfov'] = '21';
        $this->data['judete']['Maramures'] = '262';
        $this->data['judete']['Mehedinti'] = '252';
        $this->data['judete']['Mures'] = '265';
        $this->data['judete']['Neamt'] = '233';
        $this->data['judete']['Olt'] = '249';
        $this->data['judete']['Prahova'] = '244';
        $this->data['judete']['Satu Mare'] = '261';
        $this->data['judete']['Salaj'] = '260';
        $this->data['judete']['Sibiu'] = '269';
        $this->data['judete']['Suceava'] = '230';
        $this->data['judete']['Teleorman'] = '247';
        $this->data['judete']['Timis'] = '256';
        $this->data['judete']['Tulcea'] = '240';
        $this->data['judete']['Vaslui'] = '235';
        $this->data['judete']['Valcea'] = '250';
        $this->data['judete']['Vrancea'] = '237';
    }
}

class Backend_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
		ini_set('memory_limit', '-1');
        $this->data = array();
        $this->data['account'] = $this->user->load($this->session->userdata('id'));
        $this->data['original_options'] = $this->option->get();
        $this->data['options'] = array();
        if (!empty($this->data['original_options'])) {
            foreach($this->data['original_options'] as $option) {
                $this->data['options'][$option->option_name] = $option->option_value;
            }
        }
        if (empty($this->data['account']) || $this->data['account']->user_admin != 1) {
            $this->session->set_userdata('notification','You don`t have access in the backend');
            redirect(base_url());
        }
    }
}