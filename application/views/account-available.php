<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Your Available Companies</div>
                <div class="panel-body">
                    <p>Now you have <?php echo !empty($availableCompanies) ? $availableCompanies->available_companies_number : '0'; ?> free companies.</p>
                    <form method="post" action="<?php echo site_url('paypal/update'); ?>">
                        <input type="hidden" name="type" value="add" />
                        <input type="hidden" name="product_qty" value="1" size="3" />
                        <input type="hidden" name="product_code" value="AVAILABLE1000" />
                        <input type="hidden" name="return_url" value="<?php echo base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>" />
                        <button class="add_to_cart btn btn-warning">Buy 1000 available companies for 10 USD.</button>
                    </form>
                    <?php if (!empty($companies)) { ?>
                        <table class="table table-hover" style="table-layout: fixed;word-break: break-word;">
                            <thead>
                            <tr>
                                <!--<td style="width:100px">Id</td>-->
                                <td>Name</td>
                                <td>Contact</td>
                                <td>Email</td>
                                <td>Fax</td>
                                <td>Website</td>
                                <td>Phone</td>
                                <td>Domains</td>
                                <!--<td>Paypal Email</td>-->
                                <td>Ordered Date</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($companies as $key => $company) { ?>
                                <tr>
                                    <!--<td style="width: 75px;text-align: center"><?php /*echo $company->company_id; */?></td>-->
                                    <td><a href="<?php echo site_url(url_slug($company->company_name).'-'.$company->company_id); ?>" style="color:#0061c2"><?php echo $company->company_name; ?></a></td>

                                    <td><?php echo $company->company_contact; ?></td>
                                    <td><?php echo $company->company_email; ?></td>
                                    <td><?php echo $company->company_fax; ?></td>
                                    <td><?php echo $company->company_website; ?></td>
                                    <td><?php echo $company->company_phone; ?></td>
                                    <td><?php echo $company->company_domain; ?></td>

                                    <!--<td><?php /*echo urldecode(urldecode($company->order_email)); */?></td>-->
                                    <td><?php echo time_elapsed_string($company->order_date); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <p>You didn`t use free companies.</p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>