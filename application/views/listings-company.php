<?php // echo 'dheeraj';die();?>
<div class="jumbotron home-tron-search well ">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="home-tron-search-inner">
                    <div class="row">
                        <div class="col-sm-8 col-xs-9" style="text-align: center">
                            <div class="row">
                                <div class="col-sm-12 col-sm-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon input-group-addon-text">Find the company</span>
                                        <input type="text" class="form-control col-sm-3 search-input" placeholder="e.g. Company Name" >
                                        <div class="input-group-addon hidden-xs" style="display:none">
                                            <div class="btn-group">
                                                <button class="btn dropdown-toggle search-dropdown-current" data-toggle="dropdown" data-by="company_name">
                                                    Company Name
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-3" style="text-align: center">
                            <div class="row">
                                <div class="col-sm-11 pull-right">
                                    <button class="btn btn-primary search-btn"><i class="icon-search"></i>&nbsp;&nbsp;&nbsp;&nbsp;Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container" id="listings-page">
    <div class="row">
        <div class="col-sm-12 listing-wrapper listings-top listings-bottom">
            <br />
            <br />
            <div class="row">
                <div class="col-sm-7">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo (!empty($_SERVER["HTTP_REFERER"]) && strpos('listings', $_SERVER["HTTP_REFERER"]) === true) ? $_SERVER["HTTP_REFERER"] : site_url('listings'); ?>" class="link-info"><i class="fa fa-chevron-left"></i> Back</a></li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1 style="line-height: 30px;"><?php echo $company->company_name; ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-3" style="width: 100px;">
                            <div class="g-plus" data-action="share" data-annotation="bubble"></div>
                            <script type="text/javascript">
                                (function() {
                                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                    po.src = 'https://apis.google.com/js/platform.js';
                                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                                })();
                            </script>
                        </div>
                        <div class="col-xs-3" style="width: 100px;">
                            <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        </div>
                        <div class="col-xs-3" style="width: 100px;">
                            <div class="fb-share-button" data-href="<?php echo site_url(url_slug($company->company_name).'-'.$company->company_id); ?>" data-type="button_count"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-sm-12">
                    <h3>Details</h3>
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table <?php echo (!empty($orderedIds) && is_array($orderedIds) && in_array($company->company_id, $orderedIds)) ? 'paid-listing' : ''; ?>">
                                <tbody>
                                <?php if (!empty($fields)) { ?>
                                    <?php foreach ($fields as $field) { ?>
                                        <?php if (!empty($company->$field)) { ?>
                                            <tr>
                                            <?php $fieldLabel = ucwords(str_replace('_',' ',str_replace('company_','',$field))); ?>
                                            <th><?php echo $fieldLabel; ?></th>
                                            <?php if ($field == 'company_country_id') { ?>
                                                <td><?php echo $countriesDefault[$company->company_country_id]->country_short_name; ?></td>
                                            <?php } elseif ($field == 'company_phone') { ?>
                                                <td><?php echo premium($company->company_phone, false, $orderedIds, $company->company_id); ?></td>
                                            <?php } elseif ($field == 'company_fax') { ?>
                                                <td><?php echo premium($company->company_fax, false, $orderedIds, $company->company_id); ?></td>
                                            <?php } elseif ($field == 'company_email') { ?>
                                                <td><?php echo premium($company->company_email,'@', $orderedIds, $company->company_id); ?></td>
                                            <?php } elseif ($field == 'company_website') { ?>
                                                <td><?php echo premium($company->company_website, false, $orderedIds, $company->company_id); ?></td>
                                            <?php } elseif ($field == 'company_2009' || $field == 'company_2010' || $field == 'company_2011' || $field == 'company_2012') { ?>
                                                <td>
                                                    <?php $jsonYear = json_decode($company->$field); ?>
                                                    <?php if (!empty($jsonYear)) { ?>
                                                        <?php foreach ($jsonYear as $fieldName => $fieldValue) { ?>
                                                            <?php if (!empty($fieldValue) && !empty($fieldName) && $fieldValue != '\N') { ?>
                                                                <div class="field-row">
                                                                    <div class="field-label"><?php echo $fieldName; ?></div>
                                                                    <div class="field-value"><?php echo $fieldValue; ?></div>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </td>
                                            <?php } else { ?>
                                                <td><?php echo $company->$field; ?></td>
                                            <?php } ?>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br/>
                    <p><strong>Domains</strong></p>
                    <?php
                    $domains = !empty($company->company_domain) ? explode(',', $company->company_domain) : array();
                    foreach ($domains as $domain) echo '<a href="'.site_url('listings/?filter=1&company_domain='.$domain).'"><strong>'.$domain.'</strong></a> | ';
                    ?>
                    <br/><br/><br/><br/>
                    <p>Ad ref: <?php echo $company->company_id; ?> | Posted <?php echo time_elapsed_string($company->company_date_added); ?></p>
                    <?php if (!(!empty($orderedIds) && is_array($orderedIds) && in_array($company->company_id, $orderedIds))) { ?>
                        <p>
                        <span class="classified_links ">
                            <a class="link-info" href="<?php echo site_url('listings/favorite/'.$company->company_id); ?>"><i class="fa fa-star"></i> Add to favorites</a>&nbsp;
                            <!--<a class="link-info" href="<?php /*echo site_url('listings/email/'.$company->company_id); */?>"><i class="fa fa-envelope-o"></i> Send Email</a>&nbsp;-->
                            <a class="link-info" href="<?php echo site_url('account/available/'.$company->company_id); ?>"> <i class="fa fa-unlock"></i> Free</a>
                        </span>
                        </p>
                        <br/><br/>
                        <div class="buy-forms">
                            <form method="post" action="<?php echo site_url('paypal/update'); ?>">
                                <input type="hidden" name="type" value="add" />
                                <input type="hidden" name="product_qty" value="1" size="3" />
                                <input type="hidden" name="product_code" value="<?php echo $company->company_id; ?>" />
                                <input type="hidden" name="return_url" value="<?php echo base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>" />
                                <button class="add_to_cart btn btn-primary">Add To Cart</button>
                            </form>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <br/><br/>
            <div class="row">
                <div class="col-sm-12 listings">
                    <h2>Related Companies</h2>
                    <?php if (!empty($companies)) { ?>
                        <?php foreach ($companies as $key => $company) { ?>
                            <div class="row listing-row <?php echo (!empty($orderedIds) && is_array($orderedIds) && in_array($company->company_id, $orderedIds)) ? 'paid-listing' : ''; ?>">
                                <div class="col-sm-12">
                                    <p class="pull-right"><input type="checkbox" checked id="c-<?php echo $key; ?>" name="selected" value="<?php echo $company->company_id; ?>" /><label for="c-<?php echo $key; ?>"></label></p>
                                    <h3><a href="<?php echo site_url(url_slug($company->company_name).'-'.$company->company_id); ?>"><?php echo $company->company_name; ?></a></h3><?php //die();?>
                                    <p class="muted"><?php echo $company->company_address; ?></p>
                                    <p>
                                        <?php $description  = (!empty($company->company_country_id)) ? '<strong>Country: </strong>'.$countriesDefault[$company->company_country_id]->country_short_name.', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_city)) ? '<strong>City: </strong>'.$company->company_city.', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_contact)) ? '<strong>Contact: </strong>'.$company->company_contact.', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_position)) ? '<strong>Contact`s Position: </strong>'.$company->company_position.', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_phone)) ? '<strong>Phone: </strong>'.premium($company->company_phone, false, $orderedIds, $company->company_id).', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_fax)) ? '<strong>Fax: </strong>'.premium($company->company_fax, false, $orderedIds, $company->company_id).', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_email)) ? '<strong>Email: </strong>'.premium($company->company_email,'@', $orderedIds, $company->company_id).', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_website)) ? '<strong>Website: </strong>'.premium($company->company_website, false, $orderedIds, $company->company_id).', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_employee)) ? '<strong>Employee: </strong>'.$company->company_employee.', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_contacts)) ? '<strong>Other Contacts: </strong>'.$company->company_contacts.', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_turnover)) ? '<strong>Turnover: </strong>'.$company->company_turnover.', ' : ''; ?>
                                        <?php $description .= (!empty($company->company_capital_invested)) ? '<strong>CUI: </strong>'.$company->company_capital_invested.', ' : ''; ?>
                                        <?php echo trim($description, ', '); ?>
                                    </p>
                                    <p class="ad-description">
                                        <?php // echo !empty($company->company_setup_date) ? '<strong>'.date('Y', strtotime($company->company_setup_date)).'</strong> ' : ''; ?>
                                        <?php
                                        $domains = !empty($company->company_domain) ? explode(',', $company->company_domain) : array();
                                        foreach ($domains as $domain) echo '<a href="'.site_url('listings/?filter=1&company_domain='.$domain).'"><strong>'.$domain.'</strong></a> | ';
                                        ?>

                                        <?php // echo !empty($company->company_subdomain) ? '<strong>'.$company->company_subdomain.'</strong> ' : ''; ?>
                                    </p>
                                    <p>
                                    <span class="classified_links ">
                                        <a class="link-info" href="<?php echo site_url('listings/favorite/'.$company->company_id); ?>"><i class="fa fa-star"></i> Add to favorites </a>&nbsp;
                                        <!--<a class="link-info" href="<?php /*echo site_url('listings/email/'.$company->company_id); */?>"><i class="fa fa-envelope-o"></i> Send Email</a>&nbsp;-->
                                        <?php if (!(!empty($orderedIds) && is_array($orderedIds) && in_array($company->company_id, $orderedIds))) { ?>
                                            <a class="link-info submit-form" href="#ci-<?php echo $key; ?>"> <i class="fa fa-shopping-cart"></i> Add to cart </a>&nbsp;
                                        <a class="link-info" href="<?php echo site_url('account/available/'.$company->company_id); ?>"> <i class="fa fa-unlock"></i> Free</a>
                                        <?php } ?>
                                        <form method="post" action="<?php echo site_url('paypal/update'); ?>" id="ci-<?php echo $key; ?>">
                                            <input type="hidden" name="type" value="add" />
                                            <input type="hidden" name="product_qty" value="1" size="3" />
                                            <input type="hidden" name="product_code" value="<?php echo $company->company_id; ?>" />
                                            <input type="hidden" name="return_url" value="<?php echo base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); ?>" />
                                        </form>
                                    </span>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <p><br/>There are no companies related to this company.</p>
                    <?php } ?>
                </div>
            </div>
