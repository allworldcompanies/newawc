<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Orders</small></h1><br/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td><b>Order Id</b></td>
                    <td><b>Order User Id</b></td>
                    <td><b>Order Companies</b></td>
                    <td><b>Order Email</b></td>
                    <td><b>Order Total</b></td>
                    <td><b>Order Date</b></td>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($orders)) { ?>
                    <?php foreach($orders as $order) { ?>
                        <?php $companyIdsArray = explode(',', $order->order_company_ids); ?>
                        <tr>
                            <td><?php echo $order->order_id; ?></td>
                            <td><?php echo $order->order_user_id; ?></td>
                            <td><?php echo count($companyIdsArray); ?> companies</td>
                            <td><?php echo $order->order_email; ?></td>
                            <td><?php echo $order->order_price_total.' '.PayPalCurrencyCode; ?></td>
                            <td><?php echo time_elapsed_string($order->order_date); ?></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <br/>
            <ul class="pagination">
                <?php echo $links; ?>
            </ul>
        </div>
    </div>
</div>