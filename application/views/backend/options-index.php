<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1>Options</small></h1><br/>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-4">
            <a class="btn btn-primary" href="<?php echo site_url('backend/options/option'); ?>">Create New Option</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td><b>Option Id</b></td>
                    <td><b>Option Name</b></td>
                    <td><b>Option Value</b></td>
                    <td><b>Delete</b></td>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($original_options)) { ?>
                    <?php foreach($original_options as $option) { ?>
                        <tr>
                            <td><?php echo $option->option_id; ?></td>
                            <td><?php echo $option->option_name; ?></td>
                            <td><?php echo $option->option_value; ?></td>
                            <td>
                                <a href="<?php echo site_url('backend/options/option/'.$option->option_id); ?>">Edit</a> |
                                <a href="<?php echo site_url('backend/options/delete/'.$option->option_id); ?>"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>