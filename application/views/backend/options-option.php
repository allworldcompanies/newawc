<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1><?php echo !empty($option) ? $option->option_name : 'New Option'; ?></h1>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-4">
            <a href="<?php echo site_url('backend/options'); ?>">Back to options</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" enctype="multipart/form-data" action="<?php echo !empty($option) ? site_url('backend/options/save/'.$option->option_id) : site_url('backend/options/save/'); ?>" method="post" role="form">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Option Name</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($option) ? $option->option_name : ''; ?>" name="option_name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Option Value</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($option) ? $option->option_value : ''; ?>" name="option_value">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div><!-- /#page-wrapper -->