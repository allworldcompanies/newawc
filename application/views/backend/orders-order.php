<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h1><?php echo !empty($user) ? $user->user_name : 'New Ordedr'; ?></h1>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-lg-4">
            <a href="<?php echo site_url('backend/users'); ?>">Back to users</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" enctype="multipart/form-data" action="<?php echo !empty($user) ? site_url('backend/users/save/'.$user->user_id) : site_url('backend/companies/save/'); ?>" method="post" role="form">
                <div class="form-group">
                    <label class="col-sm-2 control-label">User Name</label>
                    <div class="col-sm-10">
                        <input type="text" required class="form-control" value="<?php echo !empty($user) ? $user->user_name : ''; ?>" name="user_name">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div><!-- /#page-wrapper -->