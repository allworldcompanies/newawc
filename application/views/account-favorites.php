<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Your Favorite Companies</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <td>Favorite Id</td>
                            <td>Favorite Company</td>
                            <td>Date Added</td>
                            <td>Actions</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($favorites)) { ?>
                            <?php foreach($favorites as $favorite) { ?>
                                <tr>
                                    <td style="width: 80px;text-align: center"><?php echo $favorite->favorite_id; ?></td>
                                    <td style="word-break: break-word"><?php echo $favorite->company->company_name; ?></td>
                                    <td style="width: 110px"><?php echo time_elapsed_string($favorite->favorite_date_added); ?></td>
                                    <td><a href="<?php echo site_url(url_slug($favorite->company->company_name).'-'.$favorite->company->company_id); ?>" style="color:#0061c2">Link</a></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>