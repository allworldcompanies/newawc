<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Your Companies</div>
                <div class="panel-body">
                    <a href="<?php echo site_url('excel/paidCompanies'); ?>" style="position: absolute;top: 10px;right: 30px;" >Export to Excel</a>
                    <?php if (!empty($paidCompanies)) { ?>
                        <table class="table table-hover" style="table-layout: fixed;word-break: break-word;">
                            <thead>
                            <tr>
                                <!--<td style="width:100px">Id</td>-->
                                <td>Name</td>
                                <td>Contact</td>
                                <td>Email</td>
                                <td>Fax</td>
                                <td>Website</td>
                                <td>Phone</td>
                                <td>Domains</td>
                                <!--<td>Paypal Email</td>-->
                                <td>Ordered Date</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($paidCompanies as $key => $company) { ?>
                                <tr>
                                    <!--<td style="width: 75px;text-align: center"><?php /*echo $company->company_id; */?></td>-->
                                    <td><a href="<?php echo site_url(url_slug($company->company_name).'-'.$company->company_id); ?>" style="color:#0061c2"><?php echo $company->company_name; ?></a></td>

                                    <td><?php echo $company->company_contact; ?></td>
                                    <td><?php echo $company->company_email; ?></td>
                                    <td><?php echo $company->company_fax; ?></td>
                                    <td><?php echo $company->company_website; ?></td>
                                    <td><?php echo $company->company_phone; ?></td>
                                    <td><?php echo $company->company_domain; ?></td>

                                    <!--<td><?php /*echo urldecode(urldecode($company->order_email)); */?></td>-->
                                    <td><?php echo time_elapsed_string($company->order_date); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <p>You didn`t buy anything.</p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>