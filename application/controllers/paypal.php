<?php if ( ! defined('BASEPATH')) exit;
ob_start();
class Paypal extends Frontend_Controller {

    public function process()
    {
        $paypalmode = (PayPalMode=='sandbox') ? '.sandbox' : '';

        if($_POST) //Post Data received from product list page.
        {
            //Other important variables like tax, shipping cost
            $TotalTaxAmount 	= 0;  //Sum of tax for all items in this order.
            $HandalingCost 		= 0;  //Handling cost for this order.
            $InsuranceCost 		= 0;  //shipping insurance cost for this order.
            $ShippinDiscount 	= 0; //Shipping discount for this order. Specify this as negative number.
            $ShippinCost 		= 0; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.

            //we need 4 variables from product page Item Name, Item Price, Item Number and Item Quantity.
            //Please Note : People can manipulate hidden field amounts in form,
            //In practical world you must fetch actual price from database using item id.
            //eg : $ItemPrice = $mysqli->query("SELECT item_price FROM products WHERE id = Product_Number");
            $paypal_data ='';
            $ItemTotalPrice = 0;

            foreach($_POST['item_name'] as $key=>$itmname)
            {
                $product_code 	= filter_var($_POST['item_code'][$key], FILTER_SANITIZE_STRING);
                $paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$key.'='.urlencode($_POST['item_name'][$key]);
                $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$key.'='.urlencode($_POST['item_code'][$key]);
                $paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$key.'='.urlencode($_POST['item_price'][$key]);
                $paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$key.'='. urlencode($_POST['item_qty'][$key]);

                // item price X quantity
                $subtotal = ($_POST['item_price'][$key]);

                //total price
                $ItemTotalPrice = $ItemTotalPrice + $subtotal;

                //create items for session
                $paypal_product['items'][] = array('itm_name'=>$_POST['item_name'][$key],
                    'itm_price'=> $_POST['item_price'][$key],
                    'itm_code'=>$_POST['item_code'][$key],
                    'itm_qty'=>$_POST['item_qty'][$key]
                );
            }

            //Grand total including all tax, insurance, shipping cost and discount
            $GrandTotal = ($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost + $ShippinCost + $ShippinDiscount);


            $paypal_product['assets'] = array('tax_total'=>$TotalTaxAmount,
                'handaling_cost'=>$HandalingCost,
                'insurance_cost'=>$InsuranceCost,
                'shippin_discount'=>$ShippinDiscount,
                'shippin_cost'=>$ShippinCost,
                'grand_total'=>$GrandTotal);

            //create session array for later use
            $this->session->set_userdata('paypal_products', $paypal_product);

            //Parameters for SetExpressCheckout, which will be sent to PayPal
            $padata = 	'&METHOD=SetExpressCheckout'.
                '&RETURNURL='.urlencode(PayPalReturnURL ).
                '&CANCELURL='.urlencode(PayPalCancelURL).
                '&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").$paypal_data.
                '&NOSHIPPING=0'. //set 1 to hide buyer's shipping address, in-case products that does not require shipping
                '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
                '&PAYMENTREQUEST_0_TAXAMT='.urlencode($TotalTaxAmount).
                '&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($ShippinCost).
                '&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($HandalingCost).
                '&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($ShippinDiscount).
                '&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($InsuranceCost).
                '&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
                '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode(PayPalCurrencyCode).
                '&LOCALECODE=GB'. //PayPal pages to match the language on your website.
                '&LOGOIMG=https://www.paypalobjects.com/en_US/i/logo/paypal_logo.gif'. //site logo
                '&CARTBORDERCOLOR=FFFFFF'. //border color of cart
                '&ALLOWNOTE=1';

            //We need to execute the "SetExpressCheckOut" method to obtain paypal token
            $paypal= new MyPayPal();
            $httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, PayPalApiUsername, PayPalApiPassword, PayPalApiSignature, PayPalMode);

            //Respond according to message we receive from Paypal
            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
            {
                //Redirect user to PayPal store with Token received.
                $paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
                redirect($paypalurl);
            }
            else
            {
                //Show error message
                echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
                p($httpParsedResponseAr);
                exit;
            }

        }

//Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
        if(isset($_GET["token"]) && isset($_GET["PayerID"]))
        {
            //we will be using these two variables to execute the "DoExpressCheckoutPayment"
            //Note: we haven't received any payment yet.

            $token = $_GET["token"];
            $payer_id = $_GET["PayerID"];

            //get session variables
            $paypal_product = $this->session->userdata('paypal_products');
            $paypal_data = '';
            $ItemTotalPrice = 0;

            $companyIds = array();
            foreach($paypal_product['items'] as $key=>$p_item)
            {
                $paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$key.'='. urlencode($p_item['itm_qty']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$key.'='.urlencode($p_item['itm_price']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$key.'='.urlencode($p_item['itm_name']);
                $paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$key.'='.urlencode($p_item['itm_code']);

                // item price X quantity
                $subtotal = ($p_item['itm_price']*$p_item['itm_qty']);

                //total price
                $ItemTotalPrice = ($ItemTotalPrice + $subtotal);

                $companyIds[] = $p_item['itm_code'];
            }

            $padata = 	'&TOKEN='.urlencode($token).
                '&PAYERID='.urlencode($payer_id).
                '&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
                $paypal_data.
                '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
                '&PAYMENTREQUEST_0_TAXAMT='.urlencode($paypal_product['assets']['tax_total']).
                '&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($paypal_product['assets']['shippin_cost']).
                '&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($paypal_product['assets']['handaling_cost']).
                '&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($paypal_product['assets']['shippin_discount']).
                '&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($paypal_product['assets']['insurance_cost']).
                '&PAYMENTREQUEST_0_AMT='.urlencode($paypal_product['assets']['grand_total']).
                '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode(PayPalCurrencyCode);

            //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
            $paypal= new MyPayPal();
            $httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, PayPalApiUsername, PayPalApiPassword, PayPalApiSignature, PayPalMode);

            //Check if everything went ok..
            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
            {
                echo '<h2>Success</h2>';
                echo 'Your Transaction ID : '.urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);

                /*
                //Sometimes Payment are kept pending even when transaction is complete.
                //hence we need to notify user about it and ask him manually approve the transiction
                */

                if('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"])
                {
                    echo '<div style="color:green">Payment Received! Go To <a href="'.site_url('account/companies').'">Paid Companies</a> Section to see the premium content.</div>';
                }
                elseif('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"])
                {
                    echo '<div style="color:red">Transaction Complete, but payment is still pending! '.
                        'You need to manually authorize this payment in your <a target="_new" href="http://www.paypal.com">Paypal Account</a></div>';
                }

                // we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
                // GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
                $padata = 	'&TOKEN='.urlencode($token);
                $paypal= new MyPayPal();
                $httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, PayPalApiUsername, PayPalApiPassword, PayPalApiSignature, PayPalMode);

                if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
                {
                    $orderedIds = array();
                    if (!empty($companyIds)) {
                        foreach($companyIds as $preId) {
                            if (strpos($preId, 'PACKAGE') !== false) {
                                $order = $this->order->load(str2int($preId));
                                $orderIdsArray = explode(',', $order->order_company_ids);
                                foreach($orderIdsArray as $orderId) if(!empty($orderId)) $orderedIds[] = $orderId;
                                $free = 1;
                            } elseif (strpos($preId, 'AVAILABLE') !== false) {
                                $aa = $this->available_companies->getByUserId($this->data['account']->user_id);
                                $aaObj = new Available_companies();
                                $aaObj->available_companies_id = $aa->available_companies_id;
                                $aaObj->available_companies_paid = ++$aa->available_companies_paid;
                                $aaObj->available_companies_number = $aa->available_companies_number+1000;
                                $aaObj->available_companies_date_added = date('Y-m-d H:i:s');
                                $aaObj->available_companies_user_id = $this->data['account']->user_id;
                                $aaObj->save();
                            } else {
                                $orderedIds[] = $preId;
                            }
                        }
                    }
                    $orderObj = new Order();
                    $orderObj->order_type = ORDER_TYPE_FINISHED;
                    $orderObj->order_price_company = $this->data['options']['company_price'];
                    $orderObj->order_price_total = count($orderedIds)*$this->data['options']['company_price'];
                    $orderObj->order_user_id = !empty($this->data['account']) ? $this->data['account']->user_id : null;
                    $orderObj->order_company_ids = implode(',', $orderedIds);
                    $orderObj->order_email = urldecode(urldecode($httpParsedResponseAr['EMAIL']));
                    $orderObj->order_date = date('Y-m-d H:i:s');
                    $orderObj->save();
                    $this->session->unset_userdata('products');
                    $this->session->unset_userdata('paypal_products');
                    redirect('account/companies');
                } else {
                    echo '<div style="color:red"><b>GetTransactionDetails failed:</b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
                    echo '<pre>';
                    print_r($httpParsedResponseAr);
                    echo '</pre>';

                }

            }else{
                echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
                echo '<pre>';
                print_r($httpParsedResponseAr);
                echo '</pre>';
            }
        }
        exit;
    }

    public function update()
    {
        if (empty($this->data['account']))
        {
            $this->session->set_userdata('notification', 'You have to login or register before buying.</a>');
            redirect('login');
        }

        //empty cart by distroying current session
        if(isset($_GET["emptycart"]) && $_GET["emptycart"]==1)
        {
            $return_url = base64_decode($_GET["return_url"]); //return url
            $this->session->unset_userdata('products');
            $this->session->unset_userdata('paypal_products');
            header('Location:'.$return_url);
        }

        if(isset($_POST["type"]) && $_POST["type"]=='add')
        {
            $submit = $this->input->post('submit');
            $submit = (!empty($submit) && $submit == 'all') ? 'all' : 'page';
            $product_code 	= filter_var($_POST["product_code"], FILTER_SANITIZE_STRING); //product code
            $product_qty 	= filter_var($_POST["product_qty"], FILTER_SANITIZE_NUMBER_INT); //product code
            $return_url 	= base64_decode($_POST["return_url"]); //return url

            //MySqli query - get details of item from db using product code
            $new_product = array();

            $countIds = 0;
            if ($product_code == 'AVAILABLE1000') {
                $obj = new stdClass();
                $obj->company_name = 'Package for 1000 available companies';
                $new_product[] = array('name'=>$obj->company_name, 'code'=>'AVAILABLE1000', 'qty'=>1, 'price'=>10);
                $countIds = 1;
            } else {
                if ($submit == 'all') {
                    $objFilter = $this->session->userdata('filter');
                    $objFilter = !empty($objFilter) ? $objFilter : array();
                    $companyIds = $this->company->filter($objFilter, null, null, 'ids');
                    $idsStringArray = array();
                    if (!empty($companyIds)) {
                        foreach($companyIds as $co) {
                            $idsStringArray[] = $co->company_id;
                            $countIds++;
                        }
                    }
                } else {
                    $idsStringArray = explode(',', $product_code);
                    $countIds       = count($idsStringArray);
                }

                if ($countIds > 50) {
                    $orderObj = new Order();
                    $orderObj->order_user_id = $this->data['account']->user_id;
                    $orderObj->order_company_ids = implode(',', $idsStringArray);
                    $orderObj->order_date = date('Y-m-d H:i:s');
                    $orderObj->save();

                    $new_product[] = array('name'=>'Package for '.$countIds.' companies', 'code'=>'PACKAGE'.$orderObj->order_id, 'qty'=>$product_qty, 'price'=>($this->data['options']['company_price']*$countIds));
                } else {
                    foreach($idsStringArray as $product_code) {
                        $obj = $this->company->load($product_code);
                        if (!empty($obj)) $new_product[] = array('name'=>$obj->company_name, 'code'=>$product_code, 'qty'=>$product_qty, 'price'=>$this->data['options']['company_price']);
                    }
                }
            }


            $sessionProducts = $this->session->userdata('products');
            if(!empty($sessionProducts)) {
                foreach ($sessionProducts as $cart_itm) {
                    $product[] = array('name'=>$cart_itm["name"], 'code'=>$cart_itm["code"], 'qty'=>$cart_itm["qty"], 'price'=>$cart_itm["price"]);
                    //$product[] = array('name'=>$cart_itm["name"], 'code'=>$cart_itm["code"], 'qty'=>$cart_itm["qty"], 'price'=>$cart_itm["price"]);
                }

                $arrayMergeProducts = array_merge($product, $new_product);
                $this->session->set_userdata('products', $arrayMergeProducts);
            } else {
                $this->session->set_userdata('products', $new_product);
            }

            if ($countIds > 1) {
                $this->session->set_userdata('notification', $countIds.' companies were successfully added to your cart. Check here you cart <b><a style="color:#fff"  href="'.site_url('account/cart').'">'.site_url('account/cart').'</a></b>');
            } else {
                $this->session->set_userdata('notification', 'Company "'.$obj->company_name.'" was successfully added to your cart. Check here you cart <b><a style="color:#fff"  href="'.site_url('account/cart').'">'.site_url('account/cart').'</a></b>');
            }

            $uniqueProducts = array();
            $currentProducts = $this->session->userdata('products');
            if(!empty($currentProducts)) {
                foreach($currentProducts as $keycp => $cp) {
                    $uniqueProducts[$cp['code']] = $cp;
                }
            }
            $this->session->set_userdata('products', $uniqueProducts);

            //redirect back to original page
            header('Location:'.$return_url);
        }
//remove item from shopping cart
        $sessionProducts = $this->session->userdata('products');
        if(isset($_GET["removep"]) && isset($_GET["return_url"]) && isset($sessionProducts))
        {
            $product_code 	= $_GET["removep"]; //get the product code to remove
            $return_url 	= base64_decode($_GET["return_url"]); //get return url


            foreach ($sessionProducts as $cart_itm) //loop through session array var
            {
                if($cart_itm["code"]!=$product_code){ //item does,t exist in the list
                    $product[] = array('name'=>$cart_itm["name"], 'code'=>$cart_itm["code"], 'qty'=>$cart_itm["qty"], 'price'=>$cart_itm["price"]);
                }

                //create a new product list for cart
                $product = !empty($product) ? $product : array();
                $this->session->set_userdata('products', $product);
            }

            //redirect back to original page
            header('Location:'.$return_url);
        }
    }
}