<?php if ( ! defined('BASEPATH')) exit;
ob_start();
class Excel extends Frontend_Controller {

    public function users()
    {
        $this->load->helper('php-excel');
        $query = $this->user->get();
        $fields = $field_array[] = array ("User id", "User name", "User email", "User Date Confirmation", "User Date Registration");
        foreach ($query as $row)
        {
            $data_array[] = array( $row->user_id, $row->user_name, $row->user_email, $row->user_date_confirmation, $row->user_date_registration);
        }
        $xls = new Excel_XML;
        $xls->addArray ($field_array);
        $xls->addArray ($data_array);
        $xls->generateXML ("users");
        exit;
    }

    public function paidCompanies()
    {
        $obj['order_user_id'] = !empty($this->data['account']) ? $this->data['account']->user_id : null;
        $obj['order_type'] = ORDER_TYPE_FINISHED;
        $orders = $this->order->search($obj, null, null, false);
        $companies = array();
        if (!empty($orders)) {
            foreach($orders as $order) {
                $companyIds = explode(',', $order->order_company_ids);
                foreach ($companyIds as $companyId) {
                    $company = $this->company->load(trim($companyId));
                    if (!empty($company)) {
                        $companies[] = (object) array_merge((array) $company, (array) $order);
                    }
                }
            }
        }
        $this->load->helper('php-excel');
        $fields = $field_array[] = array("Id", "Name", "Contact", "Position", "Domain", "Setup Date", "Address", "Country", "City", "Phone", "Website", "Capital Invested", "Fax", "Email", "Employee", "Contacts", "Turnover");
        foreach ($companies as $company)
        {
            $data_array[] = array($company->company_id, $company->company_name, $company->company_contact, $company->company_position, $company->company_domain, $company->company_setup_date, $company->company_address, $company->company_country_id, $company->company_city, $company->company_phone, $company->company_website, $company->company_capital_invested, $company->company_fax, $company->company_email, $company->company_employee, $company->company_contacts, $company->company_turnover);
        }
        $xls = new Excel_XML;
        $xls->addArray($field_array);
        $xls->addArray($data_array);
        $xls->generateXML("companies");
    }
}