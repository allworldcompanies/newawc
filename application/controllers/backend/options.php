<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Options extends Backend_Controller {

    public function index($page = 0)
    {
        $this->load->view('backend/header');
        $this->load->view('backend/options-index', $this->data);
        $this->load->view('backend/footer');
    }

    public function option($id = null)
    {
        if (!empty($id)) $this->data['option'] = $this->option->load($id);
        $this->load->view('backend/header');
        $this->load->view('backend/options-option', $this->data);
        $this->load->view('backend/footer');
    }

    public function save($id = null)
    {
        $object = new Option();
        $object->option_id = $id;
        $object->option_name = $this->input->post('option_name');
        $object->option_value = $this->input->post('option_value');
        if (empty($id)) $object->option_date = date('Y-m-d H:i:s');
        $object->save();
        redirect('backend/options');
    }

    public function delete($id)
    {
        $object = new Option();
        $object->option_id = $id;
        $object->delete();
        redirect('backend/options');
    }
}