<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends Backend_Controller {

    public function index($page = 0)
    {
        $page =  (!isset($page) || $page == 0) ? 1 : $page;
        // ----------> Start Pagination
        $this->db->where('order_type',ORDER_TYPE_FINISHED);
        $this->load->library("pagination");
        $config                             = array();
        $config["base_url"]                 = site_url('backend/orders');
        $config["total_rows"]               = $this->db->count_all_results('new_orders');
        $config["per_page"]                 = 10;
        $config["uri_segment"]              = 3;
        $config['use_page_numbers']         = TRUE;
        $this->pagination->initialize($config);
        $orderObj['order_type'] = ORDER_TYPE_FINISHED;
        $orderObj['order_price_total > '] = 0;
        $this->data['orders']            = $this->order->search($orderObj, ($page-1)*$config["per_page"], $config['per_page']);
        $this->data["links"]                = $this->pagination->create_links();
        // ----------> End Pagination

        $this->load->view('backend/header');
        $this->load->view('backend/orders-index', $this->data);
        $this->load->view('backend/footer');
    }

    public function order($id = null)
    {
        if (!empty($id)) $this->data['order'] = $this->order->load($id);
        $this->load->view('backend/header');
        $this->load->view('backend/orders-order', $this->data);
        $this->load->view('backend/footer');
    }
}