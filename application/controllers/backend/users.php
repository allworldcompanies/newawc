<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Backend_Controller {

    public function index($page = 0)
    {
        $page =  (!isset($page) || $page == 0) ? 1 : $page;
        // ----------> Start Pagination
        $this->load->library("pagination");
        $config                             = array();
        $config["base_url"]                 = site_url('backend/users');
        $config["total_rows"]               = $this->db->count_all_results('new_users');
        $config["per_page"]                 = 10;
        $config["uri_segment"]              = 3;
        $config['use_page_numbers']         = TRUE;
        $this->pagination->initialize($config);
        $toL = (int)($page-1)*$config["per_page"];
        $this->data['users']                = $this->user->search(array(), $toL, $config['per_page']);
        $this->data["links"]                = $this->pagination->create_links();
        // ----------> End Pagination

        $this->load->view('backend/header');
        $this->load->view('backend/users-index', $this->data);
        $this->load->view('backend/footer');
    }

    public function user($id = null)
    {
        if (!empty($id)) $this->data['user'] = $this->user->load($id);
        $this->load->view('backend/header');
        $this->load->view('backend/users-user', $this->data);
        $this->load->view('backend/footer');
    }
}