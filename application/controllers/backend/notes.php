<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notes extends Backend_Controller {

    public function index($page = 0)
    {
        $this->data['notes'] = $this->note->get();
        foreach($this->data['notes'] as $k => $n) {
            $this->data['notes'][$k]->user = $this->user->load($n->note_user_id);
        }
        $this->load->view('backend/header');
        $this->load->view('backend/notes-index', $this->data);
        $this->load->view('backend/footer');
    }

    public function option($id = null)
    {
        if (!empty($id)) $this->data['option'] = $this->option->load($id);
        $this->load->view('backend/header');
        $this->load->view('backend/notes-note', $this->data);
        $this->load->view('backend/footer');
    }

    public function save($id = null)
    {
        $object = new Note();
        $object->note_user_id = $this->data['account']->user_id;
        $object->note_content = $this->input->post('note_content');
        $object->note_date = date('Y-m-d H:i:s');
        $object->save();
        redirect('backend/notes');
    }

    public function delete($id)
    {
        $object = new Note();
        $object->note_id = $id;
        $object->delete();
        redirect('backend/notes');
    }
}