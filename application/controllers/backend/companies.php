<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies extends Backend_Controller {

    public function index($page = 0)
    {
     $search  =  $this->input->post('search',true);
	
	 if($search != '')
	 {
		$field  =  $this->input->post('id',true);//die();
	 	if($field !='')
		$where = array($field=>$search);
		else
		$where = array('company_name'=>$search);
	 	$data =array('company_id',$field);   
	 	$dta  = $this->search->like('new_companies',$where,$data);
		$this->data['companies'] = $dta;
	 }
	  else{	
		
		$page =  (!isset($page) || $page == 0) ? 1 : $page;
        // ----------> Start Pagination
        $this->load->library("pagination");
        $config                             = array();
        $config["base_url"]                 = site_url('backend/companies');
        $config["total_rows"]               = $this->db->count_all_results('new_companies');
        $config["per_page"]                 = 10;
        $config["uri_segment"]              = 3;
        $config['use_page_numbers']         = TRUE;
        $this->pagination->initialize($config);
        $this->data['companies']            = $this->company->search(array(), ($page-1)*$config["per_page"], $config['per_page'],true,1);
        $this->data["links"]                = $this->pagination->create_links();
        // ----------> End Pagination

       
	  }
	   $this->load->view('backend/header');
        $this->load->view('backend/companies-index', $this->data);
        $this->load->view('backend/footer');
	}

    public function company($id = null)
    {
		//print_r($this->data);die();
        if (!empty($id)) $this->data['company'] = $this->company->load($id);
        $this->load->view('backend/header');
        $this->load->view('backend/companies-company', $this->data);
        $this->load->view('backend/footer');
    }

    public function fields_order()
    {
        if (!empty($_POST)) {
            $finalOrder = array();
            $order = $this->input->post('order');
            foreach ($order as $o) {
                $finalOrder[] = $o;
            }
            $this->db->query('DELETE FROM new_default_info WHERE default_info_name = "company_fields_order"');
            $obj = new Default_info();
            $obj->default_info_name = 'company_fields_order';
            $obj->default_info_array = json_encode($finalOrder);
            $obj->default_info_date_added = date('Y-m-d H:i:s');
            $obj->save();

            redirect('backend/companies/fields_order');
        } else {
            $fieldsDB = $this->db->query('SELECT default_info_array FROM new_default_info WHERE default_info_name = "company_fields_order"')->row();
            if (!empty($fieldsDB)) $fieldsDB = json_decode($fieldsDB->default_info_array);
            if (!empty($fieldsDB)) {
                $this->data['fields'] = $fieldsDB;
            } else {
                $columns = $this->db->query('SHOW COLUMNS FROM new_companies')->result();
                if (!empty($columns)) {
                    $this->data['fields'] = array();
                    foreach ($columns as $column) {
                        $this->data['fields'][] = $column->Field;
                    }
                }
            }

            $this->load->view('backend/header');
            $this->load->view('backend/companies-fields-order', $this->data);
            $this->load->view('backend/footer');
        }
    }

    public function fields_order_reset()
    {
        $this->db->query('DELETE FROM new_default_info WHERE default_info_name = "company_fields_order"');
        redirect('backend/companies/fields_order');
    }

    public function save($id = null)
    {
        $obj = new Company();
        $obj->company_id = $id;
        $obj->company_name = $this->input->post('company_name');
        $obj->company_address = $this->input->post('company_address');
        $obj->company_contact = $this->input->post('company_contact');
        $obj->company_position = $this->input->post('company_position');
        $obj->company_phone = $this->input->post('company_phone');
        $obj->company_email = $this->input->post('company_email');
        $obj->company_fax = $this->input->post('company_fax');
        $obj->company_city = $this->input->post('company_city');
        $obj->company_employee = (int)$this->input->post('company_employee');
        $obj->company_turnover = (float)$this->input->post('company_turnover');
        $obj->company_domain = $this->input->post('company_domain');
        $obj->company_subdomain = $this->input->post('company_subdomain');
        $obj->company_country_id = $this->input->post('company_country_id');
        $obj->company_contacts = $this->input->post('company_contacts');
        $obj->company_capital_invested = $this->input->post('company_capital_invested');
        $obj->company_setup_date = $this->input->post('company_setup_date');
		$obj->seenunseen = '2';
        $obj->save();
        if (empty($id)) $obj->company_date_added = date('Y-m-d H:i:s');
        $obj->save();
        redirect('backend/companies');
    }

    public function delete($id)
    {
        $object = new Company();
        $object->company_id = $id;
        $object->delete();
        redirect('backend/companies');
    }
   
    public function search()
   {
    
     $search  =  $this->input->post('search',true);
	 $where = array('company_name'=>$search);
	 $data =array('company_id','company_name');   
	 $dta  = $this->search->like('new_companies',$where,$data);
	 //print_r($dta);
	 if($dta != '')
	 {
		$this->data['companies'] = $dta;
	    $this->load->view('backend/header');
        $this->load->view('backend/companies-index', $this->data);
        $this->load->view('backend/footer');
		 
	 }  	
   
   }    

      public function seenunseen()
	{
	     $ids	= $this->input->post('ids',true);
		 $exp = explode(",",$ids);
		 //print_r($exp);
		 $this->load->library('user_agent');

        if ($this->agent->is_browser())
       {
       $agent = $this->agent->browser().' '.$this->agent->version();
       }
	  //echo  $this->session->userdata('user_id');die();
	  // print_r($agent);
		if($agent != '')
		{
		 for($i=0;$i<count($exp);$i++)
		 {	
		  $select = "update new_companies set seenunseen = '1'  where company_id = '".$exp[$i]."'";
		  $query = mysql_query($select);
		  print_r('true');	
		 }
		}
	} 

        
	 public function update()
	 {
		$id = $this->input->post('id',true);
		$name = $this->input->post('name',true);
		$contact =  $this->input->post('contact',true);
		$position = $this->input->post('position',true);
		$domain = $this->input->post('domain',true);
		$setupdate = $this->input->post('setupdate',true);
		$address = $this->input->post('address',true);
		$country = $this->input->post('country',true);
		$city =  $this->input->post('city',true);
		$fax = $this->input->post('fax',true);
		$email = $this->input->post('email',true);
		$employee = $this->input->post('employee',true);
		$contacts = $this->input->post('contacts',true);
	    $array =array
		(
		  'company_name'=>$name,
		  'company_contact'=>$contact,
		  'company_position'=>$position,
		  'company_domain'=>$domain,
		  'company_setup_date'=>$setupdate,
		  'company_address'=>$address,
		  'company_country_id'=>$country,
		  'company_city'=>$city,
		  'company_fax'=>$fax,
		  'company_email'=>$email,
		  'company_employee'=>$employee,
		  'company_contacts'=>$contacts,
		  'seenunseen'=>2
		);	
		 $where = array('company_id'=>$id);
		 $this->default_info->updatedata('new_companies',$where,$array);
		 echo 'true';
		//print_r($array); 
     }	

}