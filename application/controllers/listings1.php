<?php if ( ! defined('BASEPATH')) exit;
class Listings extends Frontend_Controller {

    public function index($query = 1)
    {
        $startTime = microtime_float();
        $filterGet = $this->input->get_post('filter', true);//die();
        if (!empty($filterGet))
        {
            $obj['company_name']            = $this->input->get_post('company_name', true);
            $obj['company_country_id']      = $this->input->get_post('company_country_id', true);
            $obj['company_city']            = $this->input->get_post('company_city', true);
            $obj['company_domain']          = $this->input->get_post('company_domain', true);
            $obj['company_contact']         = $this->input->get_post('company_contact', true);
            $obj['company_subdomain']       = $this->input->get_post('company_subdomain', true);
            $obj['company_setup_date']      = $this->input->get_post('company_setup_date', true);
            $obj['company_address']         = $this->input->get_post('company_address', true);
            $obj['company_phone']           = $this->input->get_post('company_phone', true);
            $obj['company_website']         = $this->input->get_post('company_website', true);
            $obj['company_fax']             = $this->input->get_post('company_fax', true);
            $obj['company_email']           = $this->input->get_post('company_email', true);
            $obj['with_email']              = $this->input->get_post('with_email', true);
            $obj['with_website']            = $this->input->get_post('with_website', true);
            $obj['with_phone']              = $this->input->get_post('with_phone', true);
            $obj = array_filter($obj);
            $this->session->set_userdata('filter', $obj);
            redirect('listings');
        }
        else
        {
            $obj = $this->session->userdata('filter');
            $this->data['filter'] = !empty($obj) ? $obj : array();
			   // print_r($this->data['filter']);die();
			
        }

        if (!empty($this->data['filter'])) {
            $searchObj = new Be_search();
            $searchObj->search_user_id = !empty($this->data['account']) ? $this->data['account']->user_id : null;
            $searchObj->search_array = json_encode($this->data['filter']);
            $searchObj->search_date_added = date('Y-m-d H:i:s');
            $searchObj->save();
        }

        // ----------> Start Pagination
        $this->load->library("pagination");
        $config                             = array();
        $config["base_url"]                 = site_url('listings');
        //$config["total_rows"]               = $this->company->filter($this->data['filter'], null, null, 'count');
        $config["total_rows"]               = 25;
        $config["per_page"]                 = 25;
        $config["uri_segment"]              = 2;
        $config['use_page_numbers']         = TRUE;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $this->data['page'] = $page;

        $this->data['companies']            = $this->company->filter($this->data['filter'], (int)$page*$config['per_page'], (int)$config['per_page'], 'object'/*, ' ORDER BY RAND() '*/);
        $this->data["links"]                = $this->pagination->create_links();
        $this->data['total_companies']      = $config["total_rows"];
        // ----------> End Pagination
        $endTime = microtime_float();
        $this->data['countries']            = $this->country->getCountriesWithCompanies();
        $this->data['total_time']           = $endTime-$startTime;

        $this->data['orderedIds'] = array();
        if (!empty($this->data['account'])) {
            $orderObj['order_user_id'] = $this->data['account']->user_id;
            $orderObj['order_type <> '] = ORDER_TYPE_SESSION;
            $orders = $this->order->search($orderObj);

            $this->data['orderedIds'] = array();
            foreach($orders as $order) {
                $orderCompanyIdsArray = explode(',', $order->order_company_ids);
                if (!empty($orderCompanyIdsArray)) {
                    foreach($orderCompanyIdsArray as $ocid) {
                        $ocid = trim($ocid);
                        if (!empty($ocid))$this->data['orderedIds'][] = $ocid;
                    }
                }
            }
        }

        $thirdSegment = $this->uri->segment(3);

        if (!empty($thirdSegment) && $thirdSegment == 'json') {
            $this->load->view('template/listings', $this->data);
        } else {
            $this->data['listings'] = $this->load->view('template/listings', $this->data, true);

            $this->load->view('header', $this->data);
            $this->load->view('listings-index', $this->data);
            $this->load->view('footer');
        }
    }

    public function country($id)
    {
        $obj['company_country_id']          = array($id);
        $obj                                = array_filter($obj);
        $this->session->set_userdata('filter', $obj);
        redirect('listings');
    }

    public function company($id)
    {
        $this->data['orderedIds'] = array();
        if (!empty($this->data['account'])) {
            $orderObj['order_user_id'] = $this->data['account']->user_id;
            $orderObj['order_type <> '] = ORDER_TYPE_SESSION;
            $orders = $this->order->search($orderObj);

            $this->data['orderedIds'] = array();
            foreach($orders as $order) {
                $orderCompanyIdsArray = explode(',', $order->order_company_ids);
                if (!empty($orderCompanyIdsArray)) {
                    foreach($orderCompanyIdsArray as $ocid) {
                        $ocid = trim($ocid);
                        if (!empty($ocid))$this->data['orderedIds'][] = $ocid;
                    }
                }
            }
        }

        $fieldsDB = $this->db->query('SELECT default_info_array FROM new_default_info WHERE default_info_name = "company_fields_order"')->row();
        if (!empty($fieldsDB)) $fieldsDB = json_decode($fieldsDB->default_info_array);
        if (!empty($fieldsDB)) {
            $this->data['fields'] = $fieldsDB;
        } else {
            $columns = $this->db->query('SHOW COLUMNS FROM new_companies')->result();
            if (!empty($columns)) {
                $this->data['fields'] = array();
                foreach ($columns as $column) {
                    $this->data['fields'][] = $column->Field;
                }
            }
        }

        $this->data['company'] = $this->company->load($id);
        $this->data['title'] = $this->data['company']->company_name;
        $this->data['companies'] = $this->company->getRelatedCompaniesByCompanyId($this->data['company']->company_id);
        $this->load->view('header', $this->data);
        $this->load->view('listings-company', $this->data);
        $this->load->view('footer');
    }

    public function save()
    {
        if (!empty($this->data['account']))
        {
            $obj = $this->session->userdata('filter');
            $searchObj = new Search();
            $searchObj->search_user_id = !empty($this->data['account']) ? $this->data['account']->user_id : null;
            $searchObj->search_array = json_encode($obj);
            $searchObj->search_date_added = date('Y-m-d H:i:s');
            $searchObj->save();
            $this->session->set_userdata('notification', 'Your query was successfully saved. You can check here <a style="color:#fff" href="'.site_url('account/searches').'"><b>'.site_url('account/searches').'</b></a>');
            redirect('listings');
        }
        else
        {
            $this->session->set_userdata('notification', 'You have to login or register before you will save a search.</a>');
            redirect('login');
        }
    }

    public function favorite($id)
    {
        if (!empty($this->data['account']))
        {
            $obj = new Favorite();
            $obj->favorite_user_id = !empty($this->data['account']) ? $this->data['account']->user_id : null;
            $obj->favorite_company_id = $id;
            $obj->favorite_date_added = date('Y-m-d H:i:s');
            $obj->save();
            $this->session->set_userdata('notification', 'This company was successfully added to favorites. You can check here all favorites companies <a style="color:#fff" href="'.site_url('account/favorites').'"><b>'.site_url('account/favorites').'</b></a>');
            redirect('listings');
        }
        else
        {
            $this->session->set_userdata('notification', 'You have to login or register before you will add a company to favorites.</a>');
            redirect('login');
        }
    }
	public function name_fetch()
    {
		$sql = "select company_name from new_companies limit 0,20";
		$result=$this->db->query($sql);//print_r($result);die();
		$str="";
		$companyname=array();
		$row =$result->result();
		$c = count($row);
		$i=0;
		foreach($row as $r){
		//while ($row =$result->row()) {
				//echo $r->company_name;
				$companyname[$i++]=$r->company_name;
				
		}
		echo json_encode($companyname);
	}
}