<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import_maria extends CI_Controller {
    /**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see http://codeigniter.com/user_guide/general/urls.html
	*/

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common_model');
        $local_set 		= array(
		'path_upld_file_unchk' 	=> 'file_upl_unchk',
		'path_upld_file_chked' 	=> 'file_upl_chked',
		'con_user' 				=> 'abhi',
		'con_psw' 				=> 'm@r$adBB88%2',
		'con_database' 			=> 'tbl_comp_data_test',
		'con_host' 				=> '54.161.168.1'
        );

        //error_reporting(E_ALL);      
		//  ini_set('display_errors', 			TRUE);
		// ini_set('display_startup_errors',	TRUE);
        date_default_timezone_set('Europe/London');
        define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br>');
        /**		Include PHPExcel		*/
        require_once APPPATH . '/Classes/PHPExcel.php';
        /**		Include PHPExcel_IOFactory		*/
        require_once APPPATH . '/Classes/PHPExcel/IOFactory.php';
		}
		public function index()
		{
			//echo "adsf";exit;
			global $columndb;
			global $tblname;
			$data				 = array();
			$data['dataupload']	 =	'';
			$table_exists		 = False;
			$columndb			 = array();
			$colmn_exists		 = False;
			if (isset($_POST['send_file'])) {
				$tblname 		= $_POST['v_upl_tbname'];
				$_SESSION['tblname'] = $tblname;
				if(!empty($tblname)) {
					$table_exists		= $this->common_model->table_exists($tblname,2);
					if($table_exists)	{
						$columndb 			= $this->common_model->get_colummns_db($tblname,2);
						if(count($columndb)>0)	{
							$colmn_exists 	= true;
							}
							}
							}
							if($colmn_exists)	{
								$date 			= date_create();
								$uploaddir 		= dirname($_SERVER["SCRIPT_FILENAME"]).'/arch_file/';
								$file_name		= date_format($date, 'YmdHis'). '_' . basename($_FILES['v_upl_file']['name']);
								$uploadfile 	= $uploaddir.$file_name;
								$config['upload_path'] = $uploaddir;
								$config['allowed_types'] = 'xls|xlsx|csv';
								$config['file_name'] = $file_name;
								//print_r($config);die();
								$this->load->library('upload', $config);
								$this->common_model->scrie_log("$uploadfile = ".$uploadfile);
								if ($this->upload->do_upload("v_upl_file")) {
									$data['dataupload']	 .= "<div id='uploadcontent'>File is valid, and was successfully uploaded.\n";
									$data['dataupload']	 .= date('H:i:s') ;
									if (substr($uploadfile, -3) == "xls"){
										$objReader = PHPExcel_IOFactory::createReader('Excel5');
										}elseif(substr($uploadfile, -4) == "xlsx"){
											$objReader = PHPExcel_IOFactory::createReader('Excel2007');
											}elseif (substr($uploadfile, -3) == "csv"){
												$objReader = PHPExcel_IOFactory::createReader('CSV');
												}
												$objPHPExcel 	= $objReader->load("$uploadfile");
												$_SESSION['uploadfile'] = $uploadfile;
												$this->common_model->get_table(5, $uploadfile, $columndb, $tblname,2);
												$data['dataupload']	 .= "<br><br><br><br><br>";
												$data['dataupload']	 .= '<input type="button" name="valid_map" value="Generate Mapping" onClick="generate_map(1)">';
												$data['dataupload']	 .= '<br><br>';
												$data['dataupload']	 .= '<div id="show_mapping" style="layer-background-color:lightyellow;background-color:lightyellow;width:120;visibility:hidden;border:2px solid black;padding:0px">					</div></div>';
												$this->load->view('header');
												$this->load->view('import', $data);
												$this->load->view('footer');
												exit;
												}
												else {
													echo $this->upload->display_errors();
													echo "Possible file upload attack!\n";
													$data['dataupload']	 =	$this->common_model->prepare_upload(2);
													}
													}
													else 	{
														echo "Table not exists.\n";
														$data['dataupload']	 =	$this->common_model->prepare_upload(2);
														}
														}
														elseif(isset($_POST['do_process'])) {
															$this->common_model->scrie_log("do_process");
															$tblname 		= $_SESSION['tblname'];
															if(!empty($tblname)) {
																$table_exists		= $this->common_model->table_exists($tblname,2);
																if($table_exists)	{
																	$columndb	= $this->common_model->get_colummns_db($tblname,2);
																	$map0		= (isset($_POST['map0'])) ? $_POST['map0']: '';
																	$map1		= (isset($_POST['map1'])) ? $_POST['map1']: '';
																	$this->common_model->read_for_import($map0, $map1,2);
																	$data['dataupload']	 =	$this->common_model->prepare_upload(2);
																	}
																	else 	{
																		echo "Table not exists.\n";
																		$data['dataupload']	 =	$this->common_model->prepare_upload(2);
																		}
																		}
																		}
																		else	{
																			$data['dataupload']	 =	$this->common_model->prepare_upload(2);
																			}
																			$this->load->view('header');
																			$this->load->view('import', $data);
																			$this->load->view('footer');
																			}
																			}
																			/* End of file home.php *//* Location: ./application/controllers/home.php */