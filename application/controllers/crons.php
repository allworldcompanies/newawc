<?php exit; if ( ! defined('BASEPATH')) exit;

class Crons extends CI_Controller {

    public function files()
    {
        $file = $this->file->getLastFile();
        if (!empty($file)) {
            $obj = new File();
            $obj->upload_id = $file->upload_id;
            $obj->upload_status = UPLOAD_STATUS_PARSING;
            $obj->save();
            $inputFileName = dirname(__FILE__) . '/../../waiting/'.$file->upload_filename;
            if (($handle = fopen($inputFileName, "r")) !== FALSE) {
                while (($company = fgetcsv($handle, 5000, ",")) !== FALSE) {
                    $obj = new Company();
                    $obj->company_name = $company[0];
                    $obj->company_setup_date = date('Y-m-d H:i:s', strtotime(substr($company[4], -4)));
                    $obj->company_city = $company[6];
                    $obj->company_district = $company[7];
                    $obj->company_address = $company[5];
                    $obj->company_phone = $company[9];
                    $obj->company_website = $company[12];
                    $obj->company_domain = $company[3];
                    $obj->company_contact = $company[1];
                    $obj->company_position = $company[2];
                    if (strlen($company[14] > 3)) $obj->company_email = $company[14];
                    $obj->company_country_id = 46;
                    $obj->company_turnover = $company[11];
                    //$obj->company_employee = $company[101];
                    $obj->save();
                    unset($obj);
                    unset($company);
                }
                fclose($handle);
            }

            $obj = new File();
            $obj->upload_id = $file->upload_id;
            $obj->upload_status = UPLOAD_STATUS_FINISHED;
            $obj->save();

            file_put_contents(PUBLIC_FOLDER."cron-logs.txt", 'cron-opened at '.date('Y-m-d H:i:s', time())." and was uploaded ".$file->upload_filename."\n", FILE_APPEND);
            exit;
        }
    }

    public function files2()
    {
        $file = $this->file->getLastFile();
        if (!empty($file)) {
            $obj = new File();
            $obj->upload_id = $file->upload_id;
            $obj->upload_status = UPLOAD_STATUS_PARSING;
            $obj->save();
            $inputFileName = dirname(__FILE__) . '/../../waiting/'.$file->upload_filename;
            if (($handle = fopen($inputFileName, "r")) !== FALSE) {
                while (($company = fgetcsv($handle, 5000, ",")) !== FALSE) {
                    $c2009 = array();
                    $c2009['Imobilizate'] = $company[11];
                    $c2009['Circulante'] = $company[12];
                    $c2009['Stocuri'] = $company[13];
                    $c2009['Creante'] = $company[14];
                    $c2009['Casa Conturi'] = $company[15];
                    $c2009['Cheltuiele Avans'] = $company[16];
                    $c2009['Datorii'] = $company[17];
                    $c2009['Venituri Avans'] = $company[18];
                    $c2009['Provizioane'] = $company[19];
                    $c2009['Capitaluri'] = $company[20];
                    $c2009['Capital'] = $company[21];
                    $c2009['Patrimoniul Regiei'] = $company[22];
                    $c2009['Patrimoniul Public'] = $company[23];
                    $c2009['Venituri Totale'] = $company[25];
                    $c2009['Cheltuieli Totale'] = $company[26];
                    $c2009['Profit Brut'] = $company[27];
                    $c2009['Pierdere Brut'] = $company[28];
                    $c2009['Profit Net'] = $company[29];;
                    $c2009['Pierdere Net'] = $company[30];
                    $c2009['Salariati'] = $company[31];
                    $c2009['Tipul'] = $company[32];
                    $c2010 = array();
                    $c2010['Imobilizate'] = $company[33];
                    $c2010['Circulante'] = $company[34];
                    $c2010['Stocuri'] = $company[35];
                    $c2010['Creante'] = $company[36];
                    $c2010['Casa Conturi'] = $company[37];
                    $c2010['Cheltuiele Avans'] = $company[38];
                    $c2010['Datorii'] = $company[39];
                    $c2010['Venituri Avans'] = $company[40];
                    $c2010['Provizioane'] = $company[41];
                    $c2010['Capitaluri'] = $company[42];
                    $c2010['Capital'] = $company[43];
                    $c2010['Patrimoniul Regiei'] = $company[44];
                    $c2010['Patrimoniul Public'] = $company[45];
                    $c2010['Venituri Totale'] = $company[47];
                    $c2010['Cheltuieli Totale'] = $company[48];
                    $c2010['Profit Brut'] = $company[49];
                    $c2010['Pierdere Brut'] = $company[50];
                    $c2010['Profit Net'] = $company[51];;
                    $c2010['Pierdere Net'] = $company[52];
                    $c2010['Salariati'] = $company[53];
                    $c2010['Tipul'] = $company[54];
                    $c2011 = array();
                    $c2011['Imobilizate'] = $company[55];
                    $c2011['Circulante'] = $company[56];
                    $c2011['Stocuri'] = $company[57];
                    $c2011['Creante'] = $company[58];
                    $c2011['Casa Conturi'] = $company[59];
                    $c2011['Cheltuiele Avans'] = $company[60];
                    $c2011['Datorii'] = $company[61];
                    $c2011['Venituri Avans'] = $company[62];
                    $c2011['Provizioane'] = $company[63];
                    $c2011['Capitaluri'] = $company[64];
                    $c2011['Capital'] = $company[65];
                    $c2011['Patrimoniul Regiei'] = $company[66];
                    $c2011['Patrimoniul Public'] = $company[67];
                    $c2011['Venituri Totale'] = $company[69];
                    $c2011['Cheltuieli Totale'] = $company[70];
                    $c2011['Profit Brut'] = $company[71];
                    $c2011['Pierdere Brut'] = $company[72];
                    $c2011['Profit Net'] = $company[73];;
                    $c2011['Pierdere Net'] = $company[74];
                    $c2011['Salariati'] = $company[75];
                    $c2011['Tipul'] = $company[76];
                    $c2012 = array();
                    $c2012['Imobilizate'] = $company[77];
                    $c2012['Circulante'] = $company[78];
                    $c2012['Stocuri'] = $company[79];
                    $c2012['Creante'] = $company[80];
                    $c2012['Casa Conturi'] = $company[81];
                    $c2012['Cheltuiele Avans'] = $company[82];
                    $c2012['Datorii'] = $company[83];
                    $c2012['Venituri Avans'] = $company[84];
                    $c2012['Provizioane'] = $company[85];
                    $c2012['Capitaluri'] = $company[86];
                    $c2012['Capital'] = $company[87];
                    $c2012['Patrimoniul Regiei'] = $company[88];
                    $c2012['Patrimoniul Public'] = $company[89];
                    $c2012['Venituri Totale'] = $company[91];
                    $c2012['Cheltuieli Totale'] = $company[92];
                    $c2012['Profit Brut'] = $company[93];
                    $c2012['Pierdere Brut'] = $company[94];
                    $c2012['Profit Net'] = $company[95];;
                    $c2012['Pierdere Net'] = $company[96];
                    $c2012['Salariati'] = $company[97];
                    $c2012['Tipul'] = $company[98];
                    $obj = new Company();
                    $obj->company_url = $company[99];
                    $obj->company_name = $company[0];
                    $obj->company_cui = $company[1];
                    $obj->company_inchiriere = $company[2];
                    $obj->company_setup_date = end(explode('/', $company[3]));
                    $obj->company_country_id = 181;
                    $obj->company_city = $company[4];
                    $obj->company_address = $company[5];
                    $obj->company_postal_code = $company[6];
                    $obj->company_phone = $company[7];
                    $obj->company_fax = $company[8];
                    $obj->company_website = $company[9];
                    $obj->company_email = $company[10];
                    $obj->company_domain = $company[32];
                    $obj->company_2009 = json_encode($c2009);
                    $obj->company_2010 = json_encode($c2010);
                    $obj->company_2011 = json_encode($c2011);
                    $obj->company_2012 = json_encode($c2012);
                    $obj->company_employee = $company[31];
                    $obj->company_date_added = date('Y-m-d H:i:s');
                    $obj->save();
                    unset($obj);
                    unset($company);
                    /*                    $obj = new Company();
                                        $obj->company_name = $company[1];
                                        $obj->company_setup_date = date('Y-m-d H:i:s', strtotime(substr($company[5], -4)));
                                        $obj->company_city = $company[6];
                                        $obj->company_address = $company[7];
                                        $obj->company_phone = $company[10];
                                        $obj->company_website = $company[12];
                                        $obj->company_email = $company[13];
                                        $obj->company_domain = $company[36];
                                        $obj->company_country_id = 181;
                                        $obj->company_capital_invested = $company[91];
                                        $obj->company_employee = $company[101];
                                        $obj->save();
                                        unset($obj);
                                        unset($company); */
                }
                fclose($handle);
            }

            $obj = new File();
            $obj->upload_id = $file->upload_id;
            $obj->upload_status = UPLOAD_STATUS_FINISHED;
            $obj->save();

            file_put_contents(PUBLIC_FOLDER."cron-logs.txt", 'cron-opened at '.date('Y-m-d H:i:s', time())." and was uploaded ".$file->upload_filename."\n", FILE_APPEND);
            exit;
        }
    }

    public function testFile()
    {
        exit;
        $i = 0;
        $inputFileName = dirname(__FILE__) . '/../../waiting/romania_0.csv';
        if (($handle = fopen($inputFileName, "r")) !== FALSE) {
            while (($company = fgetcsv($handle, 5000, ",")) !== FALSE) {
                $c2009 = array();
                $c2009['Imobilizate'] = $company[11];
                $c2009['Circulante'] = $company[12];
                $c2009['Stocuri'] = $company[13];
                $c2009['Creante'] = $company[14];
                $c2009['Casa Conturi'] = $company[15];
                $c2009['Cheltuiele Avans'] = $company[16];
                $c2009['Datorii'] = $company[17];
                $c2009['Venituri Avans'] = $company[18];
                $c2009['Provizioane'] = $company[19];
                $c2009['Capitaluri'] = $company[20];
                $c2009['Capital'] = $company[21];
                $c2009['Patrimoniul Regiei'] = $company[22];
                $c2009['Patrimoniul Public'] = $company[23];
                $c2009['Venituri Totale'] = $company[25];
                $c2009['Cheltuieli Totale'] = $company[26];
                $c2009['Profit Brut'] = $company[27];
                $c2009['Pierdere Brut'] = $company[28];
                $c2009['Profit Net'] = $company[29];;
                $c2009['Pierdere Net'] = $company[30];
                $c2009['Salariati'] = $company[31];
                $c2009['Tipul'] = $company[32];
                $c2010 = array();
                $c2010['Imobilizate'] = $company[33];
                $c2010['Circulante'] = $company[34];
                $c2010['Stocuri'] = $company[35];
                $c2010['Creante'] = $company[36];
                $c2010['Casa Conturi'] = $company[37];
                $c2010['Cheltuiele Avans'] = $company[38];
                $c2010['Datorii'] = $company[39];
                $c2010['Venituri Avans'] = $company[40];
                $c2010['Provizioane'] = $company[41];
                $c2010['Capitaluri'] = $company[42];
                $c2010['Capital'] = $company[43];
                $c2010['Patrimoniul Regiei'] = $company[44];
                $c2010['Patrimoniul Public'] = $company[45];
                $c2010['Venituri Totale'] = $company[47];
                $c2010['Cheltuieli Totale'] = $company[48];
                $c2010['Profit Brut'] = $company[49];
                $c2010['Pierdere Brut'] = $company[50];
                $c2010['Profit Net'] = $company[51];;
                $c2010['Pierdere Net'] = $company[52];
                $c2010['Salariati'] = $company[53];
                $c2010['Tipul'] = $company[54];
                $c2011 = array();
                $c2011['Imobilizate'] = $company[55];
                $c2011['Circulante'] = $company[56];
                $c2011['Stocuri'] = $company[57];
                $c2011['Creante'] = $company[58];
                $c2011['Casa Conturi'] = $company[59];
                $c2011['Cheltuiele Avans'] = $company[60];
                $c2011['Datorii'] = $company[61];
                $c2011['Venituri Avans'] = $company[62];
                $c2011['Provizioane'] = $company[63];
                $c2011['Capitaluri'] = $company[64];
                $c2011['Capital'] = $company[65];
                $c2011['Patrimoniul Regiei'] = $company[66];
                $c2011['Patrimoniul Public'] = $company[67];
                $c2011['Venituri Totale'] = $company[69];
                $c2011['Cheltuieli Totale'] = $company[70];
                $c2011['Profit Brut'] = $company[71];
                $c2011['Pierdere Brut'] = $company[72];
                $c2011['Profit Net'] = $company[73];;
                $c2011['Pierdere Net'] = $company[74];
                $c2011['Salariati'] = $company[75];
                $c2011['Tipul'] = $company[76];
                $c2012 = array();
                $c2012['Imobilizate'] = $company[77];
                $c2012['Circulante'] = $company[78];
                $c2012['Stocuri'] = $company[79];
                $c2012['Creante'] = $company[80];
                $c2012['Casa Conturi'] = $company[81];
                $c2012['Cheltuiele Avans'] = $company[82];
                $c2012['Datorii'] = $company[83];
                $c2012['Venituri Avans'] = $company[84];
                $c2012['Provizioane'] = $company[85];
                $c2012['Capitaluri'] = $company[86];
                $c2012['Capital'] = $company[87];
                $c2012['Patrimoniul Regiei'] = $company[88];
                $c2012['Patrimoniul Public'] = $company[89];
                $c2012['Venituri Totale'] = $company[91];
                $c2012['Cheltuieli Totale'] = $company[92];
                $c2012['Profit Brut'] = $company[93];
                $c2012['Pierdere Brut'] = $company[94];
                $c2012['Profit Net'] = $company[95];;
                $c2012['Pierdere Net'] = $company[96];
                $c2012['Salariati'] = $company[97];
                $c2012['Tipul'] = $company[98];
                $obj = new Company();
                $obj->company_url = $company[99];
                $obj->company_name = $company[0];
                $obj->company_cui = $company[1];
                $obj->company_inchiriere = $company[2];
                $obj->company_setup_date = end(explode('/', $company[3]));
                $obj->company_district = $company[4];
                $obj->company_address = $company[5];
                $obj->company_postal_code = $company[6];
                $obj->company_phone = $company[7];
                $obj->company_fax = $company[8];
                $obj->company_website = $company[9];
                $obj->company_email = $company[10];
                $obj->company_domain = $company[32];
                $obj->company_2009 = json_encode($c2009);
                $obj->company_2010 = json_encode($c2010);
                $obj->company_2011 = json_encode($c2011);
                $obj->company_2012 = json_encode($c2012);
                $obj->company_employee = $company[31];
                $obj->company_date_added = date('Y-m-d H:i:s');
                $obj->save();
                unset($obj);
                unset($company);
            }
            fclose($handle);
        }
    }

    public function domains()
    {
        /*        $arn = array();
                for ($i = 0; $i <= 100000; $i++) {
                    $arn[] = rand(0,12000000);
                }
                $arn = implode(',',$arn);*/

        $finalDomains = array();
        $this->db->query('truncate table `new_domains`');
        $domains = $this->db->query('select `company_domain` from `new_companies` group by `company_domain`')->result();
        foreach($domains as $domain) {
            $domainArray = explode(',', $domain->company_domain);
            if (!empty($domainArray)) {
                foreach($domainArray as $da) {
                    $da = trim($da);
                    if (!empty($da)) {
                        $finalDomains[$da] = $da;
                    }

                }
            }
        }
        foreach($finalDomains as $fa) {
            $obj = new Domain();
            $obj->domain_name = $fa;
            $obj->domain_date_added = date('Y-m-d H:i:s');
            $obj->save();
            unset($obj);
        }
        exit;
    }

    public function defaultInfo($case)
    {
        switch($case) {
            case 'countries':
                $this->db->query('DELETE FROM new_default_info WHERE default_info_name = "countries_with_companies"');
                $obj = new Default_info();
                $obj->default_info_name = 'countries_with_companies';
                $obj->default_info_array = json_encode($this->country->getCountriesWithCompanies());
                $obj->default_info_date_added = date('Y-m-d H:i:s');
                $obj->save();
                break;

            default:
                exit('no case');
                break;
        }
    }
}